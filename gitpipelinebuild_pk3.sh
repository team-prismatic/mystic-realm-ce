#!/bin/zsh

#	  Compile PK3 for Linux/MacOS
#	  By Ashi
#	  make sure you have 7z and zsh installed on your system

#	  Skydusk: This is merely version for gitlab.
#	  Only actual difference is output folder and version being specified by cmd arguments

NAME="SRML_MysticRealmCE"
VERSION="$2"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
VERSION_DATE=$(date '+%Y%m%d')

if [ -n "$2" ]; then
    sed -i 's/autobuild =.*/autobuild = true,/' $SCRIPT_DIR'/src/Lua/Core/globals.lua'
    sed -i 's/versionstring =.*/versionstring = \"'$2'-'$VERSION_DATE'\",/' $SCRIPT_DIR'/src/Lua/Core/globals.lua'

    sed -i 's/CustomVersion =.*/CustomVersion = MRCE '$2'-'$VERSION_DATE'/' $SCRIPT_DIR'/src/SOC/A - Core/MAINCFG'

    printf '=====================================================================\n' >> $SCRIPT_DIR/pk3_output.log
    printf 'PK3 Compile Script for Linux/MacOS\nCreated by Ashi\n\nCurrent Directory: '$SCRIPT_DIR'\n\nCompiling' $NAME'-'$VERSION'.pk3' >> $SCRIPT_DIR/pk3_output.log

    7z a $NAME-$VERSION.pk3 ./src/* -x!.bak -x!.dbs -x!.DS_Store -x!.backup1 -x!.backup2 -x!.backup3 -tzip >> $SCRIPT_DIR/pk3_output.log -o/$1
    printf '=====================================================================\n' >> $SCRIPT_DIR/pk3_output.log;
fi