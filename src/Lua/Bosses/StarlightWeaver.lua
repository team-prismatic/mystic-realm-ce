freeslot(
"MT_STARWEAVER",
"MT_STARWEAVER_CHASER_HORIZONTAL",
"MT_STARWEAVER_CHASER_VERTICAL",
"MT_SW_REPLIKA_GUN",
"MT_SW_REPLIKA_VILE",
"MT_SW_REPLIKA_SHOCK",
"MT_SW_REPLIKA_SHOCK_FRYER",
"MT_SW_REPLIKA_SPAWNER",
"SPR_MRCE_SEWZONES"
)
freeslot("S_WVDMGZONE1", "S_WVDMGZONE2")
-- Freeslots ordered by boss body, boss munitions, spawner, summons

freeslot(
	"S_STARWEAVER_LOOK",
	"S_STARWEAVER_SPOTDELAY",
	"S_STARWEAVER_SPOTREPEAT",
	"S_STARWEAVER_RINGSOUND",
	"S_STARWEAVER_RINGLAUNCH1",
	"S_STARWEAVER_RINGLAUNCHCHOOSE",
	"S_STARWEAVER_RINGH1",
	"S_STARWEAVER_RINGV1",
	"S_STARWEAVER_CHASER_HORIZONTAL_LOOK",
	"S_STARWEAVER_CHASER_VERTICAL_LOOK",
	"S_STARWEAVER_CHASER_HORIZONTAL_ITBEGINS",
	"S_STARWEAVER_CHASER_VERTICAL_ITBEGINS",
	"S_STARWEAVER_INIT1",
	"S_STARWEAVER_INIT2",
	"S_STARWEAVER_INIT3",
	"S_STARWEAVER_RINGCHASE1",
	"S_STARWEAVER_RINGCHASE2",
	"S_STARWEAVER_DASHONE1",
	"S_STARWEAVER_DASHONE2",
	"S_STARWEAVER_DASHONE3",
	"S_STARWEAVER_DASHONE4",
	"S_STARWEAVER_DASHONE5",
	"S_STARWEAVER_DASHONE6",
	"S_STARWEAVER_DASHONE7",
	"S_STARWEAVER_DASHONE8",
	"S_STARWEAVER_REPLONE1",
	"S_STARWEAVER_REPLONE2",
	"S_STARWEAVER_REPLONE3",
	"S_STARWEAVER_REPLONE4",
	"S_STARWEAVER_DASHTWO1",
	"S_STARWEAVER_DASHTWO2",
	"S_STARWEAVER_DASHTWO3",
	"S_STARWEAVER_DASHTWO4",
	"S_STARWEAVER_DASHTWO5",
	"S_STARWEAVER_DASHTWO6",
	"S_STARWEAVER_DASHTWO7",
	"S_STARWEAVER_DASHTWO8",
	"S_STARWEAVER_REPLTWO1",
	"S_STARWEAVER_REPLTWO2",
	"S_STARWEAVER_REPLTWO3",
	"S_STARWEAVER_REPLTWO4",
	"S_STARWEAVER_DASHTHREE1",
	"S_STARWEAVER_DASHTHREE2",
	"S_STARWEAVER_DASHTHREE3",
	"S_STARWEAVER_DASHTHREE4",
	"S_STARWEAVER_DASHTHREE5",
	"S_STARWEAVER_DASHTHREE6",
	"S_STARWEAVER_DASHTHREE7",
	"S_STARWEAVER_DASHTHREE8",
	"S_STARWEAVER_REPLTHREE1",
	"S_STARWEAVER_REPLTHREE2",
	"S_STARWEAVER_REPLTHREE3",
	"S_STARWEAVER_REPLTHREE4",
	"S_STARWEAVER_DASHFOUR1",
	"S_STARWEAVER_DASHFOUR2",
	"S_STARWEAVER_DASHFOUR3",
	"S_STARWEAVER_DASHFOUR4",
	"S_STARWEAVER_DASHFOUR5",
	"S_STARWEAVER_DASHFOUR6",
	"S_STARWEAVER_DASHFOUR7",
	"S_STARWEAVER_DASHFOUR8",
	"S_STARWEAVER_REPLFOUR1",
	"S_STARWEAVER_REPLFOUR2",
	"S_STARWEAVER_REPLFOUR3",
	"S_STARWEAVER_REPLFOUR4",
	"S_STARWEAVER_MGCHOOSE",
	"S_STARWEAVER_JETCHOOSE",
	"S_STARWEAVER_MG1",
	"S_STARWEAVER_MG2",
	"S_STARWEAVER_MG3",
	"S_STARWEAVER_MG4",
	"S_STARWEAVER_MG5",
	"S_STARWEAVER_MG6",
	"S_STARWEAVER_MG7",
	"S_STARWEAVER_MG8",
	"S_STARWEAVER_MG9",
	"S_STARWEAVER_MG10",
	"S_STARWEAVER_MG11",
	"S_STARWEAVER_JET1",
	"S_STARWEAVER_JET2",
	"S_STARWEAVER_JET3",
	"S_STARWEAVER_JET4",
	"S_STARWEAVER_JET5",
	"S_STARWEAVER_JET6",
	"S_STARWEAVER_JET7",
	"S_STARWEAVER_JET8",
	"S_STARWEAVER_PAIN1",
	"S_STARWEAVER_PAIN2",
	"S_STARWEAVER_PAIN3",
	"S_STARWEAVER_PAIN4",
	"S_STARWEAVER_PAIN5",
	"S_STARWEAVER_PAIN6",
	"S_STARWEAVER_PAIN7",
	"S_STARWEAVER_PAIN8",
	"S_STARWEAVER_PAIN9",
	"S_STARWEAVER_PAIN10",
	"S_STARWEAVER_PAIN11",
	"S_STARWEAVER_PAIN12",
	"S_STARWEAVER_PAIN13",
	"S_STARWEAVER_PAIN14",
	"S_SW_REPLIKA_SPAWNER_INIT",
	"S_SW_REPLIKA_SPAWNER_CHOOSE",
	"S_SW_REPLIKA_SPAWNER_GUN",
	"S_SW_REPLIKA_SPAWNER_SHOCK",
	"S_SW_REPLIKA_SPAWNER_VILE",
	"S_SW_REPLIKA_SPAWNER_CLEAR",
	"S_SW_REPLIKA_GUN_LOOK",
	"S_SW_REPLIKA_GUN_CHOOSE",
	"S_SW_REPLIKA_GUN_WAIT1",
	"S_SW_REPLIKA_GUN_WAIT2",
	"S_SW_REPLIKA_GUN_FIRE1",
	"S_SW_REPLIKA_GUN_FIRE2",
	"S_SW_REPLIKA_GUN_FIRE3",
	"S_SW_REPLIKA_GUN_FIRE4",
	"S_SW_REPLIKA_GUN_FIRE5",
	"S_SW_REPLIKA_SHOCK_LOOK",
	"S_SW_REPLIKA_SHOCK_CHOOSE",
	"S_SW_REPLIKA_SHOCK_WAIT1",
	"S_SW_REPLIKA_SHOCK_WAIT2",
	"S_SW_REPLIKA_SHOCK_FIRE1",
	"S_SW_REPLIKA_SHOCK_FRYER_INIT",
	"S_SW_REPLIKA_SHOCK_FRYER_FRYAIR",
	"S_SW_REPLIKA_VILE_LOOK",
	"S_SW_REPLIKA_VILE_CHOOSE",
	"S_SW_REPLIKA_VILE_WAIT1",
	"S_SW_REPLIKA_VILE_WAIT2",
	"S_SW_REPLIKA_VILE_FIRE1",
	"S_SW_REPLIKA_VILE_FIRE2",
	"S_SW_REPLIKA_VILE_FIRE3",
	"S_SW_REPLIKA_VILE_FIRE4",
	"S_SW_REPLIKA_ONDEATH",
	"sfx_slwvds",
	"sfx_slwvmg",
	"sfx_rplcte",
	"sfx_rpldsp"
)



--Sin Sew Damage Zones
freeslot("MT_SINSEWDMGZONE")
mobjinfo[MT_SINSEWDMGZONE] = {
	--$Name Weaver Damage Zone
	--$Category Utility
	doomednum = 722,
	spawnstate = S_WVDMGZONE1,
	spawnhealth = 1000,
	speed = 10,
	radius = 300*FRACUNIT,
	height = 2*FRACUNIT,
	attacksound = sfx_none,
	activesound = sfx_None,
	painstate = S_NULL,
	painchance = -1,
	painsound = sfx_None,
	meleestate = S_NULL,
	missilestate = S_NULL,
	deathstate = S_XPLD1,
	xdeathstate = S_NULL,
	deathsound = sfx_None,
	raisestate = S_NULL,
	reactiontime = 1,
	seestate = S_NULL,
	seesound = sfx_None,
	mass = 8*FRACUNIT,
	dispoffset = 0,
	flags = MF_SPECIAL
}




states[S_WVDMGZONE1] = {
	sprite = SPR_MRCE_SEWZONES,
	frame = A,
	tics = 1,
	nextstate = S_WVDMGZONE2
}

states[S_WVDMGZONE2] = {
	sprite = SPR_MRCE_SEWZONES,
	frame = A|FF_FLOORSPRITE|FF_ADD,
	tics = 1000,
	nextstate = S_WVDMGZONE2
}

local function sinSewDamageZoneHandler(dmgZone)
	if not dmgZone or not dmgZone.valid then return end

	if not dmgZone.timer then dmgZone.timer = 5*TICRATE end

	if dmgZone.timer > 0 then
		dmgZone.timer = $ - TICRATE
		dmgZone.scale = $ - FRACUNIT/120
	end

	if dmgZone.timer <= 0 then
		dmgZone.fuse = TICRATE/2
	end

end
addHook("MobjThinker", sinSewDamageZoneHandler, MT_SINSEWDMGZONE)


local function sinSewDamageZoneDMG(dmgZone, toucher)
	if not dmgZone or not toucher then return end

	if IsPlayerAroundBool(dmgZone, 256*dmgZone.scale ,TICRATE/12) then
		P_DamageMobj(toucher) --ADD INFLICTER
	end


	return true
end
addHook("TouchSpecial", sinSewDamageZoneDMG, MT_SINSEWDMGZONE)



--[[
-- Define the Starlight Weaver boss
mobjinfo[MT_STARWEAVER] = {
--$Name Starlight Weaver
--$Sprite EGGMA1
--$Category Mystic Realm Objects
    doomednum = 8000,
	spawnstate = S_STARWEAVER_LOOK,
	spawnhealth = 8,
	SeeState = S_STARWEAVER_RINGSOUND,
	reactiontime = TICRATE/2,
	attacksound = sfx_slwvmg,
	painstate = S_STARWEAVER_PAIN1,
	deathstate = S_EGGMOBILE_DIE1,
	xdeathstate = S_EGGMOBILE_FLEE1,
    radius = 24*FRACUNIT,
    height = 76*FRACUNIT,
    painchance = 128,
    painsound = sfx_dmpain,
    deathsound = sfx_cybdth,
	activesound = sfx_zoom,
	speed = 8*FRACUNIT,
	damage = 3,
	flags = MF_SPECIAL|MF_SHOOTABLE|MF_NOGRAVITY|MF_BOSS
}

]]




			--======================THE BRAIN==========================--

local BALLL1, BALLL2 = MT_STARWEAVER_CHASER_HORIZONTAL, MT_STARWEAVER_CHASER_VERTICAL

local function fireball(mo) --continuous thinker
    if (mo.z - mo.floorz) < 24*mo.scale then
        mo.z = mo.floorz + (24*mo.scale)
    end
    if mo.theball then
        mo.theball = $ + 1 --detect the oldest existing fireball and delete new ones if old one still exists
    end
end

--spawn thinker
local function ballsonfire(mo)
    mo.theball = 1
    for ball in mobjs.iterate() do
        if ball.type ~= BALLL1 and ball.type ~= BALLL2 then
            continue
        end
        if ball.theball and ball.theball > mo.theball then
            mo.fuse = 1
            mo.state = S_INVISIBLE
            return
        end
    end
end

addHook("MobjSpawn", ballsonfire, BALLL1)
addHook("MobjSpawn", ballsonfire, BALLL2)
addHook("MobjThinker", fireball, BALLL1)
addHook("MobjThinker", fireball, BALLL2)

local function livetogetherdietogether()
    for summons in mobjs.iterate() do
        if summons.type ~= BALLL1 and summons.type ~= BALLL2 and summons.type ~= MT_SW_REPLIKA_GUN and summons.type ~= MT_BUMBLEBORE and summons.type ~= MT_SW_REPLIKA_VILE and summons.type ~= MT_JETTBOMBER and summons.type ~= MT_JETTGUNNER and summons.type ~= MT_SW_REPLIKA_SHOCK then
            continue
        end
        --print("die")
        P_KillMobj(summons)
    end
    if modeattacking then
		for p in players.iterate do
			P_DoPlayerExit(p)
		end
    end
end

addHook("MobjDeath", livetogetherdietogether, MT_STARWEAVER)

addHook("MobjSpawn", function(mo)
    mo.colorized = true
    mo.color = SKINCOLOR_SANDY
end, MT_SW_REPLIKA_GUN)

addHook("MobjSpawn", function(mo)
    mo.colorized = true
    mo.color = SKINCOLOR_OCEAN
end, MT_SW_REPLIKA_SHOCK)

addHook("MobjSpawn", function(mo)
    mo.colorized = true
    mo.color = SKINCOLOR_KETCHUP
end, MT_SW_REPLIKA_VILE)