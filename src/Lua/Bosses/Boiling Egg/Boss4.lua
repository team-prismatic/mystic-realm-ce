--Boss
freeslot(
	"MT_BOILINGEGG",
	"S_BOILINGEGG_SPAWN",
	"S_BOILINGEGG_MAIN",
	"S_BOILINGEGG_PAIN",
	"MT_BE_QUAKESPAWNER",
	"S_QUAKESPAWNER_THINK",
	"S_QUAKESPAWNER_THINK2",
	"S_QUAKESPAWNER_TARGET",
	"S_QUAKESPAWNER_SOUND",
	"S_QUAKESPAWNER_SPAWN",
	"S_QUAKESPAWNER_RAM",
	"S_QUAKESPAWNER_CEASE",
	"MT_BE_QUAKEMISSILE",
	"S_QUAKEMISSILE_THINK",
	"MT_BE_NAPALM",
	"S_BE_NAPALM_THRUST",
	"S_BE_NAPALM_EXPAND",
	"S_BE_NAPALM_LOOP",
	"S_BE_NAPALM_LINGER",
	"SPR_EGNB",
	"sfx_subept",
	"sfx_submrg",
	"sfx_b4flmr",
	"sfx_rage"
)

--temporary
freeslot(
	"MT_EGGBALLER_FIRE",
	"S_EGGBALLER_FIRE"
)

sfxinfo[sfx_submrg].caption = "Submerging"
sfxinfo[sfx_subept].caption = "Emerging"
sfxinfo[sfx_b4flmr].caption = "Flamethrower"
sfxinfo[sfx_rage].caption = "Boss enraged!"

-- Initiate the waypoints function (by Skydusk)
local waypoints = {}
local quake_waypoints = {}


---------------------------------
-- CUSTOM SOC ACTIONS
----------------------------------
-- Initiate the function to make the napalm grow in size
function A_NapalmGrow(actor, var1)
	P_SetScale(actor, actor.scale*var1)
end

--has to be placed here so that the game can see "quake_waypoints"
function A_FindFangWaypoint(actor, var1, var2)
	if not (actor.target and actor.target.valid) then
		actor.state = S_XPLD1
		return
	end
	--find fang waypoint closest to the current target player
	local nearest = nil
	local maxdist = 9999*FRACUNIT
	for k,v in ipairs(quake_waypoints) do
		local dist = R_PointToDist2(actor.target.x, actor.target.y, v.x, v.y)
		if (dist < maxdist) then
			maxdist = dist
			nearest = v
		end
	end
	--aim for it
	if (nearest ~= nil) then
		actor.target = nearest
		actor.armed = true
		actor.fuse = 3*TICRATE --failsafe
	else
		print("YOU SKIPPED THE TABLE?")
	end
end

-- Statblock for the Boiling Egg boss
mobjinfo[MT_BOILINGEGG] = {
	--$Name Boiling Egg
	--$Sprite EGGOA1
	--$Category Bosses/Mystic Realm
	doomednum = 4200,
	spawnstate = S_BOILINGEGG_SPAWN,
    seestate = S_BOILINGEGG_MAIN,
	painstate = S_BOILINGEGG_PAIN,
	deathstate =  S_EGGMOBILE3_DIE1,
	xdeathstate = S_EGGMOBILE3_FLEE1,
	meleestate = S_NULL,
	missilestate = S_NULL,
	raisestate = S_NULL,
	painchance = 100,
	spawnhealth = 8,
	reactiontime = 16,
	mass = 8,
	damage = 1,
	painsound = sfx_dmpain,
	activesound = sfx_telept,
	deathsound = sfx_cybdth,
	speed = 8*FRACUNIT,
	radius = 32*FRACUNIT,
	height = 124*FRACUNIT,
	flags = MF_SPECIAL|MF_SHOOTABLE|MF_BOSS|MF_NOGRAVITY
}

-- Statblock for the Quake Missile's launching phase
mobjinfo[MT_BE_QUAKESPAWNER] = {
	doomednum = -1,
	spawnstate = S_QUAKESPAWNER_TARGET,
    seestate = S_QUAKESPAWNER_THINK,
	painstate = S_NULL,
	deathstate =  S_CYBRAKDEMONNAPALMBOMBLARGE_DIE1,
	xdeathstate = S_NULL,
	meleestate = S_NULL,
	missilestate = S_NULL,
	raisestate = S_NULL,
	painchance = 100,
	spawnhealth = 1,
	reactiontime = 16,
	mass = 1,
	damage = 1,
	painsound = sfx_bkpoof,
	activesound = sfx_s3ka0,
	deathsound = sfx_cybdth,
	speed = 40*FRACUNIT,
	radius = 11*FRACUNIT,
	height = 8*FRACUNIT,
	flags = MF_NOBLOCKMAP|MF_MISSILE|MF_NOGRAVITY|MF_RUNSPAWNFUNC
}

-- Statblock for the Quake Missile's diving phase
mobjinfo[MT_BE_QUAKEMISSILE] = {
	doomednum = -1,
	spawnstate = S_QUAKEMISSILE_THINK,
    seestate = S_NULL,
	painstate = S_NULL,
	deathstate =  S_CYBRAKDEMONNAPALMBOMBLARGE_DIE1,
	xdeathstate = S_NULL,
	meleestate = S_NULL,
	missilestate = S_NULL,
	raisestate = S_NULL,
	painchance = 100,
	spawnhealth = 1,
	reactiontime = 16,
	mass = 1,
	damage = 1,
	painsound = sfx_bkpoof,
	activesound = sfx_s3ka0,
	deathsound = sfx_bkpoof,
	speed = 40*FRACUNIT,
	radius = 11*FRACUNIT,
	height = 8*FRACUNIT,
	flags = MF_NOBLOCKMAP|MF_MISSILE|MF_NOGRAVITY
}

-- Statblock for the Flamethrower's napalm
mobjinfo[MT_BE_NAPALM] = {
	doomednum = -1,
	spawnstate = S_BE_NAPALM_THRUST,
    seestate = S_NULL,
	painstate = S_NULL,
	deathstate =  S_NULL,
	xdeathstate = S_NULL,
	meleestate = S_NULL,
	missilestate = S_NULL,
	raisestate = S_NULL,
	painchance = 0,
	spawnhealth = 1000,
	reactiontime = 8,
	mass = 100,
	damage = 1,
	painsound = sfx_None,
	activesound = sfx_None,
	deathsound = sfx_None,
	speed = 0*FRACUNIT,
	radius = 16*FRACUNIT,
	height = 24*FRACUNIT,
	flags = MF_NOGRAVITY|MF_PAIN|MF_FIRE|MF_NOBLOCKMAP|MF_RUNSPAWNFUNC
}

mobjinfo[MT_EGGBALLER_FIRE] = {
	doomednum = -1,
	spawnstate = S_EGGBALLER_FIRE,
	painstate = S_EGGBALLER_FIRE,
	painchance = MT_FLAMEPARTICLE,
	seesound = sfx_s3kc2s,
	reactiontime = 2*TICRATE,
	deathstate = S_XPLD1,
	spawnhealth = 1000,
	speed = 15*FRACUNIT,
	radius = 28*FRACUNIT,
	height = 68*FRACUNIT,
	flags = MF_PAIN|MF_BOUNCE|MF_FIRE
}

-- Dummy Boiling Egg states just in case
states[S_BOILINGEGG_SPAWN] = {SPR_EGGO, A, 1, nil, 0, 0, S_BOILINGEGG_MAIN}
states[S_BOILINGEGG_MAIN] = {SPR_EGGO, A, 1, nil, 0, 0, S_BOILINGEGG_MAIN}
states[S_BOILINGEGG_PAIN] = {SPR_EGGO, A, 35, A_Pain, 0, 0, S_BOILINGEGG_MAIN}

-- State transitions for the boss' projectiles
-- Quake Missile, launching phase: looks for target, rises for a bit, spawns the diver with a sound, then removes itself from play
states[S_QUAKESPAWNER_TARGET] = {SPR_EGNB, A, 1, A_Look, 1, 1, S_QUAKESPAWNER_TARGET}
states[S_QUAKESPAWNER_THINK] = {SPR_EGNB, A, 1, A_ZThrust, 15, 1+1*FRACUNIT, S_QUAKESPAWNER_THINK2}
states[S_QUAKESPAWNER_THINK2] = {SPR_EGNB, A, 1, function(actor,var1,var2)
	actor.momz = FixedMul($, 95*FRACUNIT/100)
	--as soon as it stops moving, be ANGRY
	if (abs(actor.momz) < 2*FRACUNIT) then
		actor.state = var1
		actor.momz = 0
	end
end, S_QUAKESPAWNER_SOUND, nil, S_QUAKESPAWNER_THINK2}
states[S_QUAKESPAWNER_SOUND] = {SPR_EGNB, B, TICRATE/4, A_PlaySound, sfx_beshot, 0, S_QUAKESPAWNER_SPAWN}
states[S_QUAKESPAWNER_SPAWN] = {SPR_EGNB, B, 1, A_FindFangWaypoint, 0, 0, S_QUAKESPAWNER_RAM}
states[S_QUAKESPAWNER_RAM] = {SPR_EGNB, B, 1, A_HomingChase, 30*FRACUNIT, 0, S_QUAKESPAWNER_RAM}
states[S_QUAKESPAWNER_CEASE] = {SPR_EGNB, B, 1, nil, 0, 2, S_QUAKESPAWNER_CEASE}


-- Quake Missile, diving phase: does nothing, this is a missile through and through
states[S_QUAKEMISSILE_THINK] = {SPR_RCKT, B, 1, nil, 0, 0, S_QUAKEMISSILE_THINK}
-- [TODO: the napalm is bugged, it's way too fast, doesn't lose speed with time, and doesn't grow] Napalm: initiates its velocity, grows in size for a bit, then keeps itself in play permanently
states[S_BE_NAPALM_THRUST] = {SPR_DFLM, FF_FULLBRIGHT|B, 1, A_Thrust, 5, 1, S_BE_NAPALM_EXPAND}
states[S_BE_NAPALM_EXPAND] = {SPR_DFLM, FF_FULLBRIGHT|TR_TRANS20|C, 1, A_NapalmGrow, 2, 0, S_BE_NAPALM_LINGER}
states[S_BE_NAPALM_LOOP] = {SPR_DFLM, FF_FULLBRIGHT|TR_TRANS20|C, 0, A_Repeat, 175, S_BE_NAPALM_EXPAND, S_BE_NAPALM_LINGER}
states[S_BE_NAPALM_LINGER] = {SPR_DFLM, FF_FULLBRIGHT|TR_TRANS50|D, 3*TICRATE, nil, 0, 0, S_NULL}



states[S_EGGBALLER_FIRE] = {SPR_BFBR, A|FF_FULLBRIGHT|FF_ANIMATE, 13*TICRATE/4, nil, 15, 1, S_DEATHSTATE}


--override the original temporarily
states[S_EGGBALLER_FIRE] = {
	sprite = SPR_BFBR,
	frame = A|FF_FULLBRIGHT|FF_ANIMATE,
	tics = 7*TICRATE,
	nextstate = S_DEATHSTATE,
	var1 = 15,
	var2 = 1
}

-- Initialize the boss' stats
addHook("MobjSpawn", function(boss)
	boss.timer = 10*TICRATE -- Timer: decreases by 1 every tic while the boss is alive, used to control the timing of everything it does
	boss.phase = 1 -- Phase: dictates which behaviour the boss should be doing. 1: wait in place and lob a Fireball. 2: chase the player. 3: pain state, flee from the player (+ pinch: fire Flamethrower). 4: pain state, submerge and reappear elsewhere (+ pinch: fire Quake Missile).
	boss.angle = 0 -- Tracks the boss' angle
	boss.stasis = 0 -- Stasis is set to 1 when the boss ricochets while in phase 2 to prevent the directional change from instantly being overridden
	boss.health = 8 -- We will have to set the boss' health through Lua
	boss.dead = 0 -- Tracks how alive the boss is
	boss.pinch = 0 -- Tracks whether or not the boss is in pinch mode yet

	boss.roampoint = 0 --waypoint number
	boss.roamtarget = nil --waypoint mobj
	boss.roammaxtimer = 7*TICRATE/2
	boss.roamtimer = boss.roammaxtimer

	boss.storedzpos = boss.z
end, MT_BOILINGEGG)

-- Establish the waypoint array
addHook("MapLoad", function()
	waypoints = {}
	quake_waypoints = {}
end)

-- Get the location of every Boss Waypoint at spawn
addHook("MobjThinker", function(mo)
  if leveltime == 1 then
    table.insert(waypoints, mo)
  end
end, MT_BOSS3WAYPOINT)

-- Get the location of every Fang Waypoint at spawn
addHook("MobjThinker", function(mo)
  if leveltime == 1 then
    table.insert(quake_waypoints, mo)
  end
end, MT_FANGWAYPOINT)

-- If the boss runs into a wall, simulate a really bad ricochet
addHook("MobjMoveBlocked", function(boss, thing, line)
  if line then -- We check if a linedef is the cause of the movement blocking
	if boss.stasis == 0 then
		boss.stasis = 1 -- Enter stasis to prevent phase 2's constant InstaThrust from overriding this script
		S_StartSound(nil, sfx_s259) -- Sound is tied to stasis so it only plays once per sequence of ricochets, so as to avoid ear bleeding
	end
	if P_RandomRange(1, 2) == 1 then -- 50% chance to invert either x or y momentum - causes boss to have unpredictable scattering off walls
		boss.momx = $ * -1
	else
		boss.momy = $ * -1
	end
  end
  return true
end, MT_BOILINGEGG)

local function FindNearestWaypoint(boss)
	local maxdist = 9999*FRACUNIT
	local nearest = nil
	local waypointnumber = -1

	for k,v in ipairs(waypoints) do
		if (boss.roampoint == v.spawnpoint.args[0]) then continue end

		local waypoint_dist = R_PointToDist2(boss.x, boss.y, v.x, v.y)
		if (waypoint_dist < maxdist) then
			maxdist = waypoint_dist
			nearest = v
			waypointnumber = v.spawnpoint.args[0]
		end
	end

	boss.roampoint = waypointnumber
	boss.roamtarget = nearest
	boss.roamtimer = boss.roammaxtimer
end

local function FindNextWaypoint(boss)
	local node = boss.roamtarget.spawnpoint
	local num1 = node.args[1]
	local num2 = node.args[2]

	if (num1 > 0 and num2 > 0) then
		--prioritise going to the next waypoint instead of picking a path
		local random = P_RandomRange(1,4)
		if (random == 1) then
			boss.roampoint = num1
		elseif (random == 2) then
			boss.roampoint = num2
		else
			boss.roampoint = (boss.roampoint % #waypoints) + 1
		end
	elseif (num1 > 0) then
		boss.roampoint = num1
	else
		boss.roampoint = (boss.roampoint % #waypoints) + 1
	end

	for k,v in ipairs(waypoints) do
		if v.spawnpoint.args[0] == boss.roampoint then
			boss.roamtarget = v
			boss.roamtimer = boss.roammaxtimer
			return
		end
	end

	boss.roamtimer = boss.roammaxtimer --just in case
end


local function FRZBossRoam(boss)
	if (boss.roamtarget == nil) then return end

	local distance = R_PointToDist2(boss.x, boss.y, boss.roamtarget.x, boss.roamtarget.y)

	if boss.roamtimer > 0 then
		local current_angle = R_PointToAngle2(0,0,boss.momx, boss.momy)
		local target_angle = R_PointToAngle2(boss.x, boss.y, boss.roamtarget.x, boss.roamtarget.y)

		local angle_diff = (target_angle - current_angle) / 5
		local destangle = current_angle + angle_diff

		local current_magnitude = R_PointToDist2(0,0,boss.momx, boss.momy)

		if (distance < 125*FRACUNIT) then
			current_magnitude = FixedMul($, 90*FRACUNIT/100)
		else
			current_magnitude = max($, 3*FRACUNIT)
			if (current_magnitude < 12*FRACUNIT) then
				current_magnitude = FixedMul($, 110*FRACUNIT/100)
			end
		end

		boss.momx = P_ReturnThrustX(boss, destangle, current_magnitude)
		boss.momy = P_ReturnThrustY(boss, destangle, current_magnitude)

		boss.roamtimer = $ - 1

		if boss.roamtimer == 3*TICRATE/4 or boss.roamtimer == TICRATE/4 then
			-- if only there was a function for spawning things at a set speed within a radius
			for i = 0, 7 do
				local set_angle = ANGLE_45 * i
				local speed = 20*FRACUNIT
				local xspeed = P_ReturnThrustX(boss, set_angle, speed)
				local yspeed = P_ReturnThrustY(boss, set_angle, speed)

				local dust = P_SpawnMobj(boss.x, boss.y, boss.z + boss.height/2, MT_SPINDUST)
				dust.momx = xspeed
				dust.momy = yspeed
				dust.scale = 2*FRACUNIT
				dust.fuse = FRACUNIT/2
			end
		end

	else
		distance = R_PointToDist2(boss.x, boss.y, boss.roamtarget.x, boss.roamtarget.y)
		if (distance > 250*FRACUNIT) then --eggman is a complete jobber and could not reach the waypoint
			FindNearestWaypoint(boss)
		else
			FindNextWaypoint(boss)
		end
	end
end

-- Phase 1: face the target menacingly and launch a Fireball on entry
local function FRZBossPhase1(boss)
	FRZBossRoam(boss)
	
	if (boss.target and boss.target.valid) then
		local target_angle = R_PointToAngle2(boss.x, boss.y, boss.target.x, boss.target.y)
		boss.angle = target_angle
		for i = 0,2 do
			local firetime = (1+i*3)*TICRATE
			if (boss.timer == firetime)then
				S_StartSound(nil, sfx_s22e)
				A_HoodFire(boss, MT_EGGBALLER_FIRE, 0)
			end
		end
	end
end

-- Phase 2: rotating flamethrowers
local function FRZBossPhase2(boss)
	local timer = boss.timer
	if (timer == 8*TICRATE or timer == 7*TICRATE) then
		S_StartSound(nil, sfx_cdfm33)
		if (boss.pinch == 0) then --don't stop in pinch
			P_InstaThrust(boss, boss.angle, 0)
		end
	end
	if (timer <= 8*TICRATE and timer >= 6*TICRATE) then
		boss.angle = $ + ANG10

	elseif (timer <= 6*TICRATE and timer > 2*TICRATE) then
		boss.angle = $ + ANG1 * 4

		if (timer % TICRATE == 0) then
			S_StartSound(nil, sfx_b4flmr)
		end

		local offsetx = P_ReturnThrustX(boss, boss.angle, 30*FRACUNIT)
		local offsety = P_ReturnThrustY(boss, boss.angle, 30*FRACUNIT)
		local napalm = P_SpawnMobj(boss.x + offsetx, boss.y + offsety, boss.z + boss.height/2 + 10*FRACUNIT, MT_BE_NAPALM)
		napalm.angle = boss.angle
		napalm.fuse = 3*TICRATE
		napalm.momx = offsetx
		napalm.momy = offsety

		local napalmr = P_SpawnMobj(boss.x - offsetx, boss.y - offsety, boss.z + boss.height/2 + 10*FRACUNIT, MT_BE_NAPALM)
		napalmr.angle = -boss.angle
		napalmr.fuse = 3*TICRATE
		napalmr.momx = -offsetx
		napalmr.momy = -offsety


		if boss.pinch == 1 and boss.stasis == 0 then -- Checks that we didn't ricochet before applying any InstaThrust
			P_InstaThrust(boss, boss.angle, 15*FRACUNIT)
		end
	elseif (timer == 2*TICRATE) then
		A_TurretStop(boss, 1, 0)
		P_InstaThrust(boss, boss.angle, 15*FRACUNIT)
	elseif (timer > 0 and timer < 2*TICRATE) then
		boss.angle = $ + ANG10
	end

	if boss.momx == 0 and boss.momy == 0 then -- If we've somehow lost all our speed remove the stasis condition so that the InstaThrusts can begin anew
		boss.stasis = 0
	end
end

-- Phase 3: retreat at high speeds.
-- If in pinch mode, this phase lasts slightly longer and has a Flamethrower attack tied to it
local function FRZBossPhase3(boss)
	if (boss.target and boss.target.valid) then
		local target_angle = R_PointToAngle2(boss.x, boss.y, boss.target.x, boss.target.y)
		boss.angle = target_angle
	end
	if boss.pinch == 1 then -- Pinch version: lasts 3 seconds, dash at 2, Flamethrower at 1.5
		if boss.timer == 2*TICRATE then -- Retreat
			S_StartSound(nil, sfx_zoom)
			S_StartSound(nil, sfx_telept)
			P_SpawnGhostMobj(boss)
			P_InstaThrust(boss, boss.angle, -30*FRACUNIT)
		end
		if boss.timer == 3*TICRATE/2 then -- Begin the Flamethrower
			S_StartSound(nil, sfx_b4flmr)
			A_TurretFire(boss, MT_BE_NAPALM, 400000)
		end
	else -- Regular version: lasts 2 seconds, dash at 1
		if boss.timer == 1*TICRATE then -- Retreat
			S_StartSound(nil, sfx_zoom)
			S_StartSound(nil, sfx_telept)
			P_SpawnGhostMobj(boss)
			P_InstaThrust(boss, boss.angle, -40*FRACUNIT)
		end
	end
end

-- Phase 4: Submerge. Dives, hopefully into a liquid body, then teleports to a random Boss Waypoint and re-emerges.
-- If in pinch mode, spawns a Quake Missile a set distance above itself before reemerging - hopefully above the liquid body
local function FRZBossPhase4(boss)
	if (boss.target and boss.target.valid) then
		boss.angle = R_PointToAngle2(boss.x, boss.y, boss.target.x, boss.target.y)
	end
	if boss.pinch == 1 then -- Pinch version: lasts 4 seconds, submerge at 3, fire Quake Missile at 2, reemerge at 1
		if boss.timer == 3*TICRATE then
			S_StartSound(nil, sfx_submrg)
		end
		if (boss.timer <= 3*TICRATE) and (boss.timer > 2*TICRATE) then -- Submerge
			boss.momz = -10*FRACUNIT
		end
		if boss.timer == 2*TICRATE then -- Fire the missile!
			boss.momz = 0*FRACUNIT
			S_StartSound(nil, sfx_brakrl)
			P_SpawnMobjFromMobj(boss, 0, 0, 420*FRACUNIT, MT_BE_QUAKESPAWNER)
		end
	else -- Regular version: lasts 3 seconds, submerge at 2, reemerge at 1
		if boss.timer == 2*TICRATE then -- Submerge
			S_StartSound(nil, sfx_submrg)
		end
		if (boss.timer <= 2*TICRATE and boss.timer > 1*TICRATE) then
			boss.momz = -10*FRACUNIT
		end
	end

	--shared between both
	if boss.timer == 1*TICRATE then
		boss.tracer = waypoints[P_RandomKey(#waypoints)] -- Skydusk's implementation of finding the Waypoint to go to
		if boss.tracer then -- We first check that the Waypoint is valid before teleporting
			P_SetOrigin(boss, boss.tracer.x, boss.tracer.y, boss.z) -- Teleport to waypoint but keep original z
			FindNearestWaypoint(boss) --set current waypoint as current target so it can pick a new one
		end
		boss.momz = 0
	end
	
	if (boss.timer <= 1*TICRATE and boss.timer > 0) then
		boss.momz = 10*FRACUNIT -- Reemerge
	end	
	
	if boss.timer == 1 then -- Right before going back to regular behaviours, announce presence
		S_StopSound(boss)
		S_StartSound(nil, sfx_subept)
	end
end

-- Main boss thinking body
addHook("BossThinker", function(boss)
	if boss.dead > 0 then return false end -- We only run the boss thinker while it's alive

	if (leveltime == 5) then --set up waypoints
		--sea egg waypoint value in UZB custom args
		table.sort(waypoints, function (k1, k2) return k1.spawnpoint.args[0] < k2.spawnpoint.args[0] end)
		FindNearestWaypoint(boss)
		FindNextWaypoint(boss) --so that he doesn't stand still for 3 seconds on map load
	end

	if not boss.target then -- Make sure it has a target before doing anything
		P_LookForPlayers(boss, 0, true, false)  -- Find one
	end

	-- ## Timer logic ##
	boss.timer = $ - 1 -- THIS IS IMPORTANT. This 1 timer loss per tic controls everything.
	if boss.timer < 1 then -- When timer reaches 0, stop everything: stop the Flamethrower if it's active, stop the boss' vertical momentum from Submerge if it has any, reset timer to 1 second
		A_TurretStop(boss, 1, 0)
		boss.momz = 0
		boss.timer = 1*TICRATE
		P_LookForPlayers(boss, 0, true, false) -- Find a new target, just to keep MP players on their toes
		if boss.phase == 1 then -- If the boss is in phase 1 when the timer hits 0, go to phase 2
			boss.phase = 2
			boss.timer = 8*TICRATE
		elseif boss.phase == 2 then -- If the boss is in phase 2 when the timer hits 0, go to phase 1
			P_InstaThrust(boss, boss.angle, 0) -- Resets the boss' momentum that it has from phase 2 movement

			boss.phase = 1
			FindNearestWaypoint(boss) -- find nearest waypoint after spin attack
			boss.timer = 8*TICRATE
		elseif boss.phase == 3 or boss.phase == 4 then -- Specialised check to revert from pain phases to phase 1. This enables the boss to revert to its regular behaviours after its pain state is over
			P_InstaThrust(boss, boss.angle, 0) -- Another momentum reset

			S_StartSound(nil, sfx_s22e)
			A_HoodFire(boss, MT_EGGBALLER_FIRE, 0)

			boss.flags2 = $&~MF2_FRET -- Remove its invincibility!

			boss.phase = 1
			boss.timer = 10*TICRATE

			FindNearestWaypoint(boss)
			boss.roamtimer = boss.roammaxtimer / 3 --start roaming faster
		end
	end
	
		-- ## Boss regular phases begin here ##
	-- # Cycles back and forth between phases 1 and 2 normally #
	if boss.phase == 1 then
		FRZBossPhase1(boss)
	end
	if boss.phase == 2 then
		FRZBossPhase2(boss)
	end
	
	-- ## Boss pain phases begin here ##
	-- # When hit, selects at random via the pain script one of these two behaviours to adopt #
	if boss.phase == 3 then
		FRZBossPhase3(boss)
	end
	if boss.phase == 4 then
		FRZBossPhase4(boss)
	end

	return true
end, MT_BOILINGEGG)

addHook("ShouldDamage", function(mo, inf, src, dmg, dmgtype)
	if (dmgtype == DMG_FIRE) then
		return false
	end
	if (inf == nil or src == nil) then
		return
	end
	if (inf.type == MT_CYBRAKDEMON_NAPALM_BOMB_SMALL or src.type == MT_CYBRAKDEMON_NAPALM_BOMB_SMALL) then
		return false
	end
end, MT_BOILINGEGG)

addHook("MobjDamage", function(boss, iflct, src, dmg, dmgtype) -- On-hit routine
	P_InstaThrust(boss, boss.angle, 0) -- Reset all the momentum

	S_StartSound(nil, sfx_dmpain) -- it wont play a painsound so i have to do it myself
	if boss.health == 4 then -- See if we made him pissed off or not
		S_StartSound(nil, sfx_rage)
		boss.pinch = 1
	end
	boss.health = $-1 -- Decrease the boss' health by one

	if boss.health <= 0 then --if the boss died, exit early
		boss.dead = 1

		local cleantypes = {
			[MT_EGGBALLER_FIRE] = true,
			[MT_BE_QUAKEMISSILE] = true,
			[MT_BE_QUAKESPAWNER] = true
		}

		for mo in mobjs.iterate() do
			if (cleantypes[mo.type] == true) then
				P_KillMobj(mo)
			end
		end

		return false --use regular behaviour
	end

	--yeah no I'm disabling this for now, needs to be implemented inside the quake missile
	--P_LinedefExecute(possibleTriggers[P_RandomRange(1, #possibleTriggers)], boss, boss.subsector.sector)

	--TEMPORARY

	boss.flags2 = $|MF2_FRET -- No? OK, good. Make him FUCKING INVINCIBLE!
	P_LookForPlayers(boss, 0, true, false) -- Find a new target, just to keep players on their toes again

	if (P_RandomRange(1, 2) == 1 and boss.pinch == 0) then -- Pick a random number between 1 and 2
		boss.phase = 3 -- If it's 1 we go to phase 3 and initialise the corresponding timer
		if boss.pinch == 1 then
			boss.timer = 3*TICRATE
		else
			boss.timer = 2*TICRATE
		end
	else
		boss.phase = 4 -- If it's 2 we go to phase 4 and initialise the corresponding timer
		if boss.pinch == 1 then
			boss.timer = 4*TICRATE
		else
			boss.timer = 3*TICRATE
		end
	end

	return true
end, MT_BOILINGEGG)

addHook("BossDeath", function(boss)
	--temporary, make boss do linedef actions here for the real FRZ3 probably
	A_ForceWin(boss, 0, 0)
	return false
end, MT_BOILINGEGG)


local function FRZFireThink(mo)
	if not (leveltime % 4) then
		P_SpawnGhostMobj(mo)
	end
	if (mo.type == MT_EGGBALLER_FIRE) then
		local mosector = mo.subsector.sector
		local onfloor = P_IsObjectOnGround(mo)
		--bounce off floors. lava FOFs are also floors.
		if (not onfloor) then
			for fof in mosector.ffloors() do
				if (mo.z <= fof.topheight and fof.flags & FOF_SWIMMABLE) then
					onfloor = true
					break
				end
			end
		end

		if (onfloor) then
			mo.momz = 8*FRACUNIT
			A_FlameParticle(mo)

			if not (mo.tracer and mo.tracer.valid) then
				--might somehow not find any players, but that's fine, not every bounce will break olympic records
				P_LookForPlayers(mo, 2000*FRACUNIT, true, true)
			end

			A_FaceTracer(mo)

			--chase tracer
			if (mo.tics <= 5*TICRATE) then
				local speed = R_PointToDist2(0,0,mo.momx, mo.momy)
				P_InstaThrust(mo, mo.angle, 9*speed/10)
			end
		end
	end
end

addHook("MobjThinker", FRZFireThink, MT_EGGBALLER_FIRE)

local function QuakeBombThink(mo)
	mo.exploding = $ or false
	if (mo.armed and not mo.exploding) then
		P_SpawnGhostMobj(mo)
		if (R_PointToDist2(mo.x, mo.y, mo.target.x, mo.target.y) < 30*FRACUNIT) then
			P_MoveOrigin(mo, mo.target.x, mo.target.y, mo.z)
			P_InstaThrust(mo, mo.angle, 0)
			mo.state = S_QUAKESPAWNER_CEASE
			mo.exploding = true
			P_StartQuake(10*FRACUNIT, TICRATE)
		end
	end
	if (mo.exploding) then
		mo.extravalue1 = $ + 1
		if (mo.extravalue1 % 4 == 0) then
			A_BossScream(mo, 1, 0)
		end
		if (mo.extravalue1 == TICRATE) then
			local waypoint = mo.target
			local triggerTag = waypoint.spawnpoint.tag
			P_LinedefExecute(triggerTag, waypoint, waypoint.subsector.sector)
			P_KillMobj(mo)
		end
	end
end

addHook("MobjThinker", QuakeBombThink, MT_BE_QUAKESPAWNER)