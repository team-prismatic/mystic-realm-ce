freeslot(
"MT_POSEIDONCORE",
"MT_POSEIDONCORE_BOTTOM",
"S_POSEIDONCORE",
"S_POSEIDONCORE_BOTTOM",
"S_POSEIDONCORE_DEATH",
"MT_POSEIDONBLAST",
"MT_POSEIDONBLAST_PAIN",
"S_POSEIDONBLAST",
"S_POSEIDONBLAST_TYPE2",
"MT_POSEIDONSHIELD",
"S_POSEIDONSHIELD",
"MT_POSEIDONTRUECORE"
)

freeslot(
"MT_POSEIDONGENERATOR",
"S_POSEIDONGENERATOR",
"S_POSEIDONGENERATOR_SHINE",
"S_POSEIDONGENERATOR_PAIN"
)

freeslot(
"SPR_GENH",
"SPR_GENS",
"SPR_PSDN",
"SPR_PCBL",
"sfx_pschrg",
"sfx_psbeam",
"sfx_prleas",
"sfx_pshock",
"sfx_pschg2",
"sfx_pschg3",
"sfx_psexpd",
"sfx_psexp2",
"sfx_pspnch",
"sfx_psdmgd",
"sfx_psdead"
)

-- Statblock for the Poseidon Core
mobjinfo[MT_POSEIDONCORE] = {
	--$Name Poseidon Core
	--$Sprite PSDNA0
	--$Category Bosses/Mystic Realm
	doomednum = 4500,
	spawnstate = S_POSEIDONCORE,
    seestate = S_POSEIDONCORE,
	painstate = S_POSEIDONCORE,
	deathstate =  S_POSEIDONCORE,
	xdeathstate = S_POSEIDONCORE,
	meleestate = S_NULL,
	missilestate = S_NULL,
	raisestate = S_NULL,
	painchance = 100,
	spawnhealth = 4,
	reactiontime = 16,
	mass = 8,
	damage = 1,
	painsound = sfx_dmpain,
	activesound = sfx_telept,
	deathsound = sfx_cybdth,
	speed = 8*FRACUNIT,
	radius = 180*FRACUNIT,
	height = 360*FRACUNIT,
	flags = MF_SPECIAL|MF_SHOOTABLE|MF_BOSS|MF_NOGRAVITY
}

states[S_POSEIDONCORE] = {SPR_PSDN, A|FF_FULLBRIGHT, -1, nil, 0, 0, S_POSEIDONCORE}
states[S_POSEIDONCORE_DEATH] = {SPR_PSDN, B, -1, nil, 0, 0, S_POSEIDONCORE_DEATH}

-- Bottom Piece

mobjinfo[MT_POSEIDONCORE_BOTTOM] = {
	doomednum = -1,
	spawnstate = S_POSEIDONCORE_BOTTOM,
	painchance = 100,
	spawnhealth = 1000,
	radius = 180*FRACUNIT,
	height = 360*FRACUNIT,
	flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_NOCLIPTHING
}

states[S_POSEIDONCORE_BOTTOM] = {SPR_PSDN, C|FF_FULLBRIGHT, 1, nil, 0, 0, S_POSEIDONCORE_BOTTOM}

--Regular attack blast

mobjinfo[MT_POSEIDONBLAST] = {
	doomednum = -1,
	spawnstate = S_POSEIDONBLAST,
	spawnhealth = 1000,
	speed = 48*FRACUNIT,
	radius = 24*FRACUNIT,
	height = 40*FRACUNIT,
	meleestate = S_NULL,
	missilestate = S_NULL,
	deathstate = S_NULL,
	reactiontime = 5,
	seestate = S_NULL,
	painsound = sfx_dmpain,
	seesound = sfx_None,
	mass = 8*FRACUNIT,
	flags = MF_SPECIAL|MF_NOGRAVITY|MF_MISSILE
}

mobjinfo[MT_POSEIDONBLAST_PAIN] = {
	doomednum = -1,
	spawnstate = S_POSEIDONBLAST,
	spawnhealth = 1000,
	speed = 48*FRACUNIT,
	radius = 20*FRACUNIT, --make it less scary than it seems
	height = 36*FRACUNIT,
	meleestate = S_NULL,
	missilestate = S_NULL,
	deathstate = S_NULL,
	reactiontime = 5,
	seestate = S_NULL,
	painsound = sfx_dmpain,
	seesound = sfx_None,
	mass = 8*FRACUNIT,
	flags = MF_SPECIAL|MF_NOGRAVITY|MF_PAIN
}

mobjinfo[MT_POSEIDONTRUECORE] = {
	doomednum = -1,
	spawnstate = S_INVISIBLE,
	radius = 4*FRACUNIT,
	height = 8*FRACUNIT,
	flags = MF_NOCLIP|MF_NOGRAVITY
}

states[S_POSEIDONBLAST] = {SPR_PCBL, A|FF_FULLBRIGHT, -1, nil, nil, nil, S_POSEIDONBLAST}
states[S_POSEIDONBLAST_TYPE2] = {SPR_GENS, C|FF_FULLBRIGHT|FF_ANIMATE|FF_FLOORSPRITE, -1, nil, 5, 2, S_POSEIDONBLAST_TYPE2}

--(Cosmetic) Shield

mobjinfo[MT_POSEIDONSHIELD] = {
	doomednum = -1,
	spawnstate = S_MAGN1,
	spawnhealth = 4,
	speed = 3,
	radius = 32*FRACUNIT,
	height = 40*FRACUNIT,
	meleestate = S_NULL,
	missilestate = S_NULL,
	deathstate = S_NULL,
	reactiontime = 5,
	seestate = S_NULL,
	seesound = sfx_None,
	mass = 8*FRACUNIT,
	flags = MF_NOGRAVITY
}

--Generators for the boss
mobjinfo[MT_POSEIDONGENERATOR] = {
	--$Name Poseidon Core Generator
	--$Sprite GENHA0
	--$Category Mystic Realm Bosses
	doomednum = 4501,
	spawnstate = S_POSEIDONGENERATOR,
	deathstate =  S_XPLD1,
	meleestate = S_NULL,
	painstate = S_POSEIDONGENERATOR_PAIN,
	missilestate = S_NULL,
	raisestate = S_NULL,
	painchance = 100,
	spawnhealth = 2,
	reactiontime = 16,
	mass = 8,
	damage = 1,
	painsound = sfx_dmpain,
	activesound = sfx_telept,
	deathsound = sfx_cybdth,
	speed = 8*FRACUNIT,
	radius = 70*FRACUNIT,
	height = 180*FRACUNIT,
	flags = MF_SPECIAL|MF_ENEMY|MF_NOGRAVITY
}

states[S_POSEIDONGENERATOR] = {SPR_GENH, A|FF_FULLBRIGHT, 1, nil, nil, nil, S_POSEIDONGENERATOR}
states[S_POSEIDONGENERATOR_PAIN] = {SPR_GENH, A|FF_FULLBRIGHT, 3*TICRATE, nil, nil, nil, S_POSEIDONGENERATOR}
states[S_POSEIDONGENERATOR_SHINE] = {SPR_GENS, A|FF_ANIMATE, -1, nil, 1, 2, S_POSEIDONGENERATOR_SHINE}