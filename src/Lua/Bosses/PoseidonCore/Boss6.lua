local generators = {}
local mapPinchLinedef = 210


--GENERAL HELPERS

local function GetCurrentSineValue(freq)
	local angle = ANG1 * ((leveltime * freq) % 360)
	return sin(angle)
end

local function GetReturnThrust(dist, angle)
	local x = P_ReturnThrustX(nil, angle, dist)
	local y = P_ReturnThrustY(nil, angle, dist)
	return x, y
end

local function GetRandomOffset3D(offx, offy, offz)
	offy = offy or offx
	offz = offz or offx

	local xdiff = P_RandomRange(-offx, offx) * FRACUNIT
	local ydiff = P_RandomRange(-offy, offy) * FRACUNIT
	local zdiff = P_RandomRange(-offz, offz) * FRACUNIT

	return xdiff, ydiff, zdiff
end


addHook("MapLoad", function()
	generators = {}
end)

----BOSS HOOKS START HERE

--Boss Spawn

local function BossSpawn(mo)
	P_SpawnMobj(mo.x, mo.y, mo.z - 60*FRACUNIT, MT_POSEIDONCORE_BOTTOM)
	mo.shield = P_SpawnMobj(mo.x, mo.y, mo.z + mo.height/2, MT_POSEIDONSHIELD)
	mo.shield.destscale = 8*FRACUNIT
	mo.shield.scalespeed = FRACUNIT/2


	mo.phase = 0
	mo.invulnframes = 0
	mo.bosstimer = 3*TICRATE
	mo.bosstimermax = 5*TICRATE
end

addHook("MobjSpawn", BossSpawn, MT_POSEIDONCORE)


--Boss Thinker and Helpers

local phaseLengths = {
	[1]  = {normal = 6*TICRATE, pinch = 5*TICRATE},
	[2]  = {normal = 3*TICRATE, pinch = 3*TICRATE},
	[3]  = {normal = 7*TICRATE, pinch = 7*TICRATE},
	[100]  = {normal = 4*TICRATE, pinch = 4*TICRATE},
	[101]  = {normal = 8*TICRATE, pinch = 8*TICRATE}
}

local function GetPhaseTime(mo)
	if (mo.pinch) then
		return phaseLengths[mo.phase].pinch
	end

	return phaseLengths[mo.phase].normal
end

local function InvulnTimer(mo)
	if (mo.invulnframes > 1) then
		if (mo.shield and mo.shield.valid) then
			if (mo.invulnframes % 2 == 0) then
				mo.shield.flags2 = $ | MF2_DONTDRAW
			else
				mo.shield.flags2 = $ &~ MF2_DONTDRAW
			end
		end

		mo.invulnframes = $ - 1
	elseif (mo.invulnframes == 1) then
		if (mo.shield and mo.shield.valid) then
			mo.shield.flags2 = $ & ~MF2_DONTDRAW
		end
		mo.flags = $ | MF_SPECIAL
		mo.flags2 = $ & ~MF2_FRET
		mo.invulnframes = 0
	end
end

local function RunBossTimer(mo)
	if mo.bosstimer == nil then
		mo.bosstimer = 2*TICRATE
		mo.bosstimermax = 2*TICRATE
	end

	mo.bosstimer = $ - 1
	if (mo.bosstimer == 0) then
		--pick attack
		if (mo.pinch) then
			mo.phase = P_RandomRange(1, 3)
		else
			if (mo.pinchenter) then
				mo.phase = 101
			elseif (mo.phase == 0 or mo.phase == 100 or mo.phase == 101) then
				mo.phase = P_RandomRange(1, 3)
			--regular attack finished, use energy-intensive attack
			elseif (mo.phase >= 1 and mo.phase <= 3) then
				mo.phase = 100
			end
		end
		mo.bosstimer = GetPhaseTime(mo)
	end
end

local function Phase1Attack(mo)
	local phasetime = GetPhaseTime(mo)
	if (mo.bosstimer == phasetime - 1) then
		S_StartSound(nil, sfx_pschrg)
	end
	if (mo.bosstimer <= phasetime and mo.bosstimer > TICRATE) then
		local ghost = P_SpawnGhostMobj(mo)
		ghost.momx = P_RandomRange(-3,3) * FRACUNIT
		ghost.momy = P_RandomRange(-3,3) * FRACUNIT
		ghost.blendmode = AST_ADD
		ghost.destscale = 12*FRACUNIT/10
		mo.frame = B|FF_FULLBRIGHT
	end
	if (mo.bosstimer <= 3*TICRATE and mo.bosstimer >= TICRATE and mo.bosstimer % (TICRATE/2) == 0) then
		local offsetRadius = 60*FRACUNIT

		for i = 0, 7 do
			local ang = ANGLE_45 * i
			local x, y = GetReturnThrust(offsetRadius, ang)

			local blast = P_SpawnMobj(mo.x + x, mo.y + y, mo.z + mo.height/4, MT_POSEIDONBLAST)
			blast.angle = ang
			blast.momx, blast.momy = GetReturnThrust(40*FRACUNIT, blast.angle)
			blast.momz = 15*FRACUNIT
			blast.target = mo
			blast.attacktype = 1

			x, y = GetReturnThrust(offsetRadius / 2, ang)
			local blast2 = P_SpawnMobj(mo.x + x, mo.y + y, mo.z + mo.height/2, MT_POSEIDONBLAST)
			blast2.angle = ang
			blast2.momx, blast2.momy = GetReturnThrust(40*FRACUNIT, blast.angle)
			blast2.momz = 15*FRACUNIT
			blast2.target = mo
			blast2.attacktype = 1
		end

		S_StartSound(nil, sfx_psbeam)
	end
	if (mo.bosstimer == TICRATE) then
		mo.frame = A|FF_FULLBRIGHT
	end
end

local function Phase2Attack(mo)
	local phasetime = GetPhaseTime(mo)
	if (mo.bosstimer == phasetime - 1) then
		S_StartSound(nil, sfx_prleas)
	end
	if (mo.bosstimer <= phasetime and mo.bosstimer > 3*TICRATE/2) then
		local ghost = P_SpawnGhostMobj(mo)
		ghost.momx = P_RandomRange(-3,3) * FRACUNIT
		ghost.momy = P_RandomRange(-3,3) * FRACUNIT
		ghost.blendmode = AST_ADD
		ghost.destscale = 12*FRACUNIT/10
		mo.frame = B|FF_FULLBRIGHT
	end
	if (mo.bosstimer == TICRATE) then
		local offsetRadius = 60*FRACUNIT

		for i = 0, 7 do
			local ang = ANGLE_45 * i
			local x, y = GetReturnThrust(offsetRadius, ang)

			local blast = P_SpawnMobj(mo.x + x, mo.y + y, mo.z + mo.height/4, MT_POSEIDONBLAST)
			blast.angle = ang
			blast.momx, blast.momy = GetReturnThrust(70*FRACUNIT, blast.angle)
			blast.momz = 15*FRACUNIT
			blast.target = mo
			blast.attacktype = 2
			blast.flags = MF_NOGRAVITY
			blast.frame = C
			blast.timer = 4*TICRATE + P_RandomRange(-TICRATE, TICRATE)
		end

		S_StartSound(nil, sfx_pshock)
		mo.frame = A|FF_FULLBRIGHT
	end
end

local function Phase3Attack(mo)
	local phasetime = GetPhaseTime(mo)
	if (mo.bosstimer == phasetime - 1) then
		S_StartSound(nil, sfx_pschrg)
	end
	if (mo.bosstimer <= phasetime and mo.bosstimer > 5*TICRATE) then
		local ghost = P_SpawnGhostMobj(mo)
		ghost.momx = P_RandomRange(-3,3) * FRACUNIT
		ghost.momy = P_RandomRange(-3,3) * FRACUNIT
		ghost.blendmode = AST_ADD
		ghost.destscale = 12*FRACUNIT/10
		mo.frame = B|FF_FULLBRIGHT
	end
	--wave1
	local createCircle = function(fuse)
		if not (mo.circle) then mo.circle = {} end

		local offsetRadius = 5*FRACUNIT
		for i = 0,7 do
			local ang = ANGLE_45 * i
			local x, y = GetReturnThrust(offsetRadius, ang)
			local blast = P_SpawnMobj(mo.x + x, mo.y + y, mo.z + mo.height/4, MT_POSEIDONBLAST)
			blast.target = mo
			blast.scale = 2*FRACUNIT
			blast.destscale = 13*FRACUNIT/5
			blast.scalespeed = FRACUNIT/128
			blast.flags = $ | MF_NOCLIPHEIGHT
			blast.fuse = fuse
			table.insert(mo.circle, blast)
		end

		S_StartSound(nil, sfx_psbeam)
	end

	if (mo.bosstimer == 5*TICRATE) then
		createCircle(5*TICRATE)
	end
	--wave2
	if (mo.bosstimer == 4*TICRATE) then
		createCircle(4*TICRATE)
	end

	if (mo.bosstimer < 5*TICRATE and mo.bosstimer > TICRATE/2) then
		local diff = (5*TICRATE - mo.bosstimer)
		local offsetAngle = ANG1 * (diff * 3 % 360)
		local offsetRadius = 5*FRACUNIT + min(diff*16*FRACUNIT , 2048*FRACUNIT)
		local offsetZ = FixedMul(240*FRACUNIT, cos(offsetAngle)) - 240*FRACUNIT
		for i = 1,8 do
			local blast = mo.circle[i]
			if (blast and blast.valid) then
				local ang = ANGLE_45 * (i-1) + offsetAngle
				local x, y = GetReturnThrust(offsetRadius, ang)
				P_MoveOrigin(blast, mo.x + x, mo.y + y, mo.z + mo.height/2 + offsetZ)
			end
		end
		if (mo.bosstimer < 4*TICRATE) then
			local wave2diff = (4*TICRATE - mo.bosstimer)
			local wave2offsetRadius = 5*FRACUNIT + min(wave2diff*16*FRACUNIT , 1024*FRACUNIT)
			local wave2offsetZ = FixedMul(240*FRACUNIT, cos(offsetAngle + ANGLE_90)) - 240*FRACUNIT
			for i = 9,16 do
				local index = i-8
				local blast = mo.circle[i]
				if (blast and blast.valid) then
					local ang = ANGLE_45 * (index-1) + offsetAngle
					local x, y = GetReturnThrust(wave2offsetRadius, ang)
					P_MoveOrigin(blast, mo.x + x, mo.y + y, mo.z + mo.height/2 + wave2offsetZ)
				end
			end
		end
	end
	if (mo.bosstimer == 2*TICRATE) then
		mo.frame = A|FF_FULLBRIGHT
	end
	--cleanup
	if (mo.bosstimer == TICRATE/2) then
		for i = 1,16 do
			local blast = mo.circle[i]
			if (blast and blast.valid) then
				P_KillMobj(blast, mo, mo, DMG_INSTAKILL)
			end
		end
		mo.circle = {}
	end
end

local function PhaseEnergyAttack(mo)
	local phasetime = GetPhaseTime(mo)
	if (mo.bosstimer == phasetime - 1) then
		S_StartSound(nil, sfx_pschg2)
		for k,gen in pairs(generators) do
			if not (gen and gen.valid)  then continue end
			if not (gen.shielded) then continue end

			gen.shielded = false
			gen.flags = $ | MF_SHOOTABLE
			if (gen.targetmarker and gen.targetmarker.valid) then
				gen.targetmarker.drawtarget = true
			end
			for i=1, #gen.shields do
				if not (gen.shields[i] and gen.shields[i].valid) then return end
				gen.shields[i].flags2 = $ | MF2_DONTDRAW
			end
		end
	end
	if (mo.bosstimer <= phasetime and mo.bosstimer > TICRATE) then
		local ghost = P_SpawnGhostMobj(mo)
		ghost.momx = P_RandomRange(-3,3) * FRACUNIT
		ghost.momy = P_RandomRange(-3,3) * FRACUNIT
		ghost.blendmode = AST_ADD
		ghost.destscale = 12*FRACUNIT/10
		mo.frame = B|FF_FULLBRIGHT

		if (mo.bosstimer % 3 == 0) then
			for k,gen in pairs(generators) do
				if not (gen and gen.valid)  then continue end

				local offz = gen.z + gen.height + 40*FRACUNIT
				local shine = P_SpawnMobj(gen.x, gen.y, offz, MT_THOK)
				shine.blendmode = AST_ADD
				shine.scale = 2*FRACUNIT
				shine.state = S_POSEIDONGENERATOR_SHINE
				shine.fuse = TICRATE/2

				local dist = FixedHypot(R_PointToDist2(gen.x, gen.y, mo.x, mo.y), mo.z + mo.height / 2 - offz)
				local speed = dist / shine.fuse
				local ang = R_PointToAngle2(0, 0, R_PointToDist2(gen.x, gen.y, mo.x, mo.y),  mo.z + mo.height / 2 - offz)
				local hang = R_PointToAngle2(gen.x, gen.y, mo.x, mo.y)

				shine.momz = FixedMul(sin(ang), speed)
				shine.momx = FixedMul(cos(hang), FixedMul(cos(ang), speed))
				shine.momy = FixedMul(sin(hang), FixedMul(cos(ang), speed))
			end
		end
	end

	if (mo.bosstimer == 3*TICRATE) then
		mo.blast = P_SpawnMobj(mo.x, mo.y, mo.z + mo.height/4, MT_POSEIDONBLAST)
		mo.blast.flags = $ | MF_NOCLIPHEIGHT | MF_NOCLIP | MF_PAIN
		mo.blast.target = mo
		mo.blast.scale = 2*FRACUNIT
		mo.blast.destscale = 8*FRACUNIT

		S_StartSound(nil, sfx_pschrg)
	end
	if (mo.bosstimer == TICRATE) then
		if (mo.blast and mo.blast.valid) then
			mo.blast.attacktype = 1
			mo.blast.fuse = 3*TICRATE
			mo.blast = nil
			S_StartSound(nil, sfx_psexp2)
		end
	end
	if (mo.bosstimer == 1) then
		for k,gen in pairs(generators) do
			if not (gen and gen.valid)  then continue end
			if (gen.health <= 0) then continue end
			if (gen.shielded) then continue end

			gen.shielded = true
			gen.flags = $ & ~MF_SHOOTABLE --homing attack fix
			if (gen.targetmarker and gen.targetmarker.valid) then
				gen.targetmarker.drawtarget = false
			end
			for i=1, #gen.shields do
				if not (gen.shields[i] and gen.shields[i].valid) then return end
				gen.shields[i].flags2 = $ & ~MF2_DONTDRAW
			end
		end
	end
end

local function PhasePinchPrep(mo)
	local phasetime = GetPhaseTime(mo)
	if (mo.bosstimer == phasetime - 1) then
		S_StartSound(nil, sfx_pschg3)
	end
	if (mo.bosstimer <= phasetime and mo.bosstimer > 2*TICRATE) then
		local ghost = P_SpawnGhostMobj(mo)
		ghost.momx = P_RandomRange(-3,3) * FRACUNIT
		ghost.momy = P_RandomRange(-3,3) * FRACUNIT
		ghost.blendmode = AST_ADD
		ghost.destscale = 12*FRACUNIT/10
		mo.frame = B|FF_FULLBRIGHT
	end

	if (mo.bosstimer > TICRATE) then
		--activated
		if (mo.bosstimer % 4 == 0) then
			local offx, offy, offz = GetRandomOffset3D(240)
			local particle = P_SpawnMobj(mo.x + offx, mo.y + offy, mo.z + mo.height/2 + offz, MT_THOK)
			particle.sprite = SPR_PCBL
			particle.frame = C | FF_FULLBRIGHT
			particle.blendmode = AST_ADD
			particle.tics = TICRATE/2
			particle.momz = 8*FRACUNIT
			particle.scale = P_RandomRange(360, 540) * FRACUNIT / 100
			particle.destscale = FRACUNIT
		end
	end

	if (mo.bosstimer == 6*TICRATE) then
		P_LinedefExecute(mapPinchLinedef+1, mo, nil) --brings red aura up
		S_StartSound(nil, sfx_pspnch)
	end

	if (mo.bosstimer <= 4*TICRATE) then
		if (mo.bosstimer == 4*TICRATE) then
			mo.pinch = true
			P_LinedefExecute(mapPinchLinedef, mo, nil) --raises platforms
			S_StartSound(nil, sfx_psbeam)

			for p in players.iterate do
				P_FlashPal(p, PAL_NUKE, TICRATE/2)
			end
		end

		if mo.truecore == nil then
			mo.truecore = {}
			mo.coreexpand = 1
			for i = 1, 3 do
				mo.truecore[i] = P_SpawnMobjFromMobj(mo, 0, 0, mo.height/2, MT_POSEIDONTRUECORE)
				mo.truecore[i].flags2 = $|MF2_DONTDRAW
				mo.truecore[i].orbitballs = MRCElibs.SpawnVerticalOrbitObjects(MT_POSEIDONBLAST_PAIN, mo.truecore[i], 12, 16*FRACUNIT) --oh boy, nested tables
				mo.truecore[i].roll = FixedAngle(P_RandomRange(1, 358)*FRACUNIT)
				mo.truecore[i].pitch = FixedAngle(P_RandomRange(1, 358)*FRACUNIT)
				mo.truecore[i].angle = FixedAngle(P_RandomRange(1, 358)*FRACUNIT) --randomize the starting rotation
			end
		else--move the objects around
			for i = 1, 3 do
				mo.truecore[i].roll = $ + FixedAngle(3*FRACUNIT)
				mo.truecore[i].pitch = $ + FixedAngle(4*FRACUNIT) --this manages the orbits spinning as they first spawn in, how fast, and in what direction they do so. SHould probably spin faster than it would for the rest of the fight
				mo.truecore[i].angle = $ + FixedAngle(2*FRACUNIT)
				if mo.coreexpand < 512 then
					MRCElibs.MoveOrbitalRadius(mo.truecore[i], mo.truecore[i].orbitballs, mo.coreexpand*FRACUNIT)
				end
				for j = 1, #mo.truecore[i].orbitballs do
					MRCElibs.Rotation3D(mo.truecore[i], mo.truecore[i].orbitballs[j], mo.truecore[i].orbitballs[j].offset)
				end
			end
			mo.coreexpand = min($ + 16, 512)
		end
	end

	if (mo.bosstimer == TICRATE) then
		mo.frame = A|FF_FULLBRIGHT
	end

	if (mo.bosstimer == 1) then
		mo.flags2 = $ & ~MF2_FRET
	end
end

local function RunCurrentPhase(mo)
	if (mo.phase == 1) then
		Phase1Attack(mo)
	elseif (mo.phase == 2) then
		Phase2Attack(mo)
	elseif (mo.phase == 3) then
		Phase3Attack(mo)
	elseif (mo.phase == 100) then
		PhaseEnergyAttack(mo)
	elseif (mo.phase == 101) then
		PhasePinchPrep(mo)
	end
end

local function RunPinch(mo)
	if (mo.shield and mo.shield.valid) then return end --not pinch
	if mo.phase ~= 101 and mo.truecore ~= nil then
		for i = 1, 3 do
			mo.truecore[i].roll = $ + FixedAngle(4*FRACUNIT/3)
			mo.truecore[i].pitch = $ + FixedAngle(2*FRACUNIT) --this manages the orbits spinningfor the duration of the pinchphase after the pinch intro, how fast, and in what direction they do so. Should probably spin faster than it would for the rest of the fight
			mo.truecore[i].angle = $ + FixedAngle(2*FRACUNIT/3)
			if mo.coreexpand < 512 then
				MRCElibs.MoveOrbitalRadius(mo.truecore[i], mo.truecore[i].orbitballs, mo.coreexpand*FRACUNIT)
			end
			for j = 1, #mo.truecore[i].orbitballs do
				MRCElibs.Rotation3D(mo.truecore[i], mo.truecore[i].orbitballs[j], mo.truecore[i].orbitballs[j].offset)
			end
		end
	end
end

local function RunDeath(mo)
	if (mo.bosstimer > 0) then
		mo.bosstimer = $ - 1
	end
	if (mo.bosstimer > TICRATE and mo.bosstimer % 4 == 0) then
		local offx, offy, offz = GetRandomOffset3D(2400)
		local explosion = P_SpawnMobj(mo.x + offx, mo.y + offy, mo.z + mo.height/2 + offz, MT_THOK)
		explosion.state = S_XPLD1
		explosion.scale = 3*FRACUNIT
		S_StartSound(nil, sfx_s3k3d)
	end
	if (mo.bosstimer % TICRATE == 0) then
		local spreadcount = 64
		for i = 1,spreadcount do
			local ang = ANG1 * (360 * i / spreadcount)
			local particle = P_SpawnMobj(mo.x, mo.y, mo.z + mo.height / 4, MT_THOK)
			particle.angle = ang + ANGLE_90
			particle.momx, particle.momy = GetReturnThrust(80*FRACUNIT, ang)
			particle.sprite = SPR_GENH
			particle.frame = C | FF_FULLBRIGHT | FF_PAPERSPRITE
			particle.blendmode = AST_ADD
			particle.tics = 2*TICRATE
			particle.scale = 3*FRACUNIT
		end
		S_StartSound(nil, sfx_psdmgd)
	end
	if (mo.bosstimer == TICRATE) then
		P_LinedefExecute(mapPinchLinedef+2, mo, nil) --brings red aura back down
		S_StartSound(nil, sfx_psdead)
		mo.sprite = SPR_NULL
		for p in players.iterate do
			P_FlashPal(p, PAL_NUKE, TICRATE)
		end
	end

	if (mo.bosstimer == 1) then
		A_ForceWin(mo, 0, 0)
		mo.state = S_NULL
	end
end

local function CoreThinker(mo)
	if (mo.health <= 0) then
		RunDeath(mo)
	else
		mo.momz = FixedMul(GetCurrentSineValue(4), 1*FRACUNIT)
		if (mo.shield and mo.shield.valid) then
			P_MoveOrigin(mo.shield, mo.x, mo.y, mo.z)
		end

		if (leveltime == 2) then
			mo.shield.health = #generators --sanity check
		end

		if (mo.pinch) then
			RunPinch(mo)
		end

		InvulnTimer(mo)
		RunCurrentPhase(mo)
		RunBossTimer(mo)
	end
end

addHook("BossThinker", CoreThinker, MT_POSEIDONCORE)

--Boss Damage

local function BossShieldCheck(mo, inf, src, dmg, dtype)
	if not (mo.shield and mo.shield.valid) then return end

	if (inf.type == MT_PLAYER and inf.valid and inf.player.valid) then
		if (src.type == MT_PLAYER) then
			local reverseangle = R_PointToAngle2(mo.x, mo.y, inf.x, inf.y)
			local oldspeed = FixedHypot(inf.momx, inf.momy)
			oldspeed = max($, 5*FRACUNIT)
			inf.momx = P_ReturnThrustX(nil, reverseangle, oldspeed)
			inf.momy = P_ReturnThrustY(nil, reverseangle, oldspeed)
			inf.state = S_PLAY_PAIN
			--P_DoPlayerPain(inf.player, mo, mo) why does this set invuln tics noooooo
			S_StartSound(mo, sfx_bnce2, inf.player)
		end
		return false
	end

	if (inf.type == MT_POSEIDONGENERATOR or src.type == MT_POSEIDONGENERATOR) then
		S_StartSound(mo, sfx_fizzle)

		--turns out it can't get hurt without MF_SHOOTABLE
		mo.shield.health = $ -1

		mo.invulnframes = 3*TICRATE
		mo.flags = $ & ~MF_SPECIAL

		if (mo.shield.health == 0) then
			P_KillMobj(mo.shield, mo, mo, DMG_INSTAKILL)
			mo.flags = $ | MF_SPECIAL
			mo.flags2 = $ | MF2_FRET
			mo.pinchenter = true
		end

		return false
	end
end

addHook("ShouldDamage", BossShieldCheck, MT_POSEIDONCORE)

local function BossDamage(mo, inf, src, dmg, dtype)
	mo.invulnframes = 3*TICRATE
	S_StartSound(nil, sfx_psdmgd)
	P_StartQuake(30*FRACUNIT, TICRATE)
	mo.flags2 = $ | MF2_FRET

	for p in players.iterate do
		P_FlashPal(p, PAL_NUKE, TICRATE/3)
	end

	local spreadcount = 64
	for i = 1,spreadcount do
		local ang = ANG1 * (360 * i / spreadcount)
		local particle = P_SpawnMobj(mo.x, mo.y, mo.z + mo.height / 4, MT_THOK)
		particle.angle = ang + ANGLE_90
		particle.momx, particle.momy = GetReturnThrust(50*FRACUNIT, ang)
		particle.sprite = SPR_GENH
		particle.frame = C | FF_FULLBRIGHT | FF_PAPERSPRITE
		particle.blendmode = AST_ADD
		particle.tics = TICRATE/2
		particle.scale = 2*FRACUNIT
	end

	if (src.type == MT_PLAYER) then
		local reverseangle = R_PointToAngle2(mo.x, mo.y, src.x, src.y)
		local oldspeed = FixedHypot(src.momx, src.momy)
		oldspeed = max($, 5*FRACUNIT)
		P_DoPlayerPain(src.player, mo, mo) --we DO need to give invuln tics this time
		src.momx = P_ReturnThrustX(nil, reverseangle, oldspeed*2)
		src.momy = P_ReturnThrustY(nil, reverseangle, oldspeed*2)
	end
end

addHook("MobjDamage", BossDamage, MT_POSEIDONCORE)

local function BossDeath(mo, inf, src, dtype)
	mo.state = S_POSEIDONCORE_DEATH
	mo.flags2 = $ & ~MF2_FRET
	mo.bosstimer = 8*TICRATE

	P_StartQuake(60*FRACUNIT, mo.bosstimer)

	for mob in mobjs.iterate() do
		if mob.type == MT_POSEIDONBLAST or mob.type == MT_POSEIDONBLAST_PAIN then
			P_RemoveMobj(mob)
		end
	end

	mo.truecore = nil

	return true
end

addHook("MobjDeath", BossDeath, MT_POSEIDONCORE)

----BOSS HOOKS END HERE


--Projectile behaviour

local function PoseidonBlastThinker_Type1(mo)
	if (mo.momz > FRACUNIT) then
		mo.momx = $ * 90/100
		mo.momy = $ * 90/100
		mo.momz = $ * 90/100
	else
		if not (mo.tracer and mo.tracer.valid) then
			A_FindTracer(mo, MT_PLAYER, 0)
		else
			mo.extravalue1 = $ + 1

			if (mo.extravalue1 < TICRATE/4) then
				local ang = R_PointToAngle2(mo.x, mo.y, mo.tracer.x, mo.tracer.y)
				local dist = R_PointToDist2(mo.x, mo.y, mo.tracer.x, mo.tracer.y)
				local zdist = mo.z - mo.tracer.z

				local distanceMult = FixedDiv(dist, 256*FRACUNIT)
				local zDistanceMult = FixedDiv(zdist, 256*FRACUNIT)
				mo.momx = $ + P_ReturnThrustX(nil, ang, distanceMult * 2)
				mo.momy = $ + P_ReturnThrustY(nil, ang, distanceMult * 2)
				mo.momz = $ + -2 * zDistanceMult
			end
		end
	end
end

local function PoseidonBlastThinker_Type2(mo)
	if mo.phase == nil then mo.phase = 0 end
	if mo.timer == nil then mo.timer = 4*TICRATE end

	if (mo.phase == 0) then
		mo.momx = $ * 86/100
		mo.momy = $ * 86/100
		mo.momz = $ - FRACUNIT

		if P_IsObjectOnGround(mo) then
			mo.phase = 1
			mo.notrail = true
			mo.flags = $ | MF_NOCLIP
			mo.scale = 2*FRACUNIT
			mo.state = S_POSEIDONBLAST_TYPE2
		end
	elseif (mo.phase == 1) then
		mo.timer = $ - 1
		if (mo.timer == 0) then
			local blast = P_SpawnMobj(mo.x, mo.y, mo.z, MT_POSEIDONBLAST)
			blast.momz = 32*FRACUNIT
			blast.scale = 2*FRACUNIT
			S_StartSound(blast, sfx_pshock)
			P_RemoveMobj(mo)
			return
		else
			if (mo.timer % 2 == 0) then
				local particle = P_SpawnMobj(mo.x, mo.y, mo.z + mo.height / 2, MT_THOK)
				particle.color = SKINCOLOR_WHITE
				particle.tics = 8
				particle.momz = 6*FRACUNIT
				particle.scale = FRACUNIT/4
			end

			if (mo.timer > TICRATE/4) then
				--target's the boss who fired them, el stupido
				--find players to trace
				if (mo.tracer) then
					A_HomingChase(mo, 16*FRACUNIT, 1)
				else
					A_FindTracer(mo, MT_PLAYER, 0)
				end
			else
				mo.momx = 0
				mo.momy = 0

				if (mo.timer == TICRATE/4) then
					S_StartSound(mo, sfx_buzz1)
				end
			end

			--can't use mo.floorz because of the water block
			mo.z = P_FloorzAtPos(mo.x, mo.y, mo.z, mo.height)
		end
	end
end

local function PoseidonBlastThinker(mo)
	if not (mo.notrail) then
		mo.blendmode = AST_ADD
		P_SpawnGhostMobj(mo)
		local trail = P_SpawnMobj(mo.x, mo.y, mo.z + mo.height / 2, MT_THOK)
		--trail.spriteyscale = FRACUNIT/2
		trail.scale = mo.scale
		trail.sprite = SPR_PCBL
		trail.frame = B
		trail.blendmode = AST_ADD
		trail.tics = 4
	end

	if not mo.attacktype then return end
	--me overcomplicating things just to save one freeslot
	if (mo.attacktype == 1) then
		PoseidonBlastThinker_Type1(mo)
	elseif (mo.attacktype == 2) then
		PoseidonBlastThinker_Type2(mo)
	end
end

addHook("MobjThinker", PoseidonBlastThinker, MT_POSEIDONBLAST)
addHook("MobjThinker", PoseidonBlastThinker, MT_POSEIDONBLAST_PAIN)

local friendlies = {
	[MT_POSEIDONCORE] = true,
	[MT_POSEIDONBLAST] = true,
	[MT_POSEIDONBLAST_PAIN] = true,
	[MT_JETTBULLET] = true,
	[MT_POSEIDONGENERATOR] = true
}

local function IgnoreCollide(tmthing, thing)
	if (friendlies[thing.type] == true and friendlies[tmthing.type] == true) then return false end
end

addHook("MobjMoveCollide", IgnoreCollide, MT_POSEIDONBLAST)
addHook("MobjMoveCollide", IgnoreCollide, MT_JETTBULLET)

--Generator Behaviour

local function GeneratorSpawn(mo)
	--here we go with the MT_THOK bullshit
	mo.shine = P_SpawnMobj(mo.x, mo.y, mo.z + mo.height + 40*FRACUNIT, MT_THOK)
	mo.shine.blendmode = AST_ADD
	mo.shine.scale = 2*FRACUNIT
	mo.shine.tics = -1
	mo.shine.state = S_POSEIDONGENERATOR_SHINE

	mo.targetmarker = P_SpawnMobj(mo.x, mo.y, mo.z + mo.height + 120*FRACUNIT, MT_THOK)
	--mo.targetmarker.sprite = SPR_GENH
	--mo.targetmarker.frame = D|FF_FULLBRIGHT
	--mo.targetmarker.scale = 3*FRACUNIT/2
	mo.targetmarker.tics = -1
	mo.targetmarker.flags2 = $ | MF2_DONTDRAW
	mo.targetmarker.drawtarget = false

	mo.extravalue1 = 0
	mo.shielded = true

	mo.shields = {}
	for i= 1,2 do
		local shield = P_SpawnMobj(mo.x, mo.y, mo.z + mo.height/2, MT_THOK)
		shield.tics = -1
		shield.sprite = SPR_GENH
		shield.frame = B|FF_PAPERSPRITE
		shield.blendmode = AST_ADD
		table.insert(mo.shields, shield)
	end
	mo.shieldangle = 0
end

local function GeneratorThinker(mo)
	--add self to generators list
	if leveltime == 1 then
		table.insert(generators, mo)
		A_FindTracer(mo, MT_POSEIDONCORE, 1)
	end

	--shield rotation
	mo.shieldangle = $ + ANG10
	local count = #mo.shields
	for i= 1,count do
		local shield = mo.shields[i]
		if not (shield and shield.valid) then continue end

		local ang = mo.shieldangle + ANG1 * ((360/count)*i)
		local offx = P_ReturnThrustX(nil, ang, 50*FRACUNIT)
		local offy = P_ReturnThrustY(nil, ang, 50*FRACUNIT)

		P_MoveOrigin(shield, mo.x + offx, mo.y + offy, mo.z)
		shield.angle = ang + ANGLE_90 --papersprite rotation
	end

	--attacktimer
	mo.extravalue1 = $ + 1
	local mex = mo.extravalue1 --I'm lazy

	local checkNewTarget = function(mo)
		if (P_LookForPlayers(mo, 1152*FRACUNIT, true)) then
			return true
		else
			mo.extravalue1 = P_RandomRange(-20, 0)
			return false
		end

		return false --is this necessary? lua is weird
	end

	if (mex == 2*TICRATE) then
		if not checkNewTarget(mo) then return end
	end

	if (mex >= 2*TICRATE) then

		--target's dead or deleted, be passive again
		if not (mo.target and mo.target.valid) then
			if not checkNewTarget(mo) then return end
		end

		--activated
		if (mex % (TICRATE/8) == 0) then
			local offx, offy, offz = GetRandomOffset3D(80)
			local particle = P_SpawnMobj(mo.x + offx, mo.y + offy, mo.z + mo.height/2 + offz, MT_THOK)
			particle.color = SKINCOLOR_WHITE
			particle.sprite = SPR_PCBL
			particle.frame = B | FF_FULLBRIGHT
			particle.blendmode = AST_ADD
			particle.tics = TICRATE/2
			particle.momz = 6*FRACUNIT
			particle.scale = P_RandomRange(120, 160) * FRACUNIT / 100
			particle.destscale = FRACUNIT/24
		end

		--target's too far away and there are no new targets, be passive again
		if (mex >= 3*TICRATE) then
			if (R_PointToDist2(mo.x, mo.y, mo.target.x, mo.target.y) > 1152*FRACUNIT) then
				if not checkNewTarget(mo) then return end
			end
		end

		if (mex == 3*TICRATE) then
			S_StartSound(mo, sfx_s3k79)
		end

		if (mex >= 5*TICRATE and mex <= 6*TICRATE and mex % 2 == 0) then
			local blast = P_SpawnMissile(mo, mo.target, MT_JETTBULLET)
			S_StartSound(mo, sfx_s3k4d)
		end

		if (mex >= 7*TICRATE and mex % (TICRATE / 3) == 0) then
			local blast = P_SpawnMissile(mo, mo.target, MT_POSEIDONBLAST)
			S_StartSound(mo, sfx_s3kc4s)
		end
	end
end

local function GeneratorDamageCheck(mo, inf, src, dmg, dtype)
	if (mo.shielded) then
		if (inf.type == MT_PLAYER and inf.valid and inf.player.valid) then
			local reverseangle = R_PointToAngle2(mo.x, mo.y, inf.x, inf.y)
			local oldspeed = FixedHypot(inf.momx, inf.momy)
			oldspeed = max(5*FRACUNIT, $)
			inf.momx = P_ReturnThrustX(nil, reverseangle, oldspeed)
			inf.momy = P_ReturnThrustY(nil, reverseangle, oldspeed)
			inf.state = S_PLAY_PAIN
			--P_DoPlayerPain(inf.player, mo, mo) why does this set invuln tics noooooo
			S_StartSound(mo, sfx_bnce1, inf.player)
		end
		return false
	end
end

local function GeneratorDamage(mo, inf, src, dmg, dtype)
	--what's with custom enemies not playing their painsounds when hit?
	--the solution is probably easy
	S_StartSound(mo, sfx_dmpain)
end

local function GeneratorDeath(mo, inf, src, dtype)
	for i= 1,#mo.shields do
		P_RemoveMobj(mo.shields[i])
	end
	P_RemoveMobj(mo.shine)
	P_RemoveMobj(mo.targetmarker)

	local boss = mo.tracer
	P_DamageMobj(boss, mo, mo, 1, DMG_ELECTRIC)

	S_StartSound(mo, sfx_bexpld)
	P_StartQuake(15*FRACUNIT, TICRATE/2)
	local explosion = P_SpawnMobj(mo.x, mo.y, mo.z + mo.height/2, MT_THOK)
	explosion.blendmode = AST_ADD
	explosion.scale = FRACUNIT/2
	explosion.destscale = 8*FRACUNIT
	explosion.scalespeed = FRACUNIT/4
	explosion.tics = 24
	explosion.sprite = SPR_PCBL
	explosion.frame = C | FF_TRANS40 | FF_FULLBRIGHT
	
	local ring = P_SpawnMobj(mo.x, mo.y, mo.z + mo.height/2, MT_FLINGRING)
	local flingangle = mo.angle + P_RandomRange(-15,15) * ANG1
	local flingspeed = 3*FRACUNIT
	ring.momx = P_ReturnThrustX(nil, flingangle, flingspeed)
	ring.momy = P_ReturnThrustY(nil, flingangle, flingspeed)
	ring.momz = P_RandomRange(2,6) * FRACUNIT
	ring.fuse = 5*TICRATE

	--P_SpawnMobj(mo.x, mo.y, mo.z + mo.height + 20*FRACUNIT, MT_LASERDUDE)
end


local function w2sTarget(v, p)
	if G_BuildMapName(gamemap) ~= "MAPAI" then return end
	for k,gen in pairs(generators) do
		if not (gen and gen.valid) then continue end

		if (gen.targetmarker and gen.targetmarker.valid)
		and gen.targetmarker.drawtarget then
			local camhack = camera.chase and camera or p.mo
			local visibleangle = abs(camhack.angle - R_PointToAngle(gen.targetmarker.x, gen.targetmarker.y))
			local pos = R_World2Screen3(v, p, camhack, gen.targetmarker, false)
			local spin = 0
			local screenwidth = v.width() / v.dupx()  --the total width of the screen. In green modes this is always 320.
			local sidextra = (screenwidth - 320) / 2 --the extra width added to the base screen width in non-green resolution, divided by two since
			local screenheight = v.height() / v.dupy()  --the total width of the screen. In green modes this is always 320.
			local topxtra = (screenheight - 200) / 2 --the extra width added to the base screen width in non-green resolution, divided by two since
			local cnterpntx = (screenwidth / 2) - sidextra
			local centerdiffx = abs(pos.x - (cnterpntx*FRACUNIT)) * 2
			local drawscale = max(min(pos.scale*2, (FRACUNIT*7) / 3), FRACUNIT/2)
			local theframe = gen.health > 1 and D or A
			if visibleangle > ANGLE_90 then
				pos.y = 192*FRACUNIT
				if pos.x > cnterpntx*FRACUNIT then
					pos.x = $ - centerdiffx
				else
					pos.x = $ + centerdiffx
				end
			end
			if pos.x < (12 - sidextra) * FRACUNIT then
				spin = ANGLE_270
				pos.x = (12 - sidextra) * FRACUNIT
			elseif pos.x > ((screenwidth - sidextra) - 12)*FRACUNIT then
				spin = ANGLE_90
				pos.x = ((screenwidth - sidextra) - 12)*FRACUNIT
			end
			if pos.y < 8*FRACUNIT then
				pos.y = 8*FRACUNIT
				if visibleangle < ANGLE_45 then
					spin = ANGLE_180
				end
			elseif pos.y > ((screenheight + topxtra))*FRACUNIT then
				pos.y = ((screenheight + topxtra))*FRACUNIT
			end
			--print(k .. " visx " .. tostring(pos.onScreen))
			--print(k .. " visy " .. tostring(pos.onScreen))
			--print(tostring(v.width()/v.dupx()))
			--print(k .. " posx " .. tostring(pos.x / FRACUNIT))
			--print(tostring(visible))
			--print(abs(camera.angle - R_PointToAngle(gen.targetmarker.x, gen.targetmarker.y)) / ANG1)
			v.drawScaled(pos.x, pos.y, drawscale, v.getSpritePatch(SPR_LCKN, theframe, 0, spin), V_PERPLAYER)
		end
	end
end

customhud.SetupItem("spz3targetlckn", "mrce", w2sTarget, "game", 2)

addHook("MobjSpawn", GeneratorSpawn, MT_POSEIDONGENERATOR)
addHook("MobjThinker", GeneratorThinker, MT_POSEIDONGENERATOR)
addHook("ShouldDamage", GeneratorDamageCheck, MT_POSEIDONGENERATOR)
addHook("MobjDamage", GeneratorDamage, MT_POSEIDONGENERATOR)
addHook("MobjDeath", GeneratorDeath, MT_POSEIDONGENERATOR)


addHook("NetVars", function(net)
	generators=net($)
end)