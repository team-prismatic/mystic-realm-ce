--#region Actions

--modified verson of Boss2Pogo
--will swap to a different type of attack if this proves to be unfun
local function A_Boss2SuperPogo(mo, var1, var2)
	local isPinch = mo.health <= mo.info.damage
	--chase spawnpoint (center of the map) if var2 is 1
	--chase players in pinch mode
	--select a random boss waypoint in normal mode
	if (var2 == 1) then
		mo.target = mo.centerwaypoint
	elseif (isPinch or #mo.waypoints == 0) then
		if not (mo.target and mo.target.valid and mo.target.flags & MF_SHOOTABLE)
		or (mo.target.player and mo.target.player.powers[pw_flashing]) then
			-- look for a new target
			if not (P_LookForPlayers(mo, 0, true, false)) then return end
		end
	else
		mo.waypointindex = $ + P_RandomRange(1, #mo.waypoints - 1)
		mo.waypointindex = ($ % #mo.waypoints) + 1
		mo.target = mo.waypoints[mo.waypointindex]
	end

	-- Target hit, retreat!
	if (mo.target.player and mo.target.player.powers[pw_flashing] > TICRATE) then
		local prandom = P_RandomByte()
		mo.momz = FixedMul(var1, mo.scale) -- Bounce up in air
		mo.angle = R_PointToAngle2(mo.x, mo.y, mo.target.x, mo.target.y)
		if (P_RandomChance(FRACUNIT/2)) then
			mo.angle = $ - prandom
		else
			mo.angle = $ + prandom
		end
		P_InstaThrust(mo, mo.angle+ANGLE_180, FixedMul(FixedMul(mo.info.speed, var1), mo.scale)) -- Move at wandering speed

	-- Try to land on top of the target.
	elseif (P_AproxDistance(mo.x-mo.target.x, mo.y-mo.target.y) < FixedMul(2048*FRACUNIT, mo.scale)) then

		-- check gravity in the sector (for later math)
		local gravity = P_GetMobjGravity(mo)
		mo.momz = FixedMul(var1 + (var1>>2), mo.scale) << 1-- Bounce up in air

		--airtime = t1 + t2
		--t1 = momz / gravity -- v = gt
		--t2 = sqrt(momz^2 + zoffset / gravity) -- x = (gt^2) * 1/2
		--...approximately anyway, I should use a paper

		-- offset by the difference in floor height plus half the player height,
		local zoffs
		if (mo.target.player) then
			zoffs = (P_GetPlayerHeight(mo.target.player)>>1) + (mo.target.floorz - mo.floorz)
		else
			zoffs = (mo.target.floorz - mo.floorz)
		end
		local offset = max(1, FixedMul(mo.momz,mo.momz)+zoffs)
		local airtime = FixedDiv((-(mo.momz + FixedSqrt(offset))), gravity) << 1
		--hella buggy when going to the middle in pinch phase
		--todo : find better approach
		if (isPinch and var1 ~= 1) then airtime = $ >> 1 end

		mo.angle = R_PointToAngle2(mo.x, mo.y, mo.target.x, mo.target.y)
		local dist = P_AproxDistance(mo.x - mo.target.x, mo.y - mo.target.y)
		P_InstaThrust(mo, mo.angle, FixedDiv(dist, airtime))

	-- Wander semi-randomly towards the target to get closer.
	else
		local prandom = P_RandomByte();
		mo.momz = FixedMul(var1, mo.scale); -- Bounce up in air
		mo.angle = R_PointToAngle2(mo.x, mo.y, mo.target.x, mo.target.y)
		if (P_RandomChance(FRACUNIT/2)) then
			mo.angle = $ - prandom
		else
			mo.angle = $ + prandom
		end
		P_InstaThrust(mo, mo.angle, FixedMul(FixedMul(mo.info.speed, var1), mo.scale)); -- Move at wandering speed
	end
	-- Boing!
	if (mo.info.activesound) then
		S_StartSound(mo, mo.info.activesound)
	end

	if (mo.info.missilestate) then -- spawn the pogo stick collision box
		local pogo = P_SpawnMobj(mo.x, mo.y, mo.z - mobjinfo[mo.info.missilestate].height, mo.info.missilestate);
		pogo.target = mo
		pogo.info.raisestate = mo.info.meleestate --HILARIOUSLY ugly MT_EGGMOBILE2_POGO hack
	end

	local spreadcount = (isPinch and 8) or 4
	for i = 1, spreadcount do
		local speed = FixedMul(3 * FRACUNIT, mo.scale)
		speed = $ + P_RandomRange(0,3) * FRACUNIT
		local angle = ANGLE_45 * (i-1)
		if not (isPinch) then angle = $ * 2 end

		local zoffset = mo.height + FixedMul(24*FRACUNIT, mo.scale)
		local goop = P_SpawnMobj(mo.x, mo.y, mo.z + zoffset, mo.info.painchance)
		goop.momx = FixedMul(cos(angle),speed)
		goop.momy = FixedMul(sin(angle),speed)
		goop.momz = FixedMul(4*FRACUNIT, mo.scale)
		goop.scale = 2*FRACUNIT

		goop.fuse = 7*TICRATE
	end
end

local function A_SpawnShieldSlime(mo, var1, var2)
	local shield = P_SpawnMobjFromMobj(mo, 0, 0, mo.height/2, MT_SHIELDSLIME)
	shield.scale = var1
	shield.targetscale = var2
	shield.target = mo
	mo.tracer = shield
end

local function A_CheckHomingLocation(mo, var1, var2)
	local dist = R_PointToDist2(mo.x, mo.y, mo.target.x, mo.target.y)
	local angle = R_PointToAngle2(mo.x, mo.y, mo.target.x, mo.target.y)

	if (dist < 10*FRACUNIT) then
		mo.momx = 0
		mo.momy = 0
		P_TryMove(mo, mo.target.x, mo.target.y)
	elseif (dist < 200*FRACUNIT) then
		mo.momx = FixedMul(dist / 5, cos(angle))
		mo.momy = FixedMul(dist / 5, sin(angle))
	end
	if (P_IsObjectOnGround(mo)) then
		if (dist < 50*FRACUNIT) then
			S_StartSound(mo, sfx_s3k96)
			mo.extravalue1 = 0
			mo.extravalue2 = 0
			mo.state = var1
		else
			mo.state = var2
		end
	end
end

local function CheckSlime(mo)
	local sec = mo.subsector.sector
	for ffloor in sec.ffloors() do
		--if the fof is goo
		if (ffloor.flags & (FF_GOOWATER|FF_SWIMMABLE) == FF_GOOWATER|FF_SWIMMABLE) then
			return ffloor
		end
	end
	return nil
end

local function ShieldDamage(mo, inflictor, source, dmg, dmgtype)
	if (mo.invulntimer > 0) then
		return false
	end

	mo.health = $ - dmg
	S_StartSound(mo, sfx_bubbl2)
	S_StartSound(mo, sfx_splish)

	if (mo.health <= 0 or dmgtype == DMG_INSTAKILL) then
		P_KillMobj(mo, inflictor, source, dmgtype)
	else
		mo.invulntimer = 2*TICRATE
	end

	local spreadcount = 8
	for i = 1, spreadcount do
		local speed = FixedMul(FRACUNIT, mo.scale)
		speed = $ + P_RandomRange(0,3) * FRACUNIT
		local angle = ANGLE_45 * (i-1)

		local zoffset = mo.height + FixedMul(24*FRACUNIT, mo.scale)
		local goop = P_SpawnMobj(mo.x, mo.y, mo.z + zoffset, mo.info.painchance)
		goop.momx = FixedMul(cos(angle),speed)
		goop.momy = FixedMul(sin(angle),speed)
		goop.momz = FixedMul(3*FRACUNIT/2, mo.scale)
		goop.scale = 2*FRACUNIT

		goop.fuse = 7*TICRATE
	end

	return true
end

local function ForceBounceBack(mo, toucher)
	if not toucher.player then return end --not a player, get out
	local player = toucher.player

	if (player.charability == CA_FLY and player.panim == PA_ABILITY) then
		toucher.momz = -1 * toucher.momz/2
	elseif (player.pflags & PF_GLIDING and !P_IsObjectOnGround(toucher)) then
		player.pflags = $ & ~(PF_GLIDING|PF_JUMPED|PF_NOJUMPDAMAGE)
		toucher.state = S_PLAY_FALL
		toucher.momz = $ + P_MobjFlip(toucher) * (player.speed >> 3)
		toucher.momx = 7 * $ >> 3
		toucher.momy = 7 * $ >> 3
	elseif ((player.powers[pw_strong] & STR_DASH) and player.panim == PA_DASH) then
		P_DoPlayerPain(player, mo, mo)
	end

	if (player.charability == CA_TWINSPIN and player.panim == PA_ABILITY) then
		player.pflags = $ & ~PF_THOKKED
	end
end

local function SpawnBossJunk(mo)
	local mo2 = P_SpawnMobjFromMobj(mo,
		P_ReturnThrustX(mo, mo.angle - ANGLE_90, 32<<FRACBITS),
		P_ReturnThrustY(mo, mo.angle - ANGLE_90, 32<<FRACBITS),
		32<<FRACBITS, MT_BOSSJUNK)

		mo2.angle = mo.angle
		P_InstaThrust(mo2, mo2.angle - ANGLE_90, 4*mo2.scale)
		P_SetObjectMomZ(mo2, 4*FRACUNIT, false)
		mo2.state = S_BOSSTANK1


	mo2 = P_SpawnMobjFromMobj(mo,
		P_ReturnThrustX(mo, mo.angle + ANGLE_90, 32<<FRACBITS),
		P_ReturnThrustY(mo, mo.angle + ANGLE_90, 32<<FRACBITS),
		32<<FRACBITS, MT_BOSSJUNK)

		mo2.angle = mo.angle
		P_InstaThrust(mo2, mo2.angle + ANGLE_90, 4*mo2.scale)
		P_SetObjectMomZ(mo2, 4*FRACUNIT, false)
		mo2.state = S_BOSSTANK2


	mo2 = P_SpawnMobjFromMobj(mo, 0, 0,
		mobjinfo[MT_EGGMOBILE2].height + (32<<FRACBITS),
		MT_BOSSJUNK)

		mo2.angle = mo.angle
		P_SetObjectMomZ(mo2, 4*FRACUNIT, false)
		mo2.momz = $ + mo.momz
		mo2.state = S_BOSSSPIGOT
end

--#endregion

-- STATE = sprite, frame, tics, action, var1, var2, nextstate
-- Why was I averse to this back in the day? It's easy to remember

--EGG AMOEBA

mobjinfo[MT_EGGAMOEBA] = {
	--$Name Egg Amoeba
	--$Sprite EGSLA1
	--$Category Bosses/Mystic Realm
	doomednum = 1715,
	spawnstate = S_EGGAMOEBA_START1,
	painstate = S_EGGAMOEBA_START1,
	deathstate = S_EGGAMOEBA_DIE1,
	xdeathstate = S_EGGMOBILE2_FLEE1,
	meleestate = S_EGGAMOEBA_POGO,
	missilestate = MT_EGGMOBILE2_POGO,
	raisestate = S_EGGAMOEBA_PINCHPREP1,
	painchance = MT_GOOP,
	spawnhealth = 4,
	mass = 10,
	damage = 2,
	painsound = sfx_s1ac,
	activesound = sfx_pogo,
	deathsound = sfx_cybdth,
	speed = 2*FRACUNIT,
	radius = 35*FRACUNIT,
	height = 120*FRACUNIT,
	flags = MF_BOSS
}

states[S_EGGAMOEBA_START1] = {SPR_EGSL, C, TICRATE, nil, nil, nil, S_EGGAMOEBA_START2}
states[S_EGGAMOEBA_START2] = {SPR_EGSL, H|FF_ANIMATE, 12, function (mo, var1, var2)
	if (not mo.waypoints) then
		mo.waypoints = {}
		mo.centerwaypoint = nil
		mo.waypointindex = 1
		for mobj in mobjs.iterate() do --not gonna ask why only mobjs requires parentheses
			if mobj.type == MT_BOSS3WAYPOINT then
				table.insert(mo.waypoints, mobj)
			end
			if mobj.type == MT_BOSSFLYPOINT then
				mo.centerwaypoint = mobj
			end
		end
	end
end, 4, 3, S_EGGAMOEBA_START3}
states[S_EGGAMOEBA_START3] = {SPR_EGSL, L, 12, A_SpawnShieldSlime, FRACUNIT, 3*FRACUNIT, S_EGGAMOEBA_START4}
states[S_EGGAMOEBA_START4] = {SPR_EGSL, C, 12, function (mo, var1, var2)
	mo.flags = $ | (MF_SPECIAL|MF_SHOOTABLE)
end, nil, nil, S_EGGAMOEBA_JUMP1}

states[S_EGGAMOEBA_JUMP1] = {SPR_EGSL, C, 3, nil, nil, nil, S_EGGAMOEBA_JUMP2}
states[S_EGGAMOEBA_JUMP2] = {SPR_EGSL, B, 3, nil, nil, nil, S_EGGAMOEBA_JUMP3}
states[S_EGGAMOEBA_JUMP3] = {SPR_EGSL, A, 5, nil, nil, nil, S_EGGAMOEBA_JUMP4}
states[S_EGGAMOEBA_JUMP4] = {SPR_EGSL, B, 2, nil, nil, nil, S_EGGAMOEBA_JUMP5}
states[S_EGGAMOEBA_JUMP5] = {SPR_EGSL, C, 12, A_Boss2SuperPogo, 4*FRACUNIT, nil, S_EGGAMOEBA_JUMP6}
states[S_EGGAMOEBA_JUMP6] = {SPR_EGSL, E, 2, function (mo, var1, var2)
	if (P_IsObjectOnGround(mo)) then
		mo.angle = R_PointToAngle2(mo.x, mo.y, mo.target.x, mo.target.y)
		mo.extravalue2 = $ + 1

		if (mo.extravalue2 >= mo.info.mass) then
			mo.state = var1 --go to special attack if we jumped "mass" times
		else
			mo.state = var2
		end
	else
		if (mo.momx == 0 and mo.momy == 0) then
			P_InstaThrust(mo,mo.angle, 4*FRACUNIT) --gets unstuck
		end
	end
end, S_EGGAMOEBA_SPOUT1, S_EGGAMOEBA_JUMP1, S_EGGAMOEBA_JUMP6}

states[S_EGGAMOEBA_SPOUT1] = {SPR_EGSL, C, TICRATE/8, A_Boss2SuperPogo, 4*FRACUNIT, 1, S_EGGAMOEBA_SPOUT2}
states[S_EGGAMOEBA_SPOUT2] = {SPR_EGSL, B, 2, A_CheckHomingLocation, S_EGGAMOEBA_SPOUT3, S_EGGAMOEBA_SPOUT1, S_EGGAMOEBA_SPOUT2}
states[S_EGGAMOEBA_SPOUT3] = {SPR_EGSL, A, 2, nil, nil, nil, S_EGGAMOEBA_SPOUT4}
states[S_EGGAMOEBA_SPOUT4] = {SPR_EGSL, B, 2, nil, nil, nil, S_EGGAMOEBA_SPOUT5}
states[S_EGGAMOEBA_SPOUT5] = {SPR_EGSL, C, 2, function(mo, var1, var2)
	local spreadcount = 8
	S_StartSound(mo, sfx_pogo)

	local isPinch = mo.health <= mo.info.damage

	for i = 1, spreadcount do
		local speed = FixedMul(30 * FRACUNIT, mo.scale)
		speed = $ + P_RandomRange(-4,4) * FRACUNIT
		if (isPinch) then speed = FixedMul($, 7*FRACUNIT/4)end
		local angle = ANGLE_45 * (i-1) + ANG1 * P_RandomRange(1,10)

		local zoffset = mo.height + FixedMul(24*FRACUNIT, mo.scale)
		local goop = P_SpawnMobj(mo.x, mo.y, mo.z + zoffset, mo.info.painchance)
		goop.momx = FixedMul(cos(angle),speed)
		goop.momy = FixedMul(sin(angle),speed)
		goop.momz = FixedMul(8*FRACUNIT, mo.scale)
		goop.scale = 2*FRACUNIT

		goop.fuse = 5*TICRATE
		if (isPinch) then goop.fuse = $ * 2 end
	end
end, nil, nil, S_EGGAMOEBA_SPOUT6}
states[S_EGGAMOEBA_SPOUT6] = {SPR_EGSL, A, 0, A_Repeat, 24, S_EGGAMOEBA_SPOUT3, S_EGGAMOEBA_JUMP1}

states[S_EGGAMOEBA_POGO] = {SPR_EGSL, B, TICRATE, function (mo, var1, var2)
	--ugly pogo hack
	mo.flags = $ & ~MF_NOGRAVITY
	mo.momz = 20*FRACUNIT
end, nil, nil, S_EGGAMOEBA_JUMP6}

states[S_EGGAMOEBA_PINCHPREP1] = {SPR_EGSL, C, TICRATE/8, A_Boss2SuperPogo, 8*FRACUNIT, 1, S_EGGAMOEBA_PINCHPREP2}
states[S_EGGAMOEBA_PINCHPREP2] = {SPR_EGSL, B, 2, A_CheckHomingLocation, S_EGGAMOEBA_PINCH1, S_EGGAMOEBA_PINCHPREP1, S_EGGAMOEBA_PINCHPREP2}

states[S_EGGAMOEBA_PINCH1] = {SPR_EGSL, B, 2*TICRATE, function (mo, var1, var2)
	mo.flags = $ | MF_NOGRAVITY
	mo.momz = 50*FRACUNIT
	S_StartSound(mo, sfx_s3k60)
end, nil, nil, S_EGGAMOEBA_PINCH2}

states[S_EGGAMOEBA_PINCH2] = {SPR_EGSL, B, TICRATE, function (mo, var1, var2)
	mo.flags = $ & ~MF_NOGRAVITY
	mo.momx = 0
	mo.momy = 0
	mo.momz = 0
	P_SetOrigin(mo, mo.centerwaypoint.x, mo.centerwaypoint.y, mo.centerwaypoint.z + 1000*FRACUNIT)
	P_LinedefExecute(-2, mo, nil)
end, nil, nil, S_EGGAMOEBA_PINCH3}

states[S_EGGAMOEBA_PINCH3] = {SPR_EGSL, B, 1, function (mo, var1, var2)
	if (P_IsObjectOnGround(mo)) then
		mo.angle = R_PointToAngle2(mo.x, mo.y, mo.target.x, mo.target.y)
		S_StartSound(mo, sfx_pstop)
		P_StartQuake(20*FRACUNIT, TICRATE)
		mo.state = S_EGGAMOEBA_PINCH4
	end
end, nil, nil, S_EGGAMOEBA_PINCH3}

states[S_EGGAMOEBA_PINCH4] = {SPR_EGSL, A, 3, nil, nil, nil, S_EGGAMOEBA_PINCH5}
states[S_EGGAMOEBA_PINCH5] = {SPR_EGSL, B, 3, nil, nil, nil, S_EGGAMOEBA_PINCH6}
states[S_EGGAMOEBA_PINCH6] = {SPR_EGSL, C, TICRATE/2, function(mo, var1, var2)
	P_LinedefExecute(-4, mo, nil)
	mo.extravalue1 = 0
	mo.extravalue2 = 0
	mo.centerwaypoint.z = mo.centerwaypoint.subsector.sector.floorheight
end, nil, nil, S_EGGAMOEBA_PINCH7}
states[S_EGGAMOEBA_PINCH7] = {SPR_EGSL, A, 1, nil, nil, nil, S_EGGAMOEBA_PINCH8}
states[S_EGGAMOEBA_PINCH8] = {SPR_EGSL, B, 1, nil, nil, nil, S_EGGAMOEBA_PINCH9}
states[S_EGGAMOEBA_PINCH9] = {SPR_EGSL, C, 1, function(mo, var1, var2)
	local spreadcount = 8
	S_StartSound(mo, sfx_pogo)
	for i = 1, spreadcount do
		local speed = FixedMul(12 * FRACUNIT, mo.scale)
		speed = $ + P_RandomRange(-4,4) * FRACUNIT
		local angle = ANGLE_45 * (i-1) + ANG1 * P_RandomRange(1,10)

		local zoffset = mo.height + FixedMul(24*FRACUNIT, mo.scale)
		local goop = P_SpawnMobj(mo.x, mo.y, mo.z + zoffset, mo.info.painchance)
		goop.momx = FixedMul(cos(angle),speed)
		goop.momy = FixedMul(sin(angle),speed)
		goop.momz = FixedMul(12*FRACUNIT, mo.scale)
		goop.scale = 2*FRACUNIT

		goop.fuse = 5*TICRATE
	end
end, nil, nil, S_EGGAMOEBA_PINCH10}
states[S_EGGAMOEBA_PINCH10] = {SPR_EGSL, A, 0, A_Repeat, 24, S_EGGAMOEBA_PINCH7, S_EGGAMOEBA_START1}

states[S_EGGAMOEBA_DIE1] = {SPR_EGSL, F, 0, A_Repeat, 1, 0, S_EGGAMOEBA_DIE2}
states[S_EGGAMOEBA_DIE2] = {SPR_EGSL, F, 5, A_BossScream, nil, nil, S_EGGAMOEBA_DIE3}
states[S_EGGAMOEBA_DIE3] = {SPR_EGSL, F, 0, A_Repeat, 17, S_EGGAMOEBA_DIE2, S_EGGAMOEBA_DIE4}
states[S_EGGAMOEBA_DIE4] = {SPR_EGSL, F, 1, function(mo, var1, var2)
	SpawnBossJunk(mo)
	--A_ForceWin(mo, var1, var2)
end, nil, nil, S_EGGAMOEBA_DIE5}
states[S_EGGAMOEBA_DIE5] = {SPR_EGSL, F, -1, A_BossDeath, nil, nil, S_EGGAMOEBA_DIE5}

--BossThinker resets the target every frame by default LMAOOOOO
local function EggAmoebaThinker(mo)
	--floor logic
	local goofloor = CheckSlime(mo)
	if goofloor then
		--if we step in goo water, face the target and get more aggressive
		if (mo.z > goofloor.bottomheight and mo.z < goofloor.topheight) then
			local spreadcount = 4
			for i = 1, spreadcount do
				local speed = FixedMul(3 * FRACUNIT, mo.scale)
				speed = $ + P_RandomRange(0,3) * FRACUNIT
				local angle = ANGLE_90 * (i-1)

				local zoffset = mo.height + FixedMul(24*FRACUNIT, mo.scale)
				local goop = P_SpawnMobj(mo.x, mo.y, mo.z + zoffset, mo.info.painchance)
				goop.momx = FixedMul(cos(angle),speed)
				goop.momy = FixedMul(sin(angle),speed)
				goop.momz = FixedMul(4*FRACUNIT, mo.scale)
				goop.scale = 2*FRACUNIT

				goop.fuse = 5*TICRATE
			end

			local speed = max(5*FRACUNIT, P_AproxDistance(mo.momx, mo.momy))
			if speed < 8*FRACUNIT then
				speed = $ * 3 / 2
			elseif speed > 20*FRACUNIT then
				speed = $ * 2 / 3
			end

			local aimangle = R_PointToAngle2(mo.x, mo.y, mo.target.x, mo.target.y)
			mo.angle = R_PointToAngle2(mo.x, mo.y, mo.target.x, mo.target.y)
			mo.momx = FixedMul(speed, cos(aimangle))
			mo.momy = FixedMul(speed, sin(aimangle))
			mo.momz = 12*FRACUNIT

			S_StartSound(mo, sfx_pogo)
		end
	end

	--invuln logic
	if (not mo.tracer or mo.tracer.invulntimer == 0) then
		mo.flags = $ | (MF_SPECIAL|MF_SHOOTABLE)
	end
	--invuln removal states
	if mo.state == S_EGGAMOEBA_JUMP4 then
	--or mo.state == S_EGGAMOEBA_SPOUT4 then
		mo.flags2 = $ & ~MF2_FRET
	end

	return true --return true because the default boss thinker is BAD.
end

local function EggAmoebaShouldDamage(mo, inflictor, source, dmg, dmgtype)
	if not (mo.tracer and mo.tracer.health > 0) then return end --no bubble, act normally
	--bubble's invincibility extends to the boss
	if (mo.tracer.invulntimer and mo.tracer.invulntimer > 0) then return false end
end

local function EggAmoebaDamage(mo, inflictor, source, dmg, dmgtype)
	--no bubble, take damage
	if not (mo.tracer and mo.tracer.health > 0) then
		S_StartSound(mo, mo.info.painsound) --why doesn't this play by default if I return false?
		--enter pinch
		if (mo.health > mo.info.damage
		and mo.health - dmg <= mo.info.damage
		and mo.health - dmg > 0) then
			mo.health = $ - dmg
			ForceBounceBack(mo, inflictor)
			mo.flags2 = $ | MF2_FRET
			mo.state = mo.info.raisestate
			return true
		else
			return false
		end
	end

	--bubble, let the bubble take the hit
	if (ShieldDamage(mo.tracer, inflictor, source, dmg, dmgtype)) then
		if (mo.momx ~= 0 or mo.momy ~= 0) then --not a fan of this immobility check
			mo.momx = $ * -1
			mo.momy = $ * -1
			mo.momz = 8*FRACUNIT
		end

		if (inflictor == source and source.player and source.player.valid) then
			ForceBounceBack(mo, inflictor)
		end
		--pretend you took damage
		mo.flags = $ & ~(MF_SPECIAL|MF_SHOOTABLE)
		if (mo.tracer.health == 0) then
			S_StartSound(mo, sfx_s1ac)
			mo.flags2 = $ | MF2_FRET
		end
	end
	return true
end

local function EggAmoebaDeath(mo, inflictor, source, dmgtype)
	mo.momx = 0
	mo.momy = 0
	mo.momz = 0
	mo.flags = $ | MF_NOGRAVITY
end

addHook("BossThinker", EggAmoebaThinker, MT_EGGAMOEBA)
addHook("ShouldDamage", EggAmoebaShouldDamage, MT_EGGAMOEBA)
addHook("MobjDamage", EggAmoebaDamage, MT_EGGAMOEBA)
addHook("BossDeath", EggAmoebaDeath, MT_EGGAMOEBA)

--SHIELD SLIME
mobjinfo[MT_SHIELDSLIME] = {
	doomednum = 1716,
	spawnstate = S_SHIELDSLIME,
	painstate = S_SHIELDSLIME,
	deathstate = S_SHIELDSLIME_POP,
	spawnhealth = 3,
	painchance = MT_GOOP,
	speed = 4*FRACUNIT,
	radius = 60*FRACUNIT,
	height = 120*FRACUNIT,
	flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT
}

states[S_SHIELDSLIME] = {SPR_SLIM, A, -1, nil, nil, nil, S_SHIELDSLIME}
states[S_SHIELDSLIME_POP] = {SPR_SLIM, B|FF_ANIMATE, 12, nil, 6, 2, S_NULL}

local function ShieldSpawn(mo)
	mo.startscale = mo.scale
	mo.targetscale = 2*FRACUNIT --NOT destscale, I will scale it manually
	mo.scaletimer = 0
	mo.invulntimer = 0
	S_StartSound(nil, sfx_s3k3f)
end

local function ShieldThinker(mo)
	--scale adjustment
	if (mo.scale ~= mo.targetscale) then
		local timer = mo.scaletimer * FRACUNIT / (TICRATE/3)
		mo.scale = ease.outcubic(timer, mo.startscale, mo.targetscale)
		mo.scaletimer = $ + 1
	else
		mo.startscale = mo.scale
		mo.targetscale = mo.scale
		mo.scaletimer = 0
	end

	--capechase
	if (mo.target and mo.target.valid) then
		P_SetOrigin(mo, mo.target.x, mo.target.y, mo.target.z + mo.target.height/8)
	end

	--invuln
	if (mo.invulntimer > 0) then
		mo.invulntimer = $ - 1
		if (mo.invulntimer & 1) then
			mo.flags2 = $ | MF2_DONTDRAW
		else
			mo.flags2 = $ & ~MF2_DONTDRAW
		end
	end

	--visuals
	local angle = ANG10*((leveltime / 2) % 36)
	local xstretch = 3*FRACUNIT/4 + sin(angle) / 4
	local ystretch = 3*FRACUNIT/4 + cos(angle + ANGLE_90) / 4
	mo.spritexscale = xstretch
	mo.spriteyscale = ystretch
	mo.blendmode = AST_ADD
end

addHook("MobjSpawn", ShieldSpawn, MT_SHIELDSLIME)
addHook("MobjThinker", ShieldThinker, MT_SHIELDSLIME)