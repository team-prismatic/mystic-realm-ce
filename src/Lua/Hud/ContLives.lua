--coded by Sylve, SimplerHud code by SteelT referenced to figure out how to draw the life icons and such in the right places, mainly how to use getSprite2Patch (thank you because I was really confused on how to do hud code lol)
-- To prevent duplicate freeslots
--$GZDB_SKIP
local function SafeFreeslot(...)
	for _, item in ipairs({...}) do
		if rawget(_G, item) == nil then
			freeslot(item)
		end
	end
end

freeslot(
"SKINCOLOR_COMPLETEBLACKDROPSHADOW"--name is very long and very specific to avoid conflicts
)

SafeFreeslot(
	"SPR2_MRCL",
	"SPR2_MRXL",
	"SKINCOLOR_IRIDESCENCE1",
	"SKINCOLOR_IRIDESCENCE2",
	"SKINCOLOR_IRIDESCENCE3",
	"SKINCOLOR_IRIDESCENCE4",
	"SKINCOLOR_IRIDESCENCE5",
	"SKINCOLOR_IRIDESCENCE6",
	"SKINCOLOR_IRIDESCENCEF" -- icy here. moved the SafeFreeslot down for neatness and added super sonic's hyper colors. this is done so that the colors "exist" but aren't defined, otherwise the game will throw a fit over the colors not existing.
)

skincolors[SKINCOLOR_COMPLETEBLACKDROPSHADOW] = {
	name = "CompleteBlack",
	ramp = {31,31,31,31,31,31,31,31,31,31,31,31,31,31,31,31},
	invcolor = SKINCOLOR_WHITE,
	invshade = 1,
	chatcolor = V_GRAYMAP,
	accessible = false
}

local anim_percent
local anim_capped
local anim_ticker = 0
local hidehud = false

--icy here, let's test shit
--simple, v is the drawer, num is the desired transparency, do a truth check with function before drawing with it!
local function adjustHUDTrans(v, num)
	local theShift = min(10,(v.localTransFlag()>>V_ALPHASHIFT)+num)
	if theShift >= 10 then
		return false
	end
	return (theShift<<V_ALPHASHIFT)
end

local fadingcolors = {SKINCOLOR_IRIDESCENCEF, SKINCOLOR_IRIDESCENCE1, SKINCOLOR_IRIDESCENCEF, SKINCOLOR_IRIDESCENCE2, SKINCOLOR_IRIDESCENCEF, SKINCOLOR_IRIDESCENCE3, SKINCOLOR_IRIDESCENCEF, SKINCOLOR_IRIDESCENCE4, SKINCOLOR_IRIDESCENCEF, SKINCOLOR_IRIDESCENCE5, SKINCOLOR_IRIDESCENCEF, SKINCOLOR_IRIDESCENCE6}
local fadedcolors = {SKINCOLOR_IRIDESCENCE1, SKINCOLOR_IRIDESCENCEF, SKINCOLOR_IRIDESCENCE2, SKINCOLOR_IRIDESCENCEF, SKINCOLOR_IRIDESCENCE3, SKINCOLOR_IRIDESCENCEF, SKINCOLOR_IRIDESCENCE4, SKINCOLOR_IRIDESCENCEF, SKINCOLOR_IRIDESCENCE5, SKINCOLOR_IRIDESCENCEF, SKINCOLOR_IRIDESCENCE6, SKINCOLOR_IRIDESCENCEF}

local MRLIVEBK, MRLIVEBKX, SUBMRLIVBG
local base_flagsbase = V_SNAPTOBOTTOM|V_SNAPTOLEFT|V_PERPLAYER
local FU300M = -300*FU

customhud.SetupItem("lives", "mrce", function(v, p, c, e)
	-- access to pointers
	local ease_outquart = ease.outquart

	local getSprite2Patch = v.getSprite2Patch
	local getColormap = v.getColormap
	local drawCropped = v.drawCropped
	local drawScaled = v.drawScaled
	local draw = v.draw

	local contsprite = getSprite2Patch(skins[p.skin].name, SPR2_XTRA, false, C)
	local soopsprite = getSprite2Patch(skins[p.skin].name, SPR2_XTRA, false, C)

	anim_capped = max(min(anim_ticker, TICRATE), 0)
	anim_percent = FU / TICRATE * anim_capped


	local base_flags = base_flagsbase|(v.userTransFlag())
	local translation_nonz = V_HUDTRANS -- mapheaderinfo[gamemap].mysticrealms ~= 'true' and V_HUDTRANS or 0

	if p.exiting <= 50
	and p.exiting > 0 then
		hidehud = true
	end

	if hidehud then
		if anim_ticker > 44 then
			anim_ticker = 44
		end

		if anim_ticker > 0 then
			anim_ticker = $ - 1
		end
		hidehud = false
	else
		anim_ticker = $ + 1
	end

	if not MRLIVEBK then
		MRLIVEBK = v.cachePatch("LMRCEHUDTT")
		MRLIVEBKX = v.cachePatch("LMRCEHUDTTX")
		SUBMRLIVBG = v.cachePatch("SUBMRCEBGL")
	end

	if (p.realmo and p.realmo.valid) --and G_PlatformGametype()
	and not (p.hypermysticsonic) and not ((p.realmo.skin == "samus") or (p.realmo.skin == "basesamus"))
	and not (p.realmo.skin == "duke") and not (srb2p) and not (maptol & TOL_NIGHTS)
	and not (G_IsSpecialStage(gamemap)) and not modeattacking and gamemap ~= 99
	and not ultimatemode and CHUD == nil
	and p.encorelives == nil and not (p.realmo.skin == "speccy" and p.speccy)
	and not (teamkinetic and loadedbots)
	and not (p.xsonictable and IsXSonic and IsXSonic(p.realmo, p) and p.xsonictable.HUDpref) then

		if (mapheaderinfo[gamemap].mrce_emeraldstage and mrce.emstage_attemptavailable) or p.lives == INFLIVES then
			return
		end

		if P_IsValidSprite2(p.realmo, SPR2_MRCL) then
			if p.realmo.skin == "supersonic" then
				if (p.powers[pw_shield] & SH_FIREFLOWER) then
					contsprite = getSprite2Patch(p.realmo.skin, SPR2_MRCL, false, B)
				else
					contsprite = getSprite2Patch(p.realmo.skin, SPR2_MRCL, false, A)
				end
			else
				contsprite = getSprite2Patch(p.realmo.skin, SPR2_MRCL, false, A)
			end
		else
			contsprite = getSprite2Patch(p.realmo.skin, SPR2_XTRA, false, C) or getSprite2Patch(p.realmo.skin, SPR2_XTRA, false, A) or getSprite2Patch(p.realmo.skin, SPR2_STND, false, A, 2)
		end
		if P_IsValidSprite2(p.realmo, SPR2_MRXL) then
			soopsprite = getSprite2Patch(p.realmo.skin, SPR2_MRXL, false, A)
		 elseif P_IsValidSprite2(p.realmo, SPR2_MRCL) then
			if p.realmo.skin == "supersonic" then
				if (p.powers[pw_shield] & SH_FIREFLOWER) then
					soopsprite = getSprite2Patch(p.realmo.skin, SPR2_MRCL, false, B)
				else
					soopsprite = getSprite2Patch(p.realmo.skin, SPR2_MRCL, false, A)
				end
			end
		else
			soopsprite = getSprite2Patch(p.realmo.skin, SPR2_XTRA, false, C) or getSprite2Patch(p.realmo.skin, SPR2_XTRA, true, A) or getSprite2Patch(p.realmo.skin, SPR2_STND, true, A, 2)
		end

		draw(ease_outquart(anim_percent, -300, 11), 176, SUBMRLIVBG, base_flags|translation_nonz|V_REVERSESUBTRACT)
		draw(ease_outquart(anim_percent, -300, 16), 176,  MRLIVEBK, base_flags|translation_nonz)


		if not (p.spectator) then
			if not p.powers[pw_super] then
				drawCropped(ease_outquart(anim_percent, FU300M, (20+12)*FRACUNIT), (176+7)*FRACUNIT, FRACUNIT, FRACUNIT, contsprite, base_flags|((p.spectator) and V_HUDTRANSHALF or translation_nonz),
				getColormap(TC_BLINK, SKINCOLOR_COMPLETEBLACKDROPSHADOW), 0, 0, contsprite.width*FRACUNIT, max(contsprite.height*FRACUNIT - 2*FRACUNIT, FRACUNIT)) --dropshadow goes first
				drawScaled(ease_outquart(anim_percent, FU300M, (20+13)*FRACUNIT), (176+6)*FRACUNIT, FRACUNIT, contsprite, base_flags|translation_nonz, getColormap(p.realmo.skin, p.realmo.color, p.realmo.translation)) --then the actual player sprite
			else
				drawCropped(ease_outquart(anim_percent, FU300M, (20+12)*FRACUNIT), (176+7)*FRACUNIT, FRACUNIT, FRACUNIT, soopsprite, base_flags|((p.spectator) and V_HUDTRANSHALF or translation_nonz),
				getColormap(TC_BLINK, SKINCOLOR_COMPLETEBLACKDROPSHADOW), 0, 0, contsprite.width*FRACUNIT, max(contsprite.height*FRACUNIT - 2*FRACUNIT, FRACUNIT)) --dropshadow goes first
				drawScaled(ease_outquart(anim_percent, FU300M, (20+13)*FRACUNIT), (176+6)*FRACUNIT, FRACUNIT, soopsprite, base_flags|translation_nonz, getColormap(p.realmo.skin, p.realmo.color, p.realmo.translation)) --then the actual player sprite
			end
		else
			drawScaled(ease_outquart(anim_percent, FU300M, (20+16)*FRACUNIT), (176)*FRACUNIT, FRACUNIT, contsprite, base_flags|((p.spectator) and V_HUDTRANSHALF), getColormap(TC_RAINBOW, p.realmo.color, p.realmo.translation)) --there is no dropshadow when you're spectating; you're a ghost
		end

		--code to make it so super sonic's hyper form still has the smooth fade on the hud.
		if p.realmo.skin == "supersonic" and p.hyper
		and p.mo and p.mo.valid
		and p.hyper.transformed and p.mo.hypercolortimer then
		--the color handling shit
			local colorTimer = ((p.mo.hypercolortimer % 132) / 11) + 1

			local hudTrans = adjustHUDTrans(v, 10 - (p.mo.hypercolortimer % 11))

			if (p.realmo) and G_PlatformGametype() and p.mrce and (p.mrce.hud == 1) --lol
			and not (srb2p) and not (maptol & TOL_NIGHTS) and not (G_IsSpecialStage(gamemap))
			and not modeattacking and gamemap ~= 99 and not ultimatemode and p.encorelives == nil then --only draw when MRCE would draw it!!
			--also thanks for the formatting, mrce devs

				local contsprite = getSprite2Patch(p.realmo.skin, SPR2_MRCL, false, A) --at this point we know super has the goods
				draw(20+16, 173+(p.spectator and 3 or 8), contsprite, base_flags|translation_nonz, getColormap(TC_DEFAULT, fadingcolors[colorTimer])) --then the actual player sprite
				if hudTrans or hudTrans == 0 then
					draw(20+16, 173+(p.spectator and 3 or 8), contsprite, base_flags|translation_nonz|hudTrans, getColormap(TC_DEFAULT, fadedcolors[colorTimer]))
				end
			end
		end

		if (G_GametypeUsesLives()) then
			if (p.lives ~= INFLIVES) and not (multiplayer and CV_FindVar("cooplives").value == 0) then --there is nothing of the sort in gamemodes without lives

				local num = 0
				if (p.fakedisplives) ~= nil and p.fakedisplives > 94 then
					num = p.fakedisplives
				else
					num = p.lives
				end
				draw(ease_outquart(anim_percent, -300, 16), 176,  MRLIVEBKX, base_flags|translation_nonz)
				TBSlib.drawTextInt(v, "MRCEHU2FNT", ease_outquart(anim_percent, -300, 50), 177, tostring(num), base_flags|translation_nonz, nil, "left", -1, 2, '0')
			end
		end
	end
end, "game")

customhud.SetupItem("matchemeralds", "mrce", function(v, p, c, e)
	local draw = v.draw
	if (gametyperules & GTR_POWERSTONES) then
		for i = 1, 7 do
			if ((p.powers[pw_emeralds]) & _G["EMERALD" .. i]) then
				if mapheaderinfo[gamemap].mysticrealms then
					draw((10 * i) + 39, 180, v.cachePatch("TEMER" .. tostring(i)), V_SNAPTOBOTTOM|V_SNAPTOLEFT|V_PERPLAYER)
				else
					draw((10 * i) + 39, 180, v.cachePatch("VTEMER" .. tostring(i)), V_SNAPTOBOTTOM|V_SNAPTOLEFT|V_PERPLAYER)
				end
			end
		end
	end
end, "game")

addHook("MapLoad", function()
  anim_ticker = 0
end)

hud.add(function(v,p,ticker,endticker)
	if p.exiting then return end
	if ticker <= 75 then
		hidehud = true
	end
end, "titlecard")
