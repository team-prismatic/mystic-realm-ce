--Credits to MotdSpork for this script is based off of his Modern Sonic HUD
--Last edited by Marcos - 20/10/2021 19:35 (UTC+2)
--$GZDB_SKIP

local cv = CV_RegisterVar({name = "powerupdisplay_size", defaultvalue = "small", flags = 0, PossibleValue = {small=0, medium=1, big=2}, func = 0}) --my first console variable
local cv1 = CV_FindVar("powerupdisplay")
local cv2 = CV_FindVar("chasecam")
local fpok

local flags = V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_PERPLAYER|V_HUDTRANS
local ITEM_IN = false
local XOFFSET_EASEMAX = 450
local XOFFSET_EASEMIN = -50
local XOFFSET_MAX = 400
local XOFFSET = 0
local TICRATE3 = 3*TICRATE

local p_box
local p_triangle
local p_boxsubst
local p_emerald
local p_fire
local p_sneakers
local p_invinc
local p_invins
local p_grav
local p_pity
local p_whirl
local p_arm
local p_pink
local p_element
local p_attract
local p_force1
local p_force2
local p_flame
local p_bubble
local p_thunder
local p_starman
local p_starmans
local p_spity
local p_swhirl
local p_sarm
local p_spink
local p_selement
local p_sattract
local p_sthunder
local p_sbubble
local p_sflame
local p_sforce1
local p_sforce2

local function PaddedMothDrawer(v, x, y, scale, str, padding, font, flags, alligment, colormap, translation)
	local base_str = tostring(str)
	local pads = padding - string.len(base_str)

	if pads > 0 then
		base_str = (string.rep('0', pads))..base_str
	end

	DrawMotdString(v, x, y, scale, base_str, font, flags, alligment, colormap, translation)
end


--move away the vanilla ones, since they can't be completely erased  --note vanilla value is 176

local function MRCEItemHUD(v, player)
	if cv1.value == 0 then
		return
	end
	if player.spectator then return end
	if not player.realmo then return end
	if player.shouldhud  == false then return end

	if player.mrce and player.mrce.hud == 0 then
		hudinfo[HUD_POWERUPS].y = 176
		return
	end

	ITEM_IN = true

	local ease_outsine = ease.outsine
	local ease_insine = ease.insine

	hudinfo[HUD_POWERUPS].y = 250
	if cv1.value == 2
	or cv1.value == 1 and cv2.value == 0 then
        if not (player.powers[pw_carry] == CR_NIGHTSMODE) then --there's no NiGHTS in MRCE anyways --there is now --there isn't --lock on moment b like. also netgames exist dummy
			-- allocate once
			if not p_box then
				p_box 		 		= v.cachePatch("BOX")
				p_boxsubst			= v.cachePatch("BOXSUBST")
				p_triangle 	 		= v.cachePatch("TRIANGLE")
				p_emerald	 	 	= v.cachePatch("EMERLD")
				p_fire	 	 		= v.cachePatch("FIRE")
				p_sneakers	 		= v.cachePatch("SNEAK")
				p_invinc		 	= v.cachePatch("INVINC")
				p_invins		 	= v.cachePatch("INVINS")
				p_grav				= v.cachePatch("GRAV")
				p_pity	 	 		= v.cachePatch("PITY")
				p_whirl		 		= v.cachePatch("WHIRL")
				p_arm			 	= v.cachePatch("ARM")
				p_pink		 		= v.cachePatch("HEART")
				p_element	 	 	= v.cachePatch("ELEMENT")
				p_attract	 	 	= v.cachePatch("MAGNET")
				p_force1		 	= v.cachePatch("FORCE1")
				p_force2		 	= v.cachePatch("FORCE2")
				p_flame		 		= v.cachePatch("FLAME")
				p_bubble		 	= v.cachePatch("BUBBLE")
				p_thunder	 	 	= v.cachePatch("THUNDER")
				p_starman	 	 	= v.cachePatch("STARMAN")
				p_starmans	 		= v.cachePatch("STARMANS")
				p_spity		 		= v.cachePatch("SPITY") --small versions start here
				p_swhirl		 	= v.cachePatch("SWHIRL")
				p_sarm		 		= v.cachePatch("SARM")
				p_spink		 		= v.cachePatch("SPINK")
				p_selement	 		= v.cachePatch("SELEMENT")
				p_sattract	 		= v.cachePatch("SMAGNET")
				p_sthunder	 		= v.cachePatch("STHUNDER")
				p_sbubble		 	= v.cachePatch("SBUBBLE")
				p_sflame		 	= v.cachePatch("SFLAME")
				p_sforce1		 	= v.cachePatch("SFORCE1")
				p_sforce2		 	= v.cachePatch("SFORCE2")
			end

			local posx --stands for position x
			local posy --stands for position y
			local numposx --position for v.drawNum is handled differently
			local numposy
			local gravposx --don't ask
			local gravposy

			local whatcolor = player.mo.color --for cool rainbow and super colors

			local size

			if cv.value == 0 then
				size = FRACUNIT/2
				posx = 307*FRACUNIT --Jump wanted it a liiiitle bit more to the right so instead of a fixed good looking 305 have a fucking 307
				posy = 185*FRACUNIT
				numposx = 280
				numposy = 180
				gravposx = 310
				gravposy = 160
			elseif cv.value == 1 then
				size = 3*FRACUNIT/4
				posx = 302*FRACUNIT
				posy = 182*FRACUNIT
				numposx = 280
				numposy = 180
				gravposx = 310
				gravposy = 160
			elseif cv.value == 2 then
				size = FRACUNIT
				posx = 297*FRACUNIT
				posy = 179*FRACUNIT
				numposx = 280
				numposy = 180
				gravposx = 310
				gravposy = 160
			else
				size = FRACUNIT/2
				posx = 307*FRACUNIT
				posy = 185*FRACUNIT
				numposx = 280
				numposy = 180
				gravposx = 310
				gravposy = 160
			end

			posx = $+(XOFFSET << FRACBITS)
			numposx = $+XOFFSET

			-- just straight up get to C function pointer...
			local drawScaled = v.drawScaled
			local drawPaddedNum = v.drawPaddedNum

			local getColormap = v.getColormap

			-- same thing with userdata.
			local pw_shields = player.powers[pw_shield]
			local no_stack = pw_shields & SH_NOSTACK

			--Always draw the box, the box is what we need
			drawScaled(posx, posy, size, p_boxsubst, flags|V_REVERSESUBTRACT);
			drawScaled(posx, posy, size, p_box, flags); --snapto flags don't even fucking work so globox moment --(20/20/2021) they did
								   --^scale for everything (0,5 of the original scale because "too big")

			--Super now is overriden by shields as you can't go Super if you have a shield
			if (player.powers[pw_super] and not (player.mo.state >= S_PLAY_SUPER_TRANS1 and player.mo.state <= S_PLAY_SUPER_TRANS6)) then
				drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", whatcolor));
				drawScaled(posx, posy, size, p_emerald, flags);
			elseif All7Emeralds(emeralds)
			and player.rings >= 50
			and no_stack == SH_NONE then
				drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_SUPERSILVER1));
				drawScaled(posx, posy, size, p_emerald, flags);
			--If both Invincibility and Super Sneakers
			elseif player.powers[pw_invulnerability] and player.powers[pw_sneakers] then
				PaddedMothDrawer(v, numposx * FRACUNIT, (numposy - 10) * FRACUNIT, FRACUNIT, (player.powers[pw_sneakers] / TICRATE), 2, 'MRCEFNT', flags, -1, nil, "FontRedOutline")
				PaddedMothDrawer(v, numposx * FRACUNIT, numposy * FRACUNIT, FRACUNIT, (player.powers[pw_invulnerability] / TICRATE), 2, 'MRCEFNT', flags, -1)
				--Triangle shit
				if mariomode == true then --Xian said it was being axed but fuck it special effects
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", whatcolor)); --RRRRRRRAAAAAAAAAAIIIIIIIIIIINNNNNNNNNBBBBBBBBBBBBOOOOOOOOOOOOOOWWWWWWWWWWWSSSSSSSSSSS
				else
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_PURPLE));
				end
				--Super Sneakers
				if player.powers[pw_sneakers] < TICRATE3 then
					if (leveltime % 2) then --flicker effect because without it "it does less of a service than vanilla"
					else	--^ courtesy of kays they explained this to me
						drawScaled(posx, posy, size, p_sneakers, flags);
					end
				else
					drawScaled(posx, posy, size, p_sneakers, flags);
				end
				--Invincibility
				if player.powers[pw_invulnerability] < TICRATE3 then
					if (leveltime % 2) then
					else
						if mariomode == true then
							drawScaled(posx, posy, size, p_starmans, flags, getColormap("-1", whatcolor));
						else
							drawScaled(posx, posy, size, p_invins, flags);
						end
					end
				else
					if mariomode == true then
						drawScaled(posx, posy, size, p_starmans, flags, getColormap("-1", whatcolor));
					else
						drawScaled(posx, posy, size, p_invins, flags);
					end
				end
				--Shields
				if no_stack == SH_PITY then
					drawScaled(posx, posy, size, p_spity, flags);
				elseif no_stack == SH_WHIRLWIND then
					drawScaled(posx, posy, size, p_swhirl, flags);
				elseif no_stack == SH_ARMAGEDDON then
					drawScaled(posx, posy, size, p_sarm, flags);
				elseif no_stack == SH_PINK then
					drawScaled(posx, posy, size, p_spink, flags);
				elseif no_stack == SH_ELEMENTAL then
					drawScaled(posx, posy, size, p_selement, flags);
				elseif no_stack == SH_ATTRACT then
					drawScaled(posx, posy, size, p_sattract, flags);
				elseif no_stack == SH_TUNDERCOIN or no_stack == SH_WHIRLWIND|SH_PROTECTELECTRIC then
					drawScaled(posx, posy, size, p_sthunder, flags);
				elseif no_stack == SH_BUBBLEWRAP then
					drawScaled(posx, posy, size, p_sbubble, flags);
				elseif no_stack == SH_FLAMEAURA then
					drawScaled(posx, posy, size, p_sflame, flags);
				elseif pw_shields & SH_FORCE then
					if pw_shields & SH_FORCEHP then
						drawScaled(posx, posy, size, p_sforce1, flags);
					else
						drawScaled(posx, posy, size, p_sforce2, flags);
					end
				end
			--Super Sneakers
			elseif player.powers[pw_sneakers] then
				if player.powers[pw_sneakers] < TICRATE3 then
					if (leveltime % 2) then
					else
						drawScaled(posx, posy, size, p_sneakers, flags);
					end
				else
					drawScaled(posx, posy, size, p_sneakers, flags);
				end
				--1st person thing for PV
				if no_stack == SH_PITY then
					drawScaled(posx, posy, size, p_spity, flags);
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_MOSS));
				elseif no_stack == SH_WHIRLWIND then
					drawScaled(posx, posy, size, p_swhirl, flags);
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_WHITE));
				elseif no_stack == SH_ARMAGEDDON then
					drawScaled(posx, posy, size, p_sarm, flags);
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_CRIMSON));
				elseif no_stack == SH_PINK then
					drawScaled(posx, posy, size, p_spink, flags);
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_ROSY));
				elseif no_stack == SH_ELEMENTAL then
					drawScaled(posx, posy, size, p_selement, flags);
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_APRICOT));
				elseif no_stack == SH_ATTRACT then
					drawScaled(posx, posy, size, p_sattract, flags);
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_SUPERGOLD3));
				elseif no_stack == SH_TUNDERCOIN or no_stack == SH_WHIRLWIND|SH_PROTECTELECTRIC then
					drawScaled(posx, posy, size, p_sthunder, flags);
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_BONE));
				elseif no_stack == SH_BUBBLEWRAP then
					drawScaled(posx, posy, size, p_sbubble, flags);
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_SKY));
				elseif no_stack == SH_FLAMEAURA then
					drawScaled(posx, posy, size, p_sflame, flags);
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_RUST));
				elseif pw_shields & SH_FORCE then
					if pw_shields & SH_FORCEHP then --Zolton said to check it inside sh_force which I guess is more optimized
						drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_NEON));
						drawScaled(posx, posy, size, p_sforce1, flags);
					else
						drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_MAGENTA));
						drawScaled(posx, posy, size, p_sforce2, flags);
					end
				else
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_RED));
				end
			--Invincibility
			elseif player.powers[pw_invulnerability] then
				PaddedMothDrawer(v, numposx * FRACUNIT, numposy * FRACUNIT, FRACUNIT, (player.powers[pw_invulnerability] / TICRATE), 2, 'MRCEFNT', flags, -1)
				if mariomode == true then
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", whatcolor));
				else
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_SUPERSILVER1));
				end
				if player.powers[pw_invulnerability] < TICRATE3 then
					if (leveltime % 2) then
					else
						if mariomode == true then
							drawScaled(posx, posy, size, p_starman, flags);
						else
							drawScaled(posx, posy, size, p_invinc, flags);
						end
					end
				else
					if mariomode == true then
						drawScaled(posx, posy, size, p_starman, flags);
					else
						drawScaled(posx, posy, size, p_invinc, flags);
					end
				end
				if no_stack == SH_PITY then
					drawScaled(posx, posy, size, p_spity, flags);
				elseif no_stack == SH_WHIRLWIND then
					drawScaled(posx, posy, size, p_swhirl, flags);
				elseif no_stack == SH_ARMAGEDDON then
					drawScaled(posx, posy, size, p_sarm, flags);
				elseif no_stack == SH_PINK then
					drawScaled(posx, posy, size, p_spink, flags);
				elseif no_stack == SH_ELEMENTAL then
					drawScaled(posx, posy, size, p_selement, flags);
				elseif no_stack == SH_ATTRACT then
					drawScaled(posx, posy, size, p_sattract, flags);
				elseif no_stack == SH_TUNDERCOIN or no_stack == SH_WHIRLWIND|SH_PROTECTELECTRIC then
					drawScaled(posx, posy, size, p_sthunder, flags);
				elseif no_stack == SH_BUBBLEWRAP then
					drawScaled(posx, posy, size, p_sbubble, flags);
				elseif no_stack == SH_FLAMEAURA then
					drawScaled(posx, posy, size, p_sflame, flags);
				elseif pw_shields & SH_FORCE then
					if pw_shields & SH_FORCEHP then
						drawScaled(posx, posy, size, p_sforce1, flags);
					else
						drawScaled(posx, posy, size, p_sforce2, flags);
					end
				end
			--Pity Shield
			elseif no_stack == SH_PITY then
				drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_MOSS));
				drawScaled(posx, posy, size, p_pity, flags);
			--Whirlwind Shield
			elseif no_stack == SH_WHIRLWIND then
				drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_WHITE));
				drawScaled(posx, posy, size, p_whirl, flags);
			--Armageddon Shield
			elseif no_stack == SH_ARMAGEDDON then
				drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_CRIMSON));
				drawScaled(posx, posy, size, p_arm, flags);
			--Amy Pity Shield
			elseif no_stack == SH_PINK then
				drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_ROSY));
				drawScaled(posx, posy, size, p_pink, flags);
			--Elemental Shield
			elseif no_stack == SH_ELEMENTAL then
				drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_APRICOT));
				drawScaled(posx, posy, size, p_element, flags);
			--Attraction Shield
			elseif no_stack == SH_ATTRACT then
				drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_SUPERGOLD3));
				drawScaled(posx, posy, size, p_attract, flags);
			--Electric Shield
			elseif no_stack == SH_TUNDERCOIN or no_stack == SH_WHIRLWIND|SH_PROTECTELECTRIC then --thundercoin alone doesn't work because lua
				drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_BONE));
				drawScaled(posx, posy, size, p_thunder, flags);
			--Bubble Shield
			elseif no_stack == SH_BUBBLEWRAP then
				drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_SKY));
				drawScaled(posx, posy, size, p_bubble, flags);
			--Flame Shield
			elseif no_stack == SH_FLAMEAURA then
				drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_RUST));
				drawScaled(posx, posy, size, p_flame, flags);
			--Force Shield
			elseif pw_shields & SH_FORCE then
				if pw_shields & SH_FORCEHP then --Zolton said to check it inside sh_force which I guess is more optimized
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_NEON));
					drawScaled(posx, posy, size, p_force1, flags);
				else
					drawScaled(posx, posy, size, p_triangle, flags, getColormap("-1", SKINCOLOR_MAGENTA));
					drawScaled(posx, posy, size, p_force2, flags);
				end
			else
				ITEM_IN = false
			end

			--Fire Flower overlay (As it can be used with anything)
			if pw_shields & SH_FIREFLOWER then
				drawScaled(posx, posy, size, p_fire, flags);
				ITEM_IN = true
			end

			--Gravity won't override everythinganymore
			if player.powers[pw_gravityboots] then
				PaddedMothDrawer(v, gravposx * FRACUNIT, gravposy * FRACUNIT, FRACUNIT, (player.powers[pw_gravityboots] / TICRATE), 2, 'MRCEFNT', flags, -1)
				PaddedMothDrawer(v, posx * FRACUNIT, posy * FRACUNIT, FRACUNIT, (player.powers[pw_gravityboots] / TICRATE), 2, 'MRCEFNT', flags, -1)
				drawScaled(posx, posy, size, p_grav, flags);
				ITEM_IN = true
			end

			--draw speed shoes counter separately so it still renders when super
			if player.powers[pw_sneakers] then
				PaddedMothDrawer(v, numposx * FRACUNIT, (numposy - 10) * FRACUNIT, FRACUNIT, (player.powers[pw_sneakers] / TICRATE), 2, 'MRCEFNT', flags, -1, nil, "FontRedOutline")
				ITEM_IN = true
			end
		end


		if ITEM_IN then
			XOFFSET = max(ease_outsine(FRACUNIT/12, XOFFSET, XOFFSET_EASEMIN), 0)
		else
			XOFFSET = min(ease_insine(FRACUNIT/12, XOFFSET, XOFFSET_EASEMAX), XOFFSET_MAX)
		end
	end
end

customhud.SetupItem("itemdisplay", "mrce", MRCEItemHUD, "game")