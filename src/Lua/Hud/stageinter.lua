--[[

	New stage card and rewritten intermission_net.lua

	Why it needs to share it's own lua file? Simple!... less globals and no need to create another caching process.

	I mean seriously, why everything needed globals? One of the slowest form of storing information.

	-- Skydusk

	TODO: Special Stages (Doesn't need to be vanilla, but preferably.)

--]]

local TRANSITIONALPLANES = {}

local BASEPLANES = {}

local setup = true

local cache_thread = coroutine.create(function(v)
	local str_base = "MRCETTBASE"
	local str_trns = "MRCETTANITR"

	TBSlib.registerFont(v, 'MRCEFUTITFNT', 'n')
	TBSlib.registerNumbers(v, 'MRCETTACT')

	for i = 1, 63 do
		BASEPLANES[i] = v.cachePatch(str_base..i)
		TRANSITIONALPLANES[i] = v.cachePatch(str_trns..i)
		if not (i % 3) then
			coroutine.yield()
		end
	end

	for i = 64, 74 do
		TRANSITIONALPLANES[i] = v.cachePatch(str_trns..i)
		if not (i % 3) then
			coroutine.yield()
		end
	end

	setup = nil
end)

--#region Stagecard


local Delay = TICRATE/2+TICRATE/5
local Start = 1 + Delay
local End = 62 + Delay

--local Easing_In = 6 + Delay
local Easing_Out = 52 + Delay

local function HUD_DRAWER(v, p, t, e)
	if t > End or t < Start then return end

	if setup then
		coroutine.resume(cache_thread, v)
	end

	local timer_in = min(t - Start, 5)*FRACUNIT/5
	local timer_out = max(min(t - Easing_Out, 10), 0)*FRACUNIT/10

	local offset = ease.outsine(timer_in - timer_out, 250, 0)

	local fontcolor = SKINCOLOR_AZURE
	local color = SKINCOLOR_SAPPHIRE
	local header = mapheaderinfo[gamemap]

	local real_t = t - Delay
	local bg_t = t - 10

	if header.mrce_emeraldstage then
		return
	end

	if header.bonustype > 0 then
		color = SKINCOLOR_RED
		fontcolor = SKINCOLOR_SALMON
	elseif header.mysticrealms then
		color = SKINCOLOR_SHAMROCK
		fontcolor = SKINCOLOR_MOSS
	end

	local colormap = v.getColormap(TC_DEFAULT, color)
	local fontcolormap = v.getColormap(TC_DEFAULT, fontcolor)

	if bg_t > 0 and bg_t < 75 then
		local intwidth = v.width()
		local intheight = v.height()
		local scale = v.dupx()
		intwidth = (intwidth * FRACUNIT / 320) / scale
		intheight = (intheight * FRACUNIT / 200) / scale

		v.drawStretched(0, 0, intwidth, intheight, TRANSITIONALPLANES[bg_t] or TRANSITIONALPLANES[1], V_SNAPTOLEFT|V_SNAPTOTOP|V_50TRANS, colormap)
	elseif bg_t < 1 then
		local intwidth = v.width()
		local intheight = v.height()
		local scale = v.dupx()
		intwidth = (intwidth * FRACUNIT / 320) / scale
		intheight = (intheight * FRACUNIT / 200) / scale

		v.drawStretched(0, 0, intwidth, intheight, TRANSITIONALPLANES[1], V_SNAPTOLEFT|V_SNAPTOTOP|V_50TRANS, colormap)
	end

	v.drawScaled(0, 0, FRACUNIT/2, BASEPLANES[real_t] or BASEPLANES[1], V_SNAPTORIGHT|V_SNAPTOBOTTOM, colormap)

	local y_offset = 0
	if not (header.levelflags & LF_NOZONE) then
		v.drawScaled((231 + offset) * FRACUNIT, 130*FRACUNIT, FRACUNIT/2, v.cachePatch("MRCETTZONE"), V_SNAPTORIGHT|V_SNAPTOBOTTOM, fontcolormap)
		y_offset = 12
	end

	local text = string.upper(header.lvlttl)
	local width = 0
	local height = v.levelTitleHeight(text)

	if text then
		if text == "DIMENSION WARP" then
			text = TBSlib.randomizeString(text) or "HAHA"
		end

		for i = 1, #text do
			width = $ + TBSlib.getTextLenght(v, nil, text, "MRCEFUTITFNT", val, -1, i)
		end
		width = 2*width/3

		TBSlib.drawStaticTextUnadjusted(v, "MRCEFUTITFNT", (231 - width + offset)*FRACUNIT, (128 - height)*FRACUNIT, 2*FRACUNIT/3, text, V_SNAPTORIGHT|V_SNAPTOBOTTOM, fontcolormap, nil, -1)
	end
	--v.drawLevelTitle(231 - width + offset, 136 - height, text, V_SNAPTORIGHT|V_SNAPTOBOTTOM)

	if tostring(header.actnum) ~= "0" then

		TBSlib.drawTextInt(v, "MRCETTACT", 272 + offset, 130, header.actnum, V_SNAPTORIGHT|V_SNAPTOBOTTOM, nil, "center", -4)
	end

	v.drawString(228 + offset, 144 + y_offset, header.subttl, V_SNAPTORIGHT|V_SNAPTOBOTTOM, "right")

	if t < 12 then
		v.fadeScreen(0xFF00, 32-t*2)
	end
end

customhud.SetupItem("stagetitle", "mrce", HUD_DRAWER, "titlecard")

--#endregion
--#region Intermission

--[[

	BUG: There is currently one bug I cannot do anything about,
	That's off focus bug, you can essentially desync sounds and intermission fake calculation

]]

---@class inter
---@field tpoints 			number -- time
---@field rpoints 			number -- rings
---@field gpoints 			number -- guard
---@field nummaprings 		number
---@field perfectbonus 		number
---@field lastplayername 	string
---@field recscore 			number
---@field rectime 			number
---@field newadd 			number
---@field intcomplete 		boolean
---@field animticker 		number
---@field display 			boolean
---@field btick 			number
---@field timebonus_t		number
---@field player			player_t?
---@field cc_title 			function?
---@field cc_calc 			function?
---@field cc_hud 			function?
---@field calcTimeBonus		function?
---@field calcRingBonus		function?
---@field calcGuardBonus	function?
---@field calcPerfBonus		function?
---@field bonus_types		table<function>?

local inter = { ---@type inter
	-- points

	tpoints = 0, -- time
	rpoints = 0, -- rings
	gpoints = 0, -- guard

	-- ring counter for perfect bonus

	nummaprings = 0,
	perfectbonus = 0,

	-- trackers

	lastplayername = "",
	player = nil,

	recscore = 0,
	rectime = 0,

	newadd = 0,
	intcomplete = false,
	timebonus_t = 0,

	-- timers

	animticker = 0,
	display = false,
	btick = 0,

	-- custom
	cc_title = nil,
	cc_calc = nil,
	cc_hud = nil,
}

-- Function used to calculate the total number of rings in a map
---@diagnostic disable-next-line: undefined-doc-param
---@param mobj mobj_t
-- Put inside of a MobjSpawn so that any rings added mid game are accounted for
addHook("MobjSpawn", function() inter.nummaprings = $ + 1 end, MT_RING)
addHook("MobjSpawn", function() inter.nummaprings = $ + 1 end, MT_COIN)
addHook("MobjSpawn", function() inter.nummaprings = $ + 1 end, MT_NIGHTSSTAR)
addHook("MobjSpawn", function() inter.nummaprings = $ + 10 end, MT_NIGHTSSTAR)
addHook("MobjSpawn", function() inter.perfectbonus = -1 end, MT_NIGHTSDRONE)
addHook("MapChange", function()
	inter.nummaprings = 0
	inter.perfectbonus = 0
end)
addHook("MapLoad", function()
	inter.cc_title = nil
	inter.cc_calc = nil
	inter.cc_hud = nil
end)
addHook("NetVars", function(net)
	inter.nummaprings = net(inter.nummaprings)

	local type = net(inter.perfectbonus)
	if type == -1 or type == 0 then
		inter.perfectbonus = type
	end
end)


---Calculates time bonus
---@param player player_t
function inter:calcTimeBonus(player)
	local secs = player.realtime / TICRATE
	local bonus = 0  --[[ TIME TAKEN: TOO LONG ]]

	-- This is horrible... can't just vanilla do it in intervals???
	if     (secs <  30) then --[[   :30 ]] bonus = 50000
	elseif (secs <  60) then --[[  1:00 ]] bonus = 10000
	elseif (secs <  90) then --[[  1:30 ]] bonus = 5000
	elseif (secs < 120) then --[[  2:00 ]] bonus = 4000
	elseif (secs < 180) then --[[  3:00 ]] bonus = 3000
	elseif (secs < 240) then --[[  4:00 ]] bonus = 2000
	elseif (secs < 300) then --[[  5:00 ]] bonus = 1000
	elseif (secs < 360) then --[[  6:00 ]] bonus = 500
	elseif (secs < 420) then --[[  7:00 ]] bonus = 400
	elseif (secs < 480) then --[[  8:00 ]] bonus = 300
	elseif (secs < 540) then --[[  9:00 ]] bonus = 200
	elseif (secs < 600) then --[[ 10:00 ]] bonus = 100
	end

	self.tpoints = bonus
end

---Calculates ring bonus
---@param player player_t
function inter:calcRingBonus(player)
	self.rpoints = max(0, player.rings * 100)
end

local guard_bonus = {
	[0] = 10000,
	[1] = 5000,
	[2] = 1000,
	[3] = 500,
	[4] = 100,
}

---Calculates 'how many times player got hit' bonus
---@param player player_t
function inter:calcGuardBonus(player)
	self.gpoints = guard_bonus[player.timeshit] or 0
end

---Calculates 'all rings collected' bonus
---@param player player_t
function inter:calcPerfBonus(player)
	if self.nummaprings == 0 then return end

	if self.perfectbonus == -1 then return end

	if player.rings >= self.nummaprings then
		self.perfectbonus = 50000
	end
end


inter.bonus_types = {
	-- No bonus type
	[-1] = function()
		inter.timebonus_t = -1
	end,

	-- Normal bonus type
	[0] = function(player)
		inter:calcTimeBonus(player)
		inter:calcRingBonus(player)
		inter:calcPerfBonus(player)

		inter.timebonus_t = 0
	end,

	-- Boss bonus type
	[1] = function(player)
		inter:calcGuardBonus(player)
		inter:calcRingBonus(player)

		inter.timebonus_t = 1
	end,

	-- BZ3
	[2] = function(player)
		inter:calcGuardBonus(player)
		inter:calcRingBonus(player)

		inter.timebonus_t = 2
	end,
}

local disable_intermission = false
local last_type = ""

local function playerThinker(player)
	if player.exiting then
		inter.animticker = 0

		if player.mo then
			inter.lastplayername = skins[player.mo.skin].realname
		else
			inter.lastplayername = skins[player.skin].realname
		end

		inter.recscore = player.score
		inter.rectime = player.realtime
		inter.display = true
		inter.player = player
		inter.newadd = 0
		inter.bonus_types[mapheaderinfo[gamemap].bonustype or 0](player)
		if customhud.CheckType("intermissiontally") == "mrce" then
			last_type = customhud.CheckType("intermissiontally")

			if G_IsSpecialStage(gamemap) then -- I don't want to bother with vanilla special stages :P at least not yet
				customhud.SetupItem("intermissiontally", "vanilla")
			end

			disable_intermission = false
		elseif customhud.CheckType("intermissiontally") == "vanilla" then
			if last_type == "mrce" and not G_IsSpecialStage(gamemap) then
				customhud.SetupItem("intermissiontally", "mrce")
			end
		else
			disable_intermission = true
		end
	end
end

addHook("PlayerThink", playerThinker)

---@param v videolib
local function HUD_DRAWBAR(v, graphic, count, x, y, width)
	local left = v.cachePatch("MRBASELEFT")
	local righ = v.cachePatch("MRBASERIGHT")
	local midd = v.cachePatch("MRBASEMID")

	if width then
		v.draw(x, y, left)
		v.draw(x+width, y, righ)

		v.drawStretched(x * FRACUNIT, y * FRACUNIT, width*FRACUNIT, FRACUNIT, midd)

		if width > 20 then

			local graphic = v.cachePatch(graphic)

			v.draw(x, y + 5, graphic)

			DrawMotdString(v, (x + 1 + width) * FRACUNIT, (y + 1) * FRACUNIT, FRACUNIT, tostring(count), "MRCEHUDFNT", 0, -1)
		end
	end
end

local TOP_LEFT_FLAGSBASE = V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER

---@param v videolib
local function HUD_INTERMISSION(v)
	-- Don't display if we shouldn't
	if not (inter.display) then return end
	if disable_intermission then return end

	if setup then
		coroutine.resume(cache_thread, v) -- need that font
	end

	local screenwidth = v.width()
	local screenheight = v.height()
	local screenscale, fx = v.dupx()

	local anim_capped = max(min(inter.animticker, TICRATE/3), 0)
	local anim_percent = FU / (TICRATE/3) * anim_capped

	local x_locs = {}

	local fontcolor = SKINCOLOR_AZURE
	local color = SKINCOLOR_SAPPHIRE
	local header = mapheaderinfo[gamemap]

	if header.bonustype > 0 then
		color = SKINCOLOR_RED
		fontcolor = SKINCOLOR_SALMON
	elseif header.mysticrealms then
		color = SKINCOLOR_SHAMROCK
		fontcolor = SKINCOLOR_MOSS
	end

	local colormap = v.getColormap(TC_DEFAULT, color)
	local fontcolormap = v.getColormap(TC_DEFAULT, fontcolor)

	--
	-- Background
	--


	local scrsclwidth = (screenwidth * FRACUNIT / 320) / screenscale
	local scrsclheight = (screenheight * FRACUNIT / 200) / screenscale

	v.drawStretched(320*FRACUNIT, 0, scrsclwidth, scrsclheight, TRANSITIONALPLANES[max(74 - inter.animticker, 20)], V_SNAPTORIGHT|V_SNAPTOTOP|V_FLIP|V_50TRANS, colormap)


	--
	-- Preserved Game Hud
	--

	if inter.cc_hud then
		inter.cc_hud(v, inter.player, inter)
	else
		-- Score
		v.draw(10, 11, v.cachePatch("SUBMRCEBG"), TOP_LEFT_FLAGSBASE|V_REVERSESUBTRACT)
		v.draw(16, 28, v.cachePatch("SMRCEHUDTT"), TOP_LEFT_FLAGSBASE)
		TBSlib.drawTextInt(v, "MRCEHUDFNT", 59, 27, inter.recscore + inter.newadd, TOP_LEFT_FLAGSBASE, nil, "left", -1, 8, '0')

		-- Time
		v.draw(27, 10, v.cachePatch("TMRCEHUDTT"), TOP_LEFT_FLAGSBASE)
		local minnutevalue = G_TicsToMinutes(inter.rectime) < 10 and '0'..G_TicsToMinutes(inter.rectime) or G_TicsToMinutes(inter.rectime)
		local secondsvalue = G_TicsToSeconds(inter.rectime) < 10 and '0'..G_TicsToSeconds(inter.rectime) or G_TicsToSeconds(inter.rectime)
		local centisevalue = G_TicsToCentiseconds(inter.rectime) < 10 and '0'..G_TicsToCentiseconds(inter.rectime) or G_TicsToCentiseconds(inter.rectime)

		TBSlib.drawTextInt(v, "MRCEHUDFNT", 67, 9, minnutevalue..":"..secondsvalue.."."..centisevalue, TOP_LEFT_FLAGSBASE, nil, "left", -1)
	end

	--
	-- Header
	--

	if inter.cc_title then
		inter.cc_title(v, inter.player, inter)
	else

		local string = {
			[1] = inter.lastplayername.." got",
			[2] = "Through act",
			[3] = "Through the act"
		}

		x_locs[1] = ease.outsine(anim_percent, -400, 160-(v.levelTitleWidth(string[1])/2))
		x_locs[2] = ease.outsine(anim_percent, 400, 150-(v.levelTitleWidth(string[2])/2))
		x_locs[6] = ease.outsine(anim_percent, 400, 175-(v.levelTitleWidth(string[3])/2))

		local bgh = ease.outsine(anim_percent, 0, 38)

		v.drawFill(0, 64 - bgh/2, screenwidth / screenscale, bgh, 27|V_SNAPTOLEFT)

		local bgem_tic = (inter.animticker % 45) + 1

		-- emerald icon
		local x_em = ease.outsine(anim_percent, 800, 251)
		local y_em = ease.outsine(anim_percent, -500, 54)

		v.drawScaled((x_em - 42) * FRACUNIT + FRACUNIT/2, (y_em - 34) * FRACUNIT, FRACUNIT/2, v.cachePatch("MRCEISHPT"..bgem_tic), 0, colormap)
		v.draw(x_em, y_em, v.cachePatch("MYSTIC3"), 0, colormap)

		-- You got through Act X
		local y_off = -25
		for i=1,2 do
			if i == 2 and mapheaderinfo[gamemap].actnum == 0 then
				TBSlib.drawStaticTextUnadjusted(v, "MRCEFUTITFNT", x_locs[6]*FRACUNIT, (60+y_off)*FRACUNIT, 2*FRACUNIT/3, string[3], 0, fontcolormap, nil, -1)
			else
				TBSlib.drawStaticTextUnadjusted(v, "MRCEFUTITFNT", x_locs[i]*FRACUNIT, (60+y_off)*FRACUNIT, 2*FRACUNIT/3, string[i], 0, fontcolormap, nil, -1)
			end
			y_off = 0
		end

		if mapheaderinfo[gamemap].actnum ~= 0 then
			TBSlib.drawTextInt(v, "MRCETTACT",
			ease.outsine(anim_percent, 800, ((166+v.levelTitleWidth(string[2])/2)+2)),
			52, tostring(mapheaderinfo[gamemap].actnum), 0, nil, "center", -4)
		end
	end


	--
	-- Body
	--

	if inter.cc_calc then
		inter.cc_calc(v, inter.player, inter)
	else

		-- fake calculation

		if inter.animticker >= TICRATE and not paused then
			if inter.gpoints then
				inter.gpoints = $ - 222

				if inter.gpoints < 0 then
					inter.newadd = $ + (222 + inter.gpoints)
					inter.gpoints = 0
				else
					inter.newadd = $ + 222
				end
			end

			if inter.tpoints then
				inter.tpoints = $ - 222

				if inter.tpoints < 0 then
					inter.newadd = $ + (222 + inter.tpoints)
					inter.tpoints = 0
				else
					inter.newadd = $ + 222
				end
			end

			if inter.rpoints then
				inter.rpoints = $ - 222

				if inter.rpoints < 0 then
					inter.newadd = $ + (222 + inter.rpoints)
					inter.rpoints = 0
				else
					inter.newadd = $ + 222
				end
			end

			if (inter.player.cmd.buttons & BT_SPIN) then
				inter.newadd = $ + inter.gpoints + inter.tpoints + inter.rpoints

				inter.gpoints = 0
				inter.tpoints = 0
				inter.rpoints = 0
			end
		end

		local small_bar_width = ease.outsine(anim_percent, 0, 100)
		local big_bar_width = ease.outsine(anim_percent, 0, 170)

		-- Guard Bonus and Time Bonus are never in effect at the same time
		-- So we don't worry that they use the same spot
		if inter.timebonus_t > 0 then
			-- Guard
			HUD_DRAWBAR(v, "MRGUARDYB", inter.gpoints, 160 - big_bar_width/2, hudpos.yb_time.y + 3, big_bar_width)
		else
			-- Time
			HUD_DRAWBAR(v, "MRTIMYB", inter.tpoints, 160 - big_bar_width/2, hudpos.yb_time.y + 3, big_bar_width)
		end
		-- Rings
		HUD_DRAWBAR(v, "MRRINGYB", inter.rpoints, 160 - big_bar_width/2, hudpos.yb_ring.y + 12, big_bar_width)

		-- Total
		HUD_DRAWBAR(v, "MRTOTALYB", inter.newadd, 160 - small_bar_width/2, hudpos.yb_total.y + 10, small_bar_width)
	end

    -- It doesn't matter if we handle anim_ticker directly in the hud. it shouldn't affect anything
	inter.animticker = $ + 1
end

customhud.SetupItem("intermissiontally", "mrce", HUD_INTERMISSION, "intermission")

rawset(_G, "MRCE_Inter", inter)

--#endregion