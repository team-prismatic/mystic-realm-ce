--[[

	|												|||
	|	MRCE Escape Menu 							|||
	|												|||
	\-----------------------------------------------\||
	.\-----------------------------------------------\|

	|- @Contributors: Skydusk, Xian
	|- @2024/2025

	TODO: Transitions inbetween submenus

	BUG:  Fix certain cvars in settings menu
]]


--#region internals
local open_menu = false
local just_pressed = false
local text_mode = false

local holding_tab = false
local caps = false
local capslock = false

local subcurrent_keyholder
local subcurrent_drawer

local scroll_const = 160 -- constant used for scrolling check
local maximum_offseted = 0 -- for scrolling check

local escape_select = "GT_COOP"
local escape_pos = 1
local maxtimer = 512
local timer = 0

local transition_timer = 0
local opening_menu_timer = 0

local temp_buffers = {}

local contents = {}
local levels = {} -- for backtracking
local poslevels = {} -- for backtracking

-- Flags/Constants
local MM_NEVERTOUCH = 1
local MM_TEXTMODE = 2

-- Main Escape Menu Stuff

local enum_to_string = {
	[GT_COOP] = "GT_COOP",
}
--#endregion
--#region menu macros

-- Closes menu and cleans all unused variables
local function close_menu()
	escape_select = "GT_COOP"
	temp_buffers = {} -- clean buffers
	levels = {}
	text_mode = false
	open_menu = false
	holding_tab = false
	subcurrent_keyholder = nil
	subcurrent_drawer = nil
	opening_menu_timer = 0
end

-- Toggles menu, essentially on/off switch
local function toggle_menu()
	escape_select = "GT_COOP"
	levels = {}
	open_menu = not (open_menu)
	just_pressed = true
	holding_tab = false
	subcurrent_keyholder = nil
	subcurrent_drawer = nil
	opening_menu_timer = 0

	-- change lenght
	maximum_offseted = 0
	for k, v in ipairs(contents[escape_select][escape_pos]) do
		maximum_offseted = $ + (v.offset_y or 0)
	end
end

-- Changes menu system's submenu, position
-- Also saves last position into stack
---@param submenu number 	submenu index
---@param pos number 		item position
---@param returning boolean if menu is returning to previous position (internal)
local function change_menu(submenu, pos, returning)
	if not returning then
		table.insert(levels, escape_select)
		table.insert(poslevels, escape_pos)
		escape_pos = pos or 1
	end

	subcurrent_keyholder = nil
	subcurrent_drawer = nil
	escape_select = tostring(submenu)
	just_pressed = true

	local pointer = contents[escape_select]

	-- change lenght
	maximum_offseted = 0
	for k, v in ipairs(pointer) do
		maximum_offseted = $ + (v.offset_y or 0)
	end

	-- check if there isn't entry function
	if pointer.entry and pointer.entry() then
		return
	end

	-- move to last touchable

	if pos then
		escape_pos = pos
	else
		while (pointer[escape_pos].flags & MM_NEVERTOUCH) do
			if escape_pos == #pointer then
				escape_pos = 1
			else
				escape_pos = min(escape_pos+1, #pointer)
			end
		end
	end
end

-- Backtracks menu by one position in stack
-- If nothing is in stack, just closes the menu
local function backtrack_menu()
	if #levels > 0 then
		change_menu(tostring(levels[#levels]), tonumber(poslevels[#poslevels]), true)
		table.remove(levels)
		table.remove(poslevels)
	else
		close_menu()
	end
end

-- Scrolls through items in the menu
---@param pointer table 	submenu pointer
---@param direction boolean true - down, false - down
local function scroll_menu(pointer, direction)
	if direction then
		if escape_pos == #pointer then
			escape_pos = 1
		else
			escape_pos = min(escape_pos+1, #pointer)
		end

		while (pointer[escape_pos].flags & MM_NEVERTOUCH) do
			if escape_pos == #pointer then
				escape_pos = 1
			else
				escape_pos = min(escape_pos+1, #pointer)
			end
		end
	else
		if escape_pos == 1 then
			escape_pos = #pointer
		else
			escape_pos = max(escape_pos-1, 1)
		end

		while (pointer[escape_pos].flags & MM_NEVERTOUCH) do
			if escape_pos == 1 then
				escape_pos = #pointer
			else
				escape_pos = max(escape_pos-1, 1)
			end
		end
	end
end

-- Just additional scroll function for tables (for mini menus within menus)
---@param direction 	boolean true - down, false - down
---@param vectors 		table 	string index to manipulate within the menu (position)
---@param vectorname 	string 	string index to manipulate within the menu (position)
---@param maxvalue		number  max limit reach
local function scroll_mini_menu(direction, vectors, vectorname, maxvalue)
	if direction then
		if vectors[vectorname] == maxvalue then
			vectors[vectorname] = 1
		else
			vectors[vectorname] = min(vectors[vectorname]+1, maxvalue)
		end
	else
		if vectors[vectorname] == 1 then
			vectors[vectorname] = maxvalue
		else
			vectors[vectorname] = max(vectors[vectorname]-1, 1)
		end
	end
end

-- Merges tables together
---@param tables table<table>
---@return table
local function merge_tables(tables)
	local result = {}

	for i = 1, #tables do
		local tab = tables[i]

		for y = 1, #tab do
			local content = tab[y]
			table.insert(result, content)
		end
	end

	return result
end


--#endregion
--#region unused
--local macro_process = 0
--local current_macro = nil

--local get_to_options = {
	--[5] = {enable = 'escape'},
	--[4] = {enable = 'down', disable = 'escape'},
	--[3] = {enable = 'down'},
	--[2] = {enable = 'enter', disable = 'down'},
	--[1] = {disable = 'enter'},

--	[2] = {enable = 193},
--	[1] = {disable = 193},
--}

--local function run_macro()
--	if macro_process then
--		local step = current_macro[macro_process]
--
--		if step.enable then
--			gamekeydown[step.enable] = true
--			if gamekeydown[step.enable] then
--				print('pressed - '..step.enable)
--			end
--		end
--
--		if step.disable then
--			gamekeydown[step.disable] = false
--			if not gamekeydown[step.disable] then
--				print('disabled - '..step.disable)
--			end
--		end
--
--		macro_process = $-1
--	end
--end

--#endregion
--#region generic drawing functions

-- Drawers

-- Do not use this function, it is sub drawing function
-- https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
---@deprecated
local function V_DrawLineLow(v, x_0, y_0, x_1, y_1, color, thickness)
	local dx = x_1-x_0
	local dy = y_1-y_0
	local yi = 1
	if dy < 0 then
		yi = -1
		dy = -dy
	end
	local D = 2*dy - dx
	local y = y_0

	for x = x_0, x_1 do
		v.drawFill(x, y - thickness/2, 1, thickness, color)
		if D > 0 then
			y = y+yi
			D = D + (2*(dy - dx))
		else
			D = D + 2*dy
		end
	end
end

-- Do not use this function, it is sub drawing function
-- https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
---@deprecated
local function V_DrawLineHigh(v, x_0, y_0, x_1, y_1, color, thickness)
	local dx = x_1-x_0
	local dy = y_1-y_0
	local xi = 1
	if dx < 0 then
		xi = -1
		dx = -dx
	end
	local D = 2*dx - dy
	local x = x_0

	for y = y_0, y_1 do
		v.drawFill(x - thickness/2, y, thickness, 1, color)
		if D > 0 then
			x = x+xi
			D = D + (2*(dx - dy))
		else
			D = D + 2*dx
		end
	end
end

-- Combines both V_DrawLineHigh and V_DrawLineLow functions
-- Please use this one, if you will draw something
---@param v 		videolib
---@param x_0 		number point 1 x position
---@param y_0 		number point 1 y position
---@param x_1 		number point 2 x position
---@param y_1 		number point 2 y position
---@param color 	number color + flags see v.drawFill
---@param thickness number thickness
local function V_DrawLine(v, x_0, y_0, x_1, y_1, color, thickness)
	if abs(y_1 - y_0) < abs(x_1 - x_0) then
		if x_0 > x_1 then
			V_DrawLineLow(v, x_1, y_1, x_0, y_0, color, thickness)
		else
			V_DrawLineLow(v, x_0, y_0, x_1, y_1, color, thickness)
		end
	else
		if y_0 > y_1 then
			V_DrawLineHigh(v, x_1, y_1, x_0, y_0, color, thickness)
		else
			V_DrawLineHigh(v, x_0, y_0, x_1, y_1, color, thickness)
		end
	end
end


-- Simplified V_DrawLine for V_DrawThickCircle (Horizontal only draw)
---@param v 	videolib
---@param x_1 	number point 1 x position
---@param x_2	number point 2 x position
---@param y		number point 1+2 y position
---@param color number color + flags see v.drawFill
local function V_DrawLine_x(v, x_1, x_2, y, color)
	v.drawFill(x_1, y, x_2-x_1, 1, color)
end

-- Simplified V_DrawLine for V_DrawThickCircle (Vertical only draw)
---@param v 	videolib
---@param x 	number point 1+2 x position
---@param y_1	number point 1 y position
---@param y_2	number point 2 y position
---@param color number color + flags see v.drawFill
local function V_DrawLine_y(v, x, y_1, y_2, color)
	v.drawFill(x, y_1, 1, y_2-y_1, color)
end

-- Draws hollowed circle, with option of both inner and outer radius
--https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
--https://stackoverflow.com/questions/27755514/circle-with-thickness-drawing-algorithm (M Oehm)
---@param v 		videolib
---@param x_center  number
---@param y_center  number
---@param inner 	number inner circle radius
---@param outer 	number outer circle radius
---@param color 	number color + flags see v.drawFill
local function V_DrawThickCircle(v, x_center, y_center, inner, outer, color)
	local x_o = outer
	local x_i = inner
	local y = 0
	local erro = 1 - x_o
	local erri = 1 - x_i

	local bandaid = 3*(outer-inner)/4
	v.drawFill(x_center - 5*x_o/7, y_center - 5*x_o/7, bandaid, bandaid, color)

	while (x_o >= y) do
		V_DrawLine_x(v, x_center + x_i, x_center + x_o, y_center + y, color)
		V_DrawLine_y(v, x_center + y, y_center + x_i, y_center + x_o, color)
		V_DrawLine_x(v, x_center - x_o, x_center - x_i, y_center + y, color)
		V_DrawLine_y(v, x_center - y, y_center + x_i, y_center + x_o, color)
		V_DrawLine_x(v, x_center - x_o, x_center - x_i, y_center - y, color)
		V_DrawLine_y(v, x_center - y, y_center - x_o, y_center - x_i, color)
		V_DrawLine_x(v, x_center + x_i, x_center + x_o, y_center - y, color)
		V_DrawLine_y(v, x_center + y, y_center - x_o, y_center - x_i, color)

		y = $+1

		if erro < 0 then
			erro = $+2*y+1
		else
			x_o = $-1
			erro = $+2*(y-x_o+1)
		end

		if (y > inner) then
			x_i = y
		else
			if erri < 0 then
				erri = $+2*y+1
			else
				x_i = $-1
				erri = $+2*(y-x_i+1)
			end
		end
	end
end
--#endregion
--#region menu-specific drawing functions

---Splits table into invidiual sets
---@param tab 		table  storing table
---@param inserts	table  table that splits
---@param typein	string category
---@param amount	number max amount of items per line
---@return table
---@return integer
local function InsertSetsPerTable(tab, inserts, typein, amount)
	local index = #tab
	local count = amount

	for _, v in ipairs(inserts) do
		if not (v and v.achivement) then continue end

		if count == amount and tab[index] and tab[index][1] then
			index = $ + 1
			tab[index] = {name = typein}
			count = 1
		end

		table.insert(tab[index], {achivement = v.achivement, unlock = v.unlock})
		count = $ + 1
	end

	return tab, index
end

local achivement_list = {}

---Splits text into lines
---@param text 	string
---@param width number number of characters per line
---@return nil
---@return integer|any
local function SplitTextintoLines(text, width)
	if not text then return nil, 0 end

	local lines = {}
	local cnt_s = 0
	local cnt_i = 1

	if not lines[cnt_i] then
		lines[cnt_i] = ""
	end

	for word in text:gmatch("([^%s]+)") do
		cnt_s = $+string.len(word)
		if cnt_s > width then
			cnt_s = 0
			cnt_i = $+1
			lines[cnt_i] = ""
		end

		lines[cnt_i] = $..' '..word
	end

	return lines, cnt_i
end

---Drawer used in level achievement menu
---@param v 		videolib
---@param p 		player_t
---@param pointer 	table
---@param offset 	number
---@param item 		table
local function relevant_achivement_drawer(v, p, pointer, offset, item)
	local achivement = item.var1 ---@type achievement_t
	local unlocked = item.var2
	local sprite = achivement.hudsprite or "TABEMBICON"
	local color = v.getColormap(TC_DEFAULT, achivement.color)
	local patch = v.cachePatch(sprite)

	local desc_eval, desc_ylen = SplitTextintoLines(achivement.minidesc, 38)
	local tips_eval, tips_ylen = SplitTextintoLines("TIP: "..achivement.tips, 38)

	local offseting = offset - 16
	local size_of_item = (desc_ylen + tips_ylen - 1) * 8

	if pointer[escape_pos] == item then
		v.drawFill(0, offseting, 520, 24 + size_of_item, 118|V_SNAPTOLEFT|V_50TRANS)
	end

	if unlocked then
		MRCElibs.drawFakeHRotation(v, 40*FRACUNIT, (offset + 4)*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, patch, V_SNAPTOLEFT, color)
		v.drawString(60, offset-16, '\x8B'..(achivement.name or ''), V_SNAPTOLEFT)

		offseting = $ + 8
		for i = 1, #desc_eval do
			v.drawString(60, offseting, '\x8A'..(desc_eval[i] or ''), V_SNAPTOLEFT, "thin")
			offseting = $ + 8
		end

		for i = 1, #tips_eval do
			v.drawString(60, offseting, '\x8A'..(tips_eval[i] or ''), V_SNAPTOLEFT, "thin")
			offseting = $ + 8
		end
	else
		MRCElibs.drawFakeHRotation(v, 40*FRACUNIT, (offset + 4)*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, patch, V_SNAPTOLEFT, v.getColormap(TC_DEFAULT, 0, "MRCEHUDEMBBG"))

		offseting = $ + 8
		v.drawString(60, offseting, '\x86'..'???', V_SNAPTOLEFT, "thin")

		for i = 1, #desc_eval do
			offseting = $ + 8
		end

		for i = 1, #tips_eval do
			v.drawString(60, offseting, tips_eval[i] or '', V_SNAPTOLEFT, "thin")
			offseting = $ + 8
		end
	end
end

---Drawer used in global achievement menu -> lists descriptions and tips
---@param v 		videolib
---@param p 		player_t
---@param pointer 	table
---@param offset 	number
---@param item 		table
local function unrelv_achivement_drawer(v, p, pointer, offset, item)
	local achievement = item.var1 ---@type achievement_t
	local unlocked = item.var2
	local color = v.getColormap(TC_DEFAULT, achievement.color)
	local patch, scale

	if achievement.gamesprite ~= nil and achievement.frame ~= nil then
		patch = v.getSpritePatch(achievement.gamesprite, achievement.frame, 0, 0)
		scale = FRACUNIT/2
	else
		local sprite = achievement.hudsprite or "TABEMBICON"

		patch = v.cachePatch(sprite)
		scale = FRACUNIT
	end

	local sc, fxs = v.dupx()

	local width_allowance = (v.width()/sc - 90)/5

	if unlocked then
		local lines = SplitTextintoLines(achievement.desc ~= "" and achievement.desc or achievement.tips, width_allowance)

		MRCElibs.drawFakeHRotation(v, 20*FRACUNIT, (offset + 4)*FRACUNIT, (leveltime/2)*ANG1, scale, patch, V_SNAPTOLEFT, color)
		v.drawString(40, offset-16, '\x8B'..(achievement.name or ''), V_SNAPTOLEFT)
		local yoff = -8

		for i = 1, #lines do
			local line = lines[i]
			v.drawString(38, offset+yoff, '\x8A'..(line or ''), V_SNAPTOLEFT, "thin")
			yoff = $+8
		end
	else
		MRCElibs.drawFakeHRotation(v, 20*FRACUNIT, (offset + 4)*FRACUNIT, (leveltime/2)*ANG1, scale, patch, V_SNAPTOLEFT, v.getColormap(TC_DEFAULT, 0, "MRCEHUDEMBBG"))
		v.drawString(40, offset-16, '\x86'..'???', V_SNAPTOLEFT, "thin")

		local lines = SplitTextintoLines('TIPS: '..achievement.tips, width_allowance)

		local yoff = -8

		for i = 1, #lines do
			local line = lines[i]
			v.drawString(38, offset+yoff, '\x8A'..(line or ''), V_SNAPTOLEFT, "thin")
			yoff = $+8
		end
	end
end


---Draws text stored in buffer
---@param v 		videolib
---@param p			player_t
---@param pointer	table
---@param offset    number
---@param item      any
local function text_buff_drawer(v, p, pointer, offset, item)
	local buffer = ""..tostring(temp_buffers[item.var1])
	local lenght = item.var2*4
	local selected = pointer[escape_pos] == item

	if ((item.var2 and string.len(buffer) < item.var2) or not item.var2) and selected and (leveltime % 8 / 4) then
		buffer = $..'_'
	end

	v.drawFill(160-lenght-2, 18 + offset, lenght*2+4, 10, text_mode and 118 or 31)
	v.drawString(160-lenght, 20 + offset, buffer)
end

local initial_cache = false
local parts = {}

---Vertical zipper drawer
---@param v 		videolib
---@param x 		number
---@param y 		number
---@param width 	number
---@param height 	number
---@param cur 		number 	current value of index
---@param min_i 	number 	minimal value of index
---@param max_i 	number 	maximal value of index
local function vertical_zipper_drawer(v, x, y, width, height, cur, min_i, max_i)
	local filled = height - ease.linear(((cur - min_i) * FRACUNIT) / (max_i - min_i), height, 0)
	local unfilled = height - filled

	v.drawFill(x, y, width, filled, 1|V_SNAPTORIGHT)
	v.drawFill(x, y + filled, width, unfilled, 14|V_SNAPTORIGHT)
end

---Bottom broken, MRCE menu drawing window
---@param v 		videolib
---@param x 		number
---@param y 		number
---@param width 	number
---@param height 	number
---@param flags 	number
local function drawing_window(v, x, y, width, height, flags)
	local seg_dim = 8
	height = height*seg_dim/seg_dim
	y = y*seg_dim/seg_dim

	if not initial_cache then
		for k, pv in ipairs({4, 5, 6, 7, 8, 9, "A", "B", "C"}) do
			parts[k] = v.cachePatch("MRCEMENUBG"..pv)
		end
		initial_cache = true
	end

	v.drawStretched(
	(x+seg_dim) * FRACUNIT,
	(y+seg_dim) * FRACUNIT,
	(width-seg_dim*2)*FRACUNIT/seg_dim,
	(height-seg_dim)*FRACUNIT/seg_dim,
	parts[4], flags)

	local x_end = max((width/seg_dim)-2, 0)

	for indx = 0, x_end do
		local actual_x = x + indx*seg_dim
		local top_gpx = parts[2]
		local und_gpx = parts[6]

		if indx == 0 then
			top_gpx = parts[1]
			und_gpx = parts[5]
		elseif indx == x_end then
			top_gpx = parts[3]
			und_gpx = parts[7]
		end

		v.draw(actual_x, y, top_gpx, flags)
		v.draw(actual_x, y+height, und_gpx, flags)
	end

	local y_end = y + height - seg_dim

	for acty = y + seg_dim, y_end, seg_dim do
		v.draw(x, acty, parts[8], flags)
		v.draw(x+width-2*seg_dim, acty, parts[9], flags)
	end
end

local function escape_menu_drawer(v, p, pointer)
	local openoffset = ease.linear(opening_menu_timer, 256, 0)

	if opening_menu_timer < FRACUNIT then
		opening_menu_timer = $ + FRACUNIT/(TICRATE/4)
	end


	local offset = 0
	local skew_x = openoffset

	v.draw(467 + openoffset, -80, v.cachePatch("MRCEMENUBG1"), V_SNAPTORIGHT)

	for k,item in ipairs(pointer) do
		offset = $ + item.offset_y
		skew_x = $ - 14

		if just_pressed then
			item.offset_x = 0
		end

		if k == escape_pos then
			item.offset_x = min(item.offset_x+2, 6)
			v.draw(235 - item.offset_x + skew_x, 52 + offset, v.cachePatch("MRCEMENUBUT2"), V_SNAPTORIGHT)
			v.drawString(235 - item.offset_x + skew_x, 48 + offset, '\x82'..item.name, V_SNAPTORIGHT, "center")
		else
			item.offset_x = max(item.offset_x-2, 0)
			v.draw(235 - item.offset_x + skew_x, 52 + offset, v.cachePatch("MRCEMENUBUT1"), V_SNAPTORIGHT)
			v.drawString(235 - item.offset_x + skew_x, 48 + offset, item.name, V_SNAPTORIGHT, "center")
		end
	end
end

local function get_base(pointer, position)
	local index = 1
	local num = 0
	while (pointer[index] and index <= position) do
		num = $ - pointer[index].offset_y
		index = $+1
	end
	return num
end

---@param v 		videolib
local function generic_background(v)
	local patch = v.cachePatch("MRCEMENUBG2")

	local scroll_y = (leveltime % patch.height) - patch.height
	local height = v.height()
	local width = v.width()
	local scalex = v.dupx()

	local width_fill = width / scalex /2 - 101 - patch.width

	v.drawFill(0, 0, width_fill+2, height/scalex, 117|V_SNAPTOTOP|V_SNAPTOLEFT)

	v.drawFill(261 + patch.width, 0, width_fill, height/scalex, 117|V_SNAPTOTOP)

	while(scroll_y < height) do
		v.draw(160-99-patch.width, scroll_y, patch, V_SNAPTOTOP)
		v.draw(160+101+patch.width, scroll_y, patch, V_FLIP|V_SNAPTOTOP)

		scroll_y = $ + patch.height
	end
end

---Used in generic menus (with scrolling)
---@param v 		videolib
---@param p 		player_t
---@param pointer 	table
local function generic_menu_drawer(v, p, pointer)
	local offset = 20

	if pointer.bg then
		pointer.bg(v)
	end

	local start_i =  max(escape_pos-1, 1)
	local end_i = max(escape_pos-(#pointer-2), 0)

	local scale, fxscale = v.dupx()
	local height = v.height()/scale - offset

	for k = start_i-end_i, #pointer do
		local item = pointer[k]
		if not item then return end

		offset = $ + item.offset_y

		if height < offset then break end


		if item.drawer then
			item.drawer(v, p, pointer, offset, item)
		else
			if k == escape_pos then
				v.draw(160, offset, v.cachePatch("MRCEMENUBUT2"))
				v.drawString(160, offset-8, '\x82'..item.name, 0, "center")
			else
				v.draw(160, offset, v.cachePatch("MRCEMENUBUT1"))
				v.drawString(160, offset-8, item.name, 0, "center")
			end

			if item.cvar then
				local cvar = item.cvar
				local val = tostring(cvar.string)
				v.drawString(160, offset, ((val == cvar.defaultvalue and '\140' or '')..'< '..val..' >'), 0, "center")
			end
		end
	end

	if pointer.name then
		local name = pointer.name

		v.draw(160, 0, v.cachePatch("MRCEMENUBG3"), V_SNAPTOTOP)
		v.drawString(160, 2, '\x8B'..name, V_SNAPTOTOP, "center")
	end

	if #pointer > 5 then
		vertical_zipper_drawer(v, 316, 0, 3, 200, escape_pos, 0, #pointer)
	end
end

---Used in non-scrolling menus + special menus
---@param v 		videolib
---@param p 		player_t
---@param pointer 	table
local function static_menu_drawer(v, p, pointer)
	local offset = scroll_const < maximum_offseted and 50+get_base(pointer, escape_pos) or 0

	if pointer.bg then
		pointer.bg(v)
	end

	for k,item in ipairs(pointer) do
		offset = $ + item.offset_y

		if item.drawer then
			item.drawer(v, p, pointer, offset, item)
		else
			if k == escape_pos then
				v.draw(160, offset, v.cachePatch("MRCEMENUBUT2"))
				v.drawString(160, offset-8, '\x82'..item.name, 0, "center")
			else
				v.draw(160, offset, v.cachePatch("MRCEMENUBUT1"))
				v.drawString(160, offset-8, item.name, 0, "center")
			end

			if item.cvar then
				local cvar = item.cvar
				local val = tostring(cvar.string)
				v.drawString(160, offset, ((val == cvar.defaultvalue and '\140' or '')..'< '..val..' >'), 0, "center")
			end
		end
	end

	if pointer.headerdrawer then
		pointer.headerdrawer(v)
	else
		if pointer.name then
			local name = pointer.name

			v.draw(160, 0, v.cachePatch("MRCEMENUBG3"), V_SNAPTOTOP)
			v.drawString(160, 2, '\x8B'..name, V_SNAPTOTOP, "center")
		end
	end

	if #pointer > 5 then
		vertical_zipper_drawer(v, 316, 0, 3, 200, escape_pos, 1, #pointer)
	end
end

--#endregion
--#region menu contents


-- Menu Contents

contents = {
	["GT_COOP"] = {
		drawer = escape_menu_drawer,

		{name = "Achievement Hints", func = function()
			change_menu("ACHIVEMENTSHINTS")
		end, offset_y = 0, offset_x = 0, flags = 0},

		{name = "Achievement List", func = function()
			change_menu("ACHIEVEMENTS")
		end, offset_y = 32, offset_x = 0, flags = 0},

		{name = "Passwords", func = function()
			temp_buffers["PASSWORD"] = "" -- open up buffer
			change_menu("PASSWORD")
		end, offset_y = 32, offset_x = 0, flags = 0},

		{name = "MRCE Settings", func = function()
			change_menu("SETTINGS")
		end, offset_y = 32, offset_x = 0, flags = 0},

		{name = "Exit Menu", func = function()
			close_menu()
		end, offset_y = 32, offset_x = 0, flags = 0},
	},

	["ACHIVEMENTSHINTS"] = {
		drawer = generic_menu_drawer,
		name = "HINTS",

		entry = function() -- setup achivement hints
			local keyword = mapheaderinfo[gamemap].keywords
			local array = MRCE_Achievements.searchByCategory(string.upper(keyword))
			local saves = MRCE_Unlock.getSaveData().achivements

			-- clean the table
			while(contents['ACHIVEMENTSHINTS'][1]) do
				table.remove(contents['ACHIVEMENTSHINTS'])
			end

			local defaultoffset = 35
			local offset_y = 24

			local prev_expansion = 0

			if array then
				for i = 1, #array do
					local key = array[i]
					local achivement = MRCE_Achievements[key] ---@cast achivement achievement_t

					if not achivement or (achivement.flags & 1 and not saves[key]) then continue end -- hidden achivements won't appear until they are unlocked
					table.insert(contents['ACHIVEMENTSHINTS'], 	{name = "ACHIEVEMENT", offset_y = i == 1 and defaultoffset or offset_y + prev_expansion, drawer = relevant_achivement_drawer, flags = 0, var1 = achivement, var2 = saves[key]})

					local desc_eval, desc_ylen = SplitTextintoLines(achivement.minidesc, 38)
					local tips_eval, tips_ylen = SplitTextintoLines("TIP: "..achivement.tips, 38)

					prev_expansion = (desc_ylen + tips_ylen - 1) * 8
				end

			else
				backtrack_menu()

				S_StartSound(nil, sfx_adderr, displayplayer)

				return true
			end

			-- exit
			table.insert(contents['ACHIVEMENTSHINTS'], 	{name = "Return To Main", func = function()
			backtrack_menu()
			end, offset_y = 24 + prev_expansion, flags = 0})

		end,

		{name = "Return To Main", func = function()
			backtrack_menu()
		end, offset_y = 56, flags = 0},
	},

	["ACHIEVEMENTS"] = {
		---@param v videolib
		headerdrawer = function(v)
			local scl, fxsc = v.dupx()
			local width = v.width()/scl

			v.drawFill(0, 0, width, 10, 26|V_SNAPTOLEFT|V_SNAPTOTOP)
			v.drawFill(0, 10, width, 6, 119|V_SNAPTOLEFT|V_SNAPTOTOP)
			v.drawFill(0, 16, width, 4, 118|V_SNAPTOLEFT|V_SNAPTOTOP)
			v.drawFill(0, 20, width, 4, 117|V_SNAPTOLEFT|V_SNAPTOTOP)
			v.drawFill(0, 24, width, 2, 0|V_SNAPTOLEFT|V_SNAPTOTOP)

			v.drawString(160, 2, '\x8B'..'ACHIEVEMENTS', V_SNAPTOTOP, "center")
		end,
		drawer = static_menu_drawer,
		name = "ACHIEVEMENTS",

		entry = function() -- setup achivement list
			local pointerx = contents["ACHIEVEMENTS"]

			local array = MRCE_Achievements
			local saves = MRCE_Unlock.getSaveData().achivements

			-- To make sure procentage is correct
			MRCE_Achievements.recount()

			local last_type = ""
			local index = 0
			local num_achivements = 0
			achivement_list = {}

			local challenge = {}
			local music = {}
			local art = {}
			local lore = {}
			local specials = {}

			if array then
				for i, achivementobj in ipairs(array) do
					if (achivementobj.flags & 1) -- hidden achievements won't appear until they are unlocked or you have debug mode
					and (not saves[i] and mrce and not mrce.debugmode) then continue end

					local typexyv = tostring(achivementobj.typein)
					if achivementobj.inlevel then
						local act = mapheaderinfo[achivementobj.inlevel].actnum

						if act and act > 0 then
							typexyv = mapheaderinfo[achivementobj.inlevel].lvlttl.." "..act
						else
							typexyv = mapheaderinfo[achivementobj.inlevel].lvlttl
						end
					end

					local new_list = (string.upper(typexyv) ~= string.upper(last_type))

					if new_list or (num_achivements > 1 and not (num_achivements % 6)) then
						if new_list then
							num_achivements = 0

							if #challenge then
								achivement_list, index = InsertSetsPerTable(achivement_list, challenge, last_type, 6)
								challenge = {}
							end

							if #music then
								achivement_list, index = InsertSetsPerTable(achivement_list, music, last_type, 6)
								music = {}
							end

							if #art then
								achivement_list, index = InsertSetsPerTable(achivement_list, art, last_type, 6)
								art = {}
							end

							if #lore then
								achivement_list, index = InsertSetsPerTable(achivement_list, lore, last_type, 6)
								lore = {}
							end

							if #specials then
								achivement_list, index = InsertSetsPerTable(achivement_list, specials, last_type, 6)
								specials = {}
							end
						end

						if (achivement_list[index] and achivement_list[index][1]) or index == 0 then
							index = $+1
						end

						last_type = typexyv


						achivement_list[index] = {name = typexyv}
					end

					if achivementobj.typein == "music" then
						table.insert(music, {achivement = achivementobj, unlock = saves[i], rot = 0})
					elseif achivementobj.typein == "challenge" then
						table.insert(challenge, {achivement = achivementobj, unlock = saves[i], rot = 0})
					elseif achivementobj.typein == "lore" then
						table.insert(lore, {achivement = achivementobj, unlock = saves[i], rot = 0})
					elseif achivementobj.typein == "art" then
						table.insert(art, {achivement = achivementobj, unlock = saves[i], rot = 0})
					elseif achivementobj.typein == "special" then
						table.insert(specials, {achivement = achivementobj, unlock = saves[i], rot = 0})
					else
						table.insert(achivement_list[index], {achivement = achivementobj, unlock = saves[i], rot = 0})
						num_achivements = $+1
					end
				end
			end

			pointerx[1].local_select_x = 1
			pointerx[1].local_select_y = 1
		end,

		{name = "ACHIVEMENT_HANDLER", offset_y = 20, flags = 0, keyhandler = function(key)

			if subcurrent_keyholder then
				if key.name == "escape" then
					subcurrent_keyholder = nil
					subcurrent_drawer = nil
				end

				if subcurrent_keyholder and subcurrent_keyholder(key) then
					return true
				end

				return true
			end


			local upx, upy = input.gameControlToKeyNum(GC_FORWARD)
			local downx, downy = input.gameControlToKeyNum(GC_BACKWARD)

			local leftx, lefty = input.gameControlToKeyNum(GC_STRAFELEFT)
			local lefta, leftb = input.gameControlToKeyNum(GC_TURNLEFT)

			local rightx, righty = input.gameControlToKeyNum(GC_STRAFERIGHT)
			local righta, rightb = input.gameControlToKeyNum(GC_TURNRIGHT)


			local vectors = contents['ACHIEVEMENTS'][1]

			if (key.num == leftx or key.num == lefty or key.num == lefta or key.num == leftb
			or key.name == "left") then
				scroll_mini_menu(false, vectors, 'local_select_x', 6)
				if achivement_list[vectors.local_select_y] and achivement_list[vectors.local_select_y][vectors.local_select_x] then
					achivement_list[vectors.local_select_y][vectors.local_select_x].rot = 360
				end

				S_StartSound(nil, sfx_menu1, displayplayer)
				return true
			end

			if (key.num == rightx or key.num == righty or key.num == righta or key.num == rightb
			or key.name == "right") then
				scroll_mini_menu(true, vectors, 'local_select_x', 6)
				if achivement_list[vectors.local_select_y] and achivement_list[vectors.local_select_y][vectors.local_select_x] then
					achivement_list[vectors.local_select_y][vectors.local_select_x].rot = 360
				end

				S_StartSound(nil, sfx_menu1, displayplayer)
				return true
			end

			if key.num == downx or key.num == downy
			or key.name == "down" then
				scroll_mini_menu(true, vectors, 'local_select_y', #achivement_list)
				if achivement_list[vectors.local_select_y] and achivement_list[vectors.local_select_y][vectors.local_select_x] then
					achivement_list[vectors.local_select_y][vectors.local_select_x].rot = 360
				end

				S_StartSound(nil, sfx_menu1, displayplayer)
				return true
			end

			if key.num == upx or key.num == upy
			or key.name == "up" then
				scroll_mini_menu(false, vectors, 'local_select_y', #achivement_list)
				if achivement_list[vectors.local_select_y] and achivement_list[vectors.local_select_y][vectors.local_select_x] then
					achivement_list[vectors.local_select_y][vectors.local_select_x].rot = 360
				end

				S_StartSound(nil, sfx_menu1, displayplayer)
				return true
			end

			if key.name == "enter" then
				local list = achivement_list
				if not list then return true end

				if list[vectors.local_select_y] and list[vectors.local_select_y][vectors.local_select_x]
				and list[vectors.local_select_y][vectors.local_select_x].achivement
				and list[vectors.local_select_y][vectors.local_select_x].achivement.specials then

					local current = list[vectors.local_select_y][vectors.local_select_x].achivement ---@cast achievement_t
					local metadata = current.metadata
					local specials = current.specials ---@cast ach_actions

					if specials.interfaceThink then
						if specials.interfaceOpen then
							specials.interfaceOpen(metadata)
						end

						subcurrent_keyholder = specials.interfaceThink

						if specials.interfaceDraw then
							subcurrent_drawer = specials.interfaceDraw
						end

						return true
					end

					if specials.onClick then
						specials.onClick(metadata)
						return true
					end

					return true
				end

				S_StartSound(nil, sfx_menu1, displayplayer)
			end
		end,
		drawer = function(v, p, pointer, offset, item)
			local list = achivement_list
			if not list then return end

			local selected_x = item.local_select_x
			local selected_y = item.local_select_y

			local wip_graphic = v.cachePatch("TABEMBICONWIP")

			local list_max = #list
			local hitblock = max(min(selected_y, list_max-5), 0)
			local last_cat = ''

			local internal_scale = v.dupx()
			local screenwidth = v.width()/internal_scale
			local screenheight = v.height()/internal_scale

			v.drawFill(131, 35, screenwidth, screenheight/2, 26|V_30TRANS|V_SNAPTOLEFT|V_SNAPTOTOP)

			drawing_window(v, 0, 138, screenwidth+8, (screenheight/2)-30, V_SNAPTOLEFT)

			local fx_progress = MRCE_Achievements.progress()
			local progress = string.format("%f", fx_progress*100)

			if string.find(progress, '%.') then
				local pos = string.find(progress, '%.')
				progress = string.sub(progress, 1, pos+2)
			end

			v.drawFill(0, 25, screenwidth, 10, 118|V_SNAPTOLEFT|V_SNAPTOTOP)
			v.drawFill(0, 25, ease.linear(fx_progress, 0, screenwidth), 10, 190|V_SNAPTOLEFT|V_SNAPTOTOP)
			v.drawString(310, 26, progress..'%', V_SNAPTORIGHT|V_SNAPTOTOP, "right")

			v.drawFill(115 + 30*selected_x - 12, 30 + offset + max(selected_y-(list_max-5), 0) * 16 - 13, 24, 16, 116|V_SNAPTOLEFT|V_40TRANS)

			local grayout = v.getColormap(TC_DEFAULT, 0, "MRCEHUDBG")
			local greenout = v.getColormap(TC_DEFAULT, 0, "MRCEHUDEMBBG")

			local line_c = 0

			for y = hitblock, hitblock+5 do
				local achieve = achivement_list[y]
				if not achieve then continue end
				local real_y = (y-hitblock) * 16


				local arrayname = tostring(achieve.name)

				if y == selected_y then
					v.drawFill(0, 19 + offset + real_y, 131, 10, 118|V_SNAPTOLEFT|V_50TRANS)
				end

				if last_cat ~= arrayname then
					v.drawString(3, 20 + offset + real_y, arrayname, V_SNAPTOLEFT)
					last_cat = arrayname
					if line_c > 0 then
						v.drawFill(131, 16 + offset + real_y, screenwidth-139, 1, 118|V_SNAPTOLEFT)
					end

					line_c = $ + 1
				end

				for x = 1, #achieve do
					local achv_local = achieve[x]
					local sprite = achv_local.achivement.hudsprite or "TABEMBICON"
					local patch = v.cachePatch(sprite)
					local rot = achv_local.rot or 0

					local selected = (x == selected_x) and ((y == selected_y) and true or false) or false

					local offx = (115 + 30*x) * FRACUNIT
					local offy = (30 + offset + real_y) * FRACUNIT

					if achv_local.unlock then
						local color = v.getColormap(TC_DEFAULT, achv_local.achivement.color)
						MRCElibs.drawFakeHRotation(v, offx, offy, rot*ANG1, FRACUNIT-FRACUNIT/3, patch, V_SNAPTOLEFT, color)
					else
						MRCElibs.drawFakeHRotation(v, offx, offy, rot*ANG1, FRACUNIT-FRACUNIT/3, patch, V_SNAPTOLEFT, selected and grayout or greenout)
					end

					if (achv_local.achivement.flags & 2) then
						v.drawScaled(offx, offy, FRACUNIT/2, wip_graphic, V_SNAPTOLEFT)
					end

					if achv_local.rot then
						achv_local.rot = max(5*achv_local.rot/6, 0)
					end
				end
			end

			if list_max > 5 then
				vertical_zipper_drawer(v, 318, 36, 3, 101, selected_y, 0, list_max)
			end

			if list[selected_y] and list[selected_y][selected_x] then
				local achivement = list[selected_y][selected_x]
				unrelv_achivement_drawer(v, p, pointer, 144 + offset, {var1 = achivement.achivement, var2 = achivement.unlock})
			end
		end, local_select_x = 1, local_select_y = 1},
	},




	["PASSWORD"] = {
		drawer = static_menu_drawer,
		bg = generic_background,
		name = "PASSWORDS",

		{name = '\x8B'.."PASSWORD", offset_y = 80, flags = MM_TEXTMODE, func = function()
			COM_BufInsertText(consoleplayer, "mrsecret "..(temp_buffers["PASSWORD"] or ""))
			temp_buffers["PASSWORD"] = ""
		end, drawer = text_buff_drawer, var1 = "PASSWORD", var2 = 16},

		{name = "Return To Main", func = function()
			backtrack_menu()
		end, offset_y = 52, flags = 0},
	},


	["SETTINGS"] = {
		drawer = generic_menu_drawer,
		bg = generic_background,
		name = "MRCE SETTINGS",

		{name = "Physics Mode", offset_y = 32, flags = 0, cvar = CV_FindVar("mr_physicsMode")},

		{name = "Jump Spindash Cancel", offset_y = 32, flags = 0, cvar = CV_FindVar("mr_spinDashJumpCancel")},

		{name = "FOV Effects", offset_y = 32, flags = 0, cvar = CV_FindVar("mr_fovfx")},

		{name = "Remove Borealis", offset_y = 32, flags = 0, cvar = CV_FindVar("mr_mfz1boost")},

		{name = "Force Season", offset_y = 32, flags = 0, cvar = CV_FindVar("mr_season")},

		{name = "Powerup Display Size", offset_y = 32, flags = 0, cvar = CV_FindVar("powerupdisplay_size")},

		{name = "Corona Size", offset_y = 32, flags = 0, cvar = CV_FindVar("corona_size")},

		{name = "Corona Strength", offset_y = 32, flags = 0, cvar = CV_FindVar("cv_corona_strength")},

		{name = "Corona Offset", offset_y = 32, flags = 0, cvar = CV_FindVar("cv_corona_offset")},

		{name = "Return To Main", func = function()
			backtrack_menu()
		end, offset_y = 40, flags = 0},
	},
}

--#endregion
--#region menu key handling

local sys1, sys2
local emblemlist = {}
local numembs = 0

addHook("MapLoad", function()
	if ((mapheaderinfo[gamemap].levelflags & LF_NORELOAD) and not multiplayer) then return end
	emblemlist = {}
	numembs = 0
	sys1, sys2 = input.gameControlToKeyNum(GC_SYSTEMMENU)
end)

addHook("PlayerSpawn", function (p) --bc noreload is FUCKING stupid and doesn't run mapload hooks. Fortunately it only affects singleplayer, so piggyback off playerspawn instead
	if multiplayer then return end
	if p ~= consoleplayer then return end
	if not (mapheaderinfo[gamemap].levelflags & LF_NORELOAD) then return end
	emblemlist = {}
	numembs = 0
	sys1, sys2 = input.gameControlToKeyNum(GC_SYSTEMMENU)
end)

-- Check for caps
---@param key keyevent_t
addHook("KeyUp", function(key)
	if (key.name == "lshift" or key.name == "rshift") and caps and not capslock then
		caps = false
	end
end)

-- Actual keyhandler
-- Considering it doesn't activates on mobile, ironically this should be just fine. -- I hope, that's just assumption
---@param key keyevent_t
addHook("KeyDown", function(key)

	if gamestate ~= GS_LEVEL
	or gametype ~= GT_COOP
	or gamemap == 99
	or menuactive
	or multiplayer then -- multiplayer for now
		if open_menu then
			close_menu()
		end
		return
	end

	if not open_menu and holding_tab and (key.num == sys1 or key.num == sys2 or key.name == "escape") then
		toggle_menu()
		return true
	end

	if text_mode then
		local pointer = contents[escape_select]

		if not (open_menu and pointer[escape_pos].flags & MM_TEXTMODE) then
			text_mode = false
			return true
		end

		if (key.name == "escape") then
			text_mode = false
			return true
		end

		if (key.name == "lshift" or key.name == "rshift") then
			-- Dear... upper case letters...
			caps = true
			return true
		end

		if (key.name == "capslock") then
			-- GUYS WE INVENTED CAPSLOCK
			capslock = not (capslock)
			caps = TBS_Menu.capslock
		end

		if (key.name == "delete") then
			-- delete this or else...
			temp_buffers[pointer[escape_pos].var1] = ""
		end

		if (key.name == "backspace") then
			-- delete this or else...
			temp_buffers[pointer[escape_pos].var1] = string.sub(temp_buffers[pointer[escape_pos].var1], 1, -2)
		end

		if input.keyNumPrintable(key.num) and key.num > 31 and key.num < 127 then
			local buffer = temp_buffers[pointer[escape_pos].var1]
			local limit = pointer[escape_pos].var2
			local chara = input.keyNumToName(caps and input.shiftKeyNum(key.num) or key.num)

			if key.num == 32 then
				chara = ' '
			end

			if limit and #buffer >= limit then return true end
			temp_buffers[pointer[escape_pos].var1] = $..chara
			S_StartSound(nil, sfx_menu1, displayplayer)
		end

		if key.name == "enter" and pointer[escape_pos].func then
			pointer[escape_pos].func()
			return true
		end

		return true
	elseif open_menu then
		local pointer = contents[escape_select]
		local upx, upy = input.gameControlToKeyNum(GC_FORWARD)
		local downx, downy = input.gameControlToKeyNum(GC_BACKWARD)

		local leftx, lefty = input.gameControlToKeyNum(GC_STRAFELEFT)
		local lefta, leftb = input.gameControlToKeyNum(GC_TURNLEFT)

		local rightx, righty = input.gameControlToKeyNum(GC_STRAFERIGHT)
		local righta, rightb = input.gameControlToKeyNum(GC_TURNRIGHT)

		local jumpx, jumpy = input.gameControlToKeyNum(GC_JUMP)
		--print(down1..' '..up1..' '..key.num)

		if pointer[escape_pos].keyhandler and pointer[escape_pos].keyhandler(key) then
			return true
		end

		if pointer[escape_pos].cvar then
			local item = pointer[escape_pos]
			local cvar = item.cvar

			if (key.num == leftx or key.num == lefty or key.num == lefta or key.num == leftb
			or key.name == "left") then
				CV_AddValue(cvar, -(item.var1 or 1))
				S_StartSound(nil, sfx_menu1, displayplayer)
			end

			if (key.num == rightx or key.num == righty or key.num == righta or key.num == rightb
			or key.name == "right") then
				CV_AddValue(cvar, item.var1 or 1)
				S_StartSound(nil, sfx_menu1, displayplayer)
			end
		end

		if key.num == downx or key.num == downy
		or key.name == "down" then
			scroll_menu(pointer, 1)
			S_StartSound(nil, sfx_menu1, displayplayer)
		end

		if key.num == upx or key.num == upy
		or key.name == "up" then
			scroll_menu(pointer, 0)
			S_StartSound(nil, sfx_menu1, displayplayer)
		end

		if key.num == jumpx
		or key.num == jumpy
		or key.name == "enter" then
			if pointer[escape_pos].flags & MM_TEXTMODE and key.name == "enter" then
				text_mode = true
				S_StartSound(nil, sfx_menu1, displayplayer)
			elseif pointer[escape_pos].func then
				pointer[escape_pos].func()
				S_StartSound(nil, sfx_menu1, displayplayer)
			else
				return true -- sound function will be there
			end
		end

		if (key.num == sys1 or key.num == sys2 or key.name == "escape") then
			backtrack_menu()
		end

		return true
	end
end)

local hackyjoysticktimer1, hackyjoysticktimer2 = 0, 0
local joyticdelay = 5 --when holding a direction how quickly should scrolling occur, in tics

-- Controller/MP player layer
---@param p player_t
addHook("PlayerThink", function(p)
	if not open_menu then return end

	if text_mode then return end

	local pointer = contents[escape_select]

	if not multiplayer then --future proofing, would like the menu to work in mp eventually
		p.powers[pw_nocontrol] = 3
		p.powers[pw_flashing] = 1
	end

	local vectors = contents['ACHIEVEMENTS'][1]

	if (input.joyAxis(JA_STRAFE)) > 200 then
		hackyjoysticktimer2 = $ < 35 and $ + 1 or 1
	elseif (input.joyAxis(JA_STRAFE)) < -200 then
		hackyjoysticktimer2 = $ > -35 and $ - 1 or -1
	else
		hackyjoysticktimer2 = 0
	end

	if (input.joyAxis(JA_MOVE)) > 200 then --holding down makes positive values ironically
		hackyjoysticktimer1 = $ < 35 and $ + 1 or 1
	elseif (input.joyAxis(JA_MOVE)) < -200 then --holding up makes negative values
		hackyjoysticktimer1 = $ > -35 and $ - 1 or -1
	else
		hackyjoysticktimer1 = 0
	end

	if hackyjoysticktimer1 > 0 and not (hackyjoysticktimer1 % joyticdelay) then
		scroll_menu(pointer, 1)
		scroll_mini_menu(true, vectors, 'local_select_y', #achivement_list)
	elseif hackyjoysticktimer1 < 0 and not (hackyjoysticktimer1 % joyticdelay) then
		scroll_menu(pointer, 0)
		scroll_mini_menu(false, vectors, 'local_select_y', #achivement_list)
	end
	if hackyjoysticktimer2 > 0 and not (hackyjoysticktimer2 % joyticdelay) then
		scroll_mini_menu(true, vectors, 'local_select_x', 8)
	elseif hackyjoysticktimer2 < 0 and not (hackyjoysticktimer2 % joyticdelay) then
		scroll_mini_menu(false, vectors, 'local_select_x', 8)
	end
end)


COM_AddCommand("debug_mrce_escapemenu", function()
	toggle_menu()
end, COM_LOCAL)

--#endregion
--#region score menu notch (tab corner)

--- Multiplies fixed value and returns interger
---@param value fixed_t
---@param mul 	number
---@return		number
local function fixedintmul(value, mul)
	return (value * mul) / FRACUNIT
end

--- Internal controller elements
--local tempstoptime = nil
local circle_bool = false
local circle_start = 0
local circle_end = FRACUNIT+FRACUNIT/4
local circlethrsh = FRACUNIT/32
local touch_timer = 0

local circle_scale = 0
local hud_disable = hud.disable
local hud_enable = hud.enable

-- TODO: figure out better disable/enable mechanism that doesn't involve one tic setup as it is now
---@param v videolib
hud.add(function(v)
	holding_tab = false
	if not multiplayer then
		circle_bool = true
		holding_tab = true
	end

	if mapheaderinfo[gamemap].mysticrealms then
		hud_disable("tokens")
		hud_disable("tabemblems")
	else
		hud_enable("tokens")
		hud_enable("tabemblems")
	end
end, "scores")

-- #################################
-- DIRECTLY COPY PASTED FROM NEWEMBLEMHUD
-- #################################

local EMB_PANEL, EMB_PANELBG, UNCOLLECTED_EMB, UNCOLLECTED_EMB_WIDTH
local EMB_COG = {}
local EMB_FLAGS = V_SNAPTORIGHT
local EMB_SCALE = FRACUNIT/3
local EMB_ANGLEOFFSET = ANGLE_90/32
local EMB_PIERANGE = ANGLE_90

---@param mo mobj_t
addHook("MobjSpawn", function(mo)
	if multiplayer then return end
	mo.update = 6 -- 6 tics of update
end, MT_MRCEACHIVEMENT)

---@param mo mobj_t
addHook("MobjSpawn", function(mo)
	if multiplayer then return end
	mo.update = 6 -- 6 tics of update
end, MT_EMBLEM)

---@param mo mobj_t
addHook("MobjThinker", function(mo)
	if not mo.update then return end
	emblemlist[mo] = {rotation = 0, frame = mo.frame, sprite = mo.sprite, color = mo.color}
	mo.update = $-1
	if not mo.update then
		numembs = $+1
	end
end, MT_MRCEACHIVEMENT)

---@param mo mobj_t
addHook("MobjThinker", function(mo)
	if not mo.update then return end
	emblemlist[mo] = {rotation = 0, frame = mo.frame, sprite = mo.sprite, color = mo.color}
	mo.update = $-1
	if not mo.update then
		numembs = $+1
	end
end, MT_EMBLEM)

---@param mo mobj_t
addHook("TouchSpecial", function(mo)
	if not emblemlist[mo] then return end
	if not emblemlist[mo].rotation then
		emblemlist[mo].rotation = 360
	end
	emblemlist[mo].collected = true
	touch_timer = 64
end, MT_MRCEACHIVEMENT)

---@param mo mobj_t
addHook("TouchSpecial", function(mo)
	if not emblemlist[mo] then return end
	if not emblemlist[mo].rotation then
		emblemlist[mo].rotation = 360
	end
	emblemlist[mo].collected = true
	touch_timer = 64
end, MT_EMBLEM)

-- #################################
-- END COPY PASTED FROM NEWEMBLEMHUD
-- #################################

-- THIS WILL GET DISMANTLED!

--$GZDB_SKIP
local tk_x = (151+3) << FRACBITS -- base+new_offset

---Medal drawer used in corner tab notch
---@param v 		videolib
---@param x 		fixed_t
---@param y 		fixed_t
---@param data		table
---@param origin	mobj_t
---@param flags		number
local function V_DrawMedal(v, x, y, data, origin, flags)
	if data.collected
	or (origin and origin.valid and (origin.flags2 & MF2_DONTDRAW or origin.alpha < FRACUNIT or origin.frame & FF_TRANSMASK or origin.state == S_INVISIBLE))
	or not origin then
		local patch = v.getSpritePatch(data.sprite, data.frame & FF_FRAMEMASK)
		local color = v.getColormap(TC_DEFAULT, data.color)

		MRCElibs.drawFakeHRotation(v, x, y, ANGLE_180+data.rotation*ANG1, EMB_SCALE, patch, flags, color)

		if data.rotation then
			data.rotation = $-18
		end

		data.collected = true
	else
		v.drawScaled(x, y, EMB_SCALE, UNCOLLECTED_EMB, flags, nil)
	end
end

---@param v videolib
---@param p player_t
customhud.SetupItem("mrce_escapemenu", "mrce", function(v, p)
	if not UNCOLLECTED_EMB then
		-- EMBLEM
		EMB_PANEL = v.cachePatch("EMBBG")
		EMB_PANELBG = v.cachePatch("EMBBG1")
		UNCOLLECTED_EMB = v.cachePatch("UNCEMB")
		UNCOLLECTED_EMB_WIDTH = (UNCOLLECTED_EMB.width / 6)*(10*FRACUNIT)

		for i = 1, 23 do
			EMB_COG[i] = v.cachePatch("TABCOG"..i)
		end
	end

	if circle_scale > circlethrsh then
		local y_pos, y_flip, y_flag, y_emboff, y_none, y_cog = 0, 1, V_SNAPTOTOP, 0, 0, 8

		if mapheaderinfo[gamemap].mrce_emeraldstage then
			y_pos = 200
			y_flip = -1
			y_flag = V_SNAPTOBOTTOM

			y_emboff = 14 * FRACUNIT
			y_cog = -8
		end

		if numembs == 0 then
			y_none = -8*y_flip
		end

		local emb_y = (y_pos+18*y_flip) << FRACBITS + y_emboff

		-- CORNER CIRCLE

		v.drawScaled(320 * FRACUNIT, (y_pos+y_none+y_cog) * FRACUNIT, circle_scale, EMB_COG[(leveltime % 23) + 1], y_flag|V_SNAPTORIGHT)

		--V_DrawThickCircle(v, 320, y_pos+y_none, fixedintmul(54, circle_scale), fixedintmul(58, circle_scale), 116|y_flag|V_SNAPTORIGHT)

		local panel_x = 320 - fixedintmul(min(numembs * 28, EMB_PANEL.width / 2), circle_scale)

		--V_DrawThickCircle(v, 320, y_pos+y_none, 0, fixedintmul(54, circle_scale), 26|y_flag|V_SNAPTORIGHT)


		if numembs > 0 then
			v.draw(panel_x, y_pos+11*y_flip, EMB_PANELBG, V_SNAPTORIGHT|y_flag)

			v.draw(panel_x, y_pos+11*y_flip, EMB_PANEL, V_SNAPTORIGHT|y_flag)
		end

		local embcnt_scale = fixedintmul(48, circle_scale) - 30
		local embindex = 1

		local achv = MRCE_Achievements

		local max_c = tostring(achv.nonextratotal)
		local count = tostring(achv.unlocked)

		TBSlib.drawTextInt(v, "MRCEGFNT", 335-embcnt_scale, y_pos+25*y_flip+y_none, 'TAB+ESC', 				EMB_FLAGS|y_flag, nil, "right", -1)
		TBSlib.drawTextInt(v, "MRCEGFNT", 335-embcnt_scale, y_pos+36*y_flip+y_none, count.."/"..max_c, 		EMB_FLAGS|y_flag, nil, "right", -1)



		for origin,data in pairs(emblemlist) do
			local x = (panel_x - 10 + embindex * 28) << FRACBITS
			V_DrawMedal(v, x, emb_y, data, origin, EMB_FLAGS|y_flag)

			embindex = $+1
		end
	end

	if circle_bool then
		circle_scale = min(ease.linear(FRACUNIT/6, circle_scale, circle_end), FRACUNIT)
	elseif touch_timer then
		touch_timer = $ - 1
		circle_scale = min(ease.linear(FRACUNIT/6, circle_scale, circle_end), FRACUNIT)
	else
		circle_scale = max(ease.linear(FRACUNIT/6, circle_scale, circle_start), 0)
	end

	-- menu
	if open_menu then
		local pointer = contents[escape_select]
		v.fadeScreen(31, 6)

		pointer.drawer(v, p, pointer)

		if subcurrent_drawer then
			subcurrent_drawer(v, p, pointer)
		end

		just_pressed = false
	else
		subcurrent_drawer = nil
	end

	circle_bool = false
	holding_tab = false
	--run_macro()
end, "game", 6)

--#endregion