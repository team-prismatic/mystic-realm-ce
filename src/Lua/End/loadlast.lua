--here we finalize loading the mod, anything that should run last after everything else is complete.
--We'll also send the player a little message telling them to come join the discord or whatever
local helloworld = false
addHook("ThinkFrame", function()
	if gamestate == GS_TITLESCREEN and leveltime > 5 then
		if not helloworld then
			print("                   ###########################################")
			print("                  ###########################################")
			print("                 ####  \132MYSTIC REALM: \130COMMUNITY EDITION\128 #####")
			print("                ####           \139DEVELOPMENT INFO\128       #####")
			print("               ###########################################")
			print("              ###########################################")
			print(" ")
			print(" #########################################################################")
			print("###########################################################################")
			print("###                                                                     ###")
			print("### THANK YOU FOR CONTRIBUTING TO PLAYTESTING AND                       ###")
			print("### DEVELOPMENT OF THE MYSTIC REALM: COMMUNITY EDITION                  ###")
			print("###                                                                     ###")
			print("### ALL MYSTIC REALM: COMMUNITY EDITION ASSETS MAY BE REUSED            ###")
			print("### \131IN FACT, WE ENCOURAGE YOU TO!\128                                       ###")
			print("###                                                                     ###")
			print("### ANY PERSON MAY MAKE ANY CONTRIBUTION, BIG OR SMALL,                 ###")
			print("### VIA EITHER OUR OFFICIAL DISCORD OR OUR GITLAB.                      ###")
			print("###                                                                     ###")
			print("### JOIN THE TEAM PRISMATIC DEVELOPMENT DISCORD                         ###")
			print("###  \136https://discord.gg/WcB2vaqbgf\128                                      ###")
			print("###                                                                     ###")
			print("### CONTRIBUTE TO THE MYSTIC REALM CE GITLAB                            ###")
			print("###  \136https://gitlab.com/team-prismatic/mystic-realm-ce\128                  ###")
			print("###                                                                     ###")
			print("### LEAVE A COMMENT ON THE TEAM PRISMATIC MESSAGE BOARD PAGE            ###")
			print("###  \136https://mb.srb2.org/threads/29931/\128                                 ###")
			print("###                                                                     ###")
			print("### \142THANK YOU\128                                                           ###")
			print("###                                                                     ###")
			print("###########################################################################")
			print(" #########################################################################")
			helloworld = true
		end
	end
end)