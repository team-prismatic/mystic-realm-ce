freeslot("MT_AGZ_WATERSPOUT", "S_AGZ_WATERSPOUT1")



mobjinfo[MT_AGZ_WATERSPOUT] = {
    --$Category MRCE - Aerial Garden Special
    --$Name Waterspout
    --$Sprite SPLHE0
    --$NotAngled
        doomednum = 1373,
        spawnstate = S_INVISIBLE,
        spawnhealth = 1000,
        reactiontime = 8,
        deathsound = sfx_pop,
        radius = 32*FRACUNIT,
        height = 96*FRACUNIT,
        mass = 100,
        flags = MF_SPECIAL|MF_NOGRAVITY
    }

--    states[S_AGZ_WATERSPOUT1] = {
--        sprite = SPR_MRCE_AGZ_WATERSPOUT,
--        frame = TR_TRANS80|A,
--        tics = 1,
--        nextstate = S_AGZ_WATERSPOUT2
--    }

addHook("TouchSpecial", function(mo, tmo)
    if not mo and mo.valid and tmo and tmo.valid then return true end
    if not tmo.player then return true end
    if tmo.player.playerstate ~= PST_LIVE or (tmo.eflags & MFE_SPRUNG) then return true end
    local argmod = 1
    if mo.spawnpoint and mo.spawnpoint.args[0] then
        argmod = mo.spawnpoint.args[0]
    end
    P_SetObjectMomZ(tmo, 2*mo.scale*argmod)
    tmo.eflags = $|MFE_SPRUNG
    tmo.state = S_PLAY_SPRING
    S_StartSound(mo, sfx_s3k39)
    return true
end, MT_AGZ_WATERSPOUT)