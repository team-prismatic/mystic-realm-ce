freeslot("MT_OBJTARGET")

mobjinfo[MT_OBJTARGET] = {
	--$Name Object Spawn Point
	--$Category MRCE Utility
	--$Sprite BNCRA1
	--$Arg0 Delay before spawn
	--$Arg0ToolTip In seconds
	--$StringArg0 Object Name
	doomednum = 4900,
	spawnstate = S_INVISIBLE,
	spawnhealth = 1,
	flags = MF_NOCLIP|MF_NOGRAVITY
}

local function hatchEgg(mo, x, y, z, moType, angle, scale)
	-- Spawn the object
	--print("Spawning " .. moType)
	
	
	local spawnedMo = P_SpawnMobj(x, y, z, moType)
	spawnedMo.tag = 0
	spawnedMo.angle = angle
	spawnedMo.scale = scale



	-- Mark the spawner as finished
	mo.readyToHatch = false
	--print("Spawn complete for", mo)
	
		-- Remove the egg shell
	P_RemoveMobj(mo.eggShell.mobj)
end

local function objectSpawnerHandler(mo)
	-- Validate the object and ensure it's ready to hatch
	if not mo or not mo.valid then return end
	if not mo.readyToHatch then return end
	if not mo.eggShell or not mo.eggShell.valid then return end

	-- Initialize the timer once
	if not mo.hatchFuseInitialized then
		mo.hatchTime = mo.eggShell.args[0] * TICRATE -- Delay in tics
		mo.hatchFuse = mo.hatchTime -- Initialize fuse
		mo.hatchFuseInitialized = true -- Mark initialization
		--print("Timer initialized for", mo, "with delay:", mo.hatchFuse)
	end

	-- Countdown logic
	if mo.hatchFuse > 0 then
		mo.hatchFuse = mo.hatchFuse - 1 -- Decrement timer
		--print("Timer ticking for", mo, ":", mo.hatchFuse)
		return
	end

	-- Timer expired, spawn the object
	local sMoType = _G[mo.eggShell.stringargs[0]]
	
	local sMoX = mo.eggShell.mobj.x
	local sMoY = mo.eggShell.mobj.y
	local sMoZ = mo.eggShell.mobj.z
	local sMoScale = mo.eggShell.mobj.scale
	local sMoAngle = mo.eggShell.mobj.angle

	hatchEgg(mo, sMoX, sMoY, sMoZ, sMoType, sMoAngle, sMoScale)
end

addHook("MobjThinker", objectSpawnerHandler, MT_OBJTARGET)

local function triggerSpawn(line, mo, d)
	for targetMoThing in mapthings.tagged(line.tag) do
		local targetMo = targetMoThing.mobj
		if not targetMo or not targetMo.valid then return end

		-- Prevent processing the same object multiple times
		if targetMo.alreadyScanned then return end

		-- Initialize the spawner
		targetMo.alreadyScanned = true
		targetMo.eggShell = targetMoThing
		targetMo.readyToHatch = true
		targetMo.hatchFuseInitialized = false -- Allow reinitialization if reused
		--print("Spawner activated for", targetMo)
	end
end

addHook("LinedefExecute", triggerSpawn, "SPAWNOBJECT_SPD")
