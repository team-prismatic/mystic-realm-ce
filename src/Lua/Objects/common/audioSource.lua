--More robust audio source, lets you control the volume, distance, and other fun stuff. MAJOR WIP


freeslot(
	"MT_AUDIOSRC"
)



mobjinfo[MT_AUDIOSRC] = {
	--$Name Audio Source
	--$Category Utility
	--$StringArg0 Sound Name
	--$Arg0 Distance
	--$Arg0Type 12
	--$Arg0Enum { 1 = "x2"; 2 = "x4"; 3 = "x8"; }
	--$Arg1 Does Volume scale with distance?
	--$Arg1Type 11
	--$Arg1Enum { 1 = "Yes"; 2 = "No"; }
	--$Arg2 Priority
	--$Arg3 Priority Flags(Non functional)
	--$Arg3Type 11
	--$Arg3Enum { 1 = "Singular"; 2 = "Multiple???"; }
	--$Arg4 Repeat Speed (Non functional)
	--$Arg5 Pitch (Non functional)
	--$Arg6 Volume

	doomednum = 4901,
	spawnhealth = 1,
	spawnstate = S_INVISIBLE,
	flags = MF_NOCLIP|MF_NOGRAVITY
}



local function playSound(mo)






end




addHook("MapThingSpawn", function(mo,mptg)


	local soundName = mo.spawnpoint.stringargs[0]
	local volume = mo.spawnpoint.args[6]
	--local distance = mo.spawnpoint.args[2]
	--local volumeScalesWithDistance =  mo.spawnpoint.args[3]
	--local pitch = mo.spawnpoint.args[4]
	--local reverb = mo.spawnpoint.args[5]

--Id like to make it so the audio source comes from the player instead of mo, and then adjust the volume the closer the player gets to the mo. This will let me create whatever distance Id wish for the sound objects. However, this might be expensive.



end, MT_AUDIOSRC)


addHook("MobjThinker", function(mo)

	if mo.valid and mo.spawnpoint then
		if not S_OriginPlaying(mo) then
			S_StartSoundAtVolume(mo,47,255)
		end
	end
end, MT_AUDIOSRC)

