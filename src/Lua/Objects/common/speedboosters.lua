--
addHook("MobjSpawn", function(mo)
    mo.height = 32*mo.scale
end, MT_REDBOOSTER)

addHook("MobjSpawn", function(mo)
    mo.height = 32*mo.scale
end, MT_YELLOWBOOSTER)

local function booststacking(mo, mobj)
    if not (mo and mo.valid and mobj and mobj.valid) then return false end
    if (mobj.eflags & MFE_SPRUNG) then return false end
    if not (mobj.flags & MF_SHOOTABLE) then return false end
	if mo.z <= mobj.z + mobj.height
	and mo.z + mo.height >= mobj.z then
        if not P_IsObjectOnGround(mobj) then
            P_SetOrigin(mobj, mobj.x, mobj.y, mo.floorz)
        end
        S_StartSound(mo, mobjinfo[mo.type].painsound)
        local rspeed = FixedDiv(FixedHypot(mobj.momx, mobj.momy), mobj.scale)
        P_InstaThrust(mobj, mo.angle, max(rspeed + FixedMul((mobjinfo[mo.type].damage/4) + ((mobjinfo[mo.type].damage - 36*FU)/2), mo.scale), mobjinfo[mo.type].damage))
        P_SetObjectMomZ(mobj, -12*mo.scale, false)
        mobj.eflags = $|MFE_SPRUNG
        if (mo.flags2 & MF2_AMBUSH) and mobj.player then
            mobj.player.pflags = $|PF_SPINNING
            mobj.state = S_PLAY_ROLL
        end
        return false
    else
        return false
    end
end

addHook("MobjCollide", booststacking, MT_REDBOOSTER)
addHook("MobjCollide", booststacking, MT_YELLOWBOOSTER)