freeslot("S_STARPOSTCUBE", "S_STARPOSTCUBEFLASH", "SPR_CTPT", "SKINCOLOR_MRCESTARPOST")

local ramp_list = {
{88, 96, 97, 98, 98, 100, 101, 104, 104, 107, 109, 109, 110, 110, 111, 111},
{1, 88, 96, 96, 112, 113, 114, 114, 115, 116, 116, 107, 109, 110, 110, 111},
{1, 88, 96, 96, 112, 113, 114, 114, 115, 116, 116, 107, 109, 110, 110, 111},
{1, 120, 121, 121, 122, 122, 123, 123, 124, 124, 126, 127, 109, 109, 110, 110},
{1, 128, 120, 121, 121, 122, 122, 123, 123, 125, 125, 126, 127, 108, 109, 110},
{1, 128, 120, 121, 121, 122, 122, 123, 123, 124, 124, 126, 143, 138, 139, 139},
{1, 128, 129, 130, 130, 131, 131, 131, 141, 141, 142, 142, 143, 143, 138, 139},
{1, 128, 129, 129, 130, 130, 131, 131, 133, 133, 134, 134, 136, 136, 138, 139},
{1, 144, 144, 145, 145, 145, 132, 132, 132, 133, 133, 134, 136, 136, 138, 138},
{1, 1, 144, 144, 145, 145, 145, 145, 146, 146, 146, 147, 147, 136, 138, 138},
{1, 2, 2, 3, 3, 3, 177, 177, 177, 161, 162, 162, 163, 165, 167, 169},
{1, 2, 2, 3, 3, 3, 177, 177, 177, 161, 162, 162, 163, 165, 167, 169},
{1, 2, 2, 3, 3, 3, 177, 177, 177, 161, 162, 162, 163, 165, 167, 169},
{1, 2, 2, 3, 3, 160, 160, 160, 161, 162, 162, 163, 163, 165, 167, 169},
{1, 1, 144, 144, 145, 145, 145, 145, 146, 146, 146, 147, 147, 136, 138, 138},
{1, 144, 144, 145, 145, 145, 132, 132, 132, 133, 133, 134, 136, 136, 138, 138},
{1, 128, 129, 129, 130, 130, 131, 131, 133, 133, 134, 134, 136, 136, 138, 139},
{1, 128, 129, 130, 130, 131, 131, 131, 141, 141, 142, 142, 143, 143, 138, 139},
{1, 128, 120, 121, 121, 122, 122, 123, 123, 124, 124, 126, 143, 138, 139, 139},
{1, 128, 120, 121, 121, 122, 122, 123, 123, 125, 125, 126, 127, 108, 109, 110},
{1, 120, 121, 121, 122, 122, 123, 123, 124, 124, 126, 127, 109, 109, 110, 110},
{1, 88, 96, 96, 112, 113, 114, 114, 115, 116, 116, 107, 109, 110, 110, 111},
{1, 88, 96, 96, 112, 113, 114, 114, 115, 116, 116, 107, 109, 110, 110, 111},
{88, 96, 97, 98, 98, 100, 101, 104, 104, 107, 109, 109, 110, 110, 111, 111},
{88, 96, 97, 98, 98, 100, 101, 104, 104, 107, 109, 109, 110, 110, 111, 111},

}

local hyperramp_list = {
{1, 1, 88, 88, 96, 96, 97, 97, 98, 101, 104, 107, 109, 109, 109, 109},
{1, 1, 88, 88, 96, 96, 97, 97, 112, 113, 116, 107, 107, 109, 109, 109},
{1, 1, 120, 120, 121, 121, 122, 122, 123, 124, 126, 127, 127, 109, 109, 109},
{1, 1, 128, 128, 120, 120, 121, 121, 122, 124, 126, 127, 127, 127, 109, 109},
{1, 1, 1, 128, 120, 120, 121, 121, 122, 123, 124, 126, 143, 143, 139, 139},
{1, 1, 128, 128, 129, 129, 130, 130, 131, 141, 141, 142, 143, 143, 138, 139},
{1, 1, 128, 128, 129, 129, 130, 130, 131, 133, 133, 134, 136, 136, 138, 139},
{1, 1, 144, 144, 145, 145, 145, 145, 132, 133, 133, 134, 136, 136, 138, 138},
{1, 1, 144, 144, 145, 145, 145, 145, 146, 146, 146, 147, 147, 136, 138, 138},
{1, 1, 144, 144, 176, 176, 177, 177, 161, 161, 162, 163, 163, 165, 167, 169},
{1, 1, 144, 144, 176, 176, 177, 177, 161, 161, 162, 163, 163, 165, 167, 169},
{1, 1, 144, 144, 145, 145, 145, 145, 146, 146, 146, 147, 147, 136, 138, 138},
{1, 1, 144, 144, 145, 145, 145, 145, 132, 133, 133, 134, 136, 136, 138, 138},
{1, 1, 128, 128, 129, 129, 130, 130, 131, 133, 133, 134, 136, 136, 138, 139},
{1, 1, 128, 128, 129, 129, 130, 130, 131, 141, 141, 142, 143, 143, 138, 139},
{1, 1, 1, 128, 120, 120, 121, 121, 122, 123, 124, 126, 143, 143, 139, 139},
{1, 1, 128, 128, 120, 120, 121, 121, 122, 124, 126, 127, 127, 127, 109, 109},
{1, 1, 120, 120, 121, 121, 122, 122, 123, 124, 126, 127, 127, 109, 109, 109},
{1, 1, 88, 88, 96, 96, 97, 97, 112, 113, 116, 107, 107, 109, 109, 109},
{1, 1, 88, 88, 96, 96, 97, 97, 98, 101, 104, 107, 109, 109, 109, 109},
}

skincolors[SKINCOLOR_MRCESTARPOST] = {
	ramp = {1, 1, 88, 88, 96, 96, 97, 97, 98, 101, 104, 107, 109, 109, 109, 109},
	accessible = false,
}

states[S_STARPOSTCUBE] = {
	sprite = SPR_CTPT,
	frame = X|FF_PAPERSPRITE|FF_SEMIBRIGHT,
}

states[S_STARPOSTCUBEFLASH] = {
	sprite = SPR_CTPT,
	frame = X|FF_PAPERSPRITE|FF_ANIMATE,
	--tics = 16,
	var1 = 3,
	var2 = 5,
	--nextstate = S_STARPOSTCUBEFLASH,
}

states[S_STARPOST_IDLE].sprite = SPR_CTPT
states[S_STARPOST_FLASH].sprite = SPR_CTPT
states[S_STARPOST_STARTSPIN].sprite = SPR_CTPT
states[S_STARPOST_SPIN].sprite = SPR_CTPT
states[S_STARPOST_ENDSPIN].sprite = SPR_CTPT

local THRHALF = FRACUNIT*13+FRACUNIT/2
local function P_SpawnCube(mo)
	mo.cube = {}
	for i = 1,4 do
		local ang = i*ANGLE_90
		local cube = P_SpawnMobjFromMobj(mo, FixedMul(THRHALF, cos(ang)), FixedMul(THRHALF, sin(ang)), 112*FRACUNIT, MT_RAY)
		cube.flags = MF_NOBLOCKMAP|MF_NOCLIP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT|MF_NOCLIPTHING
		cube.state = S_STARPOSTCUBE
		cube.angle = ang+ANGLE_90

		mo.cube[i] = cube
	end
end

local ANGFAST = ANG1*18

local amountposts = 0

addHook("MapLoad", function(mapnum)
	amountposts = 0
end)

addHook("MobjThinker", function(mo)
	if not mo and mo.valid then return end

	if Style_Pack_Active then return end

	if not mo.cube then
		P_SpawnCube(mo)
		mo.cube_rot = 0
		mo.cube_rot_speed = 0
	elseif #mo.cube < 4 then
		TBSlib.removeMobjArray(mo.cube)
		P_SpawnCube(mo)
	end

	if mo.state == S_STARPOST_SPIN then
		mo.cube_rot_aim = ANGFAST
	else
		mo.cube_rot_aim = ANG1
	end

	if mo.state == S_STARPOST_FLASH then
		mo.color = SKINCOLOR_MRCESTARPOST
		skincolors[SKINCOLOR_MRCESTARPOST].ramp = ramp_list[((leveltime/2) % #ramp_list) + 1]
	else
		mo.color = 0
	end

	mo.cube_rot_speed = ease.linear(FRACUNIT/5, mo.cube_rot_speed, mo.cube_rot_aim)
	mo.cube_rot = $+mo.cube_rot_speed

	for i = 1, 4 do
		local ang = mo.cube_rot+ANGLE_90*i
		local cube = mo.cube[i]
		local x = mo.x + FixedMul(FixedMul(THRHALF, cos(ang)), mo.scale)
		local y = mo.y + FixedMul(FixedMul(THRHALF, sin(ang)), mo.scale)
		local z = mo.z + 112*mo.scale

		P_MoveOrigin(cube, x, y, z)
		cube.z = z
		cube.color = mo.color

		--if cube.state ~= S_STARPOSTCUBEFLASH
		--and mo.state == S_STARPOST_FLASH then
		--	cube.state = S_STARPOSTCUBEFLASH
		--end

		cube.momx = mo.momx
		cube.momy = mo.momy
		cube.momz = mo.momz

		cube.angle = ang + ANGLE_90
	end
	amountposts = max(amountposts, mo.health)
	if (mo.thearrow == nil) then
		mo.thearrow = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_SPARROW)
		mo.thearrow.angle = mo.angle
		mo.thearrow.colorized = true
		mo.thearrow.color = SKINCOLOR_WHITE
		mo.thearrow.flashing = false
		mo.thearrow.flash = 0
	end
	if ((mo.state == S_STARPOST_SPIN) or (mo.state == S_STARPOST_FLASH)) and mo.thearrow and mo.thearrow.valid then
		mo.thearrow.flashing = true
	end
	if mo.thearrow and mo.thearrow.valid and (mo.thearrow.flashing) then
		if (mo.thearrow.flash == 0) or (mo.thearrow.flash == 3) or (mo.thearrow.flash == 2) then
			mo.thearrow.color = SKINCOLOR_ORANGE
		elseif (mo.thearrow.flash == 1) then
			mo.thearrow.color = SKINCOLOR_YELLOW
		else
			mo.thearrow.color = SKINCOLOR_RED
		end

		mo.thearrow.flash = $1 + 1
		if (mo.thearrow.flash == 6) then
			mo.thearrow.flash = 0
		end
	end

end, MT_STARPOST)

addHook("MobjRemoved", function(mo)
	if mo.cube then
		TBSlib.removeMobjArray(mo.cube)
	end
end, MT_STARPOST)

freeslot(
	"SPR_MRCE_STARPOSTARROW",
	"MT_SPARROW",
	"S_SPARROW"
)

mobjinfo[MT_SPARROW] = {
	doomednum = -1,
	spawnstate = S_SPARROW,
	flags = MF_NOCLIP|MF_SCENERY
}

states[S_SPARROW] = {
	sprite = SPR_MRCE_STARPOSTARROW,
	frame = TR_TRANS30|FF_PAPERSPRITE|A,
	tics = -1,
	nextstate = S_NULL
}

local function drawsphud(v, player, cam)
	if not (mrce.debugmode & 1) then return end
	if (player.starhudtimer) and (player.startouch) and (player.starpostnum) and (amountposts > 0) then
		local sphud
		local sphud1
		local sphud2
		if (v.patchExists("SPHUD")) then
			sphud = v.cachePatch("SPHUD")
		end
		if (v.patchExists("SPHUD1")) then
			sphud1 = v.cachePatch("SPHUD1")
		end
		if (v.patchExists("SPHUD2")) then
			sphud2 = v.cachePatch("SPHUD2")
		end

		local xscroll = max(player.starhudtimer - 99, 0)
		if (player.starhudtimer <= 7) then
			xscroll =  8 - player.starhudtimer
		end

		local xoffset = 9 * xscroll

		local drawamt = 0
		local scroll = 0
		if (amountposts >= 5) then
			scroll = max(0, player.startouch - 4) * -36
		end

		while(drawamt < amountposts) do
			local screenx = 277
			local screeny = 18
			local yoffset = drawamt * 36

			drawamt = $1 + 1
			local transpr = V_50TRANS
			if (drawamt == player.startouch) then
				transpr = V_10TRANS
			end
			if (drawamt <= player.starpostnum) then
				v.draw(screenx + 15 + xoffset, screeny + yoffset + scroll, sphud, transpr | V_SNAPTORIGHT | V_SNAPTOTOP, nil)
			else
				transpr = V_70TRANS
				v.draw(screenx + 15 + xoffset, screeny + yoffset + scroll, sphud1, transpr | V_SNAPTORIGHT | V_SNAPTOTOP, nil)
			end
			if (drawamt < amountposts) then
				v.draw(screenx + 15 + xoffset, screeny + yoffset + scroll + 18, sphud2, V_50TRANS | V_SNAPTORIGHT | V_SNAPTOTOP, nil)
			end
			v.drawNum(screenx + xoffset, screeny + yoffset + scroll - 5, drawamt, transpr | V_SNAPTORIGHT | V_SNAPTOTOP)
		end
	end
end

addHook("TouchSpecial", function(special, mo)
	if mo.player and (mrce.debugmode & 1) then
		mo.player.startouch = special.health

		if (mo.player.starhudtimer == nil) then
			mo.player.starhudtimer = 107
		elseif (mo.player.starhudtimer == 0) then
			mo.player.starhudtimer = 107
		elseif (mo.player.starhudtimer < 100) then
			mo.player.starhudtimer = min($1 + 2, 100)
		end
	end
end, MT_STARPOST)

addHook("PlayerThink", function(p)
	if not (mrce.debugmode & 1) then return end
	p.starhudtimer = $ and $ - 1 or 0
end)

hud.add(drawsphud, "game")