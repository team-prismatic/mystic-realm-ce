freeslot("MT_ICICLE", "sfx_s1dfal")

sfxinfo[sfx_s1dfal] = {
	singular = false,
	caption = "Falling icicle"
}

local DIST_REACH = 1024 << FRACBITS

function A_SpawnIcicle(actor)
	if actor and actor.valid then
		--[[
			Radicalicious 01/05/2023:
			Midnight Freeze is a very hardware-intensive map, so let's
			use efficient blockmap searching to not spawn any unnecessary
			ice objects when players aren't around.
			Skydusk 01/30/2024:
			WHY DO YOU NEED SUCH A BIG SEARCH AREA?!!!!
			Whatever... Created new function for it.
		--]]
		if IsPlayerAroundBool(actor, DIST_REACH, 2) then

			if not (leveltime % TICRATE) then
				local waterdrop = P_SpawnMobjFromMobj(actor, 0, 0, 0, MT_WATERDROP)
			end


			if not (leveltime % (TICRATE << 1 + actor.angle/ANG1)) then
				-- Flip for each
				local bool_flip = (P_MobjFlip(actor) == 1)

				local spike = P_SpawnMobj(
					actor.x,
					actor.y,
					bool_flip and actor.ceilingz or actor.floorz,
				MT_ICICLE)

				spike.state = spike.info.spawnstate
				spike.scale = FRACUNIT << 1
				spike.shadowscale = spike.scale

				if not bool_flip then
					spike.flags2 = $|MF2_OBJECTFLIP
				end
			end
		end
	end
end

local ICICLE_SPEED = 5 << FRACBITS
local FA_ANGLE = ANGLE_180 >> 1

addHook("MobjThinker", function(mobj)
	if mobj and mobj.valid then
		if P_IsObjectOnGround(mobj) then
			for i = 0, 8, 1 do
				local fa = i*FA_ANGLE
				local shatter = P_SpawnMobj(mobj.x, mobj.y, mobj.z+FRACUNIT, MT_ROCKCRUMBLE9)
				shatter.momx = 5*sin(fa)
				shatter.momy = 5*cos(fa)
				P_SetObjectMomZ(shatter, ICICLE_SPEED, false)
				shatter.scale = FRACUNIT
				shatter.renderflags = $ | RF_FULLBRIGHT
			end
			S_StartSound(mobj, sfx_shattr)
			P_RemoveMobj(mobj)
		end
	end
end, MT_ICICLE)