freeslot("SPR_MRCE_VALVEWHEEL", "MT_VALVEWHEEL", "S_VWHEELINIT", "S_VWHEELSTOP", "S_VWHEELROT1", "S_VWHEELROT2", "S_VWHEELROT3", "S_VWHEELROT4")

mobjinfo[MT_VALVEWHEEL] = 
{
	--$Name Valve Wheel
	--$Category Flame Rift
	--$Arg0 Linedef Executor Tag
	--Arg0Type 15
	--$Arg1 Rate of change WIP BEHAVIOR
	
	doomednum = 3902,
	spawnstate = S_VWHEELINIT,
	spawnhealth = 1000,
	radius = 45*FRACUNIT,
	height = 70*FRACUNIT,
	flags = MF_PAPERCOLLISION|MF_SOLID|MF_NOGRAVITY|MF_SPECIAL
}

states[S_VWHEELINIT] = {
        sprite = SPR_MRCE_VALVEWHEEL,
		frame = A|FF_PAPERSPRITE,
		tics = 1,
		nextstate = S_VWHEELSTOP
}

states[S_VWHEELSTOP] = {
        sprite = SPR_MRCE_VALVEWHEEL,
		frame = A|FF_PAPERSPRITE,
		tics = 100,
		nextstate = S_VWHEELSTOP
}

states[S_VWHEELROT1] = {
        sprite = SPR_MRCE_VALVEWHEEL,
		frame = A|FF_PAPERSPRITE,
		tics = 2,
		nextstate = S_VWHEELROT2
}

states[S_VWHEELROT2] = {
        sprite = SPR_MRCE_VALVEWHEEL,
		frame = B|FF_PAPERSPRITE,
		tics = 2,
		nextstate = S_VWHEELROT3
}

states[S_VWHEELROT3] = {
        sprite = SPR_MRCE_VALVEWHEEL,
		frame = C|FF_PAPERSPRITE,
		tics = 2,
		nextstate = S_VWHEELROT4
}

states[S_VWHEELROT4] = {
        sprite = SPR_MRCE_VALVEWHEEL,
		frame = D|FF_PAPERSPRITE,
		tics = 2,
		nextstate = S_VWHEELROT1
}


local function wheelSpin(mo, toucher)
	mo.state = S_VWHEELROT1
	mo.wheelSpinRate = 10
	mo.wheelTime = leveltime
	return true
end
addHook("TouchSpecial", wheelSpin, MT_VALVEWHEEL)



local function wheelThinker(mo)
	if mo.state == S_VWHEELSTOP then return end

	
	if not mo.wheelTime then return end
	if leveltime > mo.wheelTime + TICRATE/4 and mo.wheelSpinRate > 0 then
		mo.wheelSpinRate = $ - 1
		mo.wheelTime = leveltime
		
		P_LinedefExecute(mo.spawnpoint.args[0])
		S_StartSound(mo, 283)

	end
	
	if mo.wheelSpinRate <= 0 then 
		mo.state = S_VWHEELSTOP
	end
	
end
addHook("MobjThinker", wheelThinker, MT_VALVEWHEEL)