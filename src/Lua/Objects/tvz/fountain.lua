freeslot(
	"MT_GOOPFOUNTAIN"
)

mobjinfo[MT_GOOPFOUNTAIN] = {
	--$Category Tempest Valley
	--$Name Goop fountain
	--$Sprite SLGPA0
	--$Color 3
	--$Arg0 Interval/split time
	--$Arg0Default 50
	--$Arg1 Power
	--$Arg1Default 8
	--$Arg2 Amount of branches
	--$Arg2Default 12
	--$Arg3 Goop Type
	--$Arg3Type 11
	--$Arg3Enum { 0="Purple"; 1="Blue"; 2="Mixed"; }
	--$Arg4 Rotate Speed
	--$Arg5 Burst Duration
	--$Arg5Default 20

	doomednum = 2228,
	spawnstate = S_INVISIBLE,
	spawnhealth = 1000,
	reactiontime = 8,
	speed = 3200*FRACUNIT,
	radius = 16*FRACUNIT,
	height = 16*FRACUNIT,
	dispoffset = 1,
	mass = 100,
	flags = MF_NOGRAVITY
}

addHook("MapThingSpawn", function(mo,th)		-- Setup
	mo.splittimer = th.args[0]*2			-- Arg0 Interval/split time
	mo.bursttimer = th.args[5] or 20		-- Arg5 Burst Duration
	return true
end, MT_GOOPFOUNTAIN)


addHook("MobjThinker", function(mo)
	local d = mo.spawnpoint.args[0]				-- Arg0 Interval/split time
	local p = mo.spawnpoint.args[1]*FRACUNIT	-- Arg1 Power
	local b = mo.spawnpoint.args[2]				-- Arg2 Amount of branches
	local m = mo.spawnpoint.args[3]				-- Arg3 Goop Type
	local a = mo.spawnpoint.args[4]				-- Arg4 Rotate Speed
	local burstDuration = mo.spawnpoint.args[5] or 20 -- Arg5 Burst Duration

	local tmr = mo.splittimer
	local burst = mo.bursttimer
	local goop
	local angle

	-- Handle Interval Timer
	if tmr then
		mo.splittimer = $-1
	else
		mo.splittimer = d*2 -- Reset interval timer
		mo.bursttimer = burstDuration -- Reset burst timer for next burst
	end

	-- Decrement Burst Timer (only while active)
	if burst > 0 then
		mo.bursttimer = $-1
	else
		return -- Stop spewing if burst timer runs out
	end

	-- Check if a player is near
	local isPlayerNear = false
	for aPlayer in players.iterate do
		if aPlayer.valid and aPlayer then
			if R_PointToDist2(aPlayer.mo.x, aPlayer.mo.y, mo.x, mo.y) < 1250*FRACUNIT then
				isPlayerNear = true
				break
			end
		end
	end
	if not isPlayerNear then
		return
	end

	-- Create goop during bursts
	if (tmr <= d) and not (tmr % 5) then
		for i = 1, b do
			angle = mo.angle + FixedAngle((360/b)*i*FRACUNIT)
			if m == 0 or (m == 2 and i%2) then
				goop = P_SpawnMobjFromMobj(mo, 0, 0, p/4*3+1, MT_SLOWGOOP)
			else
				goop = P_SpawnMobjFromMobj(mo, 0, 0, p/4*3+1, MT_GOOP)
			end
			goop.fountain = true
			goop.momx = FixedMul(cos(angle), p)
			goop.momy = FixedMul(sin(angle), p)
			goop.momz = p/4*3
		end
		mo.angle = $ - FixedAngle(a*FRACUNIT) -- Rotate the fountain
	end
end, MT_GOOPFOUNTAIN)



local function FountainGoop(goop)
	if goop.fountain then -- If the goop comes from a fountain, remove it when it hits ground.
		if goop.z-goop.floorz <= abs(goop.momz) then -- Remove it earlier to prevent blue goop from making sounds.
			P_RemoveMobj(goop)
			return
		end
		if goop.momz <= 0 then -- Tweak the move trail of goop.
			goop.momx = $/25*23
			goop.momy = $/25*23
		end
	end
end
addHook("MobjThinker", FountainGoop, MT_GOOP)
addHook("MobjThinker", FountainGoop, MT_SLOWGOOP)