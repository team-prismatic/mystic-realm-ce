--Script provided by Skydusk from PKZ


freeslot(
"SPR_MRCE_SMASHABLE_VASES",
"MT_SM2POT",
"MT_SM2BIGPOT",
"S_SM2POT",
"S_SM2BIGPOT")

local function breakpotcollide(mo, toucher)
    local smashingSpeed = 20 * FRACUNIT
    if toucher.player then
        local player = toucher.player
        local realspeedxyz = player.mrce and player.mrce.realspeedxyz or 0

        if (toucher.z <= (mo.z + mo.height)) and 
           (toucher.z + toucher.height >= mo.z) and 
           (realspeedxyz >= smashingSpeed or toucher.state == S_PLAY_ROLL) then

            -- Smash the vase
            S_StartSound(mo, sfx_shattr)
            P_KillMobj(mo, toucher)
        end
    end
end


addHook("MobjCollide", breakpotcollide, MT_SM2POT)
addHook("MobjCollide", breakpotcollide, MT_SM2BIGPOT)

local framesmpiecestable = {H, G, F, E}
addHook("MobjDeath", function(a, t)
	for i = 1,4 do
		local ang = i*ANGLE_90 + ANG1*P_RandomRange(-30,30)
		local debries = P_SpawnMobjFromMobj(a, 8*cos(ang), 8*sin(ang), mobjinfo[a.type].height / 2, MT_FLINGRING)
		P_SetObjectMomZ(debries, P_RandomRange(3, 6)*FRACUNIT)
		debries.fuse = 5*TICRATE
		debries.angle = ang
		P_InstaThrust(debries, ang, P_RandomRange(4, 8)*FRACUNIT)
	end
end, MT_SM2POT)


local framepiecestable = {D, D, C, C, B, B, A, A}
addHook("MobjDeath", function(a, t)
	for i = 1,8 do
		local ang = i*ANGLE_90 + ANG1*P_RandomRange(-30,30)
		local debries = P_SpawnMobjFromMobj(a, 8*cos(ang), 8*sin(ang), mobjinfo[a.type].height / 2, MT_FLINGRING)
		P_SetObjectMomZ(debries, P_RandomRange(3, 6)*FRACUNIT)
		debries.fuse = 5*TICRATE
		debries.angle = ang
		P_InstaThrust(debries, ang, P_RandomRange(4, 8)*FRACUNIT)
	end
end, MT_SM2BIGPOT)





-----------Object Definitions-----------

-- SMB2 Big Pot Mobj Info
mobjinfo[MT_SM2BIGPOT] = {
--$Category Tempest Valley
--$Name Big Smashable Vase
--$Sprite SPR_MRCE_SMASHABLE_VASES
doomednum = 2306,
spawnstate = S_SM2BIGPOT,
--deathsound = sfx_zelda2,
spawnhealth = 1000,
reactiontime = 8,
radius = 16*FRACUNIT,
height = 95*FRACUNIT,
mass = 100,
flags = MF_SOLID|MF_PUSHABLE
}

--Big Pot States

states[S_SM2BIGPOT] = {
sprite = SPR_MRCE_SMASHABLE_VASES,
frame = A,
tics = -1
}

--SMB2 Small Pot Mobj Info

mobjinfo[MT_SM2POT] = {
--$Category Tempest Valley
--$Name Small Smashable Vase
--$Sprite SPR_MRCE_SMASHABLE_VASES
doomednum = 2307,
spawnstate = S_SM2POT,
spawnhealth = 1000,
--deathsound = sfx_zelda2,
reactiontime = 8,
radius = 16*FRACUNIT,
height = 32*FRACUNIT,
mass = 100,
flags = MF_SOLID|MF_PUSHABLE
}

--Small Pot States

states[S_SM2POT] = {
sprite = SPR_MRCE_SMASHABLE_VASES,
frame = B,
tics = -1
}