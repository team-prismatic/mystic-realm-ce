freeslot("MT_SLIMESHOWERVINE", "S_SHWBUSH")

mobjinfo[MT_SLIMESHOWERVINE] = {
	--$Category Tempest Valley
	--$Name Slime Shower Vines
	--$Sprite TBSHA0
	--$Color 3
	--$Arg0 Interval/split time
	--$Arg0Default 50
	--$Arg1 Power
	--$Arg1Default 8
	--$Arg2 Amount of branches
	--$Arg2Default 12
	--$Arg3 Goop Type
	--$Arg3Type 11
	--$Arg3Enum { 0="Purple"; 1="Blue"; 2="Mixed"; }
	--$Arg4 Rotate Speed
	--$Arg5 Burst Duration
	--$Arg5Default 20

	doomednum = 4125,
	spawnstate = S_SHWBUSH,
	spawnhealth = 1000,
	reactiontime = 8,
	speed = 3200*FRACUNIT,
	radius = 30*FRACUNIT,
	height = 15*FRACUNIT,
	dispoffset = 1,
	mass = 100,
	flags = MF_NOCLIP
}

states[S_SHWBUSH] = {
	sprite = SPR_TBSH,
	frame = A,
	tics = -1,
	nextstate = S_SHWBUSH
}


--=====================================--



local function initializeObject(mo, th)
	mo.splittimer = th.args[0]*2			-- Arg0 Interval/split time
	mo.bursttimer = th.args[5] or 20		-- Arg5 Burst Duration
	return true
end
addHook("MapThingSpawn", initializeObject, MT_SLIMESHOWERVINE)


local function slimePlantThinker(mo)
    local d = mo.spawnpoint.args[0] -- Arg0 Interval/split time
    local p = mo.spawnpoint.args[1] * FRACUNIT -- Arg1 Power
    local b = mo.spawnpoint.args[2] -- Arg2 Amount of branches
    local m = mo.spawnpoint.args[3] -- Arg3 Goop Type
    local a = mo.spawnpoint.args[4] -- Arg4 Rotate Speed
    local burstDuration = mo.spawnpoint.args[5] or 20 -- Arg5 Burst Duration

    local tmr = mo.splittimer
    local burst = mo.bursttimer or 0 -- Default to 0 if not initialized
    local goop
    local angle

    -- Pulsating animation variables
    local pulsateSpeed = FRACUNIT * 30 -- Speed of pulsation
    local baseScale = FRACUNIT        -- Normal scale
    local scaleVariation = FRACUNIT / 45 -- How much the size fluctuates

    -- Handle Interval Timer
    if tmr then
        mo.splittimer = $ - 1
    else
        mo.splittimer = d * 2 -- Reset interval timer
        mo.bursttimer = burstDuration -- Reset burst duration
    end

    -- Decrement Burst Timer (only while active)
    if burst > 0 then
        mo.bursttimer = $ - 1
    end

    -- Check if a player is near
    local isPlayerNear = false
    for aPlayer in players.iterate do
        if aPlayer.valid and aPlayer then
            if R_PointToDist2(aPlayer.mo.x, aPlayer.mo.y, mo.x, mo.y) < 1250 * FRACUNIT then
                isPlayerNear = true
                break
            end
        end
    end

    -- Reset scale to normal if not bursting
    if not isPlayerNear or burst <= 0 then
        mo.spritexscale = baseScale
        mo.spriteyscale = baseScale
        return -- Skip further logic if not bursting
    end

    -- Pulsating effect during bursts
    local scaleFactor = baseScale + FixedMul(sin(FixedAngle(leveltime * pulsateSpeed)), scaleVariation)
    mo.spritexscale = scaleFactor
    mo.spriteyscale = scaleFactor

    -- Create goop during bursts
    if (tmr <= d) and not (tmr % 5) then
        for i = 1, b do
            angle = mo.angle + FixedAngle((360 / b) * i * FRACUNIT)
            if m == 0 or (m == 2 and i % 2) then
                goop = P_SpawnMobjFromMobj(mo, 0, 0, (p / 4 * 3 + 1) + 200 * FRACUNIT, MT_SLOWGOOP)
            else
                goop = P_SpawnMobjFromMobj(mo, 0, 0, (p / 4 * 3 + 1) + 200 * FRACUNIT, MT_GOOP)
            end
            goop.fountain = true
            goop.momx = FixedMul(cos(angle), p)
            goop.momy = FixedMul(sin(angle), p)
            goop.momz = p / 4 * 3
        end
        mo.angle = $ - FixedAngle(a * FRACUNIT) -- Rotate the fountain
    end
end
addHook("MobjThinker", slimePlantThinker, MT_SLIMESHOWERVINE)



--Removing the goop from ground is handled in fountain.lua (Will probably remove fountain.lua in favor of this object)