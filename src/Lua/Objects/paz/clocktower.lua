freeslot(
    "SPR_MRCE_PAZ_CLOCKTOWERHANDS",
    "S_MRCE_PAZCLOCKTOWERHANDHOUR",
    "S_MRCE_PAZCLOCKTOWERHANDMINUTE",
    "S_MRCE_PAZCLOCKTOWERHANDSECOND",
    "MT_MRCE_PAZ_CLOCKTOWERHAND",
    "MT_MRCE_PAZ_CLOCKTOWERHANDSPAWN"
)

mobjinfo[MT_MRCE_PAZ_CLOCKTOWERHAND] = {
	radius = 32*FRACUNIT,
	height = 64*FRACUNIT,
	flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT
}

mobjinfo[MT_MRCE_PAZ_CLOCKTOWERHANDSPAWN] = {
    doomednum = 2229,
    spawnstate = S_INVISIBLE,
	radius = 16*FRACUNIT,
	height = 32*FRACUNIT,
	flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT
}

states[S_MRCE_PAZCLOCKTOWERHANDMINUTE] = {
	sprite = SPR_MRCE_PAZ_CLOCKTOWERHANDS,
	frame = A|FF_PAPERSPRITE,
	tics = -1,
	nextstate = S_MRCE_PAZCLOCKTOWERHANDMINUTE
}

states[S_MRCE_PAZCLOCKTOWERHANDHOUR] = {
	sprite = SPR_MRCE_PAZ_CLOCKTOWERHANDS,
	frame = B,
	tics = -1,
	nextstate = S_MRCE_PAZCLOCKTOWERHANDHOUR
}

addHook("MobjSpawn", function(mo)
    local num = ANG1 / 2
    local h1, h2 = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_MRCE_PAZ_CLOCKTOWERHAND), P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_MRCE_PAZ_CLOCKTOWERHAND)
    h1.state = S_CLOCK_OVERLAY_MIN
    h1.scale = mo.scale*3
    h1.frame = $|FF_PAPERSPRITE
    --h1.colorized
    --h1.rollangle = -1*FixedAngle(mrce.globalServerTime.min*FRACUNIT*6)
    h1.angle = mo.angle
    h2.state = S_CLOCK_OVERLAY_HR
    h2.scale = mo.scale*3
    h2.frame = $|FF_PAPERSPRITE
    --h2.rollangle = -1*(FixedAngle((mrce.globalServerTime.hour % 12)*FRACUNIT*30) + min(ANG30, num * mrce.globalServerTime.min))
    h2.angle = mo.angle
    mo.hand1 = h1
    mo.hand2 = h2
end, MT_MRCE_PAZ_CLOCKTOWERHANDSPAWN)

addHook("MobjThinker", function(mo)
    if not (mo and mo.valid and mo.hand1 and mo.hand1.valid and mo.hand2 and mo.hand2.valid) then return end
    local num = ANG1 / 2
    --mo.hand1.rollangle = -1*FixedAngle(mrce.globalServerTime.min*FRACUNIT*6)
    --mo.hand2.rollangle = -1*(FixedAngle((mrce.globalServerTime.hour % 12)*FRACUNIT*30) + min(ANG30, num * mrce.globalServerTime.min))
end, MT_MRCE_PAZ_CLOCKTOWERHANDSPAWN)