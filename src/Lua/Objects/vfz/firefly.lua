freeslot(
"SPR_MRCE_VFZ_FIREFLY",
"MT_FIREFLYVFZ",
"S_FIREFLY1",
"S_FIREFLY2")

local debug = 0


local function fireflyMovement(mo)
    if not mo.valid then return end

    -- Random angle change for more dynamic movement
    local angleChange = P_RandomRange(-20, 20) * ANG1  -- More variation for balanced movement
    mo.angle = mo.angle + angleChange

    -- Slightly randomized speed for natural variance
    local baseSpeed = 10 * FRACUNIT
    local speedVariance = P_RandomRange(-10, 10) * FRACUNIT
    local speed = baseSpeed + speedVariance

    -- Calculate the movement based on the updated angle
    mo.momx = FixedMul(cos(mo.angle), speed)
    mo.momy = FixedMul(sin(mo.angle), speed)

    -- Improved up-and-down motion for a fluttering effect
    local flutterVariance = P_RandomRange(-5, 5) * FRACUNIT
    mo.momz = mo.momz + flutterVariance

    -- Ensure the firefly stays within a certain distance from its spawn point
    local maxDistance = 128 * FRACUNIT
    local dist = R_PointToDist2(mo.homeX, mo.homeY, mo.x, mo.y)

    if dist > maxDistance then
        -- Move back towards the home point, but with some randomness for natural behavior
        local angleToHome = R_PointToAngle2(mo.x, mo.y, mo.homeX, mo.homeY)
        local returnAngleChange = P_RandomRange(-10, 10) * ANG1
        mo.angle = angleToHome + returnAngleChange

        -- Slow down slightly when returning to the home point
        mo.momx = FixedMul(cos(mo.angle), speed / 2)
        mo.momy = FixedMul(sin(mo.angle), speed / 2)
    end


	-- Move away from the player quickly and erratically
	local safeDistance = 64 * FRACUNIT
	for p in players.iterate do
		if p and p.valid then
			if R_PointToDist2(mo.x, mo.y, p.mo.x, p.mo.y) < safeDistance then
				if debug then
					print("Too close!")
				end
				local angleAwayFromPlayer = R_PointToAngle2(mo.x, mo.y, p.mo.x, p.mo.y) * -1
				mo.angle = angleAwayFromPlayer + P_RandomRange(-20, 20) * ANG1
				local escapeSpeed = baseSpeed * 2
				mo.momx = FixedMul(cos(mo.angle), escapeSpeed)
				mo.momy = FixedMul(sin(mo.angle), escapeSpeed)
				return
			end
		end
	end
end







addHook("MobjSpawn", function(mo)

	--Set origin point
	mo.homeX = mo.x
    mo.homeY = mo.y
    mo.homeZ = mo.z

	mo.fireflyLightcornea = P_SpawnMobjFromMobj(mo, mo.x, mo.y, mo.z, MT_CORONA)
	mo.fireflyLightcornea.target = mo
	mo.fireflyLightcornea.frame = FF_FULLBRIGHT|FF_ADD
	mo.fireflyLightcornea.dispoffset = 1
end, MT_FIREFLYVFZ)








mobjinfo[MT_FIREFLYVFZ] = {
	--$Name Firefly
	--$Category Verdant Forest
	--$Sprite FFLYA1

	doomednum = 592,
	spawnstate = S_FIREFLY1,
	spawnhealth = 1000,
	radius = 12*FRACUNIT,
	height = 24*FRACUNIT,
	flags = MF_NOGRAVITY|MF_NOCLIPTHING|MF_SCENERY
}

states[S_FIREFLY1] = {
	sprite = SPR_MRCE_VFZ_FIREFLY,
	frame = FF_FULLBRIGHT|A,
	tics = 5,
	action = fireflyMovement,
	nextstate = S_FIREFLY2
}


states[S_FIREFLY2] = {
	sprite = SPR_MRCE_VFZ_FIREFLY,
	frame = FF_FULLBRIGHT|B,
	tics = 5,
	action = fireflyMovement,
	nextstate = S_FIREFLY1
}