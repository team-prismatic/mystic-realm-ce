freeslot(
	"S_JCZCAKE1",
	"S_JCZCAKE2",
	"SPR_MRCE_JCZCAKE",
	"sfx_cake"
)

mobjinfo[MT_JCZCAKE] = {
--$Name JCZ Cake
--$Category Mystic Realm Special
--$Sprite CAKEA0
--$NotAngled
doomednum = 2333,
spawnstate = S_JCZCAKE1,
spawnhealth = 1000,
radius = 20*FRACUNIT,
height = 24*FRACUNIT,
speed = 8,
flags = MF_SOLID|MF_PUSHABLE
}

states[S_JCZCAKE1].sprite = SPR_MRCE_JCZCAKE
states[S_JCZCAKE1].frame = A
states[S_JCZCAKE1].tics = -1
states[S_JCZCAKE1].nextstate = S_NULL

states[S_JCZCAKE2].sprite = SPR_MRCE_JCZCAKE
states[S_JCZCAKE2].frame = A
states[S_JCZCAKE2].tics = -1
states[S_JCZCAKE2].nextstate = S_NULL

states[S_JCZCAKE3].sprite = SPR_MRCE_JCZCAKE
states[S_JCZCAKE3].frame = A|FF_ANIMATE
states[S_JCZCAKE3].var1 = 6
states[S_JCZCAKE3].var2 = 1
states[S_JCZCAKE3].tics = 7
states[S_JCZCAKE3].nextstate = S_JCZCAKE4

states[S_JCZCAKE4].sprite = SPR_MRCE_JCZCAKE
states[S_JCZCAKE4].frame = G
states[S_JCZCAKE4].tics = -1
states[S_JCZCAKE4].nextstate = S_NULL

addHook("MobjThinker", function(mo)
	mo.shadowscale = 5*FRACUNIT/4

	if (mo.state == S_JCZCAKE2)
	and P_IsObjectOnGround(mo) == true then
		mo.state = S_JCZCAKE3
		S_StartSound(mo, sfx_cake)
		mo.height = 17*FRACUNIT
	end
	if P_IsObjectOnGround(mo) == false
	and not (mo.state == S_JCZCAKE3) and not (mo.state == S_JCZCAKE4) then
		mo.state = S_JCZCAKE2
	end
end, MT_JCZCAKE)