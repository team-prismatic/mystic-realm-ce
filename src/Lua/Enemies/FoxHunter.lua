--[[
if pcall(do return _G["MT_FOXHUNTER"] end) then
    print("\131NOTICE:\128 Fox Hunter objects already loaded.")
    return
end


local function lerp (var1, var2, t)
	var1 = FixedMul($1 * FRACUNIT, FRACUNIT-t)
	var2 = FixedMul($1 * FRACUNIT, t)

	local r = var1+var2

	--r = $1/FRACUNIT
	return r
end]]

freeslot(
	"MT_FOXHUNTER",
	"S_FOXHUNTERLOOK0",
	"S_FOXHUNTERLOOK1",
	"S_FOXHUNTERAIM0",
	"S_FOXHUNTERSHOOT0",

	"MT_SNIPERSHOT",
	"S_SNIPERSHOT",

	"MT_HUNTERCORONA",
	"S_HUNTERCORONA",

	"SPR_FHUN"
)

function A_SniperShot(actor, var1, var2)

	for i = 0, 256 do
		P_TeleportMove(actor, actor.x+actor.momx, actor.y+actor.momy, actor.z+actor.momz)

		if not (actor) then return end

		if not (P_TryMove(actor, actor.x, actor.y, true)) then
			actor.state = S_NULL
			return
		end

		P_SpawnMobj(actor.x, actor.y, actor.z, MT_SPINDUST)
	end

	actor.state = S_NULL

end

function A_HunterShoot(actor, var1, var2)
	A_PlayAttackSound(actor, 0, 0)

	local shot = P_SpawnPointMissile(actor,
		actor.aimspot.x+(P_RandomRange(-16, 16)*FRACUNIT),
		actor.aimspot.y+(P_RandomRange(-16, 16)*FRACUNIT),
		actor.aimspot.z+(P_RandomRange(-16, 16)*FRACUNIT),
		MT_SNIPERSHOT,
		actor.x, actor.y, actor.z+actor.height/2)

	A_FaceTarget(actor, 0, 0)

	actor.reactiontime = $-1

	if (actor.reactiontime <= 0) then
		actor.reactiontime = mobjinfo[actor.type].reactiontime
		actor.state = mobjinfo[actor.type].spawnstate
	end
end

function A_HunterAim(actor, var1, var2)

	if not (actor.target) then
		actor.state = mobjinfo[actor.type].spawnstate
		return
	end

	if (actor.target.z < actor.z) then
		actor.state = mobjinfo[actor.type].spawnstate
		actor.reactiontime = mobjinfo[actor.type].reactiontime
		return
	end
	A_FaceTarget(actor, 0, 0)
	actor.reactiontime = $-1
	if (actor.reactiontime <= 0) then
		actor.reactiontime = 4
		actor.state = mobjinfo[actor.type].missilestate
		return

	end


	if (actor.aimspot) then
		if (actor.reactiontime > var2) then
			actor.aimspot.x = ($/2)+((actor.target.x+(actor.target.momx*var1))/2)
			actor.aimspot.y = ($/2)+((actor.target.y+(actor.target.momy*var1))/2)
			actor.aimspot.z = ($/2)+((actor.target.z+(actor.target.height/2)+(actor.target.momz*var1))/2)
		end
	else
		actor.aimspot = {
			x = actor.target.x+actor.target.momx*var1,
			y = actor.target.y+actor.target.momy*var1,
			z = actor.target.z+actor.target.momz*var1
		}
	end

	local distx = (actor.x-actor.aimspot.x)/8
	local disty = (actor.y-actor.aimspot.y)/8
	local distz = (actor.z-actor.aimspot.z)/8
	for i = 0, 32 do
		local beam = P_SpawnMobj(actor.x - (distx*i), actor.y - (disty*i), actor.z - (distz*i) + (actor.height/2),  MT_CYBRAKDEMON_TARGET_DOT)
		if not beam then continue end
		if (actor.reactiontime > var2) then
			beam.scale = (abs(actor.reactiontime % 10)*(FRACUNIT/10))
		else
			beam.scale = FRACUNIT*2
		end
	end
end

function A_HunterCoronaChase(actor, var1, var2)
	if not (actor.target) then
		actor.state = S_NULL
		return
	end
	if (actor.target.health < 1) or not (actor.valid) then
		actor.state = S_NULL
		return
	end
	P_TeleportMove(actor, actor.target.x, actor.target.y, actor.target.z+(32*FRACUNIT))
	actor.scale = ((65-actor.target.reactiontime)*FRACUNIT)/65
	if (actor.target.reactiontime < 65) then
		actor.scale = $+((FRACUNIT/20)*(actor.target.reactiontime%3))
	end


end

---@diagnostic disable-next-line
mobjinfo[MT_SNIPERSHOT] = {
	spawnstate = S_SNIPERSHOT,
	speed = 80*FRACUNIT,
	radius = 16*FRACUNIT,
	height = 16*FRACUNIT,
	spawnhealth = 1,
	dispoffset = 0,
	flags =MF_NOBLOCKMAP|MF_MISSILE|MF_NOGRAVITY
}

---@diagnostic disable-next-line
states[S_SNIPERSHOT] = {
	sprite = SPR_NULL,
	frame = A,
	tics = 4,
	action = A_SniperShot,
	nextstate = S_SNIPERSHOT
}

---@diagnostic disable-next-line
mobjinfo[MT_FOXHUNTER] = {
	--$Name Fox Hunter
	--$Category Enemies/Starlit Skyway
	--$Sprite FHUNA1
	spawnstate = S_FOXHUNTERLOOK0,
	seestate = S_FOXHUNTERAIM0,
	missilestate = S_FOXHUNTERSHOOT0,
	deathstate = S_XPLD_FLICKY,
	attacksound = sfx_s3k4d,
	deathsound = sfx_pop,
	doomednum = 1512,
	reactiontime = 65,
	speed = 6,
	radius = 20*FRACUNIT,
	height = 60*FRACUNIT,
	damage = 9,
	spawnhealth = 1,
	dispoffset = 0,
	flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE|MF_NOGRAVITY
}

---@diagnostic disable-next-line
states[S_FOXHUNTERLOOK0] = {
	sprite = SPR_FHUN,
	frame = A|FF_ANIMATE|FF_GLOBALANIM,
	tics = 1,
	var1 = 2,
	var2 = 4,
	nextstate = S_FOXHUNTERLOOK1
}

---@diagnostic disable-next-line
states[S_FOXHUNTERLOOK1] = {
	sprite = SPR_FHUN,
	frame = B,
	tics = 0,
	action = A_Look,
	var1 = (3000<<16)+1,
	nextstate = S_FOXHUNTERLOOK0
}

states[S_FOXHUNTERAIM0] = {
	sprite = SPR_FHUN,
	frame = A,
	tics = 1,
	action = A_HunterAim,
	var1 = 20,
	var2 = 10,
	nextstate = S_FOXHUNTERAIM0
}

---@diagnostic disable-next-line
states[S_FOXHUNTERSHOOT0] = {
	sprite = SPR_FHUN,
	frame = A,
	tics = 5,
	action = A_HunterShoot,
	nextstate = S_FOXHUNTERSHOOT0
}

---@diagnostic disable-next-line
mobjinfo[MT_HUNTERCORONA] = {
	spawnstate = S_HUNTERCORONA,
	reactiontime = 16,
	speed = 10*FRACUNIT,
	radius = 32*FRACUNIT,
	height = 32*FRACUNIT,
	damage = 3,
	spawnhealth = 0,
	dispoffset = 1,
	flags = MF_NOBLOCKMAP|MF_NOGRAVITY|MF_SCENERY|MF_NOCLIPHEIGHT
}

---@diagnostic disable-next-line
states[S_HUNTERCORONA] = {
	sprite = SPR_FHUN,
	frame = D|FF_FULLBRIGHT,
	tics = 1,
	action = A_HunterCoronaChase,
	nextstate = S_HUNTERCORONA
}

addHook("MobjSpawn", function(actor)
	local corona = P_SpawnMobj(actor.x, actor.y, actor.z, MT_HUNTERCORONA)
	corona.target = actor
end, MT_FOXHUNTER)

addHook("MobjSpawn", function(actor)
	actor.blendmode = AST_ADD
end, MT_HUNTERCORONA)