freeslot(
    "MT_CANCERMOUSE",
    "MT_TURBOCANCERMOUSE",
    "MT_TURBOCANCERBLARGH",
    "S_CANCERMOUSE_THINK",
    "S_TURBOCANCERMOUSE_THINK",
    "S_TURBOCANCERMOUSE_PAIN",
    "S_TURBOCANCERBLARGH_THINK",
    "S_TURBOCANCERBLARGH_EXPLODE"
)

mobjinfo[MT_CANCERMOUSE] = {
	--$Name Cancerous Rodent
	--$Sprite SHRPA1
	--$Category Bosses/Secret Bosses
	doomednum = 11000,
	spawnstate = S_CANCERMOUSE_THINK,
    seestate = S_NULL,
	painstate = S_NULL,
	deathstate =  S_XPLD_FLICKY,
	xdeathstate = S_NULL,
	meleestate = S_NULL,
	missilestate = S_NULL,
	raisestate = S_NULL,
	painchance = S_NULL,
	spawnhealth = 1,
	mass = 1,
	damage = 0,
	painsound = sfx_None,
	activesound = sfx_None,
	deathsound = sfx_lntdie,
	speed = 1*FRACUNIT,
	radius = 8*FRACUNIT,
	height = 4*FRACUNIT,
	flags = MF_SPECIAL|MF_BOSS|MF_SHOOTABLE
}

mobjinfo[MT_TURBOCANCERMOUSE] = {
	--$Name Very Cancerous Rodent
	--$Sprite CBFSA1
	--$Category Mystic Realm Bosses
	doomednum = 11001,
	spawnstate = S_TURBOCANCERMOUSE_THINK,
    seestate = S_TURBOCANCERMOUSE_THINK,
	painstate = S_TURBOCANCERMOUSE_THINK,
	deathstate =  S_XPLD_FLICKY,
	xdeathstate = S_NULL,
	meleestate = S_NULL,
	missilestate = S_NULL,
	raisestate = S_NULL,
	painchance = 100,
	spawnhealth = 8,
	mass = 111,
	damage = 1,
	painsound = sfx_s3k92,
	activesound = sfx_None,
	deathsound = sfx_lntdie,
	speed = 0*FRACUNIT,
	radius = 96*FRACUNIT,
	height = 164*FRACUNIT,
	flags = MF_SPECIAL|MF_BOSS|MF_SHOOTABLE|MF_SOLID
}

mobjinfo[MT_TURBOCANCERBLARGH] = {
	doomednum = -1,
	spawnstate = S_TURBOCANCERBLARGH_THINK,
	spawnhealth = 1000,
    deathstate = S_TURBOCANCERBLARGH_EXPLODE,
	seesound = 	sfx_s22e,
-- 	reactiontime = 8,
	deathsound = sfx_expld,
	speed = 50*FRACUNIT,
	radius = 32*FRACUNIT,
	height = 32*FRACUNIT,
-- 	mass = 100,
	damage = 32*FRACUNIT,
	flags = MF_NOBLOCKMAP|MF_MISSILE|MF_NOGRAVITY|MF_PAIN
}

states[S_CANCERMOUSE_THINK] = {SPR_SHRP, A, TICRATE, nil, nil, nil, S_CANCERMOUSE_THINK}
states[S_TURBOCANCERMOUSE_THINK] = {SPR_CBFS, A, TICRATE, nil, nil, nil, S_TURBOCANCERMOUSE_THINK}
states[S_TURBOCANCERMOUSE_PAIN] = {SPR_CBFS, A, 1, nil, nil, nil, S_TURBOCANCERMOUSE_THINK}
states[S_TURBOCANCERBLARGH_THINK] = {SPR_PUMA, A, TICRATE, nil, nil, nil, S_TURBOCANCERBLARGH_THINK}
states[S_TURBOCANCERBLARGH_EXPLODE] = {SPR_RCKT, B, 2, A_Explode, 0, 0, S_NULL}

addHook("MobjSpawn", function(boss)
	boss.timer = 2*TICRATE
    boss.burstfire = 3
end, MT_TURBOCANCERMOUSE)

addHook("BossThinker", function(boss)
    if not boss.target then
        if not (P_LookForPlayers(boss, 0, true, false)) then
            return
        end
    else
        A_HomingChase(boss, 1*FRACUNIT/10, 0)
    end
	return false
end, MT_CANCERMOUSE)

addHook("BossThinker", function(boss)
    if not boss then return end
	
	if not boss.target then
        if not (P_LookForPlayers(boss, 0, true, false)) then
            return
        end
    else
        boss.timer = $ - 1
        if boss.timer < 1 then
            if boss.burstfire > 0 then
                A_WhoCaresIfYourSonIsABee(boss, 0+MT_TURBOCANCERBLARGH*FRACUNIT, 48*FRACUNIT+192*FRACUNIT)
                boss.timer = $ + 3
                boss.burstfire = $ - 1
            else
                boss.timer = 3*TICRATE
                boss.burstfire = 3
            end
        end
    end
	return false
end, MT_TURBOCANCERMOUSE)

addHook("MobjThinker", function(blargh)
    if not blargh.target then
        if not (P_LookForPlayers(blargh, 0, true, false)) then
            return
        end
    else
        A_HomingChase(blargh, 10*FRACUNIT, 0)
    end
	return false
end, MT_TURBOCANCERBLARGH)