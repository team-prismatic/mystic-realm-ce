freeslot(
    "S_JUMPERCRAWLA_STND1", 
    "S_JUMPERCRAWLA_STND2", 
    "S_JUMPERCRAWLA_STND3", 
    "S_JUMPERCRAWLA_JUMP1", 
    "S_JUMPERCRAWLA_JUMP2", 
    "S_JUMPERCRAWLA_JUMP3", 
    "S_JUMPERCRAWLA_JUMP4", 
    "S_JUMPERCRAWLA_JUMP5", 
    "S_JUMPERCRAWLA_JUMP6", 
    "S_JUMPERCRAWLA_JUMP7", 
    "S_JUMPERCRAWLA_JUMP8", 
    "S_JUMPERCRAWLA_JUMP9", 
    "S_JUMPERCRAWLA_AMBUSH1", 
    "S_JUMPERCRAWLA_AMBUSH2", 
    "S_JUMPERCRAWLA_AMBUSH3", 
    "S_JUMPERCRAWLA_AMBUSH4", 
    "MT_JUMPERCRAWLA",
	"MT_JUMPROCK",
	"S_JUMPROCK_1",
	"S_JUMPROCK_2",
	"S_JUMPROCK_3",
	"S_JUMPROCK_4",
	"S_JUMPROCK_5"
)

mobjinfo[MT_JUMPERCRAWLA] = {
    --$Name Jumper
	--$Category Enemies/Verdant Forest Zone
	--$Sprite POSSA1
	doomednum = 3102,
	spawnstate = S_JUMPERCRAWLA_STND3,
	spawnhealth = 1,
	seestate = S_JUMPERCRAWLA_JUMP1,	
	meleestate = S_JUMPERCRAWLA_JUMP5,	
	deathstate = S_XPLD1,	
    deathSound = sfx_pop,	
    activesound = sfx_s3k7b,
    speed = 3,
    radius = 24*FRACUNIT,
    height = 32*FRACUNIT,
	flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE|MF_SPAWNCEILING|MF_BOUNCE
}

states[S_JUMPERCRAWLA_STND1] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 1,
	nextstate = S_JUMPERCRAWLA_STND2,
    action = A_Look
}

states[S_JUMPERCRAWLA_STND2] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 1,
	nextstate = S_JUMPERCRAWLA_STND3,
    action = A_ZThrust,
    var1 = 2

}

states[S_JUMPERCRAWLA_STND3] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 1,
	nextstate = S_JUMPERCRAWLA_STND1,
    action = A_SetObjectFlags,
    var1 = MF_NOGRAVITY,
	var2 = 2
}

states[S_JUMPERCRAWLA_JUMP1] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 1,
    nextstate = S_JUMPERCRAWLA_JUMP2
}

states[S_JUMPERCRAWLA_JUMP2] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 2,
	nextstate = S_JUMPERCRAWLA_JUMP3,
    action = A_NapalmScatter,
    var1 = MT_JUMPROCK + (6<<16),
    var2 = 64 + (22<<16)
}

states[S_JUMPERCRAWLA_JUMP3] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 2,
    action = A_FaceTarget,
    nextstate = S_JUMPERCRAWLA_JUMP4
}

states[S_JUMPERCRAWLA_JUMP4] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 10,
	action = function(actor)
		A_BunnyHop(actor,20,10)
		S_StartSound(actor, 118)
	end,
    nextstate = S_JUMPERCRAWLA_JUMP5
}

states[S_JUMPERCRAWLA_JUMP5] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 1,
    action = A_MinusCheck,
    nextstate = S_JUMPERCRAWLA_JUMP6
}

states[S_JUMPERCRAWLA_JUMP6] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 1,
    action = A_CheckHeight,
	var1 = 500<<0,
	var2 = S_JUMPERCRAWLA_JUMP5,
    nextstate = S_JUMPERCRAWLA_JUMP7
}

states[S_JUMPERCRAWLA_JUMP7] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 1,
    action = A_CheckRange,
	var1 = 128+128+128+128+128<<0,
	var2 = S_JUMPERCRAWLA_JUMP8,
    nextstate = S_JUMPERCRAWLA_JUMP5
}


states[S_JUMPERCRAWLA_JUMP8] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 1,
    action = A_ZThrust,
	var1 = -15,
    nextstate = S_JUMPERCRAWLA_JUMP9
}

states[S_JUMPERCRAWLA_JUMP9] = {
    sprite = SPR_POSS,
    frame = A,
    tics = 1,
    action = A_SetObjectFlags,
	var1 = MF_NOGRAVITY,
	var2 = 1,
    nextstate = S_JUMPERCRAWLA_JUMP5
}



--Rocks that come out from jumping

mobjinfo[MT_JUMPROCK] = {
    spawnstate = S_JUMPROCK_1,
    spawnhealth = 1000,
    attacksound = sfx_None,
    activesound = sfx_rocks1,
    radius = 8 * FRACUNIT,
    height = 16 * FRACUNIT,
    speed = 3,
    mass = 4,
    painchance = 255,
    flags = MF_BOUNCE | MF_PAIN | MF_GRENADEBOUNCE
}

states[S_JUMPROCK_1] = {
    sprite = SPR_ROIB,
    frame = A,
    tics = 1,
    nextstate = S_JUMPROCK_2
}
states[S_JUMPROCK_2] = {
    sprite = SPR_ROIB,
    frame = A | FF_ANIMATE,
    tics = 2,
    action = function(actor)
        A_NapalmScatter(actor, MT_SPINDUST + (6 << 16), 64 + (22 << 16))
    end,
    nextstate = S_JUMPROCK_3
}
states[S_JUMPROCK_3] = {
    sprite = SPR_ROIB,
    frame = A,
    tics = 2,
    action = function(actor)
        A_ZThrust(actor, 7)
    end,
    nextstate = S_JUMPROCK_4
}
states[S_JUMPROCK_4] = {
    sprite = SPR_ROIB,
    frame = A | FF_ANIMATE,
    tics = 50,
    action = function(actor)
        A_PlayAttackSound(actor, 4, 3)
    end,
    nextstate = S_JUMPROCK_5
}
states[S_JUMPROCK_5] = {
    sprite = SPR_ROIB,
    frame = A | FF_ANIMATE,
    tics = 1,
    action = function(actor)
        A_NapalmScatter(actor, MT_SPINDUST + (6 << 16), 64 + (22 << 16))
    end,
    nextstate = S_NULL
}











