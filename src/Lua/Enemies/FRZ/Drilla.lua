freeslot(
    "S_DASHERCRAWLA_STND",
	"S_DASHERCRAWLA_STND_X",
    "S_DASHERCRAWLA_JUMP1",
    "S_DASHERCRAWLA_JUMP2",
    "S_DASHERCRAWLA_DASH1",
    "S_DASHERCRAWLA_DASH2",
    "S_DASHERCRAWLA_DASH3",
    "MT_DASHERCRAWLA"
)

states[S_DASHERCRAWLA_STND_X] = {
    sprite = SPR_MNUD,
    frame = SPR2_E,
    tics = 1,
    nextstate = S_DASHERCRAWLA_STND
}

states[S_DASHERCRAWLA_STND] = {
    sprite = SPR_MNUD,
    frame = SPR2_E,
    tics = 5,
    action = function(actor)
		A_JetJawRoam(actor)	
		if not S_IdPlaying(557) then
			S_StartSoundAtVolume(actor, 557, 255)
		end

	end,
    nextstate = S_DASHERCRAWLA_STND
}

states[S_DASHERCRAWLA_JUMP1] = {
    sprite = SPR_MNUD,
    frame = SPR2_F,
    tics = 1,
    action = function(actor)
        A_NapalmScatter(actor, MT_FALLINGROCK, 80)
    end,
    nextstate = S_DASHERCRAWLA_JUMP2
}

states[S_DASHERCRAWLA_JUMP2] = {
    sprite = SPR_POSS,
    frame = SPR2_B,
    tics = 50,
    action = function(actor)
        A_ZThrust(actor, 13, 1)
    end,
    nextstate = S_DASHERCRAWLA_DASH1
}

states[S_DASHERCRAWLA_DASH1] = {
    sprite = SPR_NULL,
    frame = SPR2_F,
    tics = 0,
    action = function(actor)
        A_BossJetFume(actor, 2)
    end,
    nextstate = S_DASHERCRAWLA_DASH2
}

states[S_DASHERCRAWLA_DASH2] = {
    sprite = SPR_POSS,
    frame = SPR2_F,
    tics = 20,
    action = function(actor)
        A_Thrust(actor, 55, 1)
		--A_MultiShot(actor, (MT_CANNONBALL << 16) | 5, 80)
    end,
    nextstate = S_DASHERCRAWLA_DASH3
}

states[S_DASHERCRAWLA_DASH3] = {
    sprite = SPR_POSS,
    frame = SPR2_F,
    tics = 10,
    action = function(actor)
        A_FaceTarget(actor)
    end,
    nextstate = S_DASHERCRAWLA_DASH2
}

mobjinfo[MT_DASHERCRAWLA] = {
	--$Name Drilla
	--$Category Enemies/Flame Rift Zone
	--$Sprite POSSA1
    doomednum = 3103,
    spawnstate = S_DASHERCRAWLA_STND_X,
    spawnhealth = 1,
    seestate = S_DASHERCRAWLA_JUMP1,
    seeSound = sfx_None,
    attacksound = sfx_spndsh,
    painstate = S_NULL,
    painSound = sfx_None,
    deathstate = S_XPLD1,
    deathsound = sfx_pop,
    speed = 0,
    radius = 24*FRACUNIT,
    height = 32*FRACUNIT,
    mass = 100,
    damage = 0,
    flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE
}





--===================Lua stuff=================--




local function A_TOSSBIGBOIROCK(actor, var1, var2)


end

