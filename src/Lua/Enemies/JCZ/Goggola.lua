--[[
Goggles Crawla - "Goggola"

SOC by Zippy_Zolton

Lua conversion done by Ryuko
--]]

freeslot(
	"SPR_MRCE_JCZ_GOGGOLA",
	"MT_GOGGLESCRAWLA_SLOW",
	"MT_GOGGLESCRAWLA_FAST",
	"S_JOSS_STND",
	"S_JOSS_WALK1",
	"S_JOSS_WALK2",
	"S_JOSS_WALK3",
	"S_JOSS_WALK4",
	"S_JOSS_WALK5",
	"S_JOSS_WALK6",
	"S_JOSS_CHARGE1",
	"S_JOSS_CHARGE2",
	"S_JOSS_RUN1",
	"S_JOSS_RUN2",
	"S_JOSS_RUN3",
	"S_JOSS_RUN4",
	"S_JOSS_RUN5",
	"S_JOSS_RUN6",
	"S_JOSS_STOP"
)

-- "Goggles Crawla, may have colored variants later"

mobjinfo[MT_GOGGLESCRAWLA_SLOW] = {
	--$Name Goggles Crawla (Slow)
	--$Sprite SPR_MRCE_JCZ_GOGGOLA
	--$Category Enemies/Jade Coast Zone
	doomednum = 5060,
	spawnstate = S_JOSS_STND,
	spawnhealth = 1,
	seestate = S_JOSS_WALK1,
	reactiontime = 35,
	attacksound = sfx_s3kb6,
	painstate = S_NULL,
	painchance = 5*TICRATE,
	meleestate = S_JOSS_STOP,
	missilestate = S_JOSS_CHARGE1,
	deathstate = S_XPLD_FLICKY,
	xdeathstate = S_JOSS_STOP,
	deathsound = sfx_pop,
	speed = 1,
	radius = 24*FRACUNIT,
	height = 32*FRACUNIT,
	mass = 100,
	damage = 0,
	activesound = sfx_s3k5a,
	raisestate = S_NULL,
	flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE
}

mobjinfo[MT_GOGGLESCRAWLA_FAST] = {
	--$Name Goggles Crawla (Fast)
	--$Sprite SPR_MRCE_JCZ_GOGGOLA
	--$Category Enemies/Jade Coast Zone
	doomednum = 5061,
	spawnstate = S_JOSS_STND,
	spawnhealth = 1,
	seestate = S_JOSS_WALK1,
	seesound = sfx_none,
	reactiontime = 35,
	attacksound = sfx_s3kb6,
	painstate = S_NULL,
	painchance = 5*TICRATE,
	painsound = sfx_none,
	meleestate = S_JOSS_STOP,
	missilestate = S_JOSS_CHARGE1,
	deathstate = S_XPLD_FLICKY,
	xdeathstate = S_JOSS_STOP,
	deathsound = sfx_pop,
	speed = 2,
	radius = 24*FRACUNIT,
	height = 32*FRACUNIT,
	mass = 100,
	damage = 0,
	activesound = sfx_s3k5a,
	flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE,
	raisestate = S_NULL
}

states[S_JOSS_STND] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = A,
	tics = 5,
	nextstate = S_JOSS_STND,
	action = A_Look
}

states[S_JOSS_WALK1] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = A,
	tics = 3,
	nextstate = S_JOSS_WALK2,
	action = A_SharpChase
}

states[S_JOSS_WALK2] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = B,
	tics = 3,
	nextstate = S_JOSS_WALK3,
	action = A_SharpChase
}

states[S_JOSS_WALK3] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = C,
	tics = 3,
	nextstate = S_JOSS_WALK4,
	action = A_SharpChase
}

states[S_JOSS_WALK4] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = D,
	tics = 3,
	action = A_SharpChase,
	nextstate = S_JOSS_WALK1
}

-- "Charging state because A_SharpSpin sucks ass"

states[S_JOSS_CHARGE1] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = A,
	tics = 1,
	nextstate = S_JOSS_CHARGE1,
	action = A_FaceStabRev,
	var1 = 35,
	var2 = S_JOSS_CHARGE2
}

states[S_JOSS_CHARGE2] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = A,
	tics = 0,
	nextstate = S_JOSS_RUN1,
	action = A_PlayAttackSound
}

states[S_JOSS_RUN1] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = A,
	tics = 1,
	nextstate = S_JOSS_RUN2,
	action = A_SharpSpin
}

states[S_JOSS_RUN2] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = B,
	tics = 1,
	nextstate = S_JOSS_RUN3,
	action = A_SharpSpin
}

states[S_JOSS_RUN3] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = C,
	tics = 1,
	nextstate = S_JOSS_RUN4,
	action = A_SharpSpin
}

states[S_JOSS_RUN4] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = D,
	tics = 1,
	nextstate = S_JOSS_RUN1,
	action = A_SharpSpin
}
-- "Stop state for the same fucking reason"

states[S_JOSS_STOP] = {
	sprite = SPR_MRCE_JCZ_GOGGOLA,
	frame = A,
	tics = 1,
	nextstate = S_JOSS_STND
}


-- Move this to it's own source file --and break it in the process bc you forgot to import fishrot. lmao. good going


local function fishrot(mo)
	if mo.target or mo.tracer then
		local dist = R_PointToDist2(0, 0, mo.momx, mo.momy)
		local pitch = (R_PointToAngle2(0, 0, mo.momx, mo.momy) - mo.angle)/8
		local roll = R_PointToAngle2(0, 0, dist, mo.momz)
		MRCElibs.cameraSpriteRot(mo, mo.angle, roll, pitch)
	end
end


local function googlecrawlarisedown(mo)
	fishrot(mo)
	if mo.watertop then
		mo.flags = $|MF_NOGRAVITY
		if mo.target then
			if mo.watertop > mo.z and mo.watertop > mo.target.z then
				local hypot = FixedHypot(mo.momx, mo.momy)
				mo.momz = ease.linear(FRACUNIT/24, mo.momz, hypot*TBSlib.sign(mo.target.z-mo.z))
			else
				mo.target = nil
				mo.flags = $ &~ MF_NOGRAVITY
			end
		end
	else
		if P_IsObjectOnGround(mo) then
			local ang = P_RandomRange(1,360)*ANG1
			mo.momx = FixedMul(cos(ang), mo.radius)*2
			mo.momy = FixedMul(sin(ang), mo.radius)*2
			mo.momz = P_MobjFlip(mo)*mo.height
		end
		mo.flags = $ &~ MF_NOGRAVITY
		return true
	end
end

addHook("MobjThinker", googlecrawlarisedown, MT_GOGGLESCRAWLA_SLOW)
addHook("MobjThinker", googlecrawlarisedown, MT_GOGGLESCRAWLA_FAST)





