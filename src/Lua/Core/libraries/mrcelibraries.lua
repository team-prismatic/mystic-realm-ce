freeslot("SPR_7ROP", "S_DEFAULTROPE")

---@diagnostic disable-next-line
states[S_DEFAULTROPE] = {
	sprite = SPR_7ROP,
	frame = A|FF_PAPERSPRITE,
}

--$GZDB_SKIP
-- The sprite only can be 1 pixel width, perhaps in future..
local MRCElibs = {}

-- Attempt at creating ropes, in ideal scenario it would work but SRB2's sprite rotation is just spriterot algorithm to create new image, so not very useful
---@deprecated
---@param x1 fixed_t
---@param y1 fixed_t
---@param z1 fixed_t
---@param x2 fixed_t
---@param y2 fixed_t
---@param z2 fixed_t
---@param state number state id
function MRCElibs.createRope(x1, y1, z1, x2, y2, z2, state)
	local rope = P_SpawnMobj(x1, y1, z1, MT_RAY)
	rope.state = state or S_DEFAULTROPE
	rope.angle = R_PointToAngle2(x1, y1, x2, y2)
	rope.flags = mobjinfo[MT_BUSH].flags

	local dx = x2-x1
	local dy = y2-y1
	local dz = z2-z1
	local dist = R_PointToDist2(x1, y1, x2, y2)
	rope.spritexscale = abs(dist) / 8


	P_SetOrigin(rope, rope.x + dx /2, rope.y + dy / 2, rope.z + dz / 2)

	rope.rollangle = InvAngle(R_PointToAngle2(rope.x, rope.z, x2, z2))
end

-- Lerp object to position using P_SetOrigin
---@param mobj mobj_t
---@param t fixed_t delta
---@param x fixed_t goal x
---@param y fixed_t goal y
---@param z fixed_t goal z
function MRCElibs.lerpSetObjectToPos(mobj, t, x, y, z)
	local nx = ease.linear(t, mobj.x, x)
	local ny = ease.linear(t, mobj.y, y)
	local nz = ease.linear(t, mobj.z, z)
	P_SetOrigin(mobj, nx, ny, nz)
end

-- Lerp object to position using P_MoveOrigin
---@param mobj mobj_t
---@param t fixed_t delta
---@param x fixed_t goal x
---@param y fixed_t goal y
---@param z fixed_t goal z
function MRCElibs.lerpMoveObjectToPos(mobj, t, x, y, z)
	local nx = ease.linear(t, mobj.x, x)
	local ny = ease.linear(t, mobj.y, y)
	local nz = ease.linear(t, mobj.z, z)
	P_MoveOrigin(mobj, nx, ny, nz)
end

-- Lerp camera to position
---@param t fixed_t delta
---@param x fixed_t goal x
---@param y fixed_t goal y
---@param z fixed_t goal z
function MRCElibs.lerpMoveCameraToPos(t, x, y, z)
	local nx = ease.linear(t, camera.x, x)
	local ny = ease.linear(t, camera.y, y)
	local nz = ease.linear(t, camera.z, z)
	P_TeleportCameraMove(camera, nx, ny, nz)
end

-- 0 to FRACUNIT to 0 delta function
---@param t fixed_t delta 0 - 2*FRACUNIT
---@return fixed_t
function MRCElibs.backForth(t)
	return abs((t % (2 * FRACUNIT)) - FRACUNIT)
end

-- Converts angle_t to fixed_t radian angle (360 vs 400)
---@param a angle_t angle
---@return fixed_t
function MRCElibs.rad(a)
	return (AngleFixed(a)/180) % FRACUNIT
end

-- Converts fixed_t angle to fixed_t radian angle (360 vs 400)
---@param a fixed_t angle
---@return fixed_t
function MRCElibs.rawrad(a)
	return (a/180) % FRACUNIT
end

-- Draws self-scaling patch to simulate rotation in horizontal direction
---@param v videolib
---@param x fixed_t
---@param y fixed_t
---@param angle angle_t
---@param scale fixed_t
---@param patch patch_t
---@param flags? number
---@param colormap? colormap
function MRCElibs.drawFakeHRotation(v, x, y, angle, scale, patch, flags, colormap)
	local fixed_angle = AngleFixed(angle)
	local pixscale = scale/patch.width
	local flip = 0

	if angle < ANGLE_90 and angle > ANGLE_270 then
		flip = V_FLIP
	end

	local rotation = max(FixedMul(scale, MRCElibs.backForth(MRCElibs.rawrad(fixed_angle) * 2) or 1), pixscale)
	v.drawStretched(x, y, rotation, scale, patch, flags|flip, colormap)
end

-- Draws self-scaling patch to simulate rotation in horizontal direction
---@param v videolib
---@param x fixed_t
---@param y fixed_t
---@param angle angle_t
---@param scale fixed_t
---@param patch patch_t
---@param patch2 patch_t
---@param flags? number
---@param colormap? colormap
function MRCElibs.drawFakeHDoubleSideRotation(v, x, y, angle, scale, patch, patch2, flags, colormap)
	local fixed_angle = AngleFixed(angle)
	local pixscale = scale/patch.width
	local flip = 0

	if angle < ANGLE_90 and angle > ANGLE_270 then
		flip = V_FLIP
		patch = patch2
	end

	local rotation = max(FixedMul(scale, MRCElibs.backForth(MRCElibs.rawrad(fixed_angle) * 2) or 1), pixscale)
	v.drawStretched(x, y, rotation, scale, patch, flags|flip, colormap)
end


-- Draws horizontal gradient fill (WIP)
---@param v videolib
---@param x number
---@param y number
---@param width number
---@param height number
---@param colors table table of color indexes in palette
---@param flags? number global flags used in each color
---@param fixedlenght number fixed length of the gradient
function MRCElibs.drawHGradient(v, x, y, width, height, colors, flags, fixedlenght)
	local div = #colors*FRACUNIT/(fixedlenght or height)
	local last_index = -1
	local same = 0

	for i = 1, width do
		local color = colors[max(min(FixedInt(FixedRound(div*i)), #colors), 1)]
		if last_index ~= color then
			last_index = color
		elseif i < width then
			same = $+1
			continue
		end

		v.drawFill(x + i - same, y, same, height, last_index|flags)
		same = 1
	end
end

-- Draws vertical gradient fill (WIP)
---@param v videolib
---@param x number
---@param y number
---@param width number
---@param height number
---@param colors table table of color indexes in palette
---@param flags? number global flags used in each color
---@param fixedlenght number fixed length of the gradient
function MRCElibs.drawVGradient(v, x, y, width, height, colors, flags, fixedlenght)
	local div = #colors*FRACUNIT/(fixedlenght or width)
	local last_index = -1
	local same = 0
	for i = 1, height do
		local color = colors[max(min(FixedInt(FixedRound(div*i)), #colors), 1)]
		if last_index ~= color then
			last_index = color
		elseif i < width then
			same = $+1
			continue
		end

		v.drawFill(x, y + i - same, width, same, last_index|flags)
		same = 1
	end
end

--#region ???
function MRCElibs.drawHorizontalMRCELine(v, x, y, width, color)
	v.drawFill(x, y-3, width, 2, 0)
	v.drawFill(x, y-1, width, 2, color)
	v.drawFill(x, y+1, width, 2, 0)
end

function MRCElibs.drawVerticalMRCELine(v, x, y, height, color)
	v.drawFill(x-3, y, 2, height, 0)
	v.drawFill(x-1, y, 2, height, color)
	v.drawFill(x+1, y, 2, height, 0)
end

function MRCElibs.drawHorizontalClassicShadow(v, x, y, width, color)
	for i = 2,width,2 do
		v.drawFill(x + i, y, 1, i, color)
	end
end

function MRCElibs.drawVerticalClassicShadow(v, x, y, height, color)
	for i = 2,6,2 do
		v.drawFill(x + i, y, 7, height, color)
	end
end

function MRCElibs.drawMenuWindow(v, x, y, width, height, shadow_offset, color, flags)
	local corner1 = v.cachePatch('MRCEMENUC1')
	local corner2 = v.cachePatch('MRCEMENUC3')

	local shadow1 = v.cachePatch('MRCEMENUS1')
	local shadow2 = v.cachePatch('MRCEMENUS3')

	local offseted_x = x+width+corner1.width
	local offseted_y = y+height+corner1.height

	local flagcolor = color|flags

	-- SHADOW

	local sh_x = x+shadow_offset
	local sh_y = y+shadow_offset
	local sh_offset_x = offseted_x+shadow_offset
	local sh_offset_y = offseted_x+shadow_offset

	v.draw(sh_x, sh_y, shadow1, flags)

	MRCElibs.drawVerticalClassicShadow(v, sh_x, sh_y+shadow1.height+3, height, flagcolor)

	v.draw(sh_x, sh_offset_y, shadow2, flags)

	MRCElibs.drawHorizontalClassicShadow(v, sh_x+shadow1.width+2, sh_y, width, flagcolor)

	v.draw(sh_offset_x, sh_y, shadow1, flags|V_FLIP)

	MRCElibs.drawHorizontalClassicShadow(v, sh_offset_x+shadow1.width+1, sh_y, width, flagcolor)

	v.draw(sh_offset_x, sh_offset_y, shadow2, flags|V_FLIP)

	MRCElibs.drawVerticalClassicShadow(v, sh_x+shadow1.width, sh_y+shadow1.height, height, flagcolor)

	-- MAIN

	v.draw(x-1, y, corner1, flags)

	MRCElibs.drawHorizontalMRCELine(v, x+corner1.width, y+3, width, flagcolor)

	v.draw(x-1, offseted_y, corner2, flags)

	MRCElibs.drawVerticalMRCELine(v, x+3, y+corner1.height-2, height, flagcolor)

	v.draw(offseted_x, y, corner1, flags|V_FLIP)

	MRCElibs.drawHorizontalMRCELine(v, x+corner1.width, y+sh_y, width, flagcolor)

	v.draw(offseted_x, offseted_y, corner2, flags|V_FLIP)

	MRCElibs.drawVerticalMRCELine(v, x+sh_x+6, y+corner1.height, height, flagcolor)

end

function MRCElibs.vSliceDrawer(v, x, y, patch, drawf, flags, colormap, val)
	if not patch then return end
	local slice = 0

	while (slice < patch.width) do
		local slicefrac = slice << FRACBITS
		drawf(v, x, y, patch, flags, colormap, slice, slicefrac, val)

		slice = $+1
	end
end

local function drawerSliceVoxel(v, x, y, patch, flags, colormap, slice, slicefrac, val)
	local cosx = x+(slice * val)
	v.drawCropped(cosx, y, FRACUNIT, FRACUNIT, patch, flags, colormap, slicefrac, 0, FRACUNIT, patch.height*FRACUNIT)
end

local local_warning = false

function MRCElibs.voxelDrawer(v, x, y, layers, flags, colormap, angle)
	local cosine = cos(angle)
	local sine = sin(angle)

	if not layers and not local_warning then
		print('No layers, boss')
		local_warning = true
	end

	for k, layer in ipairs(layers) do
		MRCElibs.vSliceDrawer(v, x + k*sine, y, layer, drawerSliceVoxel, flags, colormap, cosine)
	end
end


function MRCElibs.drawVSlashFill(v, x, y, width, height, skips, color)
	for i = 0, width, skips do
		v.drawFill(x + i, y, 1, height, color)
	end
end

function MRCElibs.drawHSlashFill(v, x, y, width, height, skips, color)
	for i = 0, height, skips do
		v.drawFill(x, y + i, width, 1, color)
	end
end

function MRCElibs.drawHMRCESlashFill(v, x, y, width, height, skips, color1, color2)
	v.drawFill(x, y, width, height, color1)
	for i = 0, height, skips do
		v.drawFill(x - 1, y + i, width + 2, 1, color2)
	end
end

function MRCElibs.drawVMRCESlashFill(v, x, y, width, height, skips, color1, color2)
	v.drawFill(x, y, width, height, color1)
	for i = 0, width, skips do
		v.drawFill(x + i, y-1, 1, height + 2, color2)
	end
end

function MRCElibs.drawInBoundry(v, x, y, hscale, vscale, patch, xmin, xmax, ymin, ymax, flags, c)
	local crop_1 = max(ymin-y, 0)
	local crop_2 = max(ymax-y, 0)
	local crop_3 = max(xmin-x, 0)
	local crop_4 = max(xmax-x, 0)

	v.drawCropped(x, y, hscale, vscale, patch, flags, c, crop_3, crop_1, crop_4, crop_2)
end

function MRCElibs.drawProjected(v, x, y, z, offsetx, offsety, hscale, vscale, patch, flags, c)
	local projection = R_WorldToScreen2(camera, {x = x, y = y, z = z})
	local truehscale = FixedMul(projection.scale, hscale)
	local truevscale = FixedMul(projection.scale, vscale)
	local projoffsetx = FixedMul(projection.scale, offsetx)
	local projoffsety = FixedMul(projection.scale, offsety)

	v.drawStretched(projection.x + projoffsetx, projection.y + projoffsety, truehscale, truevscale, patch, flags, c)
end

function MRCElibs.drawProjectedFixed(v, x, y, z, offsetx, offsety, hscale, vscale, patch, flags, c)
	local projection = R_WorldToScreen2(camera, {x = x, y = y, z = z})
	v.drawStretched(projection.x + offsetx, projection.y + offsety, hscale, vscale, patch, flags, c)
end
--#endregion

if TBSlib then

	local function calcStrBytes(str)
		local bytes = 0

		for i = 1, #str do
			bytes = (bytes * 31 + string.byte(str, i)) % 2 ^ 32
		end

		return bytes
	end

	local styles = {
		["bold"] = 1,
		["b"] = 1,

		["italic"] = 2,
		["i"] = 2,

		["underline"] = 4,
		["u"] = 4,

		-- Style
		["style"] = 0,
	}

	local font_properties = {
		["size"] = true,
	}

	local color_shift = 16

	local colors = {
		["color=red"] = SKINCOLOR_RED,
		["color=green"] = SKINCOLOR_GREEN,
		["color=blue"] = SKINCOLOR_BLUE,
		["color=yellow"] = SKINCOLOR_YELLOW,
		["color=orange"] = SKINCOLOR_ORANGE,
	}


	function MRCElibs.MRBBCode(str)
		local strm = tostring(str)
		local attributes = {}
		local last_st = 0
		local shortened = 0

		for find in string.gmatch(strm, "(%[.+].+)") do
			local str_properties = string.match(find, "%[(.+)].+%[%/.+]")
			local st, en = string.find(strm, "%[.+](.+)%[%/"..string.match(str_properties, "^([%w]+)").."]", last_st)
			local brst, bren = string.find(strm, "(%[.+].+%[%/"..string.match(str_properties, "^([%w]+)").."])", last_st)

			local properties = {
				flags = 0,
			}

			for field in string.gmatch(str_properties, "([^,]+)") do
				local flag = string.gsub(string.lower(field), "[%s]", "")
				local name = string.gsub(string.lower(field), "%=.+", "")

				if styles[flag] then
					properties.flags = $ | styles[flag]
				elseif colors[flag] then
					properties.flags = $ | colors[flag] << color_shift
				elseif font_properties[name] then
					properties[name] = string.gsub(string.lower(field), ".+=", "")
				else
					table.insert(properties, field)
				end
			end

			local br_stlen = st - brst
			local bt_enlen = bren - en

			shortened = $ + br_stlen
			st = $ - shortened

			shortened = $ + bt_enlen
			en = $ - shortened

			last_st = st
			table.insert(attributes, {properties, st, en})
		end

		local true_string = string.gsub(strm, "%[.-%]", "")
		local char_properties = {}

		for i = 1, #attributes do
			local var = attributes[i]

			for z = var[2], var[3] do
				char_properties[z] = var[1]
			end
		end

		return true_string, char_properties
	end


	local cachedText = {}

	-- Crappy font drawer
	-- Technically not static, use case is more so for stuff that simply is just plain unchanging text
	-- Though it could be used for text as well, definitely not for constantly changing text, "once in longer term"
	--TBSlib.drawStaticText(d, font, x, y, scale, value, flags, color, alligment, padding)
	---@param d videolib
	---@param font string
	---@param x fixed_t
	---@param y fixed_t
	---@param scale fixed_t
	---@param text string
	---@param flags number
	---@param color colormap
	---@param alligment alligment_types_tbs
	---@param padding number
	---@param width fixed_t
	---@param height fixed_t
	function MRCElibs.MRBBCodeDrawer(d, font, x, y, scale, text, flags, color, alligment, padding, width, rowoffset)
		local storage = cachedText[calcStrBytes(font..'$'..text)]

		if not storage then
			local secured_text, attributes = MRCElibs.MRBBCode(text)
			storage = {}
			storage.lenght = 0
			storage.rowoffset = 0

			for i = 1,#secured_text do
				local cur = TBSlib.cacheFont(d, patch, secured_text, font, val, padding or 0, i)
				storage[i] = {patch = cur.patch, fontoffset = storage.lenght, height = rowoffset}
				if attributes[i] then
					storage[i].attributes = attributes[i]

					if attributes[i].flags > color_shift then
						local color = attributes[i].flags >> color_shift

						storage[i].color = d.getColormap(TC_DEFAULT, color)
					end
				end

				if width and storage[i].fontoffset < width then
					storage.lenght = 0
					storage.rowoffset = $ + (rowoffset or 32) * scale
				end

				storage.lenght = $+cur.width
			end
		end

		x = FixedMul(x, scale)
		y = FixedMul(y, scale)

		if alligment == "center" then
			x = $ - (storage.lenght * scale >> 1)
		elseif alligment == "right" then
			x = $ - storage.lenght * scale
		end

		for i = 1,#storage do
			local char = storage[i]
			d.drawScaled(x + char.fontoffset * scale, y + char.height, scale, char.patch, flags, char.color or color)
		end

		return storage.lenght, storage
	end
end

print(MRCElibs.MRBBCode([[
	We are testing barebones "BB"code capability,
	This way, we could write in more [bold, color = red]comfortable[/bold] way text and add more interesting descriptions.
	Even if it wouldn't be [bold]hard[/bold] to do manually, this will allow non-scripters to write more interesting descriptions.
]]))

-- If you want to get progress for ig various status bars scenarios
function MRCElibs.progress(x, maxx, minx)
	local offseted_x = max(min(x, maxx), minx) - minx
	local offseted_max = maxx - minx

	return offseted_x * FRACUNIT / offseted_max
end

-- Generalized spriterollangle sprite rotation -- not actually taken anything from it
-- Implementation +-borrowed from https://git.do.srb2.org/STJr/SRB2/-/merge_requests/1514#3e38d99d19d0cd959394dd380813dfdfdbbe9a30
-- Or rather it was, but I reduced it into basic trig...  It simply wasn't necessary what I was looking at.
function MRCElibs.cameraSpriteRot(mo, yaw, roll, pitch)
	local viewang = R_PointToAngle(mo.x, mo.y)
	if not R_PointToDist(mo.x, mo.y) then
		viewang = mo.angle
	end

	local ang = viewang - yaw
	mo.rollangle = FixedMul(cos(ang), roll) + FixedMul(sin(ang), pitch)
end

function MRCElibs.slopeRotBase(mo, slope)
	-- Reset
	if not slope then
		if mo.rollangle then
			mo.rollangle = ease.linear(FRACUNIT/4, mo.rollangle, FixedAngle(0))
		end
		return
	end

	MRCElibs.cameraSpriteRot(mo, slope.xydirection, 0, slope.zangle)
end

-- Use this function for automatic groundslope rotation
-- It is right now more so a macro, but it can change...
function MRCElibs.slopeRotation(mo)
	MRCElibs.slopeRotBase(mo, mo.standingslope)
end
--calculates coordinates on a basic 3d ellipse given an angle and radius
function MRCElibs.Polar2Cartesian2(hangle, radius)
	return FixedMul(cos(hangle), radius), FixedMul(sin(hangle), radius), FixedMul(sin(hangle), radius)
end

--spawns objects in a vertical orbit and prepares them to be fully rotated
function MRCElibs.SpawnVerticalOrbitObjects(mt_type, source ,amount, distance)

    local ha = source.angle + ANGLE_90
    local orbitals = {}
    local angDif = (360/amount)*ANG1
    for i = 1, amount do
        local va = i * angDif
        local sx = FixedMul(distance, FixedMul(cos(ha), cos(va)))
        local sy = FixedMul(distance, FixedMul(sin(ha), cos(va)))
        local sz = FixedMul(distance, sin(va))

        local orbit = P_SpawnMobjFromMobj(source, sx, sy, sz, mt_type)
        orbit.offset = {x = sx, y = sy, z = sz} --you'll use this as the relPos input on a Rotation3D function
		orbit.orbitradius = distance

        orbitals[i] = orbit
    end


    return orbitals

end

--moves orbitals relative to their origin object, increasing or decreasing the hoop's radius
function MRCElibs.MoveOrbitalRadius(source, set, distance)
	if not (source and source.valid) then
		error("tried to move the orbit radius but the origin object isn't valid!", 0)
		return
	end

    local ha = source.angle + ANGLE_90
    local angDif = (360 / #set)*ANG1
	local oyaw, oroll, oang = source.pitch, source.roll, source.angle
	source.pitch = 0
	source.roll = 0
	source.angle = 0
    for i = 1, #set do
		if not (set[i] and set[i].valid) then continue end
        local va = i * angDif
        local sx = FixedMul(distance, FixedMul(cos(ha), cos(va)))
        local sy = FixedMul(distance, FixedMul(sin(ha), cos(va)))
        local sz = FixedMul(distance, sin(va))

        P_MoveOrigin(set[i], sx + source.x, sy + source.y, sz + source.z)
        set[i].offset = {x = sx, y = sy, z = sz} --you'll use this as the relPos input on a Rotation3D function
		set[i].orbitradius = distance
		source.pitch = oyaw
		source.roll = oroll
		source.angle = oang
	end
end

-- source: needs to have x,y,z, angle, pitch, roll
-- target: the object being moved
-- relPos: the position that the target needs to be relative to the source (assume all angle values are 0 when determining the relative position)
    -- relPos should be a table with an x,y,z value, best acquired via the offset table on objects spawned via SpawnVerticalOrbitObjects
function MRCElibs.Rotation3D(source, target, relPos)
	if not (target and target.valid) then return end
	--if (target.lastlook and players[target.lastlook] and (players[target.lastlook].powers[pw_shield] & SH_PROTECTELECTRIC)) then return end
	--if target.attracttarget then return end
	local cosYaw = cos(source.angle)
	local sinYaw = sin(source.angle)
	local cosPitch = cos(source.pitch)
	local sinPitch = sin(source.pitch)
	local cosRoll = cos(source.roll)
	local sinRoll = sin(source.roll)

	local ox, oy, oz = relPos.x, relPos.y, relPos.z

	-- Gonna be honest, I know it works, but this is wizardry to me. I needed to get some help on the algorithm for this one

	local rx = FixedMul(FixedMul(cosYaw,cosPitch),ox)
			+ FixedMul((FixedMul(FixedMul(cosYaw,sinPitch),sinRoll) - FixedMul(sinYaw,cosRoll)),oy)
			+ FixedMul((FixedMul(FixedMul(cosYaw,sinPitch),cosRoll) + FixedMul(sinYaw,sinRoll)),oz)

	local ry = FixedMul(FixedMul(sinYaw,cosPitch), ox)
			+ FixedMul((FixedMul(FixedMul(sinYaw,sinPitch),sinRoll) + FixedMul(cosYaw,cosRoll)),oy)
			+ FixedMul((FixedMul(FixedMul(sinYaw,sinPitch),cosRoll) - FixedMul(cosYaw,sinRoll)),oz)

	local rz = FixedMul(-1 * sinPitch,ox)
			+ FixedMul(FixedMul(cosPitch,sinRoll),oy)
			+ FixedMul(FixedMul(cosPitch,cosRoll),oz)

	local newx = source.x + rx
	local newy = source.y + ry
	local newz = source.z + rz

	P_MoveOrigin(target, newx, newy, newz)
end

-- What the hell you mean you cannot find this file???

rawset(_G, "MRCElibs", MRCElibs)