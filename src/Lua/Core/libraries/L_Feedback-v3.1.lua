local SND_ERROR = sfx_s3kb2
local SND_NEUTRAL = sfx_s3k65
local SND_GOOD = sfx_s245

--printF(player_t player, string str)
--Print for the given player
local function printF(player,str)
	if consoleplayer == player then
		print(str)
	end
end

--printD(string str)
--Print for the dedicated server
--couldn't you just CONS_Printf(server, str)
--???
local function printD(str)
	if isdedicatedserver then
		print(str)
	end
end

local function shouldsave()
	if isdedicated then return true end
	if isserver then return true end
	if not netgame then return true end
	return false
end

addHook("MobjDeath", function(mo)
	-- record data
	if not shouldsave() then return end
	if mo.player.bot then return end

	local file = io.openlocal("Deaths.txt", "a")
	local map = gamemap
	local cx = mo.x
	local cy = mo.y
	local cz = mo.z + mo.height / 2

	file:write(map.."\n"..cx.."\n"..cy.."\n"..cz.."\n" )

	--print("Saved death.")
end, MT_PLAYER)

addHook("ThinkFrame", function()
	for player in players.iterate do
		if player.comment_timer == nil then
			player.comment_timer = 0
		elseif player.comment_timer > 0 then
			player.comment_timer = $ - 1
		end
	end
end)
addHook("IntermissionThinker", function()
	for player in players.iterate do
		if player.comment_timer == nil then
			player.comment_timer = 0
		elseif player.comment_timer > 0 then
			player.comment_timer = $ - 1
		end
	end
end)

local function comment(player, ...)
	-- Check mobj
	local mo = player.mo
	if player.truemo then
		mo = player.truemo
	end

	-- Check comment
	local comment = {...}
	if comment == nil  or #comment == 0 or comment == " "  or comment == "" then
		printF(player,"Write some map feedback! It will be placed at your coordinates, even when spectating. You can comment during intermissions too.")
		S_StartSound(nil, SND_NEUTRAL, player)
		return
	end

	-- Check timer
	if player.comment_timer and player.comment_timer > 0 then
		local secs = max(1, 1 + player.comment_timer / TICRATE)
		printF(player,"Please wait "..secs.." seconds before making a new comment.")
		S_StartSound(nil, SND_ERROR, player)
		return
	end

	-- Check invalid chars
	local fulltext = "["..player.name.."] "..table.concat(comment, " ")
	if string.match(fulltext, "\\") then
		printF(player,"Your comment contains one or more invalid characters.")
		S_StartSound(nil, SND_ERROR, player)
		return
	end

	-- Check length
	if string.len(fulltext) > 200 then
		printF(player,"Please use 200 characters or less.")
		S_StartSound(nil, SND_ERROR, player)
		return
	end

	-- Set timer
	player.comment_timer = 8 * TICRATE;

	-- Record data
	if not shouldsave() then
		printF(player, "Sent your comment to the server!")
		return
	end

	local map = gamemap
	local cx
	local cy
	local cz
	if mo then
		cx = mo.x
		cy = mo.y
		cz = mo.z
	else
		cx = "i"
		cy = "i"
		cz = "i"
	end

	local file = io.openlocal("Comments.txt", "a")
	file:write(map.."\n"..cx.."\n"..cy.."\n"..cz.."\n"..fulltext.."\n" )
	print("Saved comment.")

	S_StartSound(nil, SND_GOOD, player)
	return
end

COM_AddCommand("comment", comment)


addHook("PlayerMsg", function(player, audience, target, message)
	if audience == 1 then
		return true
	end
	if message:sub(1,8) == "!comment" then
		if message:sub(9,9) == " " then
			comment(player, message:sub(9,200))
		else
			comment(player, nil)
		end
	end
end)