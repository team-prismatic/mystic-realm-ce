--[[
		Team Blue Spring's Series of Libaries.
		Lite Node Framework - mobjnodes.lua

		This is cutdown version of lib_nodes.lua for MRCE use
		Whole idea behind said nodes is to have object holding any pre-written script
		-- hopefully this will expand to in-editor writing one day

		Compare to main library, this version should be ->
			- Netplay friendly
			- Strict yet easy for modders to use

Contributors: Skydusk
@Team Blue Spring 2024
]]

--$GZDB_SKIP

----------------------
-- Documentation
----------------------

--[[

	--Script:

	--Map nodes are in-map object that going to constantly use their set script.
	--Scripts are stored in ScriptNodes array in static way.

	--To add script into database, merely assign into global table called 'ScriptNodes' a function
	--like this:

	-- ScriptNodes[string key] = function

	--or

	-- ScriptNodes[string key] = function(self, args, textarg)
	--	<algorithm>
	--end


	--Only assign the index of the function, just make sure to not call the function!

	--X ScriptNodes[string key] = function(sandwich, melon, grass)

	--Technically all functions are variables so in this case treat it as such in this case.

	--You should expect these arguments to be send into the function

	--function(mobj_t self, array args, array textarg)
	-- <self> being node itself, mobj acting as node found in the level
	-- <args> is array of arguments from mapthing -- the whole range.
	-- <textarg> is mapthing_t.stringargs[1] only considering mapthing_t.stringargs[0] is used by mapthing for script identification.

	--Mapping:

	--In map editor, spawn object with ID 2704 and then set individual arguments based on script description provided to mapper.

	--Most important will be string argument 1 (Should be mapthing_t.stringargs[0] in code) in which they add script key from ScriptNodes.
]]

	--Script Example ->

	-- ScriptNodes["SpawnThings_1"] = function(self, args, textarg)
	-- 	local arg_1 = args[1] -- delay
	-- 	if not (leveltime % arg_1) then
	-- 		if _G[textarg] and mobjinfo[_G[textarg]] then
	-- 			P_SpawnMobjFromMobj(self, 0, 0, 0, _G[textarg])
	-- 		end
	-- 	end
	-- end

	-- or

	-- local function P_SpawnThings_2(self, args, textarg)
	--	local arg_1 = args[1] -- delay
	--	if not (leveltime % arg_1) then
	--		if _G[textarg] and mobjinfo[_G[textarg]] then
	--			P_SpawnMobjFromMobj(self, 0, 0, 0, _G[textarg])
	--		end
	--	end
	--end

	--ScriptNodes["SpawnThings_2"] = P_SpawnThings_2

----------------------
-- metatable handler
----------------------

rawset(_G, 'ScriptNodes', setmetatable({scripts = {}, len = 0},
	{
		__metatable = false, -- prevents anyone modifying the metatable itself
		-- Not that it is not impossible, merely avoiding people from using it however they want

		__index = function(array, key) -- array[key]
			if type(key) == 'string' then
				local kupper = key:upper()
				if kupper == "SCRIPTS" or kupper == "LEN" then
					error("[Script Nodes] ".."\133".."Error: ".."\128".."You don't have access to internals.")
				else
					return array.scripts[kupper]
				end
			else
				error("[Script Nodes] ".."\133".."Error: ".."\128".."Key indexed in script nodes is not a string.")
			end
			return
		end, -- due to Lua being case sensitive, make all keys uppercase
		-- It is precausion for map makers, they will be ones using those scripts

		__newindex = function(array, key, value) -- array[key] = value
			if type(key) == 'string' and type(value) == 'function' then
				local kupper = key:upper()
				if kupper == "SCRIPTS" or kupper == "LEN" then
					error("[Script Nodes] ".."\133".."Error: ".."\128".."You cannot overwrite internals.")
				else
					array.scripts[kupper] = value
					array.len = $+1
					print("[Script Nodes] Script \'"..key.."\' registered")
				end
			elseif type(key) ~= 'string' then
				error("[Script Nodes] ".."\133".."Error: ".."\128".."Key indexed in script nodes is not a string.")
			elseif type(value) ~= 'function' then
				error("[Script Nodes] ".."\133".."Error: ".."\128".."Indexed attmpted value/script is not a function.")
			else
				error("[Script Nodes] ".."\133".."Error: ".."\128".."Exception.")
			end
			return
		end, -- due to Lua being case sensitive, make all keys uppercase

		__usedindex = function(array, key, value) -- array[key] = value
			if not array.scripts[key] then return end

			if type(key) == 'string' and type(value) == 'function' then
				local kupper = key:upper()
				if kupper == "SCRIPTS" or kupper == "LEN" then
					error("[Script Nodes] ".."\133".."Error: ".."\128".."You cannot overwrite internals.")
				else
					error("[Script Nodes] ".."\133".."Error: ".."\128".."Indexed key already exists, please choose different key")
				end
			elseif type(key) ~= 'string' then
				error("[Script Nodes] ".."\133".."Error: ".."\128".."Key indexed in script nodes is not a string.")
			elseif type(value) ~= 'function' then
				error("[Script Nodes] ".."\133".."Error: ".."\128".."Indexed attmpted value/script is not a function.")
			else
				error("[Script Nodes] ".."\133".."Error: ".."\128".."Exception.")
			end
			return
		end, -- no overwriting

		__len = function(array) -- #array
			return array.len
		end -- entries
	}
))

----------------------
-- SCRIPT MOBJ NODE
----------------------

freeslot("MT_LUASCRIPTNODE")

mobjinfo[MT_LUASCRIPTNODE] = {
--$Category TBS Library
--$Name Lua Script Node
--$Sprite BOM1A0
	doomednum = 2704,
	spawnstate = S_INVISIBLE,
	spawnhealth = 1,
	reactiontime = 1,
	speed = 12,
	radius = 8,
	height = 8,
	mass = 100,
	flags = MF_NOGRAVITY|MF_NOBLOCKMAP|MF_NOSECTOR|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_NOCLIPTHING
}

addHook("MapThingSpawn", function(node, mt)
	if ScriptNodes[mt.stringargs[0]] then
		node.script = ScriptNodes[mt.stringargs[0]]
		node.args = mt.args
		node.textarg = mt.stringargs[1]
	else
		print("[Script Nodes - Node] ".."\133".."Error: ".."\128".."Mobj "..(#mt).." has either invalid script set or none!")
		P_RemoveMobj(node)
	end
end, MT_LUASCRIPTNODE)

addHook("MobjThinker", function(node)
	if not (node.script and node.args) then return end
	node:script(node.args, node.textarg)
end, MT_LUASCRIPTNODE)
