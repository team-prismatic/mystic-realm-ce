freeslot('SPR_MRCE_MEDALS')
--[[
In-Level emblems go in this order: Frame A (waterdrop) + color Blue, Frame F (Lightning) + color Topaz,
Frame C (Leaf) + color Emerald, Frame D (Flame) + color Flame, and frame E (Snow) + color Icy
Special frames go to other purposes, Frame I (Prism) + color Black is used for emerald shrines
Time Emblems Frame T + color Bone, Ring Emblems Frame R + color Goldenrod
the first 9 frames actually represent each of the zones in order,
from Jade Coast (waterdrop) to the Mystic Realm (prism) but we won't use them quite like that
in future, separate stylized sprites of each of these sprites, based on each zone,
will instead be made, akin to the current Jade Coast Waterdrop Medal sprite

DEV TIMES
Dev tmes will be used as a bonus super challenge for each stage, with a significantly tougher time limit
and potentially skin-locked, but will be counted separately from normal acheivements, contributing
an additional 1% beyond 100% should the player manage to get them all
They will not be visible in the achievements list until after the standard Time emblem is earned
However, the functionality for this isn't ready yet, so I'll doc some of the expected times here for now

JCZ1 - 0:18
JCZ2 - 0:20
JCZ3 - 0:15
MHK  - 0:35
MFZ1 - 1:15
MFZ3 - 0:15

SHADOW ACHIEVEMENTS
optional achievements that are not counted normally like standard achievements,
these do not appear in the achievements list until they have been unlocked. They do not count towards 100%,
but unlocking all of them grants an additional 1% past 100%. They are ridiculously difficult and not remotely fair,
by no means would I expect anyone to try to go for them. Things like
clearing ultimate mode all emeralds + special stages on lockon as amy in less than 2 hours without using any continues or shields or whatever
but naturally, there will always be people that are crazy enough to try insane stuff like that,
and who am I to not at least acknowledge that?
Functionality is partially there, acheivements can be marked as EXTRA, so that they won't count towards the 100% tracker,
but I don't believe they are hidden from the achievement list when locked, nor do they count for the extra 1% yet

counting both Shadow Achievements and Dev Times,
the max % obtainable in the % tracker should be 102% as of now, once that functionality is complete
]]
--
-- GLOBAL
--

local playerdamagedlevel = false
local playerdamagedglobal = false

addHook("MobjDamage", function(mo)
	--if mapheaderinfo[gamemap].bonustype < 1 then return end
	if mo.player.bot then return end
	if not playerdamagedlevel then playerdamagedlevel = true end
	if not playerdamagedglobal then playerdamagedglobal = true end
end, MT_PLAYER)

addHook("MapLoad", function(map)
	if playerdamagedlevel then playerdamagedlevel = false end
	if gamestate == GS_TITLESCREEN then
		playerdamagedglobal = false
	end
end)

addHook("MobjDeath", function()
	local stats = MRCE_Unlock.getSaveData().stats

	if not stats.eggboxes then
		stats.eggboxes = 1
	else
		stats.eggboxes = $+1
	end

	if stats.eggboxes > 100 then
		MRCE_Achievements.unlock(Masochist)
	end
end, MT_EGGMAN_BOX)

-- These functions are checked automatically
-- Returning false boolean makes the check pass, true results in requirement failure

local function checkIfCombi(achievement, save, player) --Used to check if the player exited the stage with a combine ring active
	if player.mo.valid and player.mo.pw_combine and player.mo.pw_combine == 1 then
		return false
	end

	return true
end

local function checkGuard()
	if not playerdamagedlevel then
		return false
	else
		return true
	end
end

local function checkEmRank()
	if mrce.emstage_stageRank >= 6 then
		return false
	else
		return true
	end
end

local function mapClear()
	return true
end

-- ============Jade Coast Zone 1 Achievements===========
MRCE_Achievements.add{
	id = "JCZ1_Rings",
	typein = "Challenge",
	set = {"JCZ", "JCZ1", "Rings"},
	name = "JCZ1 Ring Attack",
	desc = "",
	tips = "Collect 300 rings and reach the exit",
	inlevel = "A1",
	requirements = {
		{type = 3, field = "rings", value = 300},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ1_Time",
	typein = "Challenge",
	set = {"JCZ", "JCZ1", "Time"},
	name = "JCZ1 Time Attack",
	desc = "",
	tips = "Reach the exit in under 40 seconds",
	inlevel = "A1",
	requirements = {
		{type = 1, field = "realtime", value = 40*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ1_Beach", -- unique identificator
	typein = "Medal",  -- How to get, this is not category
	set = {"JCZ", "JCZ1", "Medal"}, -- categories
	name = "First Medal!",
	desc = "Quite easy, ey?",
	tips = "In middle of starting open section...",
	inlevel = "A1", -- Merely helps with search in code and makes sure
	-- Also makes sure that unlock is only possible in said level.

	-- sprite definitions
	gamesprite = SPR_MRCE_MEDALS,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ1_House",
	typein = "Medal",
	set = {"JCZ", "JCZ1", "Medal"},
	name = "Beach House!",
	desc = "Definitely no creepy empty vibes.",
	tips = "A common trope, a secret behind the waterfall",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ1_Ocean",
	typein = "Medal",
	set = {"JCZ", "JCZ1", "Medal"},
	name = "Monolith Over Ocean.",
	desc = "View on droplets of world",
	tips = "Out of the cave, there is worth flying somewhere...",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ1_RBDCave",
	typein = "Medal",
	set = {"JCZ", "JCZ1", "Medal"},
	name = "Goodies up the Cave Wall",
	desc = "Found a medal in Sonic's rebound cave",
	tips = "Learning to bounce up walls, but don't forget to look up",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ1_Lake",
	typein = "Medal",
	set = {"JCZ", "JCZ1", "Medal"},
	name = "Did Someone Drop This?",
	desc = "Found a medal sitting out in the lake",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = E,
	color = SKINCOLOR_ICY,

	hudsprite = "JCZ101",
}

local JCZ1_RedCrawlas = MRCE_Achievements.add{
	id = "JCZ1_RedCrawlas",
	typein = "Task",
	set = {"JCZ", "JCZ1", "Task"},
	name = "Revenge of BLU.",
	desc = "It took 10 times of 3 and it still isn't out.",
	tips = "Destroy 30 times red crawlas in any JCZ1 playthrough...",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = SPR_MRCE_MEDALS,
	frame = A,
	color = SKINCOLOR_PEPPER,

	hudsprite = "JCZ101",
}

addHook("MobjDeath", function(mo)
	if not (mapheaderinfo[gamemap].lvlttl == "Jade Coast" and mapheaderinfo[gamemap].actnum == 1) then return end

	local stats = MRCE_Unlock.getSaveData().stats

	if not stats.redcrawlas then
		MRCE_Unlock.writeIntoSaveData('stats', 'redcrawlas', 1)
	else
		MRCE_Unlock.writeIntoSaveData('stats', 'redcrawlas', stats.redcrawlas+1)
	end

	if stats.redcrawlas > 30 then
		MRCE_Achievements.unlock(JCZ1_RedCrawlas, sfx_chchng, p)
	end
end, MT_REDCRAWLA)

MRCE_Achievements.add{
	id = "JCZ1_Posters",
	typein = "Special",
	set = {"JCZ", "JCZ1", "Special"},
	name = "Someone is missing?",
	desc = "Or someone is evading paying rent.",
	tips = "Collect all posters",
	inlevel = "A1",

	-- sprite definitions
	gamesprite = MRCE_JCZ_Deco2,
	frame = 11,
	color = SKINCOLOR_ISLAND,

	hudsprite = "JCZWANT111WHATEVER",
}

MRCE_Achievements.add{
	id = "JCZ1_COMBI",
	typein = "Challenge",
	set = {"JCZ", "JCZ1", "Task"},
	name = "Apple of my eye",
	desc = "",
	tips = "Finish the level with a combine ring active",
	inlevel = "A1",
	requirements = {
		{type = 7, field = "combi_ring", value = checkIfCombi},
	},

	-- sprite definitions
	gamesprite = SPR_MRCE_MEDALS,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}


-- ============Jade Coast Zone 2 Achievements===========
MRCE_Achievements.add{
	id = "JCZ2_Rings",
	typein = "Challenge",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "JCZ2 Ring Attack",
	desc = "",
	tips = "Collect 400 rings and reach the exit",
	inlevel = "A2",
	requirements = {
		{type = 3, field = "rings", value = 400},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ2_Time",
	typein = "Challenge",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "JCZ2 Time Attack",
	desc = "",
	tips = "Reach the exit in under 45 seconds",
	inlevel = "A2",
	requirements = {
		{type = 1, field = "realtime", value = 45*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ2_CavePond",
	typein = "Medal",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "Hidden Pond on the Cliff",
	desc = "A secret room near the final checkpoint",
	tips = "Instead of progressing up the cliffside, maybe try looking up",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_MRCE_MEDALS,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ2_Waterfall",
	typein = "Medal",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "Secret at the Foot of the Falls",
	desc = "A secret room near the bottom of the lone waterfall",
	tips = "A giant waterfall from above. Follow it down into a small cave.",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ2_HouseCliff",
	typein = "Medal",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "Who's House is this?",
	desc = "A not so hidden path way up high",
	tips = "I climbed so far up, I can see my house from here!\n...Well, it's someone's house.",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ2_WaterRun",
	typein = "Medal",
	set = {"JCZ", "JCZ2", "Medal"},
	name = "High Within the Coral Cave",
	desc = "Run across the water's surface to reach a hidden aclove",
	tips = "Hidden inside a watery cave. Perhaps with enough speed, one could stay dry?",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ2_Shrine",
	typein = "Task",
	set = {"JCZ", "JCZ2", "Task"},
	name = "Shrine by the Shore",
	desc = "High above the water, near the final climb, the blue emerald shrine awaits",
	tips = "All roads must eventually converge somewhere. The shrine looks down from above",
	inlevel = "A2",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_BLACK,

	hudsprite = "JCZ101",
}

-- ============Jade Coast Zone 3 Achievements===========
MRCE_Achievements.add{
	id = "JCZ3_Guard",
	typein = "Challenge",
	set = {"JCZ", "JCZ3", "Medal"},
	name = "JCZ3 Damageless",
	desc = "",
	tips = "Defeat the boss without taking damage",
	inlevel = "A3",
	requirements = {
		{type = 7, field = "bosscheck", value = checkGuard}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = S,
	color = SKINCOLOR_BROWN,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "JCZ3_Time",
	typein = "Challenge",
	set = {"JCZ", "JCZ3", "Medal"},
	name = "JCZ3 Time Attack",
	desc = "",
	tips = "Defeat the boss in under 20 seconds",
	inlevel = "A3",
	requirements = {
		{type = 1, field = "realtime", value = 20*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "JCZ101",
}

--
-- JCZ-Emerald
--

MRCE_Achievements.add{
	id = "MHK_Bridge",
	typein = "Medal",
	set = {"MKZ", "Medal"},
	name = "Under the Bridge",
	desc = "unda",
	tips = "When you cross a bridge, jump to a platform below. There lies your emblem.",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_MRCE_MEDALS,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "MHK_Ledge",
	typein = "Medal",
	set = {"MKZ", "Medal"},
	name = "The High Ledge",
	desc = "High above in a small corridor",
	tips = "Three ring monitors, on a rock in a cavern. You'll need to climb up.",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "MHK_Spikes",
	typein = "Medal",
	set = {"MKZ", "Medal"},
	name = "Breaking Through",
	desc = "A room hiding behind some spikes",
	tips = "Flying off a ramp. Before the starpost, look back. Jump and break some spikes.",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "MHK_Egg",
	typein = "Medal",
	set = {"MKZ", "Medal"},
	name = "Turning Pain into Profit",
	desc = "With invincibility, bounce off an egghead from way up high",
	tips = "Bounce from a plateau and go flying through the sky to reach the platform.",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "JCZ101",
}

local MHK_Cake = MRCE_Achievements.add{
	id = "MHK_Cake",
	typein = "Task",
	set = {"MKZ", "Task"},
	name = "The Cake is a Lie",
	desc = "Found a cake, and pushed it over.",
	tips = "When life gives you a big old ramp to slide off of, get mad! Rebound back!",
	inlevel = "AN",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = E,
	color = SKINCOLOR_ICY,

	hudsprite = "JCZ101",
}

addHook("MobjThinker", function(mo)
	--print("still alive")
	if mo.state == S_JCZCAKE3 then
		MRCE_Achievements.unlock(MHK_Cake)
		--print("You're the moron they made to make me an idiot")
	end
end, MT_JCZCAKE)

MRCE_Achievements.add{
	id = "MHK_Rings",
	typein = "Challenge",
	set = {"MHK", "Rings"},
	name = "MHK Ring Attack",
	desc = "",
	tips = "Collect 300 rings and reach the exit",
	inlevel = "AN",
	requirements = {
		{type = 3, field = "rings", value = 300},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "MHK_Time",
	typein = "Challenge",
	set = {"MHK", "Time"},
	name = "MHK Time Attack",
	desc = "",
	tips = "Reach the exit in under 45 seconds",
	inlevel = "AN",
	requirements = {
		{type = 1, field = "realtime", value = 45*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "JCZ101",
}

MRCE_Achievements.add{
	id = "MHK_SRank",
	typein = "Challenge",
	set = {"MHK", "Task"},
	name = "MHK Emerald Attack",
	desc = "",
	tips = "Chain big combos as fast as you can to get an S rank",
	inlevel = "AN",
	requirements = {
		{type = 7, field = "rank_check", value = checkEmRank}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_MOONSTONE,

	hudsprite = "JCZ101",
}

--
-- TVZ1
--

MRCE_Achievements.add{
	id = "TVZ1_OldTVZ2StartAclove",
	typein = "Medal",
	set = {"TVZ", "TVZ1", "Medal"},
	name = "Nice place to hide, maybe read a book",
	desc = "",
	tips = "Roll down the big slope, turn left, then go through the goop. Try dropping from above",
	inlevel = "A4",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ1_BackHill",
	typein = "Medal",
	set = {"TVZ", "TVZ1", "Medal"},
	name = "To go forwards, try looking back first",
	desc = "",
	tips = "After a small tunnel, look up for a small cliff. It awaits at the top.",
	inlevel = "A4",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ1_GoopFlow",
	typein = "Medal",
	set = {"TVZ", "TVZ1", "Medal"},
	name = "Riding the Flow",
	desc = "Where does all this goop come from anyway",
	tips = "Inside a cave. Tons of falling goop. One of them seems different.",
	inlevel = "A4",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ1_OldTempleRuins",
	typein = "Medal",
	set = {"TVZ", "TVZ1", "Medal"},
	name = "The Old Temple Ruins",
	desc = "There used to be something here. Not much left now but some old rocks",
	tips = "Find a tunnel in, the room full of moving goop. Jump when it's lowest.",
	inlevel = "A4",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = E,
	color = SKINCOLOR_ICY,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ1_ElemShard",
	typein = "Special",
	set = {"TVZ", "TVZ1", "Task"},
	name = "Shocking Lightning Temple Conquered",
	desc = "This Temple is in development and may later strike down here in this location.",
	tips = "???",
	inlevel = "A4",

	-- sprite definitions
	gamesprite = SPR_KYST,
	frame = A,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "KEY1S",
}

MRCE_Achievements.add{
	id = "TVZ1_Time",
	typein = "Challenge",
	set = {"TVZ", "TVZ1", "Time"},
	name = "TVZ1 Time Attack",
	desc = "",
	tips = "Reach the exit in under 2:15",
	inlevel = "A4",
	requirements = {
		{type = 1, field = "realtime", value = 135*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ1_Rings",
	typein = "Challenge",
	set = {"TVZ", "TVZ1", "Rings"},
	name = "TVZ1 Ring Attack",
	desc = "",
	tips = "Collect 400 rings and reach the exit",
	inlevel = "A4",
	requirements = {
		{type = 3, field = "rings", value = 400},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

-- ============Tempest Valley Zone 2 Achievements===========

MRCE_Achievements.add{
	id = "TVZ2_ANGELSTATUE",
	typein = "Medal",
	set = {"TVZ", "TVZ2", "Medal"},
	name = "Angel Statue",
	desc = "Open the door behind you at the start.",
	tips = "Angels grateful hands, open a door to secrets, pray to the statue.",
	inlevel = "A5",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ2_TORIEMBLEM",
	typein = "Medal",
	set = {"TVZ", "TVZ2", "Medal"},
	name = "The Tori",
	desc = "Collect the emblem at the top of the tori.",
	tips = "Respect the tori, bask in the towers glory, high but not too high.",
	inlevel = "A5",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = B,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ2_LOSEREMBLEM",
	typein = "Medal",
	set = {"TVZ", "TVZ2", "Medal"},
	name = "Slime Time",
	desc = "Jump from a high spot and claim the emblem.",
	tips = "He's tucked away, resting deep in purple goo, where bridge and cave meet.",
	inlevel = "A5",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ2_CAVEEMBLEM",
	typein = "Medal",
	set = {"TVZ", "TVZ2", "Medal"},
	name = "Where the purple slime flows",
	desc = "Check the source of the moving slime.",
	tips = "Crushers and sharp spears, boost you to addicting speed, look behind and claim.",
	inlevel = "A5",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ2_Shrine",
	typein = "Task",
	set = {"TVZ", "TVZ2", "Task"},
	name = "Shrine of the valley",
	desc = "Activate the Shrine",
	tips = "Check the farthest corner of the valley.",
	inlevel = "A5",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_PURPLE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ2_COMBI",
	typein = "Task",
	set = {"TVZ", "TVZ2", "Task"},
	name = "View of the temple",
	desc = "",
	tips = "Finish the level with a combine ring active",
	inlevel = "A5",
	requirements = {
		{type = 7, field = "combi_ring", value = checkIfCombi},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = E,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}


MRCE_Achievements.add{
	id = "TVZ2_LOSS",
	typein = "Task",
	set = {"TVZ", "TVZ2", "Task"},
	name = "Is that loss?",
	desc = "Yes it is.",
	tips = "Grab the 1up before it's lost",
	inlevel = "A5",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ2_Time",
	typein = "Challenge",
	set = {"TVZ", "TVZ2", "Time"},
	name = "TVZ2 Time Attack",
	desc = "",
	tips = "Reach the exit in under 1:45",
	inlevel = "A5",
	requirements = {
		{type = 1, field = "realtime", value = 105*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ2_Rings",
	typein = "Challenge",
	set = {"TVZ", "TVZ2", "Rings"},
	name = "TVZ2 Ring Attack",
	desc = "",
	tips = "Collect 500 rings and reach the exit",
	inlevel = "A5",
	requirements = {
		{type = 3, field = "rings", value = 500},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

--
-- TVZ3
--

MRCE_Achievements.add{
	id = "TVZ3_Guard",
	typein = "Challenge",
	set = {"TVZ", "TVZ3", "Medal"},
	name = "TVZ3 Damageless",
	desc = "",
	tips = "Defeat the boss without taking damage",
	inlevel = "A6",
	requirements = {
		{type = 7, field = "bosscheck", value = checkGuard}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = S,
	color = SKINCOLOR_BROWN,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "TVZ3_Time",
	typein = "Challenge",
	set = {"TVZ", "TVZ3", "Time"},
	name = "TVZ3 Time Attack",
	desc = "",
	tips = "Defeat the boss in under 1:30",
	inlevel = "A6",
	requirements = {
		{type = 1, field = "realtime", value = 90*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

--
-- TVZ-Emerald
--

MRCE_Achievements.add{
	id = "RSK_BridgeStart",
	typein = "Medal",
	set = {"TVZ", "RSK", "Medal"},
	name = "A Bridge Too Long",
	desc = "",
	tips = "Bridges usually keep you from falling into pits. But what if you fall in anyway?",
	inlevel = "AO",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "RSK_BridgeStart",
	typein = "Medal",
	set = {"TVZ", "RSK", "Medal"},
	name = "Ride With the Winds",
	desc = "",
	tips = "Philip is so strong, he's tearing this place apart! Try riding up with the debris",
	inlevel = "AO",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "RSK_SRank",
	typein = "Challenge",
	set = {"TVZ", "RSK", "Task"},
	name = "RSK Emerald Attack",
	desc = "",
	tips = "Chain big combos as fast as you can to get an S rank",
	inlevel = "AO",
	requirements = {
		{type = 7, field = "rank_check", value = checkEmRank}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_MOONSTONE,

	hudsprite = "TABEMBICON",
}

-- ============Verdant Forest Zone 1 Achievements===========

MRCE_Achievements.add{
	id = "VFZ1_TreeTop",
	typein = "Medal",
	set = {"VFZ", "VFZ1", "Medal"},
	name = "Top of the forest",
	desc = "Climbed a hollow tree to it's highest branch",
	tips = "Inside a big tree. Normally you would go down. But instead, go up.",
	inlevel = "A7",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "VFZ1_WaterTree",
	typein = "Medal",
	set = {"VFZ", "VFZ1", "Medal"},
	name = "The Apple that fell not far into the Water",
	desc = "Found a Medal hiding behind a tree in a large lake",
	tips = "Behind a small tree, in the deep part of a lake. Near a whirlwind shield.",
	inlevel = "A7",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "VFZ1_Lario",
	typein = "Task",
	set = {"VFZ", "VFZ1", "Task"},
	name = "Everybody wanna be a Superstar",
	desc = "Get a lot of money, drive a fancy car",
	tips = "Near the greatest slope, reject the promise of speed. Break open the wall",
	inlevel = "A7",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "VFZ1_AltExit",
	typein = "Task",
	set = {"VFZ", "VFZ1", "Task"},
	name = "Another Way Out",
	desc = "Found the alternate exit",
	tips = "Try taking the high path",
	inlevel = "A7",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "VFZ1_WaterFall",
	typein = "Medal",
	set = {"VFZ", "VFZ1", "Medal"},
	name = "Over the Waterfall",
	desc = "Found a Medal hiding at the mouth of the falls",
	tips = "Above the triplets, Nearest the end of the wood. From here springs great life.",
	inlevel = "A7",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = E,
	color = SKINCOLOR_ICY,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "VFZ1_Time",
	typein = "Challenge",
	set = {"VFZ", "VFZ1", "Time"},
	name = "VFZ1 Time Attack",
	desc = "",
	tips = "Reach the exit in under 0:45",
	inlevel = "A7",
	requirements = {
		{type = 1, field = "realtime", value = 45*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "VFZ1_Rings",
	typein = "Challenge",
	set = {"VFZ", "VFZ1", "Rings"},
	name = "VFZ1 Ring Attack",
	desc = "",
	tips = "Collect 300 rings and reach the exit",
	inlevel = "A7",
	requirements = {
		{type = 3, field = "rings", value = 300},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

-- ============Verdant Forest Zone 2 Achievements===========
MRCE_Achievements.add{
	id = "VFZ2_Shrine",
	typein = "Task",
	set = {"VFZ", "VFZ2", "Task"},
	name = "Shrine of the Forest",
	desc = "Low below the tree houses, within the lower village, a shrine hide ",
	tips = "After jumping down, Amongst roots and glowing ponds. The village Tree Shrine.",
	inlevel = "A8",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_BLACK,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "VFZ2_Time",
	typein = "Challenge",
	set = {"VFZ", "VFZ2", "Time"},
	name = "VFZ2 Time Attack",
	desc = "",
	tips = "Reach the exit in under 0:55",
	inlevel = "A8",
	requirements = {
		{type = 1, field = "realtime", value = 55*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

--
-- VFZ3
--

MRCE_Achievements.add{
	id = "VFZ3_Guard",
	typein = "Challenge",
	set = {"VFZ", "VFZ3", "Medal"},
	name = "VFZ3 Damageless",
	desc = "",
	tips = "Defeat the boss without taking damage",
	inlevel = "A9",
	requirements = {
		{type = 7, field = "bosscheck", value = checkGuard}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = S,
	color = SKINCOLOR_BROWN,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "VFZ3_Time",
	typein = "Challenge",
	set = {"VFZ", "VFZ3", "Time"},
	name = "VFZ3 Time Attack",
	desc = "",
	tips = "Defeat the boss in under 0:20",
	inlevel = "A9",
	requirements = {
		{type = 1, field = "realtime", value = 20*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

--
-- VFZ-Emerald
--

MRCE_Achievements.add{
	id = "LBW_Start",
	typein = "Medal",
	set = {"LWZ", "Medal"},
	name = "Labyrinth Woods",
	desc = "Brave the forest labyrinth",
	tips = "Thanks for your patience. This next emblem location has been discarded",
	inlevel = "AP",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "LBW_Reverse",
	typein = "Medal",
	set = {"LWZ", "Medal"},
	name = "Go Back",
	desc = "Go back the way you came, and find something not previously there",
	tips = "L-E-A-V-E. Deeper past the winding floors; When you light that torch!",
	inlevel = "AP",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "LBW_FakeWall",
	typein = "Medal",
	set = {"LWZ", "Medal"},
	name = "Bush Behind the Wall",
	desc = "Crawlama",
	tips = "Space rends, all two paths bend to one definite end. This place don't stop there...",
	inlevel = "AP",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "LBW_Fly",
	typein = "Medal",
	set = {"LWZ", "Medal"},
	name = "Swinging on High",
	desc = "High above the bridges and swings",
	tips = "Go, go in! First room, higher off the flyover! Don't get spiked ringless.",
	inlevel = "AP",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "LBW_RRLair",
	typein = "Medal",
	set = {"LWZ", "Medal"},
	name = "The Resourceful Rat",
	desc = "",
	tips = "Lost? Frightened? Confused? Seven notes, retry, my lair... So long, chump! -R.R.",
	inlevel = "AP",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = E,
	color = SKINCOLOR_ICY,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "LBW_Rings",
	typein = "Challenge",
	set = {"LBW", "Rings"},
	name = "LBW Ring Attack",
	desc = "",
	tips = "Collect 500 rings and reach the exit",
	inlevel = "AP",
	requirements = {
		{type = 3, field = "rings", value = 500},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "LBW_Time",
	typein = "Challenge",
	set = {"LBW", "Time"},
	name = "LBW Time Attack",
	desc = "",
	tips = "Reach the exit in under 1:45",
	inlevel = "AP",
	requirements = {
		{type = 1, field = "realtime", value = 105*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "LBW_SRank",
	typein = "Challenge",
	set = {"LBW", "Task"},
	name = "LBW Emerald Attack",
	desc = "",
	tips = "Chain big combos as fast as you can to get an S rank",
	inlevel = "AP",
	requirements = {
		{type = 7, field = "rank_check", value = checkEmRank}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_MOONSTONE,

	hudsprite = "TABEMBICON",
}

-- ============Flame Rift Zone 1 Achievements===========

MRCE_Achievements.add{
	id = "FRZ1_StartingLava",
	typein = "Medal",
	set = {"FRZ", "FRZ1", "Medal"},
	name = "Toasty",
	desc = "",
	tips = "Near the very start, Protected by falls of flame, Might want some shielding",
	inlevel = "AA",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "FRZ1_Cliffside",
	typein = "Medal",
	set = {"FRZ", "FRZ1", "Medal"},
	name = "Great view up here, too bad there's not much to see",
	desc = "",
	tips = "Scale the Great Cliffside, Over pipes and foliage, Not much further now",
	inlevel = "AA",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "FRZ1_DoorValves",
	typein = "Medal",
	set = {"FRZ", "FRZ1", "Medal"},
	name = "The old storage room",
	desc = "",
	tips = "Lava generators deactivated all over, perhaps if you turn them on an old door might receive power",
	inlevel = "AA",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "FRZ1_EndingBridge",
	typein = "Medal",
	set = {"FRZ", "FRZ1", "Medal"},
	name = "A Bridge too Short",
	desc = "",
	tips = "Before you move on, make sure to check underneath, lest you miss your prize",
	inlevel = "AA",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "FRZ1_ElemShard",
	typein = "Special",
	set = {"FRZ", "FRZ1", "Task"},
	name = "Temple of Flames Conquered",
	desc = "Completed the firey catacombs and claimed your prize",
	tips = "High above modern shipping paths lies an ancient collapsed entryway to a temple long forgotten",
	inlevel = "AA",

	-- sprite definitions
	gamesprite = SPR_KYST,
	frame = B,
	color = SKINCOLOR_RED,

	hudsprite = "KEY2S",
}

MRCE_Achievements.add{
	id = "FRZ1_COMBI",
	typein = "Task",
	set = {"FRZ", "FRZ1", "Task"},
	name = "What are bones?",
	desc = "",
	tips = "Finish the level with a combine ring active",
	inlevel = "AA",
	requirements = {
		{type = 7, field = "combi_ring", value = checkIfCombi},
	},

	-- sprite definitions
	gamesprite = SPR_MRCE_MEDALS,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "FRZ1_Rings",
	typein = "Challenge",
	set = {"FRZ", "FRZ1", "Rings"},
	name = "LBW Ring Attack",
	desc = "",
	tips = "Collect 400 rings and reach the exit",
	inlevel = "AA",
	requirements = {
		{type = 3, field = "rings", value = 400},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "FRZ1_Time",
	typein = "Challenge",
	set = {"FRZ", "FRZ1", "Time"},
	name = "FRZ1 Time Attack",
	desc = "",
	tips = "Reach the exit in under 1:45",
	inlevel = "AA",
	requirements = {
		{type = 1, field = "realtime", value = 105*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

-- ============Flame Rift Zone 2 Achievements===========

MRCE_Achievements.add{
	id = "FRZ2_TrainAttic",
	typein = "Medal",
	set = {"FRZ", "FRZ2", "Medal"},
	name = "Training Emblem!",
	desc = "",
	tips = "As you exit the traincart, you may need to look back and up.",
	inlevel = "AB",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}


MRCE_Achievements.add{
	id = "FRZ2_Shrine",
	typein = "Task",
	set = {"FRZ", "FRZ2", "Task"},
	name = "Shrine of the Fiery Falls",
	desc = "",
	tips = "At the mouth of flame, high above stone and magma. The shrine basks in sun.",
	inlevel = "AB",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_BLACK,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "FRZ2_Time",
	typein = "Challenge",
	set = {"FRZ", "FRZ2", "Time"},
	name = "FRZ2 Time Attack",
	desc = "",
	tips = "Reach the exit in under 2:30",
	inlevel = "AB",
	requirements = {
		{type = 1, field = "realtime", value = 150*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "FRZ2_Rings",
	typein = "Challenge",
	set = {"FRZ", "FRZ2", "Rings"},
	name = "FRZ2 Ring Attack",
	desc = "",
	tips = "Collect 400 rings and reach the exit",
	inlevel = "AB",
	requirements = {
		{type = 3, field = "rings", value = 400},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

--
-- FRZ3
--

MRCE_Achievements.add{
	id = "FRZ3_Time",
	typein = "Challenge",
	set = {"FRZ", "FRZ3", "Time"},
	name = "FRZ3 Time Attack",
	desc = "",
	tips = "Beat the boss in under 1 minute",
	inlevel = "AC",
	requirements = {
		{type = 1, field = "realtime", value = 60*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "FRZ3_Guard",
	typein = "Challenge",
	set = {"FRZ", "FRZ3", "Medal"},
	name = "FRZ3 Damageless",
	desc = "",
	tips = "Defeat the boss without taking damage",
	inlevel = "AC",
	requirements = {
		{type = 7, field = "bosscheck", value = checkGuard}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = S,
	color = SKINCOLOR_BROWN,

	hudsprite = "TABEMBICON",
}

--
-- FRZ-Emerald
--

MRCE_Achievements.add{
	id = "VFG_SRank",
	typein = "Challenge",
	set = {"FRZ", "VFG", "Task"},
	name = "VFG Emerald Attack",
	desc = "",
	tips = "Chain big combos as fast as you can to get an S rank",
	inlevel = "AQ",
	requirements = {
		{type = 7, field = "rank_check", value = checkEmRank}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_MOONSTONE,

	hudsprite = "TABEMBICON",
}

--
-- MFZ1
--

MRCE_Achievements.add{
	id = "MFZ1_TreeGaurd",
	typein = "Medal",
	set = {"MFZ", "MFZ1", "Medal"},
	name = "Vanguard over the Mountainside",
	desc = "",
	tips = "Before the great falls, guarded by three tall trees, a short hop away.",
	inlevel = "AD",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ1_WaterCaveSecret",
	typein = "Medal",
	set = {"MFZ", "MFZ1", "Medal"},
	name = "Water Cave Skylight",
	desc = "",
	tips = "There's something at the top of the freezing water cave.",
	inlevel = "AD",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ1_KnuxCave",
	typein = "Medal",
	set = {"MFZ", "MFZ1", "Medal"},
	name = "The Echidna Pool",
	desc = "",
	tips = "Hidden passage underneath a big lake. You may also need a Water Shield and a powerful punch!",
	inlevel = "AD",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ1_Pool",
	typein = "Medal",
	set = {"MFZ", "MFZ1", "Medal"},
	name = "Lakeside Treasure Diving",
	desc = "",
	tips = "Take a cautious look at the small lake.",
	inlevel = "AD",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ1_FangCliff",
	typein = "Medal",
	set = {"MFZ", "MFZ1", "Medal"},
	name = "Bouncing up the Hills over the Lodge",
	desc = "",
	tips = "Find a way to climb up to the well, located in the final pathway, with the jerboa's powers.",
	inlevel = "AD",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = E,
	color = SKINCOLOR_ICY,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ1_Time",
	typein = "Challenge",
	set = {"MFZ", "MFZ1", "Time"},
	name = "MFZ1 Time Attack",
	desc = "",
	tips = "Reach the exit in under 2:30",
	inlevel = "AD",
	requirements = {
		{type = 1, field = "realtime", value = 150*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ1_Rings",
	typein = "Challenge",
	set = {"MFZ", "MFZ1", "Rings"},
	name = "MFZ1 Ring Attack",
	desc = "",
	tips = "Collect 450 rings and reach the exit",
	inlevel = "AD",
	requirements = {
		{type = 3, field = "rings", value = 450},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

--
-- MFZ2
--

MRCE_Achievements.add{
	id = "MFZ2_Pillar",
	typein = "Medal",
	set = {"MFZ", "MFZ2", "Medal"},
	name = "Pillar Just Out of Reach",
	desc = "",
	tips = "So close, but not quite. Is there not a way up there?",
	inlevel = "AE",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ2_River",
	typein = "Medal",
	set = {"MFZ", "MFZ2", "Medal"},
	name = "Frozen Creek",
	desc = "",
	tips = "A flowing river covered by a sheet of ice. Secrets underneath.",
	inlevel = "AE",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ2_IceBox",
	typein = "Medal",
	set = {"MFZ", "MFZ2", "Medal"},
	name = "Ice Box",
	desc = "",
	tips = "A dangerous jump in an underwater path. A hole in the ice.",
	inlevel = "AE",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ2_ShrineCellar",
	typein = "Medal",
	set = {"MFZ", "MFZ2", "Medal"},
	name = "The Shrine's Cellar",
	desc = "",
	tips = "The ice seems quite thin, right before the mystic shrine. There's good stuff down there.",
	inlevel = "AE",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ2_Shrine",
	typein = "Task",
	set = {"MFZ", "MFZ2", "Task"},
	name = "Calling of the cavern",
	desc = "",
	tips = "There's snow on the ice just as soon as you can see Eggman's ugly face.",
	inlevel = "AE",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_BLACK,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ2_Time",
	typein = "Challenge",
	set = {"MFZ", "MFZ2", "Time"},
	name = "MFZ2 Time Attack",
	desc = "",
	tips = "Reach the exit in under 2:30",
	inlevel = "AE",
	requirements = {
		{type = 1, field = "realtime", value = 150*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ2_Rings",
	typein = "Challenge",
	set = {"MFZ", "MFZ2", "Rings"},
	name = "MFZ2 Ring Attack",
	desc = "",
	tips = "Collect 350 rings and reach the exit",
	inlevel = "AE",
	requirements = {
		{type = 3, field = "rings", value = 350},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

--
-- MFZ3
--

MRCE_Achievements.add{
	id = "MFZ3_Guard",
	typein = "Challenge",
	set = {"MFZ", "MFZ3", "Medal"},
	name = "MFZ3 Damageless",
	desc = "",
	tips = "Defeat the boss without taking damage",
	inlevel = "AF",
	requirements = {
		{type = 7, field = "bosscheck", value = checkGuard}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = S,
	color = SKINCOLOR_BROWN,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MFZ3_Time",
	typein = "Challenge",
	set = {"MFZ", "MFZ3", "Time"},
	name = "MFZ3 Time Attack",
	desc = "",
	tips = "Defeat the boss in under 30 seconds",
	inlevel = "AF",
	requirements = {
		{type = 1, field = "realtime", value = 30*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

local MFZ_Toasty = MRCE_Achievements.add{
	id = "MFZ_Toasty",
	typein = "Task",
	set = {"MFZ", "Task"},
	name = "Stayin' Toasty",
	desc = "",
	tips = "In a single run-through, clear all of Midnight Freeze Zone without dying or being frozen",
	inlevel = "AF",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

addHook("PlayerSpawn", function(p)
	if mapheaderinfo[gamemap].lvlttl ~= "Midnight Freeze" and mapheaderinfo[gamemap].lvlttl ~= "Silver Cavern" then
		p.mfrozenc = nil
	else
		p.mfrozenc = $ or false
	end
	if gamemap == 113 and p.mo.y > 11328*FRACUNIT then --we spawned at the very start of mfz1
		p.mfrozenc = true
	end
end)

addHook("MobjDeath", function(mo)
	mo.player.mfrozenc = false
end, MT_PLAYER)

--
-- MFZ-Emerald
--

MRCE_Achievements.add{
	id = "SVC_Pillar",
	typein = "Medal",
	set = {"MFZ", "SVC", "Medal"},
	name = "Hidden in Plain Sight",
	desc = "",
	tips = "Taking the left path, look behind a pillar at the second star post.",
	inlevel = "AR",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SVC_Stalag",
	typein = "Medal",
	set = {"MFZ", "SVC", "Medal"},
	name = "Getting Dark in Here",
	desc = "",
	tips = "Look along the walls in the last big room that's filled with pillars and springs.",
	inlevel = "AR",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SVC_Time",
	typein = "Challenge",
	set = {"MFZ", "SVC", "Time"},
	name = "SVC Time Attack",
	desc = "",
	tips = "Reach the exit in under 2:15",
	inlevel = "AR",
	requirements = {
		{type = 1, field = "realtime", value = 135*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SVC_Rings",
	typein = "Challenge",
	set = {"MFZ", "SVC", "Rings"},
	name = "SVC Ring Attack",
	desc = "",
	tips = "Collect 300 rings and reach the exit",
	inlevel = "AR",
	requirements = {
		{type = 3, field = "rings", value = 300},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

--
-- SPZ1
--

MRCE_Achievements.add{
	id = "SPZ1_DeepWater",
	typein = "Medal",
	set = {"SPZ", "SPZ1", "Medal"},
	name = "Overlooking the Depths",
	desc = "",
	tips = "The most sunken part. A deep, little island in the ruined left path.",
	inlevel = "AG",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SPZ1_WaterFlow",
	typein = "Medal",
	set = {"SPZ", "SPZ1", "Medal"},
	name = "Hidden in the Flow",
	desc = "",
	tips = "Near the first post, check out the rightmost waterfall. Where does it come from?",
	inlevel = "AG",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SPZ1_ElemShard",
	typein = "Special",
	set = {"SPZ", "SPZ1", "Task"},
	name = "Water Temple Revisited",
	desc = "This ptsd is a work in progress. We thank you for your patience, and hope to haunt you again soon.",
	tips = "The last red button will unlock a door that is near the last starpost.",
	inlevel = "AG",

	-- sprite definitions
	gamesprite = SPR_KYST,
	frame = C,
	color = SKINCOLOR_BLUE,

	hudsprite = "KEY3S",
}

MRCE_Achievements.add{
	id = "SPZ1_Time",
	typein = "Challenge",
	set = {"SPZ", "SPZ1", "Time"},
	name = "SPZ1 Time Attack",
	desc = "",
	tips = "Reach the exit in under 1:20",
	inlevel = "AG",
	requirements = {
		{type = 1, field = "realtime", value = 80*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SPZ1_Rings",
	typein = "Challenge",
	set = {"SPZ", "SPZ1", "Rings"},
	name = "SPZ1 Ring Attack",
	desc = "",
	tips = "Collect 300 rings and reach the exit",
	inlevel = "AG",
	requirements = {
		{type = 3, field = "rings", value = 300},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

--
-- SPZ2
--

MRCE_Achievements.add{
	id = "SPZ2_Shed",
	typein = "Medal",
	set = {"SPZ", "SPZ2", "Medal"},
	name = "The old storage shed",
	desc = "",
	tips = "A shed at the end? Locked door, but how to open? A nearby switch, no?",
	inlevel = "AH",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SPZ2_NTCSecret",
	typein = "Medal",
	set = {"SPZ", "SPZ2", "Medal"},
	name = "Secret Across the Water",
	desc = "",
	tips = "Muck on open air, with great speed stay just above. Secret past the turn.",
	inlevel = "AH",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SPZ2_Ledge",
	typein = "Medal",
	set = {"SPZ", "SPZ2", "Medal"},
	name = "Don't Look Down!",
	desc = "",
	tips = "Open air, perhaps. I wouldn't call it fresh though. A narrow passage above.",
	inlevel = "AH",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SPZ2_Shrine",
	typein = "Task",
	set = {"SPZ", "SPZ2", "Task"},
	name = "Stolen Desert Shrine Beneath the Waves",
	desc = "",
	tips = "Many dark tunnels, only one contains the prize. The emerald glows golden.",
	inlevel = "AH",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_BLACK,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SPZ2_Time",
	typein = "Challenge",
	set = {"SPZ", "SPZ2", "Time"},
	name = "SPZ2 Time Attack",
	desc = "",
	tips = "Reach the exit in under 2:15",
	inlevel = "AH",
	requirements = {
		{type = 1, field = "realtime", value = 135*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SPZ2_Rings",
	typein = "Challenge",
	set = {"SPZ", "SPZ2", "Rings"},
	name = "SPZ2 Ring Attack",
	desc = "",
	tips = "Collect 150 rings and reach the exit",
	inlevel = "AH",
	requirements = {
		{type = 3, field = "rings", value = 150},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

--
--
-- SPZ3
--
MRCE_Achievements.add{
	id = "SPZ3_Guard",
	typein = "Challenge",
	set = {"SPZ", "SPZ3", "Medal"},
	name = "SPZ3 Damageless",
	desc = "",
	tips = "Defeat the boss without taking damage",
	inlevel = "AI",
	requirements = {
		{type = 7, field = "bosscheck", value = checkGuard}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = S,
	color = SKINCOLOR_BROWN,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "SPZ3_Time",
	typein = "Challenge",
	set = {"SPZ", "SPZ3", "Time"},
	name = "SPZ3 Time Attack",
	desc = "",
	tips = "Defeat the boss in under 1:45",
	inlevel = "AI",
	requirements = {
		{type = 1, field = "realtime", value = 105*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

--
-- SPZ-Emerald
--
MRCE_Achievements.add{
	id = "NTC_SRank",
	typein = "Challenge",
	set = {"SPZ","NTC", "Task"},
	name = "SLP Emerald Attack",
	desc = "",
	tips = "Chain big combos as fast as you can to get an S rank",
	inlevel = "AT",
	requirements = {
		{type = 7, field = "rank_check", value = checkEmRank}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_MOONSTONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "NTC_Time",
	typein = "Challenge",
	set = {"SPZ", "NTC", "Time"},
	name = "NTC Time Attack",
	desc = "",
	tips = "Reach the exit in under 1:30",
	inlevel = "AJ",
	requirements = {
		{type = 1, field = "realtime", value = 90*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

--
-- AGZ1
--

MRCE_Achievements.add{
	id = "AGZ1_SecretExit",
	typein = "Task",
	set = {"AGZ", "AGZ1", "Task"},
	name = "Yahoo",
	desc = "Find a way past the Infinite Stairway",
	tips = "Around and around, cursed stairs of infinity. Is there a way up?",
	inlevel = "AJ",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "AGZ1_Time",
	typein = "Challenge",
	set = {"AGZ", "AGZ1", "Time"},
	name = "AGZ1 Time Attack",
	desc = "",
	tips = "Reach the exit in under 0:25",
	inlevel = "AJ",
	requirements = {
		{type = 1, field = "realtime", value = 25*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

--
-- AGZ2
--

MRCE_Achievements.add{
	id = "AGZ2_Shrine",
	typein = "Task",
	set = {"AGZ", "AGZ2", "Task"},
	name = "Shrine of the Sky",
	desc = "",
	tips = "For your convenience, this shrine has been relocated until further notice",
	inlevel = "AK",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_BLACK,

	hudsprite = "TABEMBICON",
}

--
-- AGZ3
--

MRCE_Achievements.add{
	id = "AGZ3_Guard",
	typein = "Challenge",
	set = {"AGZ", "AGZ3", "Medal"},
	name = "AGZ3 Damageless",
	desc = "",
	tips = "Defeat the boss without taking damage",
	inlevel = "AL",
	requirements = {
		{type = 7, field = "bosscheck", value = checkGuard}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = S,
	color = SKINCOLOR_BROWN,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "AGZ3_Time",
	typein = "Challenge",
	set = {"AGZ", "AGZ3", "Time"},
	name = "AGZ3 Time Attack",
	desc = "",
	tips = "Defeat the boss in under 1:00",
	inlevel = "AL",
	requirements = {
		{type = 1, field = "realtime", value = 60*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

--
-- AGZ-Emerald
--
--SLP is super early but I'm adding S rank challenges to all the stages to try to round out our final count for the release. Saves won't carry over between major updates anyway, at least not until the pack is finished
MRCE_Achievements.add{
	id = "SLP_SRank",
	typein = "Challenge",
	set = {"AGZ","SLP", "Task"},
	name = "SLP Emerald Attack",
	desc = "",
	tips = "Chain big combos as fast as you can to get an S rank",
	inlevel = "AT",
	requirements = {
		{type = 7, field = "rank_check", value = checkEmRank}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_MOONSTONE,

	hudsprite = "TABEMBICON",
}

--
-- ISZ
--

MRCE_Achievements.add{
	id = "ISZ_Hyper",
	typein = "Task",
	set = {"AGZ", "ISZ", "Task"},
	name = "Chaos Resolved",
	desc = "Power Unleashed",
	tips = "With 7 aligned gems in hand, empower oneself with the Controller of Chaos",
	inlevel = "AU",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_BLACK,

	hudsprite = "TABEMBICON",
}

--
-- PAZ1
--

MRCE_Achievements.add{
	id = "PAZ1_FirstTrial",
	typein = "Medal",
	set = {"PAZ", "PAZ1", "Medal"},
	name = "The First Trial",
	desc = "",
	tips = "In the first trial, jump from the portal down to the pillars below.",
	inlevel = "AW",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "PAZ1_SecondTrial",
	typein = "Medal",
	set = {"PAZ", "PAZ1", "Medal"},
	name = "The Second Trial",
	desc = "",
	tips = "The second trial, after using the third spring, you can jump to it.",
	inlevel = "AW",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "PAZ1_ThirdTrial",
	typein = "Medal",
	set = {"PAZ", "PAZ1", "Medal"},
	name = "The Third Trial",
	desc = "",
	tips = "In the third trial, use the final springs to get on to the pillars.",
	inlevel = "AW",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "PAZ1_Time",
	typein = "Challenge",
	set = {"PAZ", "PAZ1", "Time"},
	name = "PAZ1 Time Attack",
	desc = "",
	tips = "Reach the exit in under 1:45",
	inlevel = "AW",
	requirements = {
		{type = 1, field = "realtime", value = 105*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "PAZ1_Rings",
	typein = "Challenge",
	set = {"PAZ", "PAZ1", "Rings"},
	name = "PAZ1 Ring Attack",
	desc = "",
	tips = "Collect 30 rings and reach the exit",
	inlevel = "AW",
	requirements = {
		{type = 3, field = "rings", value = 30},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

--
-- PAZ2
--

MRCE_Achievements.add{
	id = "PAZ2_ElementalTrial",
	typein = "Medal",
	set = {"PAZ", "PAZ2", "Medal"},
	name = "The Elemental Trial",
	desc = "",
	tips = "Springs give extra height when you are under water. Careful not to fall!",
	inlevel = "AX",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "PAZ2_ArmageddonTrial",
	typein = "Medal",
	set = {"PAZ", "PAZ2", "Medal"},
	name = "The Armageddon Trial",
	desc = "",
	tips = "Bring another nuke, you might end up needing it for another switch.",
	inlevel = "AX",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = F,
	color = SKINCOLOR_TOPAZ,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "PAZ2_WhirlwindTrial",
	typein = "Medal",
	set = {"PAZ", "PAZ2", "Medal"},
	name = "The Whirlwind Trial",
	desc = "",
	tips = "The only way in is a well-timed double jump. Watch out for the blocks!",
	inlevel = "AX",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = C,
	color = SKINCOLOR_EMERALD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "PAZ2_MysticGate",
	typein = "Medal",
	set = {"PAZ", "PAZ2", "Medal"},
	name = "The Mystic Gate",
	desc = "",
	tips = "Well, this is the end. The door to the Mystic Realm. What might lay beyond?",
	inlevel = "AX",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = D,
	color = SKINCOLOR_FLAME,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "PAZ2_Shrine",
	typein = "Task",
	set = {"PAZ", "PAZ2", "Task"},
	name = "The Prismatic Shrine",
	desc = "",
	tips = "Red springs at the back, then use the rising platforms. The last mystic shrine.",
	inlevel = "AX",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_BLACK,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "PAZ2_Time",
	typein = "Challenge",
	set = {"PAZ", "PAZ2", "Time"},
	name = "PAZ2 Time Attack",
	desc = "",
	tips = "Reach the exit in under 1:45",
	inlevel = "AX",
	requirements = {
		{type = 1, field = "realtime", value = 105*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "PAZ2_Rings",
	typein = "Challenge",
	set = {"PAZ", "PAZ2", "Rings"},
	name = "PAZ2 Ring Attack",
	desc = "",
	tips = "Collect 30 rings and reach the exit",
	inlevel = "AX",
	requirements = {
		{type = 3, field = "rings", value = 30},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

--
-- STV
--

MRCE_Achievements.add{
	id = "STV_WarpZone",
	typein = "Medal",
	set = {"PAZ", "STV", "Medal"},
	name = "Almost There!",
	desc = "",
	tips = "Welcome to Warp Zone! ...Wait, that's from some other game. Enjoy some free stuff!",
	inlevel = "AV",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_YELLOW,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "STV_SRank",
	typein = "Challenge",
	set = {"PAZ", "STV", "Task"},
	name = "STV Emerald Attack",
	desc = "",
	tips = "Chain big combos as fast as you can to get an S rank",
	inlevel = "AV",
	requirements = {
		{type = 7, field = "rank_check", value = checkEmRank}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_MOONSTONE,

	hudsprite = "TABEMBICON",
}

--
-- MRZ
--

MRCE_Achievements.add{
	id = "MRZ_Guard",
	typein = "Challenge",
	set = {"PAZ", "MRZ", "Medal"},
	name = "Egg Animus - Damageless",
	desc = "",
	tips = "Defeat the boss without taking damage",
	inlevel = "AZ",
	requirements = {
		{type = 7, field = "bosscheck", value = checkGuard}
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = S,
	color = SKINCOLOR_BROWN,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MRZ_Time",
	typein = "Challenge",
	set = {"PAZ", "MRZ", "Time"},
	name = "MRZ Time Attack",
	desc = "",
	tips = "Defeat the boss in under 2:40",
	inlevel = "AZ",
	requirements = {
		{type = 1, field = "realtime", value = 160*TICRATE},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = T,
	color = SKINCOLOR_BONE,

	hudsprite = "TABEMBICON",
}

--
-- DWZ
--

--
-- BONUS
--

MRCE_Achievements.add{
	id = "PMA_Complete",
	typein = "Challenge",
	set = {"PMA", "PMA1", "Task"},
	name = "Descending the Abyss",
	desc = "Completed the Level",
	tips = "???",
	inlevel = "BU",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = A,
	color = SKINCOLOR_BLUE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MRCE_RingsLots",
	typein = "Challenge",
	name = "It's like they're drawn to me!",
	desc = "",
	tips = "Have more than 900 rings when clearing any stage",
	requirements = {
		{type = 3, field = "rings", value = 900},
	},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MRCE_LivesLots",
	typein = "Challenge",
	name = "Never gonna keep me down",
	desc = "",
	tips = "Have more than 75 lives when clearing any stage",
	--requirements = {
	--	{type = 4, field = "lives", value = 75},
	--},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = R,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MRCE_AnySuperEmerald",
	typein = "Challenge",
	name = "Unlimited Power",
	desc = "Earned A Super Emerald",
	tips = "String together actions in an Emerald Srage and get a High Score!",
	--requirements = {
	--	{type = 4, field = "lives", value = 75},
	--},

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_MOONSTONE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MRCE_GameCompleteAnyPercent",
	typein = "Task",
	name = "Game Complete?....",
	desc = "Found any ending",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = J,
	color = SKINCOLOR_GOLDENROD,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MRCE_GameCompleteSuperEnd",
	typein = "Task",
	name = "Game Complete!",
	desc = "Found the good end",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = H,
	color = SKINCOLOR_SILVER,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MRCE_GameCompleteUltraEnd",
	typein = "Task",
	name = "Game Super Complete!",
	desc = "Found the true ending",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = H,
	color = SKINCOLOR_SILVER,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MRCE_MarathonAnyPercentSpeedrun",
	typein = "Task",
	name = "Blasting through with Sonic Speed!",
	desc = "In one marathon session, found the bad end in less than 00:34:00",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = K,
	color = SKINCOLOR_SAPPHIRE,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MRCE_MarathonUltraPercentSpeedrun",
	typein = "Task",
	name = "Super Sonic Racing",
	desc = "In one marathon session, found the true end in less than 1:20:00",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_MR_COMET,

	hudsprite = "TABEMBICON",
}

MRCE_Achievements.add{
	id = "MRCE_Shadow_UltimateClear",
	typein = "Task",
	name = "The True Ultimate Lifeform",
	desc = "Found the true ending on Ultimate Mode as Sonic",

	-- sprite definitions
	gamesprite = SPR_EMBM,
	frame = I,
	color = SKINCOLOR_MR_COMET,

	hidden = true,
	extras = true,

	hudsprite = "TABEMBICON",
}
addHook("PlayerThink", function (p)
	if gamemap == 121 and p.exiting and not All7Emeralds(emeralds) then
		MRCE_Achievements.unlock("MRCE_GameCompleteAnyPercent", sfx_chchng, p)
		if marathonmode and mrce.marathontimer <= 2040*TICRATE then
			MRCE_Achievements.unlock("MRCE_MarathonAnyPercentSpeedrun", sfx_chchng, p)
		end
	end
end)

addHook("PlayerSpawn", function(p)
	if mapheaderinfo[gamemap].lvlttl == "Credits" and mapheaderinfo[gamemap].mysticrealms then
		if gamemap == 98 then
			MRCE_Achievements.unlock("MRCE_GameCompleteSuperEnd", sfx_chchng, p)
			if mrce.hyperunlocked and mrce.elemshards >= 7 then
				MRCE_Achievements.unlock("MRCE_GameCompleteUltraEnd", sfx_chchng, p)
				if ultimatemode and p.mo.skin == "sonic" then
					MRCE_Achievements.unlock("MRCE_Shadow_UltimateClear", sfx_chchng, p)
				end
				if marathonmode and mrce.marathontimer <= 4800*TICRATE then
					MRCE_Achievements.unlock("MRCE_MarathonUltraPercentSpeedrun", sfx_chchng, p)
				end
			end
		end
	end
end)

addHook("PlayerThink", function(p)
	if p.spectator then return end
	if not p.realmo then return end
	if not mapheaderinfo[gamemap].mysticrealms then return end
	if p.mfrozenc and (p.frozen or p.frozentimer) then
		p.mfrozenc = false
	end
	if p.exiting then
		if p.lives >= 75 then
			MRCE_Achievements.unlock("MRCE_LivesLots", sfx_chchng, p)
		end
		if mapheaderinfo[gamemap].lvlttl == "Primordial Abyss" and p.mo.y > 28000*FRACUNIT then
			MRCE_Achievements.unlock("PMA_Complete", sfx_chchng, p)
		end
		if p.mfrozenc and gamemap == 115 then
			MRCE_Achievements.unlock(MFZ_Toasty, sfx_chchng, p)
			p.mfrozenc = nil
		end
	end
end)

-- Leave this at very end so we could measure how many achivements we have.
print('[Mystic Realms CE] '..#MRCE_Achievements.." achievements added!")

addHook("NetVars", function(net)
	--playerdamaged = net($)  --net sync that shit
	playerdamagedglobal = net($)
end)