--[[

Description:
Imported Save File Manager
from Pipe Kingdom Zone's Main - g_save.lua

Contributors: Skydusk
@Team Blue Spring 2024
]]

-- INTERNAL

local SUFFIX = mrce.autobuild and "AB" or (mrce.beta and "B" or "")
local PACK = "MysticRealmsCE"..SUFFIX
local VERSION = mrce.version

local GAME = PACK..VERSION

-- Version checking for previous version

local CHECK_PREV = true -- This disables checking for previous version's save file, this is only for major versions

-- Do not touch these carelessly.
local AUTO_CHECKPREV = mrce.autobuild and false or (mrce.beta and false or CHECK_PREV)
local PREV = AUTO_CHECKPREV and PACK..(VERSION - 1) or nil

local CONVERTPREV = nil -- Rewrite this variable into function when conversion is needed

--[[
	Pseudo-code Conversion example, can't write actual example here, Lua syntax thinks double brackets are where comment ends

	func CONVERTPREV (data)
		conv = (
			[8] = 16
			[9] = 32
		)

		acv = data.achivements

		for k, v in pairs(data.achivements)
			if k and conv(k) then
				data.achivements(k) = nil
				data.achivements(conv(k)) = true
			end
		end

		return data
	en
--]]

local MRCE_Unlock = {
	game_path = "mrce/saves",
	save_system_version = "1",

	saves = {
		-- Temponary
		["TEMP_MP"] = {
			achivements = {},
			stats = {},
		}, -- Used for current multiplayer session
		["DEFAULT"] = {
			achivements = {},
			stats = {},
		}, -- Default Template

		-- Actual Saves:
		[GAME] = {
			achivements = {},
			stats = {},
			-- We need to decide what should come here.

		}, -- Save_file for Singleplayer
		-- ... (undefined, but likely more saves)
	}

}

--
--	MAIN
--

function MRCE_Unlock.loadData()
	--local path = "client/"..(MRCE_Unlock.game_path).."/"..k..".dat"
	--local version = true
	--local check = {}

	--local file = io.openlocal("client/"..(MRCE_Unlock.game_path).."/saves.dat", "r")
	--if file then
	--	for line in file:lines() do
	--		table.insert(check, line)
	--	end
	--	file:close()
	--end

	--if check then
		local current_gamadataexists = false
		local compact_check = false

		-- Check if files exists

		if PREV then
			local file = io.openlocal("client/"..(MRCE_Unlock.game_path).."/"..PREV..".dat", "r")
			if file then
				compact_check = true
				file:close()
			end
		end

		local file = io.openlocal("client/"..(MRCE_Unlock.game_path).."/"..GAME..".dat", "r")
		if file then
			current_gamadataexists = true
			file:close()
		end

		-- In case previous version was found, and new version wasn't created
		if compact_check and not current_gamadataexists then
			MRCE_Unlock.saves[GAME] = TBSlib.deserializeIO("client/"..(MRCE_Unlock.game_path).."/"..PREV..".dat")

			if type(CONVERTPREV) == "function" then
				MRCE_Unlock.saves[GAME] = CONVERTPREV(MRCE_Unlock.saves[GAME])
			end

			print('[Mystic Realms CE]'.." "..GAME.." Save Data from Previous Version Loaded!")
		elseif current_gamadataexists then
			MRCE_Unlock.saves[GAME] = TBSlib.deserializeIO("client/"..(MRCE_Unlock.game_path).."/"..GAME..".dat")
			print('[Mystic Realms CE]'.." "..GAME.." Save Data Loaded!")
		end
	--end

	MRCE_Unlock.saves["TEMP_MP"] = MRCE_Unlock.saves[GAME] or MRCE_Unlock.saves["DEFAULT"]
end

function MRCE_Unlock.defaultData()
	local any_default = false
	local only_default = true

	for k,v in pairs(MRCE_Unlock.saves["DEFAULT"]) do
		if not MRCE_Unlock.saves[GAME][k] then
			MRCE_Unlock.saves[GAME][k] = v
			any_default = true
		else
			only_default = false
		end
	end
	-- insert actual defaults

	if only_default then
		print('[Mystic Realms CE]'.." no standard values found, defaulting values!")
	elseif any_default then
		print('[Mystic Realms CE]'.." missing values found in save, reverting to default values!")
	end
end

function MRCE_Unlock.saveData()
	--local saves = ""
	for k,v in pairs(MRCE_Unlock.saves) do
		if (k == "DEFAULT" or k == "TEMP_MP") then continue end
		TBSlib.serializeIO(v, "client/"..(MRCE_Unlock.game_path).."/"..k..".dat")
		--saves = $..k.."\n"
		print('[Mystic Realms CE]'.." "..k.." Save Data Saved!")
	end

	--local file = io.openlocal("client/"..(MRCE_Unlock.game_path).."/saves.dat", "w")
	--if file then
	--	file:write(saves)
	--	file:close()
	--end
end

function MRCE_Unlock.getSaveData()
	if multiplayer then
		return MRCE_Unlock.saves["TEMP_MP"]
	else
		return MRCE_Unlock.saves[GAME]
	end
end

function MRCE_Unlock.writeIntoSaveData(set, field, value)
	if multiplayer then
		MRCE_Unlock.saves["TEMP_MP"][set][field] = value
	else
		MRCE_Unlock.saves[GAME][set][field] = value
	end
end


MRCE_Unlock.loadData()
MRCE_Unlock.defaultData()

addHook("GameQuit", function(quit)
	if quit then
		MRCE_Unlock.saveData()
	end
end)

--
-- UTILITY
--

function MRCE_Unlock.resetProgress()
	if MRCE_Unlock.saves then
		local save_data = MRCE_Unlock.getSaveData()
		save_data = MRCE_Unlock.saves["DEFAULT"]
	end
end

--
--	MULTIPLAYER
--

-- SERVER_SAVE
addHook("GameQuit", function(quit)
	if not quit then
		if multiplayer then
			if consoleplayer == server then
				MRCE_Unlock.saves[GAME] = MRCE_Unlock.saves["TEMP_MP"]
			end
			MRCE_Unlock.saves["TEMP_MP"] = MRCE_Unlock.saves[GAME]
		end
	end
end)

-- SYNC_SERVER WITH CLIENT
addHook("NetVars", function(net)
	MRCE_Unlock.saves["TEMP_MP"] = net($)
end)

rawset(_G, "MRCE_Unlock", MRCE_Unlock)