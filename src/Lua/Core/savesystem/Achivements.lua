--[[

	|												|||
	|	MRCE Achievements --- Framework				|||
	|												|||
	\-----------------------------------------------\||
	.\-----------------------------------------------\|

	|- @Contributors: Skydusk
	|- @2024/2025
	|
	|- Code's intended use-case was for Mystic Realms Community Edition. As long contributors are credited,
	|  You are free to use, replicate, edit or contribute to this framework outside of Mystic Realms.
]]

--#region documentation function palette
--
-- CLASS MRCE_Achievements
--
---------------
-- >PROPERTIES:
---------------
--
-- #MRCE_Achievements == number of definitions (or total amount of existing achievements)
--
-- >METHODS:
--
--	MRCE_Achievements.add(table: definition) -> integer: achievement index
-- 	| adds new achievement onto stack
--
--	MRCE_Achievements.isUnlocked(integer: achievement index) -> boolean
-- 	| checks if achievement is unlocked
--
--	MRCE_Achievements.progress(void) -> fixed_t progress (100% == FRACUNIT) + excess
-- 	| calculates progress of unlocked achievements
--
--	MRCE_Achievements.recount(void) -> number
-- 	| recounts number of unlocked achievements
--
--	MRCE_Achievements.searchByCategory(string: category) -> table : achievements
-- 	| provides table of achievements of one category
--
--	MRCE_Achievements.searchByID(string: id_string) -> achievement, integer: achievement index
-- 	| searches by string ID a achievement
--
--	MRCE_Achievements.unlock(string|number: achievement id, sfx: sound effect index, player: player_t)
-- 	| unlocks achievement
--
----------------------
-- >LINEDEF EXECUTORS:
----------------------
--
-- "MRCEACHI"
-- | Unlocks specific achievement
-- |
-- | Choose only 1 out of these 2 of identification
-- |
-- |- line args{] -- in level editor arg + 1
-- |--- arg0: number index of achievement
-- |- line stringargs{] -- in level editor arg + 1
-- |--- arg1: string identification of achievement
--
--
-- "MRCECHKA"
-- | Checks if achievement is unlocked -> activates tag based on args[0]
-- |
-- |- line args{] -- in level editor arg + 1
-- |--- arg0: execute tag
-- |- line stringargs{] -- in level editor arg + 1
-- |--- arg1: string identification of achievement
--
--
-- "MRCEACCO"
-- | Counts how many achievements are unlocked and if they reached limit -> activates tag based on args[0]
-- |
-- |- line args{] -- in level editor arg + 1
-- |--- arg0: execute tag
-- |--- arg1: min achievements unlocked (optional)
-- |--- arg2: max achievements unlocked (optional)
--
--
--#endregion
--#region prompt drawer

-- Function to draw prompt, used in different part of the project
---@param v videolib
---@param prompt table
local function prompt_achv(v, prompt)
	v.draw(320-128, 200-prompt.y, v.cachePatch("PROMPTGPH"), V_SNAPTORIGHT|V_SNAPTOBOTTOM)

	local icon_color = v.getColormap(TC_DEFAULT, prompt.iconcolor or SKINCOLOR_BLUE)

	if prompt.icon then
		MRCElibs.drawFakeHRotation(v, (320-121)*FRACUNIT, (223-prompt.y)*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, v.cachePatch(prompt.icon), V_SNAPTORIGHT|V_SNAPTOBOTTOM, icon_color)
	else
		MRCElibs.drawFakeHRotation(v, (320-121)*FRACUNIT, (223-prompt.y)*FRACUNIT, (leveltime/2)*ANG1, FRACUNIT, v.cachePatch("TABEMBICON"), V_SNAPTORIGHT|V_SNAPTOBOTTOM, icon_color)
	end

	v.drawString(320-102, 203-prompt.y, '\x8B'..'Achievement Unlocked!', V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")
	if string.len(prompt.name) > 20 then
		local string_1 = string.sub(prompt.name, 1, 20)
		local string_2 = string.sub(prompt.name, 21, string.len(prompt.name))
		if string.len(string_2) > 20 then
			string_2 = string.sub(string_2, 1, 17).."..."
		end

		v.drawString(320-102, 211-prompt.y, string_1, V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")
		v.drawString(320-102, 219-prompt.y, string_2, V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")

	else
		v.drawString(320-102, 211-prompt.y, prompt.name, V_SNAPTORIGHT|V_SNAPTOBOTTOM, "thin")
	end
end

--#endregion
--#region class definitions

---@class ach_actions : table
---@field onClick 					function	On click event non interface	  format: function(achievement metadata : table)
---@field interfaceClose 			function	When interface closes			  format: function(none)
---@field interfaceThink 			function	While interface is open			  format: function(key : keyevent)
---@field interfaceOpen 			function	When interface opens			  format: function(achievement metadata : table)
---@field interfaceDraw 			function	Drawer to use to draw the action, format: function(v : drawer, player : player_t, pointer : current item)

---@alias ach_requirement_types : numbers
---| 'nil' 	achievement
---| '0' 	* achievement
---| '1' 	* time attack
---| '2' 	* score attack
---| '3' 	* ring attack
---| '4' 	* player check higher
---| '5' 	* player check equal
---| '6' 	* player check lower
---| '7' 	* custom function

---@alias ach_special_types : string
---| '"generic"'
---| '"task"'
---| '"medal"'
---| '"challenge"'
---| '"music"'
---| '"lore"'
---| '"art"'
---| '"special"'

---@class ach_requirement : table
---@field type 	ach_requirement_types
---@field field string? 				Only used by type 4, 5 and 6 (Player checks)
---@field var 	number|function			Type is a 7 (Custom function)

---@class achdefinition_t
---@field id 			string?  					string identification
---@field set 			table|string?  				category
---@field name 			string?  					name of the achievement
---@field desc 			string?						long description default to "NULL" if not short description
---@field minidesc 		string?						short description default to "NULL"
---@field tips 			string?						tips to how to get the achievement default to "???"
---@field func 			function?					function that gets called when unlocked
---@field gamesprite 	number?						index of sprite
---@field hudsprite 	string?						name of the hud sprite
---@field frame 		number?						frame of the sprite
---@field color 		number?						color of the sprite
---@field typein 		ach_special_types?			type of achievement
---@field hidden 		boolean?					gives it hidden status
---@field extras 		boolean?					gives it shadow achievement status
---@field status        boolean?					if not true, add WIP flag to achievement
---@field requirements 	table<ach_requirement>?		blocks unlock if these achievements are not met yet
---@field inlevel 		number|string?				forces achievement to be unlocked only in this level
---@field metadata 		table?						custom metadata as an addition to descriptive ones
---@field specials 		ach_actions?				should contain specific actions related to menus

---@class achievement_t
---@field id 			string  					string identification
---@field set 			table|string  				category
---@field name 			string  					name of the achievement
---@field desc 			string						long description default to "NULL" if not short description
---@field minidesc 		string						short description default to "NULL"
---@field tips 			string						tips to how to get the achievement default to "???"
---@field func 			function					function that gets called when unlocked
---@field gamesprite 	number						index of sprite
---@field hudsprite 	string						name of the hud sprite
---@field frame 		number						frame of the sprite
---@field color 		skincolor_t|number			color of the sprite
---@field typein 		ach_special_types			type of achievement
---@field flags 		number						various flags
---@field extras 		boolean						gives it shadow achievement status
---@field requirements 	table<ach_requirement>		blocks unlock if these achievements are not met yet
---@field inlevel 		number|string				forces achievement to be unlocked only in this level
---@field metadata 		table						custom metadata as an addition to descriptive ones
---@field specials 		ach_actions					should contain specific actions related to menus

---@class achivementindex : number

---@class achievementsfunc
---@field add 				function
---@field searchByName 		function
---@field searchByID		function
---@field searchByCategory 	function
---@field recount			function
---@field isUnlocked		function
---@field progress			function
---@field checkCheating		function
---@field unlock			function
---@field unlocked			number
---@field extras			number
---@field nonextratotal		number

--#endregion
--#region internals

local MRCE_Achievements = {unlocked = 0, extras = 0, nonextratotal = 0} ---@type table<achievement_t>|achievementsfunc
local identificators = {}
local id_leveldatabase = {}
local intermission_check = {}
local id_categorydb = {global = {}}

--#endregion
--#region methods

---@param info achdefinition_t
---@return achivementindex? --- index of added achievement
--* --> info table schema:
--* .id:
--* --> datatype: string defaults to "NULL"
--* --> description: string identification
--*
--* .set:
--* --> datatype: string/table
--* --> description: belong to what categories
--*
--* .name:
--* --> datatype: string defaults to "NULL"
--*
--* .desc:
--* --> datatype: string
--* --> description: long description default to "NULL" if not short description
--*
--* .minidesc:
--* --> datatype: string
--* --> description: short description default to "NULL"
--*
--* .tips:
--* --> datatype: string
--* --> description: tips to how to get the achievement default to "???"
--*
--* .func:
--* --> datatype: function
--* --> description: function that gets called when unlocked
--*
--* .gamesprite:
--* --> datatype: sprite index (number)
--* --> description: index of sprite
--*
--* .hudsprite:
--* --> datatype: string
--* --> description: name of the hud sprite
--*
--* .frame:
--* --> datatype: frame index (number)
--* --> description: frame of the sprite
--*
--* .color:
--* --> datatype: skincolor index (number)
--* --> description: color of the sprite
--*
--* .typein:
--* --> datatype: string
--* --> description: type of achievement
--*
--* .hidden:
--* --> datatype: boolean
--* --> description: gives it hidden status
--*
--* .extras:
--* --> datatype: boolean
--* --> description: gives it shadow achievement status
--*
--* .status:
--* --> datatype: boolean
--* --> description: marks achievement as finished, by default(nil) = WIP
--*
--* .requirements:
--* --> datatype: table of requirements
--* --> description: blocks unlock if these achievements are not unlocked yet, format {type = "stats check", field = nil, var = 1}
--*
--* .inlevel:
--* --> datatype: number/string
--* --> description: forces achievement to be unlocked in level
--*
--* .metadata:
--* --> datatype: table
--* --> description: Custom table, fill it with anything. Just use the contents.
--*
--* .specials:
--* --> datatype: table
--* --> description: Should contain specific actions related to menus
--*
function MRCE_Achievements.add(info)
	if type(info) ~= "table" then
		error('[Add Achievement] Not a table')
		return
	end

	-- setup temp
	local index = {} ---@type achievement_t

	-- Metadata

	if info.name then
		index.name = tostring(info.name)
	else
		index.name = "NULL"
	end

	if info.desc then
		index.desc = tostring(info.desc)

		if info.minidesc then
			index.minidesc = tostring(info.desc)
		else
			index.minidesc = info.desc
		end
	elseif info.minidesc then
		index.desc = tostring(info.minidesc)
		index.minidesc = index.desc
	else
		index.desc = "NULL"
		index.minidesc = index.desc
	end

	if info.tips then
		index.tips = tostring(info.tips)
	else
		index.tips = "???"
	end

	-- active upon unlocking

	if info.func then
		index.func = info.func
	end

	-- spriteinfo

	if info.gamesprite and type(info.gamesprite) == 'number' then
		index.gamesprite = info.gamesprite
	end

	if info.hudsprite and type(info.hudsprite) == 'string' then
		index.hudsprite = info.hudsprite
	end

	if info.frame then
		index.frame = info.frame
	else
		index.frame = A
	end

	if info.color then
		index.color = info.color
	else
		index.color = SKINCOLOR_BLUE
	end

	-- Properties


	-- flags
	index.flags = 0	 -- compression of flags if necessary

	if info.hidden then
		index.flags = $|1
	end

	if not info.status then -- forcing WIP status on all of them
		index.flags = $|2
	end


	-- other

	if info.typein and type(info.typein) == "string" then
		index.typein = string.lower(info.typein)
	else
		index.typein = "generic"
	end

	if info.inlevel then
		local num = info.inlevel

		if type(info.inlevel) == "string" then
			num = G_FindMapByNameOrCode(info.inlevel)
		end

		if type(num) == "number" then
			if not id_leveldatabase[num] then
				id_leveldatabase[num] = {}
			end
			table.insert(id_leveldatabase[num], #MRCE_Achievements+1)

			index.inlevel = num
		end
	end

	if info.requirements then
		if type(info.requirements) == "table" then
			index.requirements = info.requirements

			local reqs = index.requirements

			if index.inlevel then
				for i = 1, #reqs do
					local req = reqs[i]
					if req.type and req.type > 0 then
						if not intermission_check[index.inlevel] then
							intermission_check[index.inlevel] = {}
						end

						table.insert(intermission_check[index.inlevel], #MRCE_Achievements+1)
						break
					end
				end
			end
		else
			error('[Add Achievement] Requirements - Not a table')
		end
	end

	if info.id then
		index.id = info.id
		identificators[info.id] = #MRCE_Achievements + 1
	else
		index.id = "NULL"
	end

	-- This is in order to be done first
	-- extra is shadow achivements that do not count to %
	if info.extras then
		index.extras = true
	else
		MRCE_Achievements.nonextratotal = $ + 1
		index.extras = false
	end

	-- Categorization
	if info.set then
		if type(info.set) == "string" then
			index.set = info.set
			if not id_categorydb[info.set] then
				id_categorydb[info.set] = {}
			end

			table.insert(id_categorydb[info.set], #MRCE_Achievements + 1)
		elseif type(info.set) == "table" then
			local sets = info.set
			for i = 1, #sets do
				local set = sets[i]
				if type(set) == "string" then
					index.set = info.set
					if not id_categorydb[set] then
						id_categorydb[set] = {}
					end

					table.insert(id_categorydb[set], #MRCE_Achievements + 1)
				end
			end
		else
			index.set = "Global"
			table.insert(id_categorydb.global, #MRCE_Achievements + 1)
		end
	else
		index.set = "Global"
		table.insert(id_categorydb.global, #MRCE_Achievements + 1)
	end

	if info.metadata and type(info.metadata) == "table" then
		index.metadata = info.metadata
	end

	if info.specials and type(info.specials) == "table" then
		index.metadata = info.specials
	end

	table.insert(MRCE_Achievements, index)
	return #MRCE_Achievements
end

-- Iterative method of finding, not recommended
---@param name string
---@return achievement_t?, achivementindex?
---@deprecated
function MRCE_Achievements.searchByName(name)
	for id = 1, #MRCE_Achievements do
		local entry = MRCE_Achievements[id]
		if name == entry.name then
			return entry, id
		end
	end
end

-- Gets achievement by ID from LUT, preferable method.
---@param id string achievement_t->id
---@return achievement_t entry
---@return achivementindex id
---@return nil
function MRCE_Achievements.searchByID(id)
	if identificators[id] and MRCE_Achievements[identificators[id]] then
		return MRCE_Achievements[identificators[id]], identificators[id]
	end
end

-- Gets a list of achievements by category string from LUT
---@param category string achievement_t->category[i]
---@return table? result
function MRCE_Achievements.searchByCategory(category)
	if id_categorydb[category] then
		local array = id_categorydb[category]
		local result = {}

		for i = 1, #array do
			local ach = array[i] ---@type achievement_t
			table.insert(result, ach)
		end

		return result
	end
end

-- Recounts all available achievements
---@return number result
---@return number result
function MRCE_Achievements.recount()
	local save = MRCE_Unlock.getSaveData().achivements
	local result = 0
	local extra = 0

	for k, v in pairs(save) do
		if v and MRCE_Achievements[k] then
			if MRCE_Achievements[k].extras then
				extra = $+1
			else
				result = $+1
			end
		end
	end

	MRCE_Achievements.unlocked = result
	MRCE_Achievements.extras = extra
	return result, extra
end

-- Checks if achievement is unlocked
---@param id achivementindex index not string id
---@return boolean
function MRCE_Achievements.isUnlocked(id)
	local save = MRCE_Unlock.getSaveData().achivements
	if save[id] then
		return true
	else
		return false
	end
end

-- Calculates unlock progress
--* 1.00 (FRACUNIT) == 100% + Seperates shadow/extra achievements
---@return fixed_t
function MRCE_Achievements.progress()
	local progress = MRCE_Achievements.unlocked * FRACUNIT / MRCE_Achievements.nonextratotal

	if progress >= FRACUNIT then
		local artifical = #MRCE_Achievements - MRCE_Achievements.nonextratotal

		return progress + (MRCE_Achievements.extras * FRACUNIT / artifical) / 50
	else
		return progress
	end
end

---@alias MRA_cheatingScenarios
---| 0 clean, no cheats
---| 1 use of vanilla cheats
---| 2 debug mode

-- Checks if cheating is currently active, and returns
---@return MRA_cheatingScenarios
function MRCE_Achievements.checkCheating()
	if mrce and mrce.debugmode then
		return 2
	elseif usedCheats then
		return 1
	else
		return 0
	end
end

--- 'nil' 	achievement
--- '0' 	* achievement
--- '1' 	* time attack
--- '2' 	* score attack
--- '3' 	* ring attack
--- '4' 	* player check higher
--- '5' 	* player check equal
--- '6' 	* player check lower

local types_req = {
	[0] = function(v, save)
		if save[v] then
			return false
		else
			return true
		end
	end,
	[1] = function(v, save, player)
		if not player then return true end

		if player.realtime <= v.value then
			return false
		else
			return true
		end
	end,
	[2] = function(v, save, player)
		if not player then return true end

		if player.score >= v.value then
			return false
		else
			return true
		end
	end,
	[3] = function(v, save, player)
		if not player then return true end

		if player.rings >= v.value then
			return false
		else
			return true
		end
	end,
	[4] = function(v, save, player)
		if not player then return true end

		local field = v.field

		if field == nil then
			return false
		end

		if player[field] == nil then
			return true
		end

		if player[field] > v.value then
			return false
		else
			return true
		end
	end,
	[5] = function(v, save, player)
		if not player then return true end

		local field = v.field

		if field == nil then
			return false
		end

		if player[field] == nil then
			return true
		end

		if player[field] == v.value then
			return false
		else
			return true
		end
	end,
	[6] = function(v, save, player)
		if not player then return true end

		local field = v.field

		if field == nil then
			return false
		end

		if player[field] == nil then
			return true
		end

		if player[field] < v.value then
			return false
		else
			return true
		end
	end,
	[7] = function(v, save, player)
		if type(v.value) ~= "function" then return false end
		return v.value(v, save, player)
	end
}

local devmode_unlocks = {}

local TIC_COOLDOWN = 4*TICRATE -- just enough for pop-up
local sound_cooldown = 0

-- Unlocks achievement
---@param query_id 	achivementindex|string 		can be index or string id
---@param sfx 		number? 					optional for sounds
---@param player 	player_t?					optional for sounds but required for time/score/ring attack requirements
function MRCE_Achievements.unlock(query_id, sfx, player)
	local info_id = query_id
	local cheating = MRCE_Achievements.checkCheating()

	if cheating == 1 then
		return
	end

	if type(info_id) == "string" then
		local entry
		entry, info_id = MRCE_Achievements.searchByID(info_id)
	end

	if type(info_id) == "number" and MRCE_Achievements[info_id] then
		local achivement = MRCE_Achievements[info_id]
		local save = MRCE_Unlock.getSaveData().achivements

		if achivement.requirements then
			local unfullfilled = false

			for _,v in ipairs(achivement.requirements) do
				---@cast v ach_requirement
				if types_req[v.type] then
					unfullfilled = types_req[v.type](v, save, player)
				else
					unfullfilled = types_req[0](v, save)
				end

				if unfullfilled == true then
					return
				end
			end
		end

		if achivement.inlevel and gamemap ~= achivement.inlevel then
			return
		end

		if (not save[info_id]) or (mrce and mrce.debugmode and not devmode_unlocks[info_id]) then
			if cheating == 2 then
				devmode_unlocks[info_id] = true
			else
				save[info_id] = true -- save existence
				MRCE_Achievements.recount()
			end

			if player and not sound_cooldown then
				S_StartSound(nil, sfx or sfx_chchng, player)
			end

			if achivement.func then
				achivement.func()
			end

			mrceprompts.add{
				duration = 4*TICRATE+TICRATE/2,
				easepoint = TICRATE/2,
				drawer = prompt_achv,
				name = achivement.name,
				icon = achivement.hudsprite,
				iconcolor = achivement.color
			}

			sound_cooldown = TIC_COOLDOWN
		end
	end
end
--#endregion
--#region medal objects

-- Achievement Granting General Object
local mobjinfo_id = freeslot("MT_MRCEACHIVEMENT")

---@diagnostic disable-next-line
mobjinfo[mobjinfo_id] = {
--$Category MRCE Generic
--$Name Achievement Medal
--$Sprite MECEA0
--$Arg0 Order
--$Arg1 Frame
--$NotAngled
--$StringArg1 Achievement ID
	doomednum = 831,
	spawnstate = S_EMBLEM1,
	spawnhealth = 1000,
	reactiontime = 8,
	deathstate = S_SPRK1,
	deathsound = sfx_ncitem,
	speed = 1,
	radius = 16*FRACUNIT,
	height = 30*FRACUNIT,
	mass = 4,
	flags = MF_SPECIAL|MF_NOGRAVITY|MF_NOCLIPHEIGHT
}

---@param medal mobj_t
---@param mt mapthing_t
addHook("MapThingSpawn", function(medal, mt)
	if id_leveldatabase[gamemap] and id_leveldatabase[gamemap][mt.args[0]] then
		medal.extravalue1 = id_leveldatabase[gamemap][mt.args[0]]
		local save = MRCE_Unlock.getSaveData().achivements
		if save[medal.extravalue1] then
			medal.extravalue2 = 1
		end
	end
	if mt.stringargs[1] then
		local entry
		entry, medal.extravalue1 = MRCE_Achievements.searchByID(mt.stringargs[1])
		local save = MRCE_Unlock.getSaveData().achivements
		if save[medal.extravalue1] then
			medal.extravalue2 = 1
		end

		if entry.gamesprite then
			medal.sprite = entry.gamesprite
		end

		if entry.frame then
			medal.frame = entry.frame
		end

		if entry.color then
			medal.color = entry.color
		end
	end

	if mt.stringargs[0] then
		if _G[mt.stringargs[0]] and skincolors[_G[mt.stringargs[0]]] then
			medal.color = skincolors[_G[mt.stringargs[0]]]
		end
	end
end, mobjinfo_id)

addHook("MapLoad", function()
	devmode_unlocks = {}

	MRCE_Achievements.recount()
end)

---@param medal mobj_t
addHook("MobjSpawn", function(medal)
	medal.origz = medal.z
end, mobjinfo_id)

---@param medal mobj_t
addHook("MobjThinker", function(medal)
	if not (medal and medal.valid) then return end

	local cheating_level = MRCE_Achievements.checkCheating()

	if cheating_level == 2 then
		medal.alpha = FRACUNIT
	elseif cheating_level == 1 then
		medal.alpha = 0
	else
		medal.alpha = FRACUNIT
		if medal.extravalue2 then
			medal.alpha = FRACUNIT/2
		end
	end

	if not (medal.frame & FF_PAPERSPRITE) then
		medal.frame = $|FF_PAPERSPRITE
	end

	medal.angle = $ + FixedAngle(FRACUNIT)
	medal.z = medal.origz + 8 * abs(sin(FixedAngle(leveltime*4*FRACUNIT)))

	local sparkle = P_SpawnMobjFromMobj(medal, P_RandomRange(-10, 10) * FRACUNIT, P_RandomRange(-10, 10) * FRACUNIT, 20*FRACUNIT, MT_SUPERSPARK)
	sparkle.momx = P_RandomRange(-2, 2) * FRACUNIT
	sparkle.momy = P_RandomRange(-2, 2) * FRACUNIT
	sparkle.momz =  P_RandomRange(-2, 2) * FRACUNIT
	sparkle.colorized = true
	sparkle.color = medal.color
	sparkle.fuse = 10
	sparkle.scale = medal.scale *1/6
	sparkle.source = medal

	if medal.alpha < FRACUNIT or (medal.frame & FF_TRANSMASK) then
		sparkle.flags2 = MF2_DONTDRAW
	end
end, mobjinfo_id)

--	Touch Hook
---@param medal mobj_t
addHook("TouchSpecial", function(medal)
	local cheating_level = MRCE_Achievements.checkCheating()

	if cheating_level == 2 then
		if medal.extravalue1 then
			MRCE_Achievements.unlock(medal.extravalue1)
		end
	elseif cheating_level == 1 then
		return true
	else
		if medal.extravalue2 then
			return true
		end

		if medal.extravalue1 and not medal.extravalue2 then
			MRCE_Achievements.unlock(medal.extravalue1)
			medal.extravalue2 = 1
		end
	end
end, mobjinfo_id)


--#endregion
--#region linedef executors

-- Unlocks specific achievement
--- choose only 1 out of these 2 of identification
--- line args{] -- in level editor arg + 1
--- arg0: number index of achievement
--- line stringargs{] -- in level editor arg + 1
--- arg1: string identification of achievement
---@param line line_t
---@param mo mobj_t
addHook("LinedefExecute", function(line, mo)
	if MRCE_Achievements.checkCheating() == 1 then
		return true
	end

	if line.args[0] then
		MRCE_Achievements.unlock(line.args[0])
	end

	if line.stringargs[1] then
		if mo and mo.player then
			MRCE_Achievements.unlock(line.stringargs[1], nil, mo.player)
		else
			MRCE_Achievements.unlock(line.stringargs[1])
		end
	end
end, "MRCEACHI")

-- Checks if achievement is unlocked -> activates tag based on args[0]
--- line args{] -- in level editor arg + 1
--- arg0: execute tag
--- line stringargs{] -- in level editor arg + 1
--- arg1: string identification of achievement
---@param line line_t
---@param mo mobj_t
addHook("LinedefExecute", function(line, mo)
	if line.stringargs[1] and line.args[0]
	and line.frontsector then
		local id = MRCE_Achievements.searchByID(line.stringargs[1])

		if id and MRCE_Achievements.isUnlocked(id) then
			P_LinedefExecute(line.args[0], mo, line.frontsector)
		end
	end
end, "MRCECHKA")

-- Counts how many achievements are unlocked and if they reached limit -> activates tag based on args[0]
--- line args[] -- in level editor arg + 1
--- arg0: execute tag
--- arg1: min achievements unlocked (optional)
--- arg2: max achievements unlocked (optional)
---@param line line_t
---@param mo mobj_t
addHook("LinedefExecute", function(line, mo)
	local count = MRCE_Achievements.recount()

	if line.args[1] and line.args[1] >= count then
		return
	end

	if line.args[2] and line.args[2] <= count then
		return
	end

	if line.args[0] and line.frontsector then
		P_LinedefExecute(line.args[0], mo, line.frontsector)
	end
end, "MRCEACCO")

---Checks all time-attack achievements & reduces sound cooldown
---@param player player_t
addHook("PlayerThink", function(player)
	if player.exiting > 0 and player.exiting < 95 then --give other functions time to update before checking achievements
		local level_checks = intermission_check[gamemap]

		if level_checks then
			for i = 1, #level_checks do
				local check = level_checks[i]

				if not MRCE_Achievements.isUnlocked(check) then
					MRCE_Achievements.unlock(check, sfx_chchng, player)
				end
			end
		end
	end

	if sound_cooldown then
		sound_cooldown = $ - 1
	end
end)

--#endregion

--#globalsetup
MRCE_Achievements.recount()

rawset(_G, "MRCE_Achievements", MRCE_Achievements)