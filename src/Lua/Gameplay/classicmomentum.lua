--[[Lite version of the new momentum script by rumia1. (full version name pending)
This script may freely be reused, reproduced, or edited by
the Mystic Realm CE team for the Mystic Realm CE project.

If you wish to use this script for your own projects or level packs,
please wait for the full release which will be marked as reusable]]


--[[
=============Variable declerations ==============
]]


local physicsOptions =
{
	["Default"]= 0,
	["Recoded"] = 1,
	["Vanilla"] = 2
}

local CV_MRCE_PhysicsMode = CV_RegisterVar{
	name = "mr_physicsMode",
	defaultvalue = "Default",
	flags = CV_CHEAT,
	PossibleValue = physicsOptions
}

local CV_MRCE_JumpCancelSpin = CV_RegisterVar{
	name = "mr_spinDashJumpCancel",
	defaultvalue = 0,
	flags = CV_CALL,
	func = function(var)
		print("Behavior WIP")
	end,
	PossibleValue = CV_YesNo
}

local CV_MRCE_fovFX = CV_RegisterVar{
	name = "mr_fovfx",
	defaultvalue = 0,
	flags = CV_CALL,
	func = function(var)
		print("FOV EFFECTS are currently unfinished, Use at your own risk")
	end,
	PossibleValue = CV_OnOff
}


local SPEED_DECREASE_LEEWAY = 15*FRACUNIT -- the amount of speed below normalspeed needed to update normalspeed

for i = 1, 7, 1 do
	freeslot("sfx_rev" .. i)
end

local PEELSPEED = 60*FRACUNIT

local charactersupport = {
	["sonic"] = {true, true, 0, true},
	["tails"] = {true, true, 0, false},
	["knuckles"] = {true, true, 0, false},
	["fang"] = {true, true, 0, false},
	["sms"] = {false, false, 1, false},
	["adventuresonic"] = {false, false, 1, false},
	["blaze"] = {true, true, 1, false},
	["juniosonic"] = {false, false, 1, false},
	["iclyn"] = {false, false, 1, false},
	["ass"] = {false, true, 1, false},
	["aether"] = {false, true, 1, false},
	["inazuma"] = {false, true, 1, false},
	["amy"] = {true, false, 1, false},
	["mario"] = {false, false, 1, false},
	["luigi"] = {false, false, 1, false},
	["kiryu"] = {false, false, 1, false},
	["metaknight"] = {true, false, 1, false},
	["bandages"] = {false, false, 1, false},
	["flarethetrex"] = {false, false, 1, false},
	["bowser"] = {true, false, 1, false},
	["pointy"] = {true, false, 1, false},
	["fluffy"] = {true, false, 1, false},
	["samus"] = {false, false, 1, false},
	["basesamus"] = {false, false, 1, false},
	["rosegoldamy"] = {true, false, 1, false}
}

rawset(_G, "mrceCharacterPhysics", function(skin, momentum, stats, spindash, run2)
	local s = string.lower(skin)
	if charactersupport[s] ~= nil then return end
	local r2 = run2 or false --peel anim default to false
	charactersupport[s] = {momentum, stats, spindash, run2} --table insert doesn't like using a string as the position. makes sense ig but I don't care about position or order I just need to add entries
	print("Added CharacterPhysics support for skinname \130" .. s)
	local momcolor, statscolor, spincolor, peelcolor = momentum and "\131" or "\133", stats and "\131" or "\133", spindash and "\133" or "\131", r2 and "\131" or "\133"
	print("Data set \138Momentum: " .. momcolor .. tostring(momentum) .. " \138Stats: " .. statscolor .. tostring(stats) .. " \138Spindash: " .. spincolor .. tostring(spindash) .. " \138Peelout Animation: " .. peelcolor .. tostring(r2) "\n")
end)

local function P_SpawnCoolSkidDust(player, radius, sound, state, step)
    local particle = P_SpawnMobjFromMobj(player.mo, 0, 0, 0, MT_SPINDUST)
    local xn = P_RandomChance(FRACUNIT/2)
    local yn = P_RandomChance(FRACUNIT/2)
    if xn then xn = -1 else xn = 1 end
    if yn then yn = -1 else yn = 1 end

	local x, y = 0, 0

	x = step and (particle.x + (xn * (FixedMul(radius,P_RandomRange(FRACUNIT/3, FRACUNIT)) * player.mo.scale))) or (particle.x + (xn * (FixedMul(radius,P_RandomFixed()) * player.mo.scale)))
	y = step and (particle.y + (yn * (FixedMul(radius,P_RandomRange(FRACUNIT/3, FRACUNIT)) * player.mo.scale))) or (particle.y + (yn * (FixedMul(radius,P_RandomFixed()) * player.mo.scale)))

    local z = particle.z --+ (P_RandomRange(0, FRACUNIT-1))
    P_MoveOrigin(particle, x, y, z)
    particle.tics = 10
    particle.scale = (2*player.mo.scale)/3
    particle.momx = player.mo.momx/4
    particle.momy = player.mo.momy/4
    P_SetScale(particle, particle.destscale)
    P_SetObjectMomZ(particle, FRACUNIT, false)
    if player.powers[pw_super] then
        P_SetObjectMomZ(particle, FRACUNIT*3, false)
    end

    if sound then
        S_StartSound(player.mo, sfx_s3k7e)
    end
	if state then
		particle.state = state
	end
    return particle -- the one thing the original version of this didn't do, smh smh
end

--[[
=============COMMON FUNCTIONS==============
]]

	--65536 FRACUNIT
local fovTimer = 0
local function FOVManager(player)
	local fovThresh = 20*FRACUNIT
	local amountOfFovToAdd = FixedDiv(player.rspeed,10*FRACUNIT)
	local maxFOV = 45*FRACUNIT
	local pSpeed = player.rspeed

	amountOfFovToAdd = $ / FRACUNIT
	amountOfFovToAdd = $ * 1000


	--print("PreEase FOV: " .. amountOfFovToAdd)
	amountOfFovToAdd = ease.inquint(fovTimer, 0, maxFOV)
	print("Eased FOV: " .. amountOfFovToAdd)

	if amountOfFovToAdd > maxFOV then amountOfFovToAdd = maxFOV end --CAP the FOV


	--print("Timer: " .. fovTimer)
	--print("Speed: " .. pSpeed)


	local dif = ((fovThresh - pSpeed)/FRACUNIT)
	dif = abs($)
	dif = $ * 700

	if fovTimer < dif then fovTimer = $ + 500 end
	player.fovadd = amountOfFovToAdd

	if pSpeed < fovThresh then fovTimer = $ - 500 end

	--print("Final FOV: " .. amountOfFovToAdd)
end


--[[
===================================PHYSICS TYPES================================================
]]



local function MRCE_DefaultPhysics(player)
	if player.mo and player.mo.valid then --standard opener

		--------------------------------------------------
		--              CUSTOM VARIABLES                --
		--------------------------------------------------
		player.rspeed = FixedHypot(player.rmomx, player.rmomy) --gives the player's accurate speed in FU/T, kinda necessary for the entire project to function

		if player.mo.prevz == nil then --the previous frame's z coordinate, used for calculating slope acceleration
			player.mo.prevz = 0
		end

		player.mo.relz = player.mo.z*P_MobjFlip(player.mo) --relative z, used in gravity-sensitive situations

		player.mo.rmomz = player.mo.momz*P_MobjFlip(player.mo) --relative momentum z, used in gravity-sensitive situations

		player.mo.rprevz = player.mo.prevz*P_MobjFlip(player.mo) --same as relative z but in the past

		if player.prevnormalspd == nil then --the previous frame's normalspeed value, used for setting the current speed of the player
			player.prevnormalspd = 0
		end

		if player.mo.prevjumpfactor == nil then --the player's last known jumpfactor, used for the spindash to reset the player's jumpfactor
			player.mo.prevjumpfactor = 0
		end

		if player.mo.gpe_hasrun2 == nil then --skin toggle for run2 animation
		--used to toggle if the skin's second run animation is enabled (think the peelout or tails' faster tail animation in sonic 2)
		--used for indication of peelspeed, which has a number of effects such as being able to run on water
			player.mo.gpe_hasrun2 = false
		end

		if player.mo.gpe_hasmomentum == nil then --skin toggle for momentum and slope acceleration
			player.mo.gpe_hasmomentum = true
		end

		if player.mo.gpe_statchanges == nil then --skin toggle for stat overrides
		--(note that at the moment thrustfactor is hardcoded due to being required for proper momentum calculation)
			player.mo.gpe_statchanges = true --if the skin creator sets this to false, the script will fallback on skin defined stats
		end


		if player.mo.gpe_sdstyle == nil then --skin toggle for the player's spindash style
		--0 is the new mash-style spindash, 1 is the unmodified vanilla behavior.
			player.mo.gpe_sdstyle = 0
		end

		if player.mo.spincharge == nil then --the number of charges the spindash has, will be used for calculating the sound to use for each level of spindash
			player.mo.spincharge = 0
		end

		if player.mo.jumpdown == nil then --thinkframe compatible version of what PF_JUMPDOWN is supposed to do
			player.mo.jumpdown = false
		end
		if player.mo.usedown == nil then --thinkframe compatible version of what PF_USEDOWN is supposed to do
			player.mo.usedown = false
		end

		player.spindiv = (player.maxdash - player.mindash) / 7 --makes working with the mindash and maxdash values easier

	--------------------------------------------------
	--              STAT OVERRIDES                  --
	--------------------------------------------------
		if (player.charflags & SF_DASHMODE) and (player.dashmode > 3*FU)
		then
			player.defaultspeed = ((player.mo.gpe_statchanges and 36*FU) or skins[player.mo.skin].normalspeed) + ((player.dashmode-3*FU/3)*FU)
		else
			player.defaultspeed = ((player.mo.gpe_statchanges and 36*FU) or skins[player.mo.skin].normalspeed)
		end
		--our stand in for the skin's normalspeed value, since normalspeed is being used to modify how fast we can go in this script
		--36 FRACUNITs is the default based on the new skin standards used in vanilla, but this value can freely be modified.
		--if the skin maker chooses to disable gpe_statchanges, this value will use their skin's defined normalspeed as a fallback
		--a small amount of the player's dashmode value is added to this as a speed if they're in dashmode

		if player.mo.gpe_hasmomentum == true then
			player.thrustfactor = ((player.powers[pw_sneakers] or player.powers[pw_super]) and 6 or (player.mo.eflags & MFE_UNDERWATER) and 4 or 5)
		end
		--the player's thrustfactor is modified based on their super or sneaker status to change the rate of acceleration
		--THIS VALUE IS HARDCODED AS CHANGES TO THIS VALUE WILL BREAK MOMENTUM CALCULATION
		--UNLESS THE MOMENTUM SCRIPT IS UPDATED

		if player.mo.gpe_statchanges then
		--if the skin creator has enabled this script to change stats to match the rest of the cast,
		--these values will be used instead of their skin values
			player.topspeed = 150*FU --speed cap
			player.accelstart = 96
			player.acceleration = 40
			player.mindash = 20*FU
			player.maxdash = 70*FU
			player.downwardSlopeSens = 100
			player.upwardSlopeSens = 125
			player.speedLoss = 2

			if player.mo.eflags & MFE_UNDERWATER then
				player.topspeed = 175*FU --speed cap
				player.accelstart = 96
				player.acceleration = 40
				player.downwardSlopeSens = 75
				player.upwardSlopeSens = 300
				player.speedLoss = 1
				local lowerspeedscale = 12*FRACUNIT
				local speedscalediff = 24
				local minfric = 29*FRACUNIT/32
				local maxfric = FRACUNIT - 250
				local fricdif = maxfric - minfric
				local speedval = max(min(player.mrce.realspeed - lowerspeedscale, speedscalediff*FRACUNIT), 0)
				local speedpercent = speedval / speedscalediff
				player.mo.friction = min(max(minfric, minfric + FixedMul(speedpercent, fricdif)), maxfric) --test water friction
				--print(speedpercent)
			end
			if player.powers[pw_sneakers] or player.powers[pw_super] then --when we're powered up extra fast, we're more resistant to speed loss from uphills, and build speed faster from downhills
				player.upwardSlopeSens = (7 * $) / 3
				player.downwardSlopeSens = (4 * $) / 5
			end
		end

	--------------------------------------------------
	--  			MOMENTUM PHYSICS                --
	--------------------------------------------------


		--momentum retention
		if player.mo.gpe_hasmomentum == true and player.mrce.physics then
			--speed adding/retention
			if P_IsObjectOnGround(player.mo)-- and not (player.powers[pw_super])
			then
				--if the player is going faster than...
				local fast = player.powers[pw_sneakers] and true or player.powers[pw_super] and true or false
				local diff = abs(player.rspeed - player.normalspeed)
				local FAST_OFFSET = fast and 5*FU/3 or FU
				if player.rspeed > 150*FU then
					FAST_OFFSET = $*2
				end
				if player.rspeed > --((fast and not (player.mo.flags2 & MF2_TWOD) and (3*player.normalspeed)/2) -- two times speed for speedshoes,
				--or (((fast and player.mo.flags2 & MF2_TWOD) and player.normalspeed*2)
				--or player.mo.flags2 & MF2_TWOD and 3*player.normalspeed/2) --1.5 times speed for 2d flag, and twice that for speedshoes...
				FixedMul(player.normalspeed, FAST_OFFSET)--... or the player is just going faster than their normalspeed
				then
					local stanfric = 29*FRACUNIT/32
					local baseadd = player.normalspeed/9
					baseadd = FixedMul($, FU - FixedDiv(player.mo.friction, stanfric))
					--if player.mo.eflags & MFE_UNDERWATER then
					--	baseadd = (5*$)/3
					--end
					player.normalspeed = fast and (player.rspeed) or (player.rspeed + (baseadd))	--set their normalspeed higher, note that the extra boost from normalspeed/9 is to account for the differential from friction and scale
				end
			end

			--==speed loss==--
			if player.normalspeed > player.defaultspeed and player.rspeed < ((player.powers[pw_sneakers] and 6*player.normalspeed/4) or player.normalspeed) - (player.normalspeed/9)
			then
				player.normalspeed = $ - (FU*player.speedLoss) --reduce their max speed
			end

			if player.rspeed < player.normalspeed - SPEED_DECREASE_LEEWAY then
				player.normalspeed = player.rspeed + (player.normalspeed/9)
			end


			--==speed reset conditions==--

			if player.normalspeed <= player.defaultspeed
			or player.mo.state == S_PLAY_SPRING
			or P_PlayerInPain(player) or player.playerstate == PST_DEAD
			then
				player.normalspeed = player.defaultspeed -- reset the player's normalspeed
			end


			--slope acceleration
			if player.mo.rprevz > player.mo.relz
			and P_IsObjectOnGround(player.mo)
			and player.rspeed < player.topspeed
			and player.mo.standingslope ~= nil then
				local slope = (2*(player.mo.rprevz - player.mo.relz)) / 3
				if not (player.mo.flags2 & MF2_TWOD) then --if we aren't currently in a 2d section
					player.normalspeed = player.prevnormalspd + (player.acceleration * player.thrustfactor/46)*(slope)/player.downwardSlopeSens --add the slope's drop plus the player's acceleration accounting for thrustfactor
				else
					player.normalspeed = player.prevnormalspd + 2*((player.acceleration * player.thrustfactor/46)*(slope) / player.downwardSlopeSens)/3 --if we are in 2d, use 2/3 that speed
				end
			end



			if player.mo.rprevz < player.mo.relz -- Moving uphill
			and P_IsObjectOnGround(player.mo)
			and player.rspeed > 0 then
				player.normalspeed = player.normalspeed - (player.acceleration * player.thrustfactor / 46) * (player.mo.relz - player.mo.rprevz) / player.upwardSlopeSens
			end



			--rolling physics
			if player.pflags & PF_SPINNING
			and leveltime > 0
			and not (player.pflags & PF_STARTDASH)
			and P_IsObjectOnGround(player.mo) then
				if player.mo.rprevz ~= player.mo.relz --If we're on a slope...
				and player.rspeed < player.topspeed
				and player.mo.standingslope ~= nil then
					P_Thrust(player.mo, R_PointToAngle2(0, 0, player.rmomx, player.rmomy) + (player.rspeed*player.cmd.sidemove*ANGLE_180), (4*(player.mo.rprevz - player.mo.relz)/5) / 15) --modify the amount this is divided by to change how much rolling is affected
				else --If we're on flat ground...
					P_Thrust(player.mo, R_PointToAngle2(0, 0, player.rmomx, player.rmomy) + (player.rspeed*player.cmd.sidemove*ANGLE_180), min((player.rspeed/52), 2*FU/3) - (player.mo.friction/3)) --i did my best to match vanilla's rolling here
				end
			end
		end


	--------------------------------------------------
	--  			  GAMEPLAY CHANGES              --
	--------------------------------------------------

		--Making it so you can hold spin to roll out of a thok action
		if player.charability2 == CA2_SPINDASH
		and not (player.charability == CA_GLIDEANDCLIMB)
		and (player.cmd.buttons & BT_SPIN)
		and (player.mo.eflags & MFE_JUSTHITFLOOR)
		and not (player.pflags & PF_SHIELDABILITY and player.pflags & PF_THOKKED and player.powers[pw_shield] & SH_BUBBLEWRAP)
		and player.mo.state ~= S_PLAY_DEAD
		and not P_CheckDeathPitCollide(player.mo)
		and not player.mo.sprung
		and not (player.mo.skin == "mario" or player.mo.skin == "luigi")
		and not player.mrce.elemdive then
			player.pflags = $1 | PF_SPINNING
			player.mo.state = S_PLAY_ROLL
			if (player.rspeed > (6*player.mo.scale)) then
				S_StartSound(player.mo, sfx_spin)
			end
		end

		--Removes the roll lock you get from jumping out of a spin
		--KNOWN ISSUE: due to the thokked flag being linked to the shield ability flag,
		--this makes it so you only have one frame to do a whirlwind shield jump if you roll off a slope

		if not P_IsObjectOnGround(player.mo)
		and player.mo.gpe_sdstyle == 0
		and player.pflags & PF_SPINNING
		and player.mrce.physics then
			player.pflags = $ & ~PF_SPINNING
			player.pflags = $ | PF_JUMPED
			if not (player.pflags & PF_STARTJUMP) then
				player.pflags = $ | PF_THOKKED
			end
		end

		--mash-style spindash behavior

		if player.charability2 == CA2_SPINDASH --if the player can spindash
		and player.mrce.physics
		and player.mo.gpe_sdstyle == 0 then --and the player's skin is using this style

			if player.cmd.buttons & BT_SPIN --if the player is pressing spin
			and P_IsObjectOnGround(player.mo)--and the player is on the ground
			and (player.speed < FixedMul(5<<FRACBITS, player.mo.scale) or player.mo.state ~= S_PLAY_GLIDE_LANDING)-- and the player satisfies the conditions to need to spindash
			and (player.mo.state == S_PLAY_SPINDASH) then --eh to hell with it let's make sure the player is in their spindash animation too

				if player.mo.prevjumpfactor == 0 then --if the player's previous jumpfactor is at 0
					player.mo.prevjumpfactor = player.jumpfactor --store the player's previous jumpfactor for later
				end


				local hypa = MRCE_isHyper(player) and 2 or 1
				player.dashspeed = (player.mo.gpe_statchanges and player.mindash or skins[player.mo.skin].mindash) + (player.spindiv*player.mo.spincharge)*hypa --increase the player's dash speed by 1/8th of the difference between their mindash and maxdash
				if player.mrce.shield == 1 and not player.gotflag then --if the player is is pressing the shield button
					if player.mo.spincharge < 7 then --if we haven't hit our charge limit
						player.mo.spincharge = $ + 1 -- increase the player's spincharges by 1
					end
					if player.mo.spincharge > 1 then
						S_StopSound(player.mo)--stop the player's current sound
						S_StartSound(player.mo, sfx_rev1 + min(player.mo.spincharge - 1, 6))--play the spindash sound
					else
						S_StopSoundByID(player.mo,sfx_spndsh)--stop the player's current sound
						S_StartSound(player.mo,sfx_spndsh)--play the spindash sound
					end
					player.autospindelay = 15
				end
				player.autospindelay = $ and $ - 1 or 0
				if player.mo.spincharge < 6 and not (player.mrce.spin % 12) and not player.autospindelay then
					player.mo.spincharge = $ + 1 -- increase the player's spincharges by 1
					S_StopSoundByID(player.mo,sfx_spndsh)--stop the player's current sound
					S_StartSound(player.mo,sfx_spndsh)--play the spindash sound
				end
				--print(player.mo.spincharge)
			else
				player.mo.spincharge = 0 --reset the number of spin charges
			end

		end


	--------------------------------------------------
	--  				ANIMATION	 	       		--
	--------------------------------------------------


		--run2 animations
		if player.mo.state == S_PLAY_DASH
		or (player.mo.state == S_PLAY_RUN and player.rspeed >= PEELSPEED) then
			if player.mo.tics > 1 then
				player.mo.tics = 1
			end
		end

		if player.mo.gpe_hasrun2 then --if the player has a fast run animation set up
			if P_IsObjectOnGround(player.mo) then --if the player is on the ground
				if ((player.powers[pw_super] and player.mrce.realspeed >= 2*PEELSPEED) or (player.mrce.realspeed >= PEELSPEED and not player.powers[pw_super])) --and their speed exceeds waterrun threshold
				and player.panim == PA_RUN then --and they're in the running animation
					player.mo.state = S_PLAY_DASH --set their state to the dash state
				end

				if ((player.powers[pw_super] and player.mrce.realspeed <= 2*PEELSPEED) or (player.mrce.realspeed <= PEELSPEED and not player.powers[pw_super])) --if the player's speed no longer exceeds waterrun threshold
				and not player.skidtime then --and they're not skidding
					if player.panim == PA_DASH then --if we're still using the dash animation
						player.mo.state = S_PLAY_RUN --use the running state instead
					end
				end
			end
		end

	--------------------------------------------------
	--  				  MISC	 		            --
	--------------------------------------------------
		player.mo.prevz = player.mo.z
		player.prevnormalspd = player.normalspeed
		player.mo.jumpdown = (player.mrce.shield)
		player.mo.usedown = (player.cmd.buttons & BT_SPIN)


		if player.mrce.FovEffects == 1 then
			FOVManager(player)
		end
	end
end

-- List of special case skins that require custom momentum handling. (Recoded only)
local specialcaseskins = {
    "mario",
    "luigi"
}

local function MRCE_RecodedMomentum(p)
--custom momentum made with a lot less hacks then CBWMom, heavily referenced from ChrispyChars.
--Originally was gonna launch this in v2.0, but that's taking forever so i'm putting it in this minor update.
--ported from xmom v1.3 with assistance from frostiikin to suit the needs of MRCE
--this code has mostly gone unused since classic momentum replaced it, however Some modded characters work better with it
--I think I'll re-enable it just so that more characters can use momentum
--$GZDB_SKIP

	if p.spectator then return end
	if not p.realmo then return end
	if p.playerstate ~= PST_LIVE then return end
	--if (p.mo.skin ~= mario and p.mo.skin ~= "luigi") then return end
	--if p.mrce and p.mrce.physics == false then
	--	return
	--end
	--if (p.noxmomchar or p.xmomtoggledoff) then
	--	return
	--end
	local x = p.mrce
	if p.xmlastz == nil then
		p.xmlastspeed = x.realspeed
		p.xmlastz = p.mo.z
		p.xmlastx = p.mo.x
		p.xmlasty = p.mo.y
		p.xmlastmomx = p.mo.momx
		p.xmomlastmomy = p.mo.momy
		p.xmlaststate = p.mo.state
		--p.xmexhaust = 35
	end
	if p.fakenormalspeed == nil then
		p.fakenormalspeed = skins[p.mo.skin].normalspeed
	end
	local watermul = (p.mo.eflags & MFE_UNDERWATER) and 2 or 1
	if (p.mo and p.mo.valid and p.mo.health) then
		if p.xmlastskin and p.xmlastskin ~= p.mo.skin then
			p.hasnomomentum = false
		end
		p.xmlastskin = p.mo.skin
	end
	local speedcap = (3 * ((skins[p.mo.skin].normalspeed * 3) - (skins[p.mo.skin].normalspeed / 2))) / 4
	local speed = FixedDiv(FixedHypot(p.mo.momx - p.cmomx, p.mo.momy - p.cmomy), p.mo.scale)
	local SPEED_INCREASE_LEEWAY = 0*FRACUNIT -- the amount of speed above normalspeed needed to update normalspeed
		if not p.hasnomomentum then
			local speen = (p.pflags & PF_SPINNING) and 4 or 0
			if x.snowboard then speen = max(0, $ - 2) end
			local shine = MRCE_isHyper(p) and 1 or 0
			--print(p.xmexhaust)
			if p.xmlastz*P_MobjFlip(p.mo) > p.mo.z*P_MobjFlip(p.mo) and P_IsObjectOnGround(p.mo) and not (p.mo.eflags & MFE_JUSTHITFLOOR) then
				p.normalspeed = $ + (p.mo.z*P_MobjFlip(p.mo)-p.xmlastz*P_MobjFlip(p.mo))/(50 - (shine + speen))*-1
				--p.xmexhaust = min($ + 2, 12)
			--elseif p.xmlastz*P_MobjFlip(p.mo) < p.mo.z*P_MobjFlip(p.mo) and P_IsObjectOnGround(p.mo) and not (p.mo.eflags & MFE_JUSTHITFLOOR) then
				--if p.xmexhaust <= 0 then
				--	local lose = (p.mo.z*P_MobjFlip(p.mo)+p.xmlastz*P_MobjFlip(p.mo))/(5000 - ((speen*32) - (shine*64)))*-1
				--	p.normalspeed = max($ + lose, skins[p.mo.skin].normalspeed)
				--	p.xmexhaust = max($ - 1, -20)
				--print(tostring(lose/FRACUNIT))
				--else
				--	p.xmexhaust = max($ - 1, -20)
				--end
			elseif MRCE_isHyper(p) then
				--p.xmexhaust = min($ + 3, 12)
				if P_IsObjectOnGround(p.mo) then
					if p.powers[pw_sneakers] then
						p.normalspeed = $ + FRACUNIT/2
					else
						p.normalspeed = $ + FRACUNIT/8
					end
				end
			--else
				--p.xmexhaust = min($ + 1, 35)
			end
			local restorefakenormalspeed = p.fakenormalspeed
			--if p.dashmode > 3*TICRATE then
			--	p.fakenormalspeed = p.normalspeed
			--end
			if not p.powers[pw_super] and not p.powers[pw_sneakers] then
				if (speed*watermul > p.normalspeed + SPEED_INCREASE_LEEWAY
				or speed*watermul < p.normalspeed - SPEED_DECREASE_LEEWAY) then
					p.normalspeed = max(speed*watermul, p.fakenormalspeed)
				end
			elseif not MRCE_isHyper(p) then
				if (speed*3/5*watermul > p.normalspeed + SPEED_INCREASE_LEEWAY
				or speed*3/5*watermul < p.normalspeed - SPEED_DECREASE_LEEWAY) then
					p.normalspeed = max((speed*3/5)*watermul, p.fakenormalspeed)
				end
			else
				if speed < p.normalspeed - SPEED_DECREASE_LEEWAY then
				--or speed > p.normalspeed + SPEED_INCREASE_LEEWAY then
					p.normalspeed = max(speed*watermul, p.fakenormalspeed)
				end
			end
			p.fakenormalspeed = restorefakenormalspeed
			if not (MRCE_isHyper(p) or p.powers[pw_sneakers]) then
				if p.normalspeed > speedcap*watermul then --max speed cap
					p.normalspeed = $ - p.normalspeed/50
					if x.snowboard and p.normalspeed > 100*FRACUNIT then
						p.normalspeed = min($, 100*FRACUNIT)
						--print(p.normalspeed)
					end
				end
			end
		end
		local momangle = R_PointToAngle2(0,0,p.rmomx,p.rmomy) --Used for angling new momentum in ability cases
		local pmom = FixedDiv(FixedHypot(p.rmomx,p.rmomy),p.mo.scale) --Current speed, scaled for normalspeed calculations


		--
		-- Dummied out for MRCE.
		-- ~Radicalicious (4/21/22)
		--
		--[[
		--Knuckles momentum renewal
		if p.charability == CA_GLIDEANDCLIMB then
			--Create glide history
			if p.glidelast == nil then
				p.glidelast = 0
			end
			local gliding = p.pflags&PF_GLIDING
			local thokked = p.pflags&PF_THOKKED
			local exitglide = (p.glidelast == 1 and not(gliding) and thokked)
			local landglide = (p.glidelast == 2 and not(gliding|thokked))
			--Restore glide momentum after deactivation
			if exitglide or landglide then
				p.mo.momx = p.xmlastmomx
				p.mo.momy = p.xmlastmomy
			end
			--Update glide history
			if gliding then
				p.glidelast = 1 --Gliding
			elseif exitglide then
				p.glidelast = 2 --Falling from glide
			elseif not(gliding|thokked) then
				p.glidelast = 0 --Not in glide state
			end
		end

		------
		--Fang momentum renewal
		if p.charability == CA_BOUNCE then
			--Create bounce history
			if p.bouncelast == nil then
				p.bouncelast = false
			end
			if p.pflags&PF_BOUNCING and not(p.bouncelast) --Activate bounce
				or (not(p.pflags&PF_BOUNCING) and p.pflags&PF_THOKKED and p.bouncelast) --Deactivate bounce
				--Undo the momentum cut from bounce activation/deactivation
				p.mo.momx = p.xmlastmomx
				p.mo.momy = p.xmlastmomy
				p.mo.momz = $*2
			end
			--Update bounce history
			p.bouncelast = (p.pflags&PF_BOUNCING > 0)
		end
		]]
	p.xmlastspeed = p.speed
	p.xmlastz = p.mo.z
	p.xmlastx = p.mo.x
	p.xmlasty = p.mo.y
	p.xmlastmomx = p.mo.momx
	p.xmlastmomy = p.mo.momy
	p.xmlaststate = p.mo.state
end




--[[ ============================================HOOKS=====================================================]]


addHook("PlayerThink", function(p)
	if p.spectator then return end
	if not (p.mo and p.mo.valid) then return end
	local x = p.mrce
	if not x then return end

	x.physicsMode = CV_FindVar("mr_physicsMode").value
	--x.spindashJumpCancelActive = CV_FindVar("mr_spinDashJumpCancel").value
	x.FovEffects = CV_FindVar("mr_fovfx").value

	if x.physicsMode == 1 then
		MRCE_RecodedMomentum(p)
	elseif x.physicsMode == 2 then
		return
	else
		MRCE_DefaultPhysics(p)
	end

	if not x.physicsMode == 0 then return end


--detect if physics are enabled
	if IsCustomSkin(p) then
		x.physics = false
	else
		x.physics = true
	end

	--move this down here since playerthink runs before thinkframe, might help fix momentum breaking in netgames when switching characters a lot
	for k, v in pairs(charactersupport) do
		if p.mo.skin ~= k then continue end
		if v[1] == false then p.mo.gpe_hasmomentum = false else p.mo.gpe_hasmomentum = nil end
		if v[2] == false then p.mo.gpe_statchanges = false else p.mo.gpe_statchanges = nil end
		if v[3] ~= 0 then p.mo.gpe_sdstyle = v[3] else p.mo.gpe_sdstyle = nil end
		if v[4] ~= nil and v[4] == true then p.mo.gpe_hasrun2 = true else p.mo.gpe_hasrun2 = nil end
	end
	--print(x.dashcancel)
	--if p.speed == go fast, allow the player to run on water. aka, momentum based water running
	if (x.realspeed >= PEELSPEED) and not (skins[p.mo.skin].flags & SF_RUNONWATER) and not IsCustomSkin(p)
	and not ((p.mo.skin == "mario" or p.mo.skin == "luigi") and p.powers[pw_shield] == SH_MINI) --mini mario gets to always run on water
	and not (p.mo.skin == "inazuma" or p.mo.skin == "aether") then
		p.charflags = $1|SF_RUNONWATER
		if p.mo.eflags & (MFE_TOUCHWATER|MFE_UNDERWATER) and P_IsObjectOnGround(p.mo) then
			local sploop = P_SpawnCoolSkidDust(p, 24, false, S_SPLISH1, true)
			sploop.destscale = p.mo.scale/6+p.mo.scale/5
			sploop.scalespeed = FRACUNIT/8
		end
	elseif not IsCustomSkin(p) then
		if not (skins[p.mo.skin].flags & SF_RUNONWATER)
		and not ((p.mo.skin == "mario" or p.mo.skin == "luigi") and p.powers[pw_shield] == SH_MINI)
		and not (p.mo.skin == "inazuma" or p.mo.skin == "aether")
		and not (p.solchar and p.solchar.istransformed)
		and (x.realspeed <= PEELSPEED or (abs(p.mo.momz) > 50*FRACUNIT)) then
			p.charflags = $1 & ~(SF_RUNONWATER)
		end
	end
end)

addHook("ShouldDamage", function (pmo, inf, src, dmg, dmgtype)
	if not (pmo and pmo.valid and inf and inf.valid and src and src.valid) then return end
	if pmo.player.mrce.realspeed > 2*PEELSPEED and (src.flags & MF_ENEMY) and not (dmgtype & DMG_DEATHMASK) then
		P_DoPlayerPain(pmo.player, src, inf)
		P_DamageMobj(inf, pmo, pmo, 1)
		return false
	end
end, MT_PLAYER)

addHook("NetVars", function(net)
	charactersupport = net($)  --net sync that shit
end)