local function ContinueCodeExecution(p)
	if not p then return false end
	if not p.skin == "Amy" then return false end

	return true
end



local function init(p)
	if not ContinueCodeExecution(p) then return end
	if not p.mrce then return end

	p.mrce.usedHammerThrust = false

end
addHook("PlayerSpawn", init)


local function HammerThrust(p)
	if not ContinueCodeExecution(p) then return end
	if not (p.mo and p.mo.valid) then return end


	if p.powers[pw_strong] == STR_TWINSPIN and p.mrce.usedHammerThrust == false then
		--print("Swining le hammer")

		local bopDirection

		if p.cmd.buttons & BT_SPIN then
			bopDirection = -15*FRACUNIT
		else
			bopDirection = 11*FRACUNIT
		end


		P_Thrust(p.mo, p.mo.angle, 5*FRACUNIT)
		p.mo.momz = bopDirection
		p.mrce.usedHammerThrust = true
	end

	if P_IsObjectOnGround(p.mo) then
		p.mrce.usedHammerThrust = false
	end


end
addHook("PlayerThink", HammerThrust)