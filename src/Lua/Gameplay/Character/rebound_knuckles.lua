local function bounceOut(player, line)
   if line.special == 998 then
		local mo = player.mo
		local angle = R_PointToAngle2(0, 0, mo.momx, mo.momy)

		angle = angle + ANGLE_180 -- Reverse direction
		mo.momx = FixedMul(mo.momx, cos(angle)) * 2 -- Increase strength of the bounce
		mo.momy = FixedMul(mo.momy, sin(angle)) * 2 -- Increase strength of the bounce
		player.mo.momz = player.mo.momz + (5 * FRACUNIT)
    end
end





local function climbFast(player)
    local climbSpeedMultiplier = 2

	player.mo.momx = FixedMul(player.mo.momx, climbSpeedMultiplier * FRACUNIT)
	player.mo.momy = FixedMul(player.mo.momy, climbSpeedMultiplier * FRACUNIT)
	player.mo.momz = FixedMul(player.mo.momz, climbSpeedMultiplier * FRACUNIT)
end


--==================Hooks=========================--

addHook("PlayerThink", function(player)
	if not (player.mo and player.mo.valid) then return end
	if player.mo.skin ~= "knuckles" then return end

	if player.climbing then
		local hit = player.lastsidehit
		--print(hit)

		if sides[hit].line.special == 998 then
		    player.climbing = 0
		end


		if sides[hit].line.special == 997 then
			climbFast(player)
		end
	end
end)

addHook("MobjMoveBlocked", function(mo, thing, line)
    if not (mo and mo.valid and mo.player and mo.player.valid) then return end
	if mo.player.spectator then return end --apparently the first line wasn't enough to stop spectator mode from erroring anyway
	if not line then return end

	local player = mo.player
    if player.mo.skin ~= "knuckles" then return end

    if player.pflags & PF_GLIDING then
		bounceOut(player, line)
	end
end, MT_PLAYER)