addHook("PlayerThink",  function(p)
	local pmo = p.mo
	if not (pmo and pmo.valid) then return end
	if (p == nil) then return end
	local x = p.mrce
	if not p.eggsuperflying
	and x.realspeed > 5*p.mo.scale
	and p.powers[pw_super]
	and x.spin and not P_IsObjectOnGround(pmo)
	and not (p.mo.state >= S_PLAY_SUPER_TRANS1 and p.mo.state <= S_PLAY_SUPER_TRANS6)
	and p.charability == 18
	and x.floatpause <= 0
	and P_MobjFlip(p.mo)*p.mo.momz <= 0 then
		if x.realspeed >= FixedMul(p.runspeed, p.mo.scale) then
			p.mo.state = S_PLAY_FLOAT_RUN
		else
			p.mo.state = S_PLAY_FLOAT
		end
		x.glide = 1
		P_SetObjectMomZ(p.mo, 0)
		p.pflags = $&~(PF_STARTJUMP|PF_SPINNING)
	else
		x.glide = 0
	end

	if x.floatpause > 0 then
		if P_IsObjectOnGround(pmo) then
			x.floatpause = 0
		else
			x.floatpause = $ - 1
		end
	end

	if MRCE_isHyper(p) and p.charability == 18
	and x.realspeed < 5*p.mo.scale
	and P_MobjFlip(p.mo)*p.mo.momz <= 0
	and not (p.mo.state >= S_PLAY_SUPER_TRANS1 and p.mo.state <= S_PLAY_SUPER_TRANS6)
	and p.eggsuperflying == false
	and x.floatpause <= 0
	and x.spin and not P_IsObjectOnGround(pmo) then
		if x.realspeed >= FixedMul(p.runspeed, p.mo.scale) then
			p.mo.state = S_PLAY_FLOAT_RUN
		else
			p.mo.state = S_PLAY_FLOAT
		end
		x.glide = 1
		P_SetObjectMomZ(p.mo, -3*FRACUNIT)
		p.pflags = $&~(PF_STARTJUMP|PF_SPINNING)
	else
		x.glide = 0
	end
end)