local emerald = {EMERALD1, EMERALD2, EMERALD3, EMERALD4, EMERALD5, EMERALD6, EMERALD7}
local debug_es = 2

local RING_ATTRACT_DIST = 80*FRACUNIT

for i = 0, 9 do
	freeslot("sfx_mscry" .. i)
end

local settings_multi_timers = {
	0, --1x multiplier
	10*TICRATE, --2x multiplier
	8*TICRATE, --4x multiplier
	6*TICRATE, --8x multiplier
	4*TICRATE, --16x multiplier
	2*TICRATE, --32x multiplier
	TICRATE,   --64x multiplier
	TICRATE/2, --128x multiplier
	TICRATE/4, --256x multiplier
	TICRATE/6 --512x multiplier
}
local multipliers = { -- Could have been done with bitshifting easily, but I rather have control.
	[0] = 0, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512
}

local emstagerank = 1
local dropped = 0
local emevaluated = false

-- HUD

local rotational_time = 180/3
local mp_rot_timer = 0

local reached_goal_val = false
local reached_token_val = false
local emerald_flash_timer = 0

--local debug_mode = false

-- GAME

local last_multiplier = 1
local multiplier = 1
local multiplier_timer = 0

local freeze_time = 12*TICRATE
local multiplier_freeze = 0

local scorereq = 0
local tokenreq = 0

--COM_AddCommand("mrce_emeraldstagedebug", function()
--	debug_mode = not($)
--end)


/*

	Soap's combo bar Part 1
	here at the top so I can call its AddCombo function within the main script
	the rest is down at the bottom
	credit luigi budd for the original this is based off of

*/

//I CANT LIVE WITHOUT MY TR
local TR = TICRATE
local ranks = {
	[0] = {
		name = "Lame...",
		count = 30
	},
	[1] = {
		name = "\x83Soapy",
		count = 100
	},
	[2] = {
		name = "\x88".."Alright...",
		count = 250
	},
	[3] = {
		name = "\x82Nice!",
		count = 500
	},
	[4] = {
		name = "\x8D".."Destructive!",
		count = 1000
	},
	[5] = {
		name = "\x8D".."Supreme!",
		count = 2500
	}
}
local maxtime = 7*TR
local parttime = 2*TR
local comboup = 10
//spike stuff according tro source
// https://github.com/STJr/SRB2/blob/a4a3b5b0944720a536a94c9d471b64c822cdac61/src/p_map.c#L838
local SPIKE_LIST = {
	[MT_SPIKE] = true,
	[MT_WALLSPIKE] = true,
	[MT_SPIKEBALL] = true,
	[MT_BOMBSPHERE] = true,
}

//from chrispy chars!!! by Lach!!!!
local function SafeFreeslot(...)
	for _, item in ipairs({...}) do
		if rawget(_G, item) == nil then
			freeslot(item)
		end
	end
end

SafeFreeslot("sfx_shudcl")
sfxinfo[sfx_shudcl].caption = "Menu Close"
SafeFreeslot("sfx_shudop")
sfxinfo[sfx_shudop].caption = "Menu Open"
for i = 0, 2 do
	SafeFreeslot("sfx_comup"..i)
	sfxinfo[sfx_comup0 + i].caption = "\x83".."Combo Up!\x80"
end

rawset(_G, "MRCEGivePlayerESCombo",function(p,add,max,remove)
    if not mapheaderinfo[gamemap].mrce_emeraldstage then return end
	if (p.powers[pw_carry] == CR_NIGHTSMODE)
	or not (gametyperules & GTR_FRIENDLY) then
		return
	end
    add = $ or false
    max = $ or false
    remove = $ or false

	local c = p.mrce_escombo

	if add then
		if c.count == 0 then
			S_StartSound(nil,sfx_shudop,p)
		end
		local prevcount = c.count
		local prevrank = c.rank
		c.count = $+multipliers[multiplier]

		if c.rank < #ranks and c.count >= ranks[prevrank + 1].count then
			c.rank = $+1
			c.showrank.tics = 2*TR
			c.showrank.num = c.rank
			c.showrank.score = c.score
			S_StartSound(nil,P_RandomRange(sfx_comup0,sfx_comup2),p)
		end

		c.time = maxtime
	else
		if not c.count then
			return
		end

		if remove then
			if c.time >= parttime then
				c.time = $-parttime
			else
				c.time = 0
			end
		else
			if max == true then
				c.time = maxtime
			else
				c.time = $+parttime
			end
		end
	end
end)

local lastmap = 1

addHook("MapLoad", function(map)
	if map ~= lastmap then
		mrce.emstage_globalPoints = 0
		multiplier_freeze = 0
		multiplier_timer = 0
		multiplier = 1
		lastmap = map
	end
	emevaluated = false

	scorereq = tonumber(mapheaderinfo[gamemap].mrce_emeraldscore or 0)
	tokenreq = tonumber(mapheaderinfo[gamemap].mrce_emeraldtoken or 0)

	local num = tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)

	if emerald[num] then
		emeralds = $|emerald[num]
	end
end)

addHook("PlayerSpawn", function(p)
	if not (p.starpostnum or p.bot or multiplayer) then
		mrce.emstage_globalPoints = 0
		multiplier_freeze = 0
		multiplier_timer = 0
		multiplier = 1
	end
	if p.mo and p.mo.valid then
		p.mo.superattract = 0
	end

	if not p.mrce_emeraldstageinitscore then
		p.mrce_emeraldstageinitscore = p.score
		p.mrce_emeraldstageinitrings = p.rings
	end
end)

addHook("ThinkFrame", function()
	if not (mapheaderinfo[gamemap].mrce_emeraldstage and mrce.emstage_attemptavailable) then return end
	mrce.emstage_stageRank = emstagerank

	if multiplier_freeze then
		multiplier_freeze = $-1
	else
		if multiplier_timer then
			multiplier_timer = $-1
			if multiplier > 0 and not multiplier_timer then
				multiplier = $-1
				multiplier_timer = settings_multi_timers[multiplier]
			end
		end
	end
end)

local playerdamagedlevel = false
local emeraldmap = 0

addHook("MobjDamage", function(mo)
	if not (mapheaderinfo[gamemap].mrce_emeraldstage and mrce.emstage_attemptavailable) then return end
	if multiplayer then return end
	if mo.player.bot then
		multiplier = max(1, min($ - 1, (4 * $) / 5))
		multiplier_timer = settings_multi_timers[multiplier]
		mrce.emstage_globalPoints = max($ - 15000, 0)
	else
		if not dropped then dropped = gamemap end

		multiplier = max(min(($ / 2) + 1, $ - 1), 1)
		multiplier_timer = settings_multi_timers[multiplier]
		mrce.emstage_globalPoints = max($ - 30000, 0)
		mo.player.losstime = max(15*TICRATE, $ + 175)
	end
end, MT_PLAYER)

addHook("MapLoad", function(map)
	if multiplayer then
		emeraldmap = 0
		dropped = 0
		return
	end
	if not (mapheaderinfo[gamemap].mrce_emeraldstage and mrce.emstage_attemptavailable) then
		emeraldmap = 0
		dropped = 0
		return
	end
	if emeraldmap == 0 then
		emeraldmap = gamemap
	elseif emeraldmap == gamemap and not gamecomplete and not consoleplayer.starpostnum then --player gets infinite retries after beating the game
		dropped = gamemap
	else
		dropped = 0
	end
	if playerdamagedlevel then playerdamagedlevel = false end
end)

addHook("PlayerThink", function(p)
	if not (mapheaderinfo[gamemap].mrce_emeraldstage and mrce.emstage_attemptavailable) then return end
	if not (p.mo and p.mo.valid) then return end

	if not (leveltime % 35) then
		--print(p.numboxes)
	end

	if p.exiting == 98 then --end level bonuses, will visualize later
		local boxbonus = p.numboxes * 2000
		local multbonus = (5000 * multiplier) - 7500
		mrce.emstage_globalPoints = $ + (boxbonus + multbonus)
	end

	local num = tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)

	local elecmult = (p.powers[pw_shield] & SH_PROTECTELECTRIC) and 1 or 2 --nerf attraction shields by buffing not having one
	--print(elecmult)

	if p.mrce_emeraldstageinitscore ~= p.score then
		local dif = max(p.score - p.mrce_emeraldstageinitscore, 0)*elecmult
		mrce.emstage_globalPoints = $+dif*multipliers[multiplier]*2
		multiplier_timer = min(multiplier_timer+dif/8, settings_multi_timers[multiplier])
		p.mrce_emeraldstageinitscore = p.score
	end

	if p.mrce_emeraldstageinitrings ~= p.rings then -- will likely need some work considering there is exploit with getting rings from hurted player.
		local dif = max(p.rings - p.mrce_emeraldstageinitrings, 0)
		local scoregain = 100*dif*elecmult
		mrce.emstage_globalPoints = $+scoregain*multipliers[multiplier]
		multiplier_timer = min(multiplier_timer+scoregain/9, settings_multi_timers[multiplier])
		p.mrce_emeraldstageinitrings = p.rings
	end

	if emerald[num] and not (multiplayer or p.bot or (p.mrce_emeraldstageflyingemerald and p.mrce_emeraldstageflyingemerald.valid)) then
		p.mrce_emeraldstageflyingemerald = P_SpawnMobjFromMobj(p.mo, 0, 0, 0, MT_FAKEEMERALD1)
		p.mrce_emeraldstageflyingemerald.flags = $|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_NOCLIPTHING|MF_NOBLOCKMAP &~ MF_SPECIAL
		p.mrce_emeraldstageflyingemerald.scale = 4*p.mo.scale/5
		p.mrce_emeraldstageflyingemerald.state = S_FAKEEMERALD1 + (num - 1)
		p.mrce_emeraldstageflyingemerald.target = p.mo
		p.mrce_emeraldstageflyingemerald.emeraldchase = true
	end

	if mapheaderinfo[gamemap].mrce_emeraldscore and mapheaderinfo[gamemap].mrce_emeraldtoken and mapheaderinfo[gamemap].mrce_emtime then
		local cscore = tonumber(mapheaderinfo[gamemap].mrce_emeraldscore)
		local ascore = tonumber(mapheaderinfo[gamemap].mrce_emeraldtoken)
		if not (p.pflags & PF_FINISHED) then
			--print(tostring(dropped) .. " " .. tostring(emevaluated))
			if mrce.emstage_globalPoints < cscore / 2 then
				emstagerank = 1
			elseif mrce.emstage_globalPoints < cscore then
				emstagerank = 2
			elseif mrce.emstage_globalPoints < (((ascore - cscore) / 2) + cscore) then
				emstagerank = 3 --good job, a super emerald for you
			elseif mrce.emstage_globalPoints < ascore then
				emstagerank = 4
			else
				emstagerank = 5
			end
		elseif not multiplayer then --no S ranks in coop
			if (p.realtime <= tonumber(mapheaderinfo[gamemap].mrce_emtime)*TICRATE) and not dropped and not emevaluated then
				emstagerank = $ + 1
				emevaluated = true
			end
			dropped = 0
			--print(dropped .. " " .. emevaluated)
		end
	end

	if (p.exiting <= 88 and p.exiting > 0) and ((scorereq and mrce.emstage_globalPoints >= scorereq and num < 8) or not scorereq) then -- TEMP SOLUTION
		GlobalBanks_Array[0] = $|(1 << (num - 1))
		MRCE_Achievements.unlock("MRCE_AnySuperEmerald", sfx_chchng, p)
	end
end)

local scoretable = {
	[1] = 100,
	[2] = 200,
	[3] = 500,
	[4] = 1000,
	[5] = 1000,
	[6] = 1000,
	[7] = 1000,
	[8] = 1000,
	[9] = 1000,
	[10] = 1000,
	[11] = 1000,
	[12] = 1000,
	[13] = 1000,
	[14] = 1000,
	[15] = 10000
}

addHook("MobjDeath", function (mo, inf, src, dmg)
	if not (mapheaderinfo[gamemap].mrce_emeraldstage and mrce.emstage_attemptavailable) then return end
	if not (mo and mo.valid and inf and inf.valid) then return end
	local p = (inf and inf.valid and inf.player and inf.player.valid) and inf.player or (src and src.valid and src.player and src.player.valid) and src.player or nil
	if p == nil then return end
	local sc = scoretable[min(p.scoreadd + 1, 15)]
	local sca = sc / 2
	local elecmult = (p.powers[pw_shield] & SH_PROTECTELECTRIC) and 3 or 2
	local scoregain = (sca/elecmult) --basically gonna multiply the gain from popping badniks
	mrce.emstage_globalPoints = $+(scoregain*multipliers[multiplier])
	multiplier_timer = min((multiplier_timer+scoregain/7) + 8*multiplier, settings_multi_timers[multiplier])
end)


--
--
--
--
--  HUD
--
--
--
--



-- {offx, offy, offscale_x, offscale_y, sprite, nexts}

local function pad(str, num, symb)
	local dif = num - str:len()
	local result = str
	if dif > 0 then
		result = string.rep(symb, dif)..result
	end
	return result
end

local color_table = {
	SKINCOLOR_SAPPHIRE,
	SKINCOLOR_GALAXY,
	SKINCOLOR_APPLE,
	SKINCOLOR_SALMON,
	SKINCOLOR_ARCTIC,
	SKINCOLOR_SUNSET,
	SKINCOLOR_BONE,

	SKINCOLOR_MASTER,
	SKINCOLOR_KETCHUP,
	SKINCOLOR_GOLD,
}

local rotateringhud = 0
local last_recordedHUDrings = 0
local ring_outline = {0,-1,0,1,1,0,-1,0}

freeslot("SKINCOLOR_FULLHUDRAINBOW")

local color_ramp = {189, 74, 65, 54, 59, 39, 204, 194, 165, 150, 149, 148, 132, 123, 100, 98}

skincolors[SKINCOLOR_FULLHUDRAINBOW] = {
	accessible = false,
	ramp = color_ramp,
}

customhud.SetupItem("mrce_specialstage", "mrce", function(v, p, c, e)
	if not (mapheaderinfo[gamemap].mrce_emeraldstage and mrce.emstage_attemptavailable) then
		return
	end

	local rainbow = false

	if  (mrce.debugmode & debug_es) then
		local debug_y = 192

		v.drawString(2, debug_y, string.format("Freeze_time: %d/%d", multiplier_freeze, freeze_time), V_SNAPTOLEFT|V_SNAPTOBOTTOM)
		debug_y = $-8

		v.drawString(2, debug_y, string.format("mutliplier_timer: %d/%d", multiplier_timer, settings_multi_timers[multiplier]), V_SNAPTOLEFT|V_SNAPTOBOTTOM)
		debug_y = $-8

		v.drawString(2, debug_y, string.format("score_requirement: %d/%d/%d", mrce.emstage_globalPoints, scorereq, tokenreq), V_SNAPTOLEFT|V_SNAPTOBOTTOM)
		debug_y = $-8

		v.drawString(2, debug_y, string.format("last_values (multi, rings): %d, %d", last_multiplier, last_recordedHUDrings), V_SNAPTOLEFT|V_SNAPTOBOTTOM)
		debug_y = $-8

		v.drawString(2, debug_y, "Current Rank: " .. tostring(emstagerank), V_SNAPTOLEFT|V_SNAPTOBOTTOM)
	end


	local curcolor = color_table[tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)]
	local colormap = v.getColormap(TC_DEFAULT, curcolor)
	v.draw(160, 20, v.cachePatch("EMHUD_BG"), V_SNAPTOTOP, colormap)

	if multiplier_freeze then
		local progress = multiplier_freeze*FRACUNIT/freeze_time
		local patch = v.cachePatch("EMHUD_BGF")
		local crop_left = ease.linear(progress, patch.width/2, 0) << FRACBITS
		local crop_right = ease.linear(progress, 0, patch.width) << FRACBITS

		v.drawCropped(160 << FRACBITS + crop_left, 20 << FRACBITS, FRACUNIT, FRACUNIT, patch, V_SNAPTOTOP, colormap, crop_left, 0, crop_right, patch.height << FRACBITS)
	end


	if multiplier_timer then
		local progress = (((multiplier_timer)*FRACUNIT/settings_multi_timers[multiplier])*109)/FRACUNIT
		--v.drawFill(230, 30, progress+1, 2, 152|V_SNAPTOTOP|V_SNAPTORIGHT)
		local first_pick = skincolors[multiplier > 6 and SKINCOLOR_FULLHUDRAINBOW or curcolor].ramp[2]
		v.drawFill(148-progress, 30, progress+1, 2, first_pick|V_SNAPTOTOP)
		v.drawFill(172, 30, progress+1, 2, first_pick|V_SNAPTOTOP)
	end

	local em_meter_x = 159
	v.draw(em_meter_x, 22, v.cachePatch("EMEMERMET0"), V_SNAPTOTOP)

	if scorereq then
		local scorenum = mrce.emstage_globalPoints or 1
		local progress = max(min(FixedDiv(scorenum, scorereq), FRACUNIT), 0)
		local patch_1 = v.cachePatch("EMEMERMET1")
		local act_size = patch_1.height * FRACUNIT
		local dif = act_size - progress*patch_1.height



		if progress == FRACUNIT and not reached_goal_val then
			reached_goal_val = true
			emerald_flash_timer = FRACUNIT
		elseif progress < FRACUNIT then
			reached_goal_val = false
		end

		--print(string.format('%f', progress))

		v.drawCropped(em_meter_x << FRACBITS, 22 << FRACBITS + dif, FRACUNIT, FRACUNIT, patch_1, V_SNAPTOTOP, colormap, 0, abs(dif), patch_1.width * FRACUNIT, act_size)

		if tokenreq then
			progress = max(min(FixedDiv(max(scorenum - scorereq, 1), tokenreq), FRACUNIT), 0)
			local patch_2 = v.cachePatch("EMEMERMET2")
			local rainbowc = v.getColormap(TC_DEFAULT, SKINCOLOR_FULLHUDRAINBOW)

			if progress == FRACUNIT and not reached_token_val then
				reached_token_val = true
				emerald_flash_timer = FRACUNIT
			elseif progress < FRACUNIT then
				reached_token_val = false
			end

			act_size = patch_2.height * FRACUNIT
			dif = act_size - progress*patch_2.height

			v.drawCropped(em_meter_x << FRACBITS, 22 << FRACBITS + dif, FRACUNIT, FRACUNIT, patch_2, V_SNAPTOTOP, rainbowc, 0, abs(dif), patch_2.width * FRACUNIT, act_size)

			rainbow = true
		end


		if emerald_flash_timer then
			local color = colormap

			if reached_token_val then
				color = v.getColormap(TC_DEFAULT, SKINCOLOR_FULLHUDRAINBOW)
			end

			local transparency = ease.insine(emerald_flash_timer, 0, 10) << V_ALPHASHIFT

			v.drawScaled(em_meter_x << FRACBITS, 22 << FRACBITS, FRACUNIT + emerald_flash_timer/4, v.cachePatch("EMEMERMET3"), V_SNAPTOTOP|transparency, colormap)

			emerald_flash_timer = emerald_flash_timer/3
		end
	end

	--v.drawLevelTitle(160 + 16*cos((3*leveltime % 180)*ANG1)/FRACUNIT, 22, multipliers[multiplier], V_SNAPTOTOP)

	if last_multiplier ~= multiplier then
		if last_multiplier < multiplier then
			mp_rot_timer = rotational_time
		else
			mp_rot_timer = -rotational_time
		end
		last_multiplier = multiplier
	end

	local time_rp = mp_rot_timer + 28
	local nangle = ((time_rp + 60) % 180)*ANG1
	local rotcolor = v.getColormap(TC_DEFAULT, SKINCOLOR_FULLHUDRAINBOW)

	MRCElibs.drawFakeHRotation(v, 160*FRACUNIT - 32*cos(nangle), 17 * FRACUNIT + FRACUNIT/2, nangle+ANGLE_90, FRACUNIT/2-FRACUNIT/5, v.cachePatch("EMEMULT"..multiplier), V_SNAPTOTOP|V_20TRANS, rotcolor)


	if mp_rot_timer then
		if multiplier > 1 then
			nangle = ((time_rp) % 180)*ANG1
			MRCElibs.drawFakeHRotation(v, 160*FRACUNIT - 32*cos(nangle), 17 * FRACUNIT + FRACUNIT/2, nangle+ANGLE_90, FRACUNIT/2-FRACUNIT/5, v.cachePatch("EMEMULT"..(multiplier-1)), V_SNAPTOTOP|V_20TRANS, rotcolor)
		end


		if multiplier < #multipliers then
			nangle = ((time_rp + 120) % 180)*ANG1
			MRCElibs.drawFakeHRotation(v, 160*FRACUNIT - 32*cos(nangle), 17 * FRACUNIT + FRACUNIT/2, nangle+ANGLE_90, FRACUNIT/2-FRACUNIT/5, v.cachePatch("EMEMULT"..(multiplier+1)), V_SNAPTOTOP|V_20TRANS, rotcolor)
		end
	end

	TBSlib.drawTextInt(v, "MRCEHUDFNT", 26, 19, tostring(mrce.emstage_globalPoints), V_SNAPTOTOP, nil, "left", -1, 9, '0')

	if multiplier > 6 then
		rainbow = true
	end

	if mp_rot_timer then
		mp_rot_timer = mp_rot_timer/3
	end

	if rainbow then
		-- Color scrolling
		local first_index = color_ramp[1]
		table.remove(color_ramp, 1)
		table.insert(color_ramp, first_index)
		skincolors[SKINCOLOR_FULLHUDRAINBOW].ramp = color_ramp
	end


	local getrings = p.mo.skin == "xian" and p.xian.rings or p.rings

	if last_recordedHUDrings ~= getrings then
		if getrings > last_recordedHUDrings then
			rotateringhud = $+abs(last_recordedHUDrings - getrings)*24
		end
		last_recordedHUDrings = getrings
	end

	local ringsfont = getrings > 0 and 'MRCEHUDFNT' or ((leveltime % 12)/6 and 'MRCEHURFNT' or 'MRCEHUDFNT')
	local ringcolor = p.mo.pw_combine and v.getColormap(TC_DEFAULT, 0, 'PurpleRingTranslation') or nil

	rotateringhud = max(ease.linear(FRACUNIT/12, rotateringhud, 0), 0)
	-- Rings
	local x = 27
	local y = 25
	local a = 21
	local b = 29

	local ringtimer = (rotateringhud) % 23
	local ring = v.getSpritePatch(SPR_RING, ringtimer)
	local flip = (ringtimer/12) and V_FLIP or 0
	local color = v.getColormap(TC_ALLWHITE, 31)
	local xwidth = ring.width/4
	local xheight = ring.height/2

	for i = 2, 8, 2 do
		local xoff = ring_outline[i-1]
		local yoff = ring_outline[i]
		v.drawScaled((x+xoff+xwidth) << FRACBITS, (y+yoff+xheight) << FRACBITS, FRACUNIT/4, ring, V_SNAPTOTOP|flip, color)
	end
	v.drawScaled((x+xwidth) << FRACBITS, (y+xheight) << FRACBITS, FRACUNIT/4, ring, V_SNAPTOTOP|flip, ringcolor)

	TBSlib.drawTextInt(v, "MRCEHUDFNT", 48-5, 32, tostring(getrings), V_SNAPTOTOP, nil, "left", -1, 4, '0')

	TBSlib.drawTextInt(v, "MRCEHUDFNT", 301-3, 19, pad(tostring(G_TicsToMinutes(p.realtime)), 2, '0')..':'..pad(tostring(G_TicsToSeconds(p.realtime)), 2, '0')..':'..pad(tostring(G_TicsToCentiseconds(p.realtime)), 2, '0'), V_SNAPTOTOP, nil, "right", -1, 3, '0')

	x = 291
	y = 40
	a = 297
	b = 44

	local skin_color = v.getColormap(p.skin, p.skincolor)
	local continuepatch = v.getSprite2Patch(p.skin, SPR2_XTRA, C, 0)

	if p.continues <= 13 then
		for cont = 1, min(p.continues - 1, 12) do
			v.drawScaled(x << FRACBITS, y << FRACBITS, FRACUNIT/2-FRACUNIT/9, continuepatch, V_SNAPTOTOP, skin_color)
			x = $ - continuepatch.width/4 - 2
		end
	else
		v.drawScaled(x << FRACBITS, y << FRACBITS, FRACUNIT/2-FRACUNIT/8, continuepatch, V_SNAPTOTOP, skin_color)
		TBSlib.drawTextInt(v, "MRCEHUDFNT", (a - continuepatch.width) - (continuepatch.width / 4), (b - (continuepatch.height / 4)) - ((continuepatch.height / 4) / 2), tostring(p.continues - 1), V_SNAPTOTOP, nil, "left", -1, 0, '0')
	end
end, "game")

local message_table = {
	[3] = "ESCAPE ILLUSION FOREST",
	[4] = "RUN AWAY",
	[7] = "Enter a dream...",
	[8] = "_"
}

customhud.SetupItem("stagetitle_specialstage_mrce", "mrce", function(v, p, t, e)
	if not mapheaderinfo[gamemap].mrce_emeraldstage then
		return false
	end

	local index = tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)
	local message = message_table[index] or "REACH THE GOAL"

	local ct = max(t - 40, 0)

	if message and message ~= "_" then
		local width = v.width()
		local scale, fx = v.dupx()
		local width_full = width / scale

		local endht = e/12
		local endt = e/3 + 18

		local height = ease.linear(min(ct * FRACUNIT / endht, FRACUNIT), 0,  18) + ease.linear(max(min((ct - endt) * FRACUNIT / endht, FRACUNIT), 0), 0, -18)

		local text_in = max(min((ct - endht) * FRACUNIT / endht, FRACUNIT), 0)
		local text_out = max(min((ct + endht - endt) * FRACUNIT / endht, FRACUNIT), 0)

		local alpha = ease.outsine(text_in, 10,  0) + ease.outsine(text_out, 0, 10)
		local move = ease.outsine(text_in, 40,  0) - ease.outsine(text_out, 0,  40)

		v.drawFill(0, 200 - height, width_full, height, 26|V_SNAPTOLEFT|V_SNAPTOBOTTOM|V_30TRANS)

		if height > 2 then
			v.drawFill(0, 202 - height, width_full, 2, skincolors[color_table[index] or 1].ramp[4]|V_SNAPTOLEFT|V_SNAPTOBOTTOM|V_30TRANS)
		end

		if alpha < 10 then
			local alphaflag = alpha << V_ALPHASHIFT
			local colormap = v.getColormap(TC_DEFAULT, color_table[index])
			local lvltext = string.upper(mapheaderinfo[gamemap].lvlttl)

			local em = v.cachePatch("MRCEEMSTTIEM"..(t % 12)+1)

			v.drawScaled(240*FRACUNIT, 145*FRACUNIT, FRACUNIT/2, em, V_SNAPTORIGHT|alphaflag|V_SNAPTOBOTTOM, colormap)
			TBSlib.drawStaticTextUnadjusted(v, "MRCEFUTITFNT", (310 + move)*FRACUNIT, 155*FRACUNIT, 2*FRACUNIT/3, lvltext, V_SNAPTORIGHT|alphaflag|V_SNAPTOBOTTOM, colormap, "right", -1)
			TBSlib.drawStaticTextUnadjusted(v, "MRCEFUTITFNT", (310 + move)*FRACUNIT, 185*FRACUNIT, FRACUNIT/3, message, V_SNAPTORIGHT|alphaflag|V_SNAPTOBOTTOM, colormap, "right", -1)
		end
	end
end, "titlecard")


--
--
--
--
--  POWER UPS
--
--
--
--




local multi_crystal = freeslot("MT_MRCEMULTICRYSTAL")
freeslot("S_MRCEMULTICRYSTAL", "SPR_MRCE_EMSTAGECOLLECT", "SPR_MRCE_EMSTAGECRYSTAL",
"S_MRCEMULTICRYSTALGLASS", "SPR_MRCE_EMSTAGEGLBREA")

states[S_MRCEMULTICRYSTAL] = {
	sprite = SPR_MRCE_EMSTAGECRYSTAL,
	frame = A|FF_SEMIBRIGHT|FF_ANIMATE,
	tics = 24,
	var1 = 23,
	var2 = 1,
	nextstate = S_MRCEMULTICRYSTAL,
}

states[S_MRCEMULTICRYSTALGLASS] = {
	sprite = SPR_MRCE_EMSTAGEGLBREA,
	frame = A|FF_FULLBRIGHT|FF_ANIMATE,
	tics = 17,
	var1 = 16,
	var2 = 1,
	nextstate = S_MRCEMULTICRYSTALGLASS,
}

mobjinfo[multi_crystal] = {
--$Category Emerald Stages
--$Name Multi Crystal
--$Sprite EMSTA0
--$NotAngled
        doomednum = 1481,
        spawnstate = S_MRCEMULTICRYSTAL,
        spawnhealth = 1,
        radius = 11*FRACUNIT,
        height = 47*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

addHook("MobjSpawn", function(mo)
	if not (mapheaderinfo[gamemap].mrce_emeraldstage and mrce.emstage_attemptavailable) then
		mo.scale = 1
		mo.fuse = 1
	else
		mo.spritexscale = FRACUNIT/4
		mo.spriteyscale = FRACUNIT/4
	end

	if not color_table[tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)] then return end
	mo.color = color_table[tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)]
end, multi_crystal)

local ScaleDecrement = FRACUNIT/14

local Crystal_Pickup = {
	[0] = {offscale_x = 0, offscale_y = 0, tics = 1, nexts = 1, waittill = function(mo, anim)
		MRCElibs.lerpMoveObjectToPos(mo, FRACUNIT/8+mo.speed, mo.target.x, mo.target.y, mo.target.z)
		local dist = R_PointToDist2(mo.x, mo.y, mo.target.x, mo.target.y)

		if not anim.thresholdtoreach then
			anim.thresholdtoreach = 1
		end

		-- In case

		anim.thresholdtoreach = $ + 1

		if anim.thresholdtoreach > TICRATE * 6 then
			multiplier = min(multiplier+1, #settings_multi_timers)
			multiplier_timer = settings_multi_timers[multiplier]

			P_KillMobj(mo, anim)
			TBSlib.resetAnimator(mo)
			return true
		end

		if dist < mo.radius then
			return true
		end
	end},
	[1] = {offscale_x = 0, offscale_y = 0, tics = 1, nexts = 2, waittill = function(mo, anim)
		-- In case

		if not anim.thresholdtoreach then
			anim.thresholdtoreach = 1
		end

		anim.thresholdtoreach = $ + 1

		if anim.thresholdtoreach > TICRATE * 6 then
			multiplier = min(multiplier+1, #settings_multi_timers)
			multiplier_timer = settings_multi_timers[multiplier]

			P_KillMobj(mo, anim)
			TBSlib.resetAnimator(mo)
			return true
		end

		local tar = mo.target
		if not anim.newtics then
			anim.newtics = 1
		end

		anim.newtics = $+1
		local ang = anim.newtics*ANG20


		P_MoveOrigin(mo, tar.x + FixedMul(tar.radius, sin(ang+tar.angle)),
		tar.y + FixedMul(tar.radius, cos(ang+tar.angle)), tar.z+tar.height/8)
		mo.scale = max(mo.scale-ScaleDecrement, 8)

		if anim.newtics > TICRATE/2 then
			mo.extravalue1 = 1
			return true
		end
	end},
	[2] = {offscale_x = 0, offscale_y = 0, tics = 4, nexts = 0}, -- dummy state
}

addHook("MobjThinker", function(mo)
	if not mo.target then
		TBSlib.resetAnimator(mo)
		IsPlayerAround(mo, FRACUNIT*256, 8)
		mo.speed = 0
	else
		TBSlib.objAnimator(mo, Crystal_Pickup)

		if mo and mo.valid then
			mo.speed = $+FRACUNIT/32
		end
	end
end, multi_crystal)

addHook("TouchSpecial", function(mo, t)
	if not mo.extravalue1 then
		mo.target = t
		return true
	end

	multiplier = min(multiplier+1, #settings_multi_timers)
	multiplier_timer = settings_multi_timers[multiplier]
	if t.player and t.player.bot then
		P_KillMobj(mo, t)
	end
end, multi_crystal)

addHook("MobjDeath", function(mo, t)
	MRCE_superSpark(mo, 2, TICRATE, 1, 2*FRACUNIT, false, 56)
	MRCE_superSpark(mo, 2, TICRATE, 1, 2*FRACUNIT, false, 56)
	S_StartSound(t, (sfx_mscry0 + (multiplier - 2)))

	local glass

	for i = 1, 9 do
		glass = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_THOK)

		glass.state = S_MRCEMULTICRYSTALGLASS
		glass.fuse = 24
		glass.momx = P_RandomRange(-12, 12) * FRACUNIT
		glass.momy = P_RandomRange(-12, 12) * FRACUNIT
		glass.momz = P_RandomRange(-4, 12) * FRACUNIT
		glass.color = mo.color
		glass.scale = max(glass.scale + mo.scale / 2, FRACUNIT)
	end

	S_StartSound(glass, sfx_shattr)
end, multi_crystal)

local hoopstate = freeslot("S_MRCEMULTICRYSTAL")

states[hoopstate] = {
	sprite = SPR_MRCE_EMSTAGECOLLECT,
	frame = 14|FF_SEMIBRIGHT,
}

addHook("TouchSpecial", function (mo, toucher)
	if multiplier > 1 then
		multiplier_timer = min($ + settings_multi_timers[multiplier]/2, settings_multi_timers[multiplier])
	end

end, MT_BLUESPHERE)

addHook("MobjSpawn", function(mo)
	if not mapheaderinfo[gamemap].mrce_emeraldstage then return end
	if netgame then
		mo.scale = 1
		mo.fuse = 1
		return
	end
	if not mrce.emstage_attemptavailable then
		mo.scale = 1
		mo.fuse = 1
	end
	if not color_table[tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)] then return end
	mo.color = color_table[tonumber(mapheaderinfo[gamemap].mrce_emeraldstage)]
	mo.state = hoopstate

	if(mo.subsector.sector.lightlevel < 175) then
		mo.renderflags = mo.renderflags | RF_FULLBRIGHT
	end

end, MT_HOOP)

addHook("MobjSpawn", function(mo)
	if netgame then
		P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_BLUESPHERE)
		mo.scale = 1
		mo.fuse = 1
		return
	end
end, MT_HOOPCENTER)

addHook("MobjSpawn", function(mo)
	if netgame then
		mo.scale = 1
		mo.fuse = 1
		return
	end
	if not mapheaderinfo[gamemap].mrce_emeraldstage then return end
	if not mrce.emstage_attemptavailable then
		mo.scale = 1
		mo.fuse = 1
	end
end, MT_HOOPCOLLIDE)

local hoopbldist = 24*FU
local halfheight = 28*FU

local function hoopcollide(mo, tmo)
	if not (tmo and tmo.valid) then return false end
	if (abs(tmo.x - mo.x) >= hoopbldist
	or abs(p.mo.y - mo.y) >= hoopbldist
	or abs((p.mo.z + halfheight) - mo.z) >= hoopbldist) then
		return false
	end
	return true
end

local scorepw_mb = freeslot("MT_SCOREEMPW")
local scorepw_st = freeslot("S_SCOREEMPW")

local timescorepw_mb = freeslot("MT_TIMESCOREEMPW")
local timescorepw_st = freeslot("S_TIMESCOREEMPW")

local COLOR_LUT = {
	SKINCOLOR_SAPPHIRE,
	SKINCOLOR_MR_VOID,
	SKINCOLOR_JADE,
	SKINCOLOR_GARNET,
	SKINCOLOR_AQUAMARINE,
	SKINCOLOR_TOPAZ,
	SKINCOLOR_MOONSTONE
}

local function deletohoops(mo)
	if netgame then
		mo.executiondate = $ or 4
		if mo.executiondate > 0 then mo.executiondate = $ - 1 return end
		print("kill")
		P_RemoveMobj(mo)
		return
	end
end

addHook("MobjThinker", deletohoops, MT_HOOPCOLLIDE)
addHook("MobjThinker", deletohoops, MT_HOOP)
addHook("MobjThinker", deletohoops, MT_HOOPCENTER)

addHook("TouchSpecial", function(mo, t)
	if mapheaderinfo[gamemap].mrce_emeraldstage and not mrce.emstage_attemptavailable then return true end
	if multiplayer then
		return true
	end
	if multiplier > 1 then
		multiplier_timer = min($ + settings_multi_timers[multiplier]/2, settings_multi_timers[multiplier])
	end
end, MT_HOOPCOLLIDE)

local deleto_pw = TICRATE*2
local deletotic = deleto_pw/10

local function pw_standardizedbehavior(mo)
	if not (mapheaderinfo[gamemap].mrce_emeraldstage and mrce.emstage_attemptavailable) then
		P_RemoveMobj(mo)
	else
		if mo.extravalue1 then
			mo.angle = $+ANG10
			mo.momz = mo.scale*P_MobjFlip(mo)
			mo.scale = $+FRACUNIT/64
			mo.frame = $|((10 - mo.fuse/deletotic) << FF_TRANSSHIFT)
		else
			mo.angle = $+ANG2
			if mo.type == timescorepw_mb then
				if mo.cusval then
					local time_adj = min(max(leveltime - mo.cusval, 0) * FRACUNIT/(mo.reactiontime or 256), FRACUNIT)
					mo.extravalue2 = max(ease.linear(time_adj, mo.extravalue2 or 1000, 0), 0)
				end
				if mo.extravalue2 == 0 then
					mo.frame = $|FF_TRANS60
				end
			end
		end
		if (mo.type == scorepw_mb or mo.type == timescorepw_mb) and mo.extravalue2 and mo.extravalue2 >= 5000 and not (leveltime % 7) then
			local tot = min((mo.extravalue2 / 4000), 7)
			local widt = FixedInt((3 * (mo.radius * 2)) / 5)
			local hgt = FixedInt((4 * mo.height) / 5)
			local clr = mo.extravalue2 >= 25000 and 7 or mo.extravalue2 >= 10000 and SKINCOLOR_MR_VOID or 0
			for i = 1, tot do
				local spark = P_SpawnMobjFromMobj(mo, P_RandomRange(-widt, widt)*FRACUNIT, P_RandomRange(-widt, widt)*FRACUNIT, (P_RandomRange(hgt/5, (hgt*3)/4)*FRACUNIT) - mo.height, MT_SUPERSPARK)
				if clr then
					spark.colorized = true
					if clr == 7 then
						spark.color = COLOR_LUT[P_RandomRange(1, #COLOR_LUT)]
					else
						spark.color = clr
					end
				end
				P_SetObjectMomZ(spark, P_RandomRange(3, 6)*FRACUNIT)
			end
		end
	end
end


freeslot("MT_ATTRACTIONEMPW", "S_ATTRACTIONEMPW")

mobjinfo[MT_ATTRACTIONEMPW] = {
--$Category Emerald Stages
--$Name Attraction Power Up
--$Sprite EMSTG0
--$NotAngled
        doomednum = 1482,
        spawnstate = S_ATTRACTIONEMPW,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[S_ATTRACTIONEMPW] = {
	sprite = SPR_MRCE_EMSTAGECOLLECT,
	frame = G|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		t.superattract = 10*TICRATE
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, MT_ATTRACTIONEMPW)

addHook("MobjThinker", pw_standardizedbehavior, MT_ATTRACTIONEMPW)

local freeze_time_mb = freeslot("MT_FREEZETIMEREMPW")
local freeze_time_st = freeslot("S_FREEZETIMEREMPW")

mobjinfo[freeze_time_mb] = {
--$Category Emerald Stages
--$Name Time Freeze Power Up
--$Sprite EMSTH0
--$NotAngled
        doomednum = 1483,
        spawnstate = freeze_time_st,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[freeze_time_st] = {
	sprite = SPR_MRCE_EMSTAGECOLLECT,
	frame = H|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		multiplier_freeze = freeze_time
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, freeze_time_mb)

addHook("MobjThinker", pw_standardizedbehavior, freeze_time_mb)

local armapw_mb = freeslot("MT_ARMAGEDDONEMPW")
local armapw_st = freeslot("S_ARMAGEDDONEMPW")

mobjinfo[armapw_mb] = {
--$Category Emerald Stages
--$Name Armageddon Power Up
--$Sprite EMSTI0
--$NotAngled
        doomednum = 1484,
        spawnstate = armapw_st,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[armapw_st] = {
	sprite = SPR_MRCE_EMSTAGECOLLECT,
	frame = I|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		P_SwitchShield(t.player, SH_ARMAGEDDON)
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, armapw_mb)

addHook("MobjThinker", pw_standardizedbehavior, armapw_mb)

mobjinfo[scorepw_mb] = {
--$Category Emerald Stages
--$Name Score Up
--$Sprite EMSTJ0
--$Arg0 Custom Score Value
--$NotAngled
        doomednum = 1485,
        spawnstate = scorepw_st,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[scorepw_st] = {
	sprite = SPR_MRCE_EMSTAGECOLLECT,
	frame = J|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("MapThingSpawn", function(mo, ma)
	mo.extravalue2 = max(ma.args[0], 0)	-- scoreadd
end, scorepw_mb)

addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		mrce.emstage_globalPoints = $ + (mo.extravalue2 or 5000)
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, scorepw_mb)

addHook("MobjThinker", pw_standardizedbehavior, scorepw_mb)

mobjinfo[timescorepw_mb] = {
--$Category Emerald Stages
--$Name Timed Score Up
--$Sprite EMSTK0
--$Arg0 Regressing Threshold (1s = 35 tics)
--$Arg1 Regression Length (1s = 35 tics)
--$Arg2 Custom Score Value
--$NotAngled
        doomednum = 1486,
        spawnstate = timescorepw_st,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[timescorepw_st] = {
	sprite = SPR_MRCE_EMSTAGECOLLECT,
	frame = K|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}


freeslot("MT_INVINCEMPW", "S_INVINCEMPW")

mobjinfo[MT_INVINCEMPW] = {
--$Category Emerald Stages
--$Name Invincibility Power Up
--$Sprite EMSTF0
--$NotAngled
        doomednum = 1488,
        spawnstate = S_INVINCEMPW,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[S_INVINCEMPW] = {
	sprite = SPR_MRCE_EMSTAGECOLLECT,
	frame = F|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		t.player.powers[pw_invulnerability] = 20*TICRATE
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, MT_INVINCEMPW)

addHook("MobjThinker", pw_standardizedbehavior, MT_INVINCEMPW)

addHook("MapThingSpawn", function(mo, ma)
	mo.cusval = max(ma.args[0], 0) -- threshold
	mo.reactiontime = max(ma.args[1], 0)	-- regresing
	mo.extravalue2 = max(ma.args[2], 0)	-- scoreadd
end, timescorepw_mb)


addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		mrce.emstage_globalPoints = $ + mo.extravalue2
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, timescorepw_mb)

addHook("MobjThinker", pw_standardizedbehavior, timescorepw_mb)

local bombpw_mb = freeslot("MT_BOMBEMPW")
local bombpw_st = freeslot("S_BOMBEMPW")

mobjinfo[bombpw_mb] = {
--$Category Emerald Stages
--$Name Bomb Power Up
--$Sprite EMSTL0
--$Arg0 Radius
--$Arg0Type 23
--$Arg0RenderStyle Circle
--$NotAngled
        doomednum = 1487,
        spawnstate = bombpw_st,
        spawnhealth = 1,
		deathsound = sfx_itemup,
        radius = 24*FRACUNIT,
        height = 48*FRACUNIT,
        flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_SPECIAL
}

states[bombpw_st] = {
	sprite = SPR_MRCE_EMSTAGECOLLECT,
	frame = L|FF_SEMIBRIGHT|FF_PAPERSPRITE,
}

addHook("MapThingSpawn", function(mo, ma)
	mo.radius = max(ma.args[0] * FRACUNIT, 0) -- threshold
end, bombpw_mb)

local bombradius = 768*FRACUNIT

local function bomb_surrondings(ref, mo)
	ref.killcount = $ or 0
	ref.popcount = $ or 0
	if mo.flags & MF_ENEMY then
		ref.killcount = $ + 1
		local endmy = min(15, (ref.killcount / 2) + 1)
		ref.extravalue2 = $ + (scoretable[endmy] * 2)
		P_KillMobj(mo)
	elseif mo.type == MT_EGGMAN_BOX
	or mo.type == MT_SPIKEBALL
	or mo.type == MT_WALLSPIKE
	or mo.type == MT_SPIKE
	or mo.type == MT_BOMBSPHERE then
		if (mo.flags & MF_MONITOR) then
			ref.popcount = $ + 1
		end
		ref.killcount = $ + 1
		local endmy = min(15, (ref.killcount / 2) + 1)
		ref.extravalue2 = $ + (scoretable[endmy] * 3)
		P_KillMobj(mo)
	end
end

addHook("TouchSpecial", function(mo, t)
	if mo.extravalue1 then return true end

	if t.player and t.player.valid and t.player.playerstate == PST_LIVE and not t.player.bot then
		searchBlockmap("objects", bomb_surrondings, mo, mo.x - bombradius, mo.x + bombradius, mo.y - bombradius, mo.y + bombradius)
		if mo.extravalue2 then
			mrce.emstage_globalPoints = $ + mo.extravalue2*multipliers[multiplier]
			t.player.numboxes = $ + mo.popcount
			for i = 1, mo.popcount do
				--MRCEGivePlayerESCombo(t.player, true)
			end
			--print(mo.extravalue2)
		end
		mo.extravalue1 = 1
		mo.fuse = deleto_pw
		S_StartSound(mo, mo.info.deathsound)
		return true
	else
		return true
	end
end, bombpw_mb)

addHook("MobjThinker", pw_standardizedbehavior, bombpw_mb)

local rankvals = {
	[0] = "F",
	[1] = "E",
	[2] = "D",
	[3] = "C",
	[4] = "B",
	[5] = "A",
	[6] = "S"
}

local rankclrs = {
	[0] = SKINCOLOR_VOLCANIC,
	[1] = SKINCOLOR_CHERRY,
	[2] = SKINCOLOR_TOPAZ,
	[3] = SKINCOLOR_CHARTREUSE,
	[4] = SKINCOLOR_SAPPHIRE,
	[5] = SKINCOLOR_GALAXY,
	[6] = SKINCOLOR_MOONSTONE
}

hud.add(function(v)
	if not mapheaderinfo[gamemap].mrce_emeraldstage then return end
	if not (mapheaderinfo[gamemap].mrce_emeraldscore and mapheaderinfo[gamemap].mrce_emeraldtoken and mapheaderinfo[gamemap].mrce_emtime) then return end
	local grade = "MRCEEMMULTI" .. rankvals[emstagerank]
	local gradeclr = v.getColormap(TC_DEFAULT, rankclrs[emstagerank])
	if not mrce.emstage_attemptavailable then grade = "MRCEEMMULTIF" gradeclr = v.getColormap(TC_DEFAULT, SKINCOLOR_VOLCANIC) end
	--TBSlib.drawTextInt(v, "MRCEEMMULTI", 205, 170, grade, V_SNAPTOBOTTOM, nil, "center", -1)
	v.drawScaled(200*FRACUNIT, 160*FRACUNIT, FRACUNIT/2, v.cachePatch(grade), V_SNAPTOBOTTOM, gradeclr)
end, "intermission")

--[[hud.add(function(v)
	for i = 0, 6 do
		local grade = "MRCEEMMULTI" .. rankvals[i]
		local gradeclr = v.getColormap(TC_DEFAULT, rankclrs[i])
		--if i == 5 then
		--	gradeclr = v.getColormap(TC_DEFAULT, consoleplayer.skincolor)
		--end
		--TBSlib.drawTextInt(v, "MRCEEMMULTI", 205, 170, grade, V_SNAPTOBOTTOM, nil, "center", -1)
		v.drawScaled(50*FRACUNIT + (i*40*FRACUNIT), 160*FRACUNIT, FRACUNIT/2, v.cachePatch(grade), V_SNAPTOBOTTOM, gradeclr)
	end
end, "game")]]

freeslot("MT_MRCE_EMST_RINGBOX")
mobjinfo[MT_MRCE_EMST_RINGBOX] = {
	spawnstate = S_INVISIBLE,
	radius = 768*FRACUNIT,
	height = 1536*FRACUNIT,
	flags = MF_NOGRAVITY|MF_NOCLIPHEIGHT
}

addHook("PlayerThink", function(p)
	if not (p.mo and p.mo.valid) then return end
	if p.playerstate ~= PST_LIVE then return end
	if not (mapheaderinfo[gamemap].mrce_emeraldstage and mrce.emstage_attemptavailable) or (tonumber(mapheaderinfo[gamemap].mrce_emeraldstage) and mapheaderinfo[gamemap].lvlttl == "Inner Sanctum") then return end
	p.mo.ringbox = $ or P_SpawnMobjFromMobj(p.mo, 0, 0, 0, MT_MRCE_EMST_RINGBOX)
	p.mo.ringbox.target = p.mo
	p.mo.superattract = $ and $ - 1 or 0
end)

addHook("MobjThinker", function(mo)
	if not mo and mo.valid then return end
	if not mo.target then return end
	if mo.target and mo.target.player and mo.target.player.playerstate == PST_LIVE then
		local flipadd = (mo.target.eflags & MFE_VERTICALFLIP) and mo.height or 0
		P_SetOrigin(mo, mo.target.x, mo.target.y, (mo.target.z + (mo.target.height / 2)) - ((mo.height/2)*P_MobjFlip(mo.target)) - flipadd)
	else
		mo.target = nil
		P_KillMobj(mo)
	end
end, MT_MRCE_EMST_RINGBOX)

local function doRingMomentum(mo, dimension)
	-- where dimension == "x"
	-- mo[dimension] == mo.x
	-- mo["mom" .. dimension] == mo.momx

	local followmo = mo.emringtarget
	if followmo[dimension] > mo[dimension] then
		if mo["mom" .. dimension] > followmo["mom" .. dimension] then
			mo["mom" .. dimension] = $ + FRACUNIT
		else
			mo["mom" .. dimension] = $ + FRACUNIT*2
		end
	elseif followmo[dimension] < mo[dimension] then
		if mo["mom" .. dimension] < followmo["mom" .. dimension] then
			mo["mom" .. dimension] = $ - FRACUNIT
		else
			mo["mom" .. dimension] = $ - FRACUNIT*2
		end
	end
end

-- Perform classic ring attraction. And many checks. Soo many checks.
function A_ClassicRingAttract(mo)
	mo.fuse = 0 -- Never disappear

	if mo.hprev then mo.hprev = nil end

	mo.flags = $ | MF_NOCLIP | MF_NOCLIPHEIGHT | MF_NOGRAVITY -- Go through everything

	-- Do the actual movement
	doRingMomentum(mo, "x")
	doRingMomentum(mo, "y")
	doRingMomentum(mo, "z")

	mo.state = mo.state -- Update state

	P_MoveOrigin(mo, mo.x + mo.momx, mo.y + mo.momy, mo.z + mo.momz) -- Update position using momentum.
end

local function nyoom(mo, ring)
	if not (mo and mo.valid and ring and ring.valid) then return end
	if not (mo.target and mo.target.valid) then return end
	if (mo.z > (ring.z + ring.height))
    or ((mo.z + mo.height) < ring.z) then
    	return false
    end
	--print("grabma")
	if ring.type ~= MT_RING and ring.type ~= MT_BLUESPHERE then return false end
	local tringdist = mo.target.superattract and RING_ATTRACT_DIST * 7 or RING_ATTRACT_DIST
	if ring.type == MT_BLUESPHERE then tringdist = $*3 end
	if abs(R_PointToDist2(ring.x, ring.y, mo.target.x, mo.target.y)) <= tringdist and abs(ring.z - mo.target.z) <= tringdist then
		ring.emringtarget = mo.target
		ring.attracttarget = 1
	--else
		--print("xy " .. (abs(R_PointToDist2(ring.x, ring.y, mo.target.x, mo.target.y)) / FRACUNIT))
		--print("z " .. (abs(ring.z - mo.target.z) / FRACUNIT))
	end
	if ring.emringtarget and ring.emringtarget.valid and not (ring.emringtarget.player.powers[pw_shield] & SH_PROTECTELECTRIC) then
		if ring.type == MT_BLUESPHERE then
			P_HomingAttack(ring, mo.target)
		else
			A_ClassicRingAttract(ring)
		end
	end
end
--[[
local oldattractchase = A_AttractChase

function A_AttractChase(mo, v1, v2)
	if mo.offset then
		mo.offset = nil
	end
	oldattractchase(mo, v1, v2)
end
]]
addHook("MobjCollide", nyoom, MT_MRCE_EMST_RINGBOX)
addHook("MobjMoveCollide", nyoom, MT_MRCE_EMST_RINGBOX)
--[[
addHook("PlayerThink", function(p)
	if not (p.mo and p.mo.valid) then return end
	p.mo.rockdist = $ or 0
	p.mo.emmyspeen = $ or 0
	p.mo.returnmarocks = $ or 0
	if MRCE_isHyper(p) then
		if p.mo.testorbit == nil then
			p.mo.testorbit = {}
			for i = 0, 6, 1 do
				local emerald = P_SpawnMobjFromMobj(p.mo, 0, 0, 0, MT_FAKEEMERALD1)
				emerald.state = _G["S_FAKEEMERALD" .. i + 1]
				emerald.flags = $|MF_SCENERY|MF_NOCLIP|MF_NOCLIPHEIGHT--|MF_NOTHINK
				emerald.target = p.mo
				emerald.scale = 1
				emerald.destscale = FRACUNIT
				local ex, ey, ez = Polar2Cartesian3(FixedAngle(((360*FRACUNIT)/7)*(i + 1)), p.mo.rockdist, p.mo, false)
				--local ez = FixedMul(sin(p.mo.angle))
				P_SetOrigin(emerald, p.mo.x + ex, p.mo.y + ey, p.mo.z + (p.mo.height/2) + ez / 2)
				p.mo.testorbit[i] = emerald
			end
		else
			p.mo.rockdist = min(60*FRACUNIT, $ + 4*FRACUNIT)
			--print(p.mo.rockdist)
			for i = 0, 6, 1 do
				local ex, ey, ez = Polar2Cartesian3(FixedAngle(((360*FRACUNIT)/7)*(i + 1)) + p.mo.emmyspeen, p.mo.rockdist, p.mo, false)
				local emmy = p.mo.testorbit[i]
				P_MoveOrigin(emmy, p.mo.x + ex, p.mo.y + ey, p.mo.z + (p.mo.height/2) + ez / 2)
			end
		end
		p.mo.emmyspeen = $ + ANG1
	elseif p.mo.testorbit ~= nil then
		p.mo.rockdist = max($ - 3*FRACUNIT, 0)
		for i = 0, 6, 1 do
			local ex, ey, ez = Polar2Cartesian3(FixedAngle(((360*FRACUNIT)/7)*(i + 1)) + p.mo.emmyspeen, p.mo.rockdist, p.mo, false)
			local emmy = p.mo.testorbit[i]
			P_MoveOrigin(emmy, p.mo.x + ex, p.mo.y + ey, p.mo.z + (p.mo.height/2) + ez / 2)
			emmy.destscale = 1
			--if emmy.scale == 1 then
			--	emmy.fuse = 1
			--	p.mo.testorbit = nil
			--end
		end
		if p.mo.rockdist == 0 then
			p.mo.testorbit = nil
		end
	end
end)]]

freeslot("MT_MRCE_ESHOOPSPAWNER")

mobjinfo[MT_MRCE_ESHOOPSPAWNER] = {
	--$Name Special Hoop Spawner
	--$Category Mystic Realm Special
	--$Arg0 Hoop Radius
	--$Arg1 Thinker
	--$Arg1Type 12
	--$Arg1Enum { 1 = "Disable Orbital Object Thinkers"; }
	--$Arg2 Number of Orbital Objects
	--$Arg2ToolTip When ommitted, defaults to 7
	--$Arg3 Orbit Speed
	--$Arg3ToolTip Speed orbital objects will follow their path, measured in half degrees. If ommitted, set to 2, or 1 degree per tic
	--$Arg4 Hoop Yaw Tilt Speed
	--$Arg5 Hoop Angle Rotate Speed
	--$Arg6 Hoop Radius Test
	--$StringArg0 Orbital Object Type
	--$StringArg0ToolTip MT_ Constant, set an object type to use as the orbital objects
	--$StringArg1 Orbital Object Colorize
	--$StringArg1ToolTip Set a color to colorize all orbital objects to automatically. Accepts SKINCOLOR_ inputs. If ommitted, orbital objects will not be colorized
	doomednum = 2231,
	spawnstate = S_INVISIBLE,
	flags = MF_NOCLIP|MF_NOGRAVITY
}

addHook("MobjThinker", function(mo)
	if not (mo and mo.valid) then return end
	if mo.sparkles == nil and not (mo.target and mo.target.player) then
		if mo.spawnpoint and mo.spawnpoint.args[0] and mo.orbitradius == nil then
			mo.orbitradius = mo.spawnpoint.args[0]*FRACUNIT
		else
			mo.orbitradius = 160*FRACUNIT
		end
		local idonotam = 0
		if mo.spawnpoint and mo.spawnpoint.args[1] then
			idonotam = MF_NOTHINK
		end
		if mo.spawnpoint and mo.spawnpoint.args[2] and mo.orbitnum == nil then
			mo.orbitnum = mo.spawnpoint.args[2]
		else
			mo.orbitnum = 7
		end
		if mo.spawnpoint and mo.spawnpoint.args[3] and mo.orbitspeenroll == nil then
			mo.orbitspeenroll = mo.spawnpoint.args[3]*(ANG1 / 2)
		else
			mo.orbitspeenroll = ANG1
		end
		if mo.spawnpoint and mo.spawnpoint.args[4] and mo.orbitspeenyaw == nil then
			mo.orbitspeenyaw = mo.spawnpoint.args[4]*(ANG1 / 2)
		else
			mo.orbitspeenyaw = 0
		end
		if mo.spawnpoint and mo.spawnpoint.args[5] and mo.orbitspeenangle == nil then
			mo.orbitspeenangle = mo.spawnpoint.args[5]*(ANG1 / 2)
		else
			mo.orbitspeenangle = 0
		end
		local motype = (mo.spawnpoint and mo.spawnpoint.stringargs[0]) and _G[mo.spawnpoint.stringargs[0]] or MT_FIREBALL

		if mo.spawnpoint and mo.spawnpoint.stringargs[1] and mo.orbitcolor == nil then
			mo.orbitcolor = _G[mo.spawnpoint.stringargs[1]]
		end
		mo.sparkles = MRCElibs.SpawnVerticalOrbitObjects(motype, mo , mo.orbitnum, mo.orbitradius)
		for i = 1, #mo.sparkles, 1 do
			mo.sparkles[i].flags = $|idonotam|MF_NOCLIP|MF_NOCLIPHEIGHT
			if mo.orbitcolor then
				mo.sparkles[i].colorized = true
				mo.sparkles[i].color = mo.orbitcolor
			end
		end
	end
	if mo.spawnpoint and mo.spawnpoint.args[3] then
		mo.roll = $ + mo.spawnpoint.args[3] * (ANG1 / 2)
	end
	if mo.spawnpoint and mo.spawnpoint.args[4] then
		mo.pitch = $ + mo.spawnpoint.args[4]*(ANG1/2)
	end
	--print("YAWpoint " .. mo.spawnpoint.args[4])
	if mo.spawnpoint and mo.spawnpoint.args[5] then
		mo.angle = $ + mo.spawnpoint.args[5] * (ANG1 / 2)
	end
	if mo.spawnpoint and mo.spawnpoint.args[6] then
		local bob = sin(FixedAngle(leveltime*4*FRACUNIT))
		MRCElibs.MoveOrbitalRadius(mo, mo.sparkles, max(0, (8 * mo.spawnpoint.args[6] * bob) + 48*FRACUNIT))
		--print(mo.sparkles[1].orbitradius)
	end
	--print("ANGLEpoint " .. mo.spawnpoint.args[5])
	for i = 1, #mo.sparkles, 1 do
		if mo.sparkles[i] and mo.sparkles[i].valid and mo.sparkles[i].offset then
			MRCElibs.Rotation3D(mo, mo.sparkles[i], mo.sparkles[i].offset)
		end
	end
end, MT_MRCE_ESHOOPSPAWNER)

addHook("PlayerThink",function(p)
	if not p
	or not p.valid then
		return
	end

    if not mapheaderinfo[gamemap].mrce_emeraldstage then return end

	if not p.mrce_escombo then
		p.mrce_escombo = {
			count = 0,
			time = 0,

			rank = 0,
			score = 0,
			verys = 0,

			showrank = {num = 0,score = 0,tics = 0},
			waslost = false,

			lastskin = 0,
		}
		return
	end

	local c = p.mrce_escombo
	local me = p.realmo

	if me.skin ~= c.lastskin then
		c.time = 0
	end

	if c.time then
		if (p.powers[pw_carry] ~= CR_NONE) then
			return
		end

		if c.time > maxtime then
			c.time = maxtime
		end

		local cc = c.count
		c.score = ((cc*cc)/2)+(10*cc)

		c.verys = cc/(#ranks*comboup)

//		c.rank = ranks[ ((cc-1) % 15)+1]
		if (p.pflags & PF_FINISHED) and c.time > 2 then
			c.time = 2
		end
		c.time = $-1
	else
		if c.count then
			S_StartSound(nil,sfx_shudcl,p)
			P_AddPlayerScore(p,c.score)
			c.showrank.num = c.rank
			c.showrank.score = c.score
			c.showrank.tics = 2*TR
			c.waslost = true
			c.count = 0
		end

	end

	if c.showrank.tics then
		c.showrank.tics = $-1
	else
		if c.waslost then
			c.waslost = false
		end
		if c.showrank.num then
			c.showrank.num = 0
		end
		if c.showrank.score then
			c.showrank.score = 0
		end
	end

	c.lastskin = me.skin
end)

local function drawcombotimebar(v, p, combdisp, comby)
	local me = p.mo
	local c = p.mrce_escombo
	local maxammo = maxtime/5
	local curammo = c.time/5
	local x = hudinfo[HUD_RINGS].x+18+combdisp
	local y = hudinfo[HUD_RINGS].y+30-2+comby
	local barx = x //- maxammo/2
	local bary = y
	local patch1 = v.cachePatch("CANSEG1") //blue
	local patch3 = v.cachePatch("CANSEG2") //black

	--Ammo bar
	local pos = 0
	while (pos < maxammo) do
		local patch = patch3
		pos = $ + 1


			if pos <= curammo then
				v.draw(barx + pos - 1, bary, patch3, V_SNAPTOTOP|V_SNAPTOLEFT|V_HUDTRANS|V_PERPLAYER)
				if pos > curammo - 1 then
					if (curammo <= 1) then
						//first
						patch = patch1
					else
						//fill
						patch = patch1
					end
				else
					patch = patch1
				end
			end
		//todo: skincolors
		v.draw(barx + pos - 1, bary, patch, V_SNAPTOTOP|V_SNAPTOLEFT|V_HUDTRANS|V_PERPLAYER,v.getColormap(nil,SKINCOLOR_SUPERSILVER5))
	end
end
--[[
local function combobar(v, p)
	if not p
	or not p.valid then
		return
	end
	if not mapheaderinfo[gamemap].mrce_emeraldstage then return end

	local c = p.mrce_escombo
	local combdisp = 8
	local comby = 0
	local endmovedown = 0
	if p.pflags & PF_FINISHED then
		comby = TR
	end

	if c.time then
		v.draw(hudinfo[HUD_RINGS].x-combdisp+5,
			hudinfo[HUD_RINGS].y+14+comby,
			v.cachePatch("COMBBACK"),
			V_SNAPTOLEFT|V_SNAPTOTOP|V_30TRANS|V_PERPLAYER,
			v.getColormap(nil, SKINCOLOR_BLACK)
		)
		v.drawNum(hudinfo[HUD_RINGS].x+16+combdisp,
			hudinfo[HUD_RINGS].y+20+comby,
			c.count,
			V_HUDTRANS|V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER
		)
		v.drawString(hudinfo[HUD_RINGS].x+18+combdisp,
			hudinfo[HUD_RINGS].y+20+comby,
			"Combo!",
			V_HUDTRANS|V_ALLOWLOWERCASE|V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER,
			"thin"
		)
		drawcombotimebar(v,p,combdisp,comby)
		endmovedown = 25

	end

	if c.showrank.tics then
		--local isvery = c.showrank.num/(#ranks*comboup)
		if c.waslost then
			v.drawString(hudinfo[HUD_RINGS].x+4,
				hudinfo[HUD_RINGS].y+20+endmovedown+comby,
				"That combo was",
				V_HUDTRANS|V_ALLOWLOWERCASE|V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER,
				"thin"
			)

			local length = #ranks

			v.drawString(hudinfo[HUD_RINGS].x+34,
				hudinfo[HUD_RINGS].y+20+17+endmovedown+comby,
				ranks[ ((c.showrank.num) % length) ].name,
				V_HUDTRANS|V_ALLOWLOWERCASE|V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER,
				"thin-center"
			)

			--if isvery then
			--	v.drawScaled((hudinfo[HUD_RINGS].x-combdisp+5+7)*FU,
			--		(hudinfo[HUD_RINGS].y+14+15+endmovedown+comby)*FU,
			--		FU/3,
			--		v.cachePatch("COMBVERY"),
			--		V_SNAPTOLEFT|V_SNAPTOTOP|V_HUDTRANS|V_PERPLAYER
			--	)
			--end

			v.drawString(hudinfo[HUD_RINGS].x+35,
				hudinfo[HUD_RINGS].y+20+34+endmovedown+comby,
				c.showrank.score,
				V_HUDTRANS|V_ALLOWLOWERCASE|V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER,
				"small-center"
			)
		else
			local xdisp = 10
			local ydisp = 15
			local length = #ranks
			v.drawString(hudinfo[HUD_RINGS].x+34+xdisp,
				hudinfo[HUD_RINGS].y+20+17+ydisp,
				ranks[ ((c.showrank.num) % length) ].name,
				V_HUDTRANS|V_ALLOWLOWERCASE|V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER,
				"thin-center"
			)
			if isvery then
				v.drawScaled((hudinfo[HUD_RINGS].x-combdisp+5+7+xdisp)*FRACUNIT,(hudinfo[HUD_RINGS].y+14+15+ydisp)*FRACUNIT,FRACUNIT/3,v.cachePatch("COMBVERY"),V_SNAPTOLEFT|V_SNAPTOTOP|V_HUDTRANS|V_PERPLAYER, v.getColormap(nil, nil))
			end
		end
	end
end

customhud.SetupItem("escombobar", "mrce", combobar, "game", 3)

addHook("MobjDeath",function(mo,inf,sor)
	if not sor
	or not sor.valid then
		return
	end

	if not sor.player
	or not sor.player.valid then
		return
	end

	if sor.player.alreadyhascombometer then
		return
	end

	local p = sor.player

	if mo.flags & MF_MONITOR then
		if mo.type ~= MT_EGGMAN_BOX then
			MRCEGivePlayerESCombo(p, true)
		end
	end

	if ((mo.type == MT_RING)
	or (mo.type == MT_COIN)) then
		MRCEGivePlayerESCombo(p, false)
	end

	if p
	and p.mrce_escombo then
		local c = p.mrce_escombo

		if ((mo.flags & MF_ENEMY) or (mo.flags & MF_BOSS))
		or (SPIKE_LIST[mo.type] == true)
		or (mo.type == MT_PLAYER)
		and (not mo.ragdoll) then
			MRCEGivePlayerESCombo(p,true)
		end

	end
end)]]

addHook("MusicChange", function(om, nm)
	if not mapheaderinfo[gamemap].mrce_emeraldstage then return end
	if nm == "_clear" then
		if emstagerank >= 3 then
			return "chpass", 0, false, 0
		else
			return "chfail", 0, false, 0
		end
	end
end)

addHook("NetVars", function(net)
	mrce.emstage_globalPoints = net($) --don't think this one is necessary, the mrce global table is already netsynced

	multiplier = net($)
	multiplier_timer = net($)
	multiplier_freeze = net($)
	emstagerank = net($)
	dropped = net($)
	lastmap = net($)
	--emevaluated = net($) --this one prob isn't needed either, the variable is only used for S ranks and not used in multiplayer
end)