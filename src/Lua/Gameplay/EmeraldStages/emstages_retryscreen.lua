local lastmap
local debug = 0
local debug_es = 2

--local CV_debugcolor = CV_RegisterVar{
--	name = "MR_DEBUGCOLOR",
--	defaultvalue = 3,
--	PossibleValue = {MIN = 1, MAX = 1035}
--}

addHook("MapChange", function(map)
    if lastmap == nil then
        lastmap = map
    else
        if lastmap ~= map then --if the map we're on is different than the one we were on previously, and a super attempt was surrendered, then make super attempts available again
            lastmap = map
            if not mrce.emstage_attemptavailable then
                mrce.emstage_attemptavailable = true
            end
        end
    end
end)

addHook("PlayerSpawn", function(p)
    p.emstage_readyselect = 0
end)

addHook("PlayerThink", function (p)
    p.emstage_retryscreen = $ or 0
    p.emstage_savelives = $ or 0
    p.emstage_retryscreenbuttonheld = $ or 0
    p.emstage_readyselect = $ or 0
    p.emstage_optionselected = $ or 0
    p.emstage_selectdelay = $ or 0
    if p.emstage_selectdelay > 0 then
        p.emstage_selectdelay = max($ - 1, 0)
    end
    if debug and not (leveltime % 35) then
        print("continues " .. p.continues)
    end
    if not mapheaderinfo[gamemap].mrce_emeraldstage then return end
    if gamecomplete or modeattacking or multiplayer or (mrce.debugmode & debug_es) then return end
    if p.emstage_savelives == 0 and p.lives > 1 then
        p.emstage_savelives = p.lives
        p.lives = 1
    end
    if p.emstage_savelives > 0 and p.lives > 1 then
        p.emstage_savelives = $ + 1
        p.lives = $ - 1
    end
    if p.exiting and p.emstage_savelives > 0 then
        p.lives = p.emstage_savelives
        p.emstage_savelives = 0
    end
    if p.deadtimer and mrce.emstage_attemptavailable and not p.emstage_readyselect then
        if p.continues >= 4 then
            p.emstage_retryscreen = 7
            if p.emstage_optionselected == 0 then
                p.emstage_optionselected = 1
            end
        elseif p.continues > 1 then
            p.emstage_retryscreen = 3
            if p.emstage_optionselected == 0 then
                p.emstage_optionselected = 2
            end
        else
            p.emstage_retryscreen = 1
            p.emstage_optionselected = 3
        end
        if p.emstage_optionselected == 1 then
            if p.continues < 4 or not p.starpostnum then
                p.emstage_optionselected = 2
            else
                if p.cmd.forwardmove > 35 and not p.emstage_selectdelay then
                    p.emstage_optionselected = 3
                    p.emstage_selectdelay = 5
                    S_StartSound(nil, sfx_menu1, p)
                elseif p.cmd.forwardmove < -35 and not p.emstage_selectdelay then
                    p.emstage_optionselected = 2
                    p.emstage_selectdelay = 5
                    S_StartSound(nil, sfx_menu1, p)
                end
                if p.mrce.jump then
                    --print(p.mrce.jump)
                    p.emstage_retryscreenbuttonheld = 1
                    if p.mrce.jump >= 36 then
                        p.emstage_retryscreen = 0
                        p.emstage_readyselect = 1
                        p.lives = $ + 1
                        --p.emstage_savelives = $ - 1
                        p.emstage_retryscreenbuttonheld = 0
                        p.emstage_optionselected = 0
                        p.continues = max($ - 3, 0)
                    end
                end
            end
        elseif p.emstage_optionselected == 2 then
            if p.continues < 1 then
                p.emstage_optionselected = 3
            else
                if p.cmd.forwardmove > 35 and not p.emstage_selectdelay then
                    if p.continues >= 4 and p.starpostnum then
                        p.emstage_optionselected = 1
                        p.emstage_selectdelay = 5
                        S_StartSound(nil, sfx_menu1, p)
                    else
                        p.emstage_optionselected = 3
                        p.emstage_selectdelay = 5
                        S_StartSound(nil, sfx_menu1, p)
                    end
                elseif p.cmd.forwardmove < -35 and not p.emstage_selectdelay then
                    p.emstage_optionselected = 3
                    p.emstage_selectdelay = 5
                    S_StartSound(nil, sfx_menu1, p)
                elseif p.mrce.jump then
                    --print(p.mrce.jump)
                    p.emstage_retryscreenbuttonheld = 1
                    if p.mrce.jump >= 36 then
                        p.emstage_retryscreen = 0
                        p.emstage_readyselect = 1
                        p.lives = $ + 1
                        --p.emstage_savelives = $ - 1
                        p.emstage_retryscreenbuttonheld = 0
                        p.emstage_optionselected = 0
                        p.continues = max($ - 1, 0)
                        mrce.emstage_globalPoints = 0
                        G_SetCustomExitVars(gamemap, 1)
                        G_ExitLevel()
                    end
                end
            end
        else
            if p.cmd.forwardmove > 35 and not p.emstage_selectdelay then
                if p.continues >= 2 then
                    p.emstage_optionselected = 2
                    p.emstage_selectdelay = 5
                    S_StartSound(nil, sfx_menu1, p)
                end
            elseif p.cmd.forwardmove < -35 and not p.emstage_selectdelay then
                if p.continues >= 4 and p.starpostnum then
                    p.emstage_optionselected = 1
                    p.emstage_selectdelay = 5
                    S_StartSound(nil, sfx_menu1, p)
                elseif p.continues >= 2 then
                    p.emstage_optionselected = 2
                    p.emstage_selectdelay = 5
                    S_StartSound(nil, sfx_menu1, p)
                end
            elseif p.mrce.jump >= 41 then
                p.emstage_retryscreen = 0
                p.emstage_readyselect = 1
                p.lives = p.emstage_savelives - 1
                p.emstage_savelives = 0
                p.emstage_retryscreenbuttonheld = 0
                p.emstage_optionselected = 0
                mrce.emstage_globalPoints = 0
                mrce.emstage_attemptavailable = false
            end
        end
    end
    if p.emstage_retryscreen then
        --print(p.deadtimer)
        p.deadtimer = min($, 20)
    end
end)

local sonicanimframeRun = 0

addHook("ThinkFrame", function()
    if not mapheaderinfo[gamemap].mrce_emeraldstage then return end
    if gamecomplete or modeattacking or multiplayer or (mrce.debugmode & debug_es) then return end
    if not (leveltime % 3) then sonicanimframeRun = min($ + 1, 4) end
    if sonicanimframeRun >= 4 then sonicanimframeRun = 0 end
end)

hud.add(function (v, p)
    if not mapheaderinfo[gamemap].mrce_emeraldstage then return end
    if gamecomplete or modeattacking or multiplayer or (mrce.debugmode & debug_es) then return end
    if p.emstage_retryscreen and p.emstage_savelives > 0 then
        v.drawString(160, 60, "Continue?", V_SNAPTOTOP, "center")
        local highcontcost = (p.emstage_retryscreen == 7 and p.starpostnum) and V_SNAPTOLEFT|V_GREENMAP or V_SNAPTOLEFT|V_GRAYMAP|V_TRANSLUCENT
        local highcontcolor = SKINCOLOR_EMERALD
        if p.emstage_retryscreen < 7 or not p.starpostnum then
            highcontcolor = SKINCOLOR_SILVER
        end
        local lowcontcost = p.emstage_retryscreen >= 3 and V_SNAPTORIGHT|V_BLUEMAP or V_SNAPTORIGHT|V_GRAYMAP|V_TRANSLUCENT
        local lowcontcolor = SKINCOLOR_SAPPHIRE
        if p.emstage_retryscreen < 3 then
            lowcontcolor = SKINCOLOR_SILVER
        end
        local widthA = min(180, p.mrce.jump*5)
        local widthB = min(180, p.mrce.jump*5)
        local widthC = min(205, p.mrce.jump*5)
        v.drawFill(60, 68, 180, 20, V_SNAPTOLEFT|95)
        v.drawFill(110, 98, 180, 20, V_SNAPTORIGHT|95)
        v.drawFill(32, 128, 205, 20, V_SNAPTOLEFT|95)
        if p.emstage_optionselected == 1 then
            v.drawFill(45, 73, 10, 10)
            if p.mrce.jump then
                v.drawFill(60, 68, widthA, 20, V_SNAPTOLEFT|SKINCOLOR_GOLDENROD)
            end
        elseif p.emstage_optionselected == 2 then
            v.drawFill(95, 103, 10, 10)
            if p.mrce.jump then
                v.drawFill(110, 98, widthB, 20, V_SNAPTORIGHT|SKINCOLOR_GOLDENROD)
            end
        else
            v.drawFill(18, 92, 10, 10)
            if p.mrce.jump then
                v.drawFill(32, 128, widthC, 20, V_SNAPTOLEFT|SKINCOLOR_GOLDENROD)
            end
        end
        v.drawString(150, 70, "Continue from Starpost", highcontcost, "center")
        --DrawMotdString(v, 100*FRACUNIT, 75*FRACUNIT, FRACUNIT, "Continue from Starpost", "MRCEGFNT", highcontcost, -1, v.getColormap(TC_DEFAULT, highcontcolor)) --errors out bc it magically decides to read skincolors as userdata instead of numbers
        v.drawString(150, 80, "3 Continues", highcontcost, "small-center")
        v.drawString(200, 100, "Retry Super Challenge", lowcontcost, "center")
        --DrawMotdString(v, 200*FRACUNIT, 100*FRACUNIT, FRACUNIT, "Retry Super Challenge", "MRCEGFNT", lowcontcost, -1, v.getColormap(TC_DEFAULT, lowcontcolor))
        v.drawString(200, 110, "1 Continue", lowcontcost, "small-center")
        v.drawString(80, 130, "Surrender Super Challenge", V_REDMAP, "center")
        --DrawMotdString(v, 100*FRACUNIT, 75*FRACUNIT, FRACUNIT, "Continue from Starpost", "MRCEGFNT", highcontcost, -1, v.getColormap(TC_DEFAULT, highcontcolor)) --errors out bc it magically decides to read skincolors as userdata instead of numbers
        v.drawString(80, 140, "1 Life", V_REDMAP, "small-center")
        local sonicrunsprite = v.getSprite2Patch("sonic", SPR2_RUN, false, sonicanimframeRun, 2)
        local sonicfallsprite = v.getSprite2Patch("sonic", SPR2_PAIN, false, 0, 2)
        local sonicdeadsprite = v.getSprite2Patch("sonic", SPR2_DEAD, false, 0, 2)
        if p.emstage_optionselected == 1 then
            v.drawScaled(30*FRACUNIT, 160*FRACUNIT, 7*FRACUNIT/4, sonicrunsprite, V_SNAPTOLEFT|V_FLIP, v.getColormap(TC_DEFAULT, 0, 'TotalGreenTranslation'))
        elseif p.emstage_optionselected == 2 then
            v.drawScaled(30*FRACUNIT, 160*FRACUNIT, 7*FRACUNIT/4, sonicfallsprite, V_SNAPTOLEFT|V_FLIP, v.getColormap(TC_DEFAULT, 0, 'TotalBlueTranslation'))
        else
            v.drawScaled(30*FRACUNIT, 160*FRACUNIT, 7*FRACUNIT/4, sonicdeadsprite, V_SNAPTOLEFT|V_FLIP, v.getColormap(TC_DEFAULT, 0, 'TotalRedTranslation'))
        end
    end
end)