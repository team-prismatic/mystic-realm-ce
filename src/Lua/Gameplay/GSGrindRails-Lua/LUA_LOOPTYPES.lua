--╔════════════════════════════╗═════════════════════════════════════════════════════════════════════════════════════════════════╗
--║   	   LOOP TYPES   	   ║ Here I set up various looptypes with different behaviour. You may add your own the same way
--║   	(and WALLJUMPS)		   ║ as you'd make Railtypes, though these are very specific advanced niche cases, not as necessary.
--╚════════════════════════════╝═════════════════════════════════════════════════════════════════════════════════════════════════╝
local RAIL = GS_RAILS if RAIL.blockload return end

if GS_LOOP_TYPES==nil rawset(_G,"GS_LOOP_TYPES",{}) end 
local LOOPTYPE = GS_LOOP_TYPES

--DEFAULT LOOP! Loop Type 1 is also used to refer to the default stats. Anything you don't fill in uses these stats.
--This is a standard loop where you go into a rolling animation, which is one of few sprites that look good from all angles.
LOOPTYPE[1] = {
name = "loop",
jump = 1, --If 0, you cannot jump on this loop.
sidehop = true, --Can you sidehop from this rail? If jump is 0, you cannot sidehop regardless.
walljump = 0, --If not 0, this is specifically a walljump "rail". FRACUNIT gives standard walljump distance.

startSFX = sfx_zoom, --The sound the player plays when they first get on this rail. sfx_none would be no sound.

--Start spinning after the rail naturally ends? True makes you spin automatically if near the ground. false takes it away always.
--"keep" retains a spinstate if you had it prior to grinding, but also makes the spin button start a spin.
--"force" makes you forcibly spin even without CA2_SPINDASH.
--"always" make you enter a spin even if the grinding stops in midair, even without CA2_SPINDASH.
endwithspin = true,

speedneeded = 22, --You'll eventually fall off the rail if going too slow upsidedown! If 0, you can't fall off at all! 
failSFX = sfx_s1c0, --The sound used when you fall off from going too slow.
failstate = 0, --The state you enter when you fall from going too slow. 0 is automatic.
extendedfail = false, --If true, you'll enter the same failstate as when you've lose balance during a focus grind..

--Anim determines spriteslot used. 0 uses the default grinding animation and/or dunce pose.
--"context" uses SPR2_ROLL if you were rolling before grinding, and SPR2_RUN and SPR2_WALK if you weren't.
anim = SPR2_ROLL,
animspeed = 1, --Animation speed if anim isn't 0. 1 is the fastest speed!

spawndust = true, --Players grinding on this spawn dust effects. P_SpawnSkidDust specifically.
speedtrail = 0, --Spawns an Adventure-style spintrail when going past THIS set speed. If 0, doesn't spawn one.
}

--═════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╝

--RAIL LOOP. Intended for loops intended to still keep the player grinding!
LOOPTYPE[2] = {
name = "grindloop",
sidehop = false,
anim = 0,
endwithspin = false,
speedneeded = 0,

startSFX = sfx_none,
spawndust = false, 
}


--CINEMATIC RAIL! Extra cinematic loops that feel alot like the modern ones and are intended for semi-automation.
LOOPTYPE[3] = {
name = "runloop",
sidehop = false,
speedneeded = 22,

anim = "context",
endwithspin = "keep",
startSFX = sfx_none,

speedtrail = 70,
spawndust = true,
}

--WALLJUMP RAIL! Intended specifically for walljumping gimmicks moreso than actual grinding.
LOOPTYPE[4] = {
name = "walljump",
sidehop = false,
walljump = FRACUNIT, --The number represents your walljump strength.
speedneeded = 0,

startSFX = sfx_s1bb,
anim = "context",
endwithspin = false,
extendedfail = true,

speedtrail = 0,
spawndust = true,
}

--This function returns a Loop Type table if one exists
RAIL.LearnLoopType = function(rail)
	if rail and type(rail)=="userdata" and rail.GSLoopType
		if LOOPTYPE[rail.GSLoopType]
			return LOOPTYPE[rail.GSLoopType]
		end
	end
end