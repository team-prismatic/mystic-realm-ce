--GS_RAILS_V is the version number. It increases with every public release, so newer versions may override older versions.
if GS_RAILS_V==nil rawset(_G,"GS_RAILS_V",{}) end
local VERSION = GS_RAILS_V
VERSION.version = 0024 --This is the value that checks if we should be ignored or overwrite the old functions.
VERSION.string = "\x85"+"SAT"+"\x87"+"v0.9."

/*
╔══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╗
║ 			 Grind Rails are made by Golden Shine!(GS, duh) The day for rail grinding in SRB2 has finally arrived!   		   ║
║══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════║
║												[DOCUMENTATION LINK]														   ║
║			If you're here looking on how to USE rails, I've made much more comprehensible documentation here:				   ║
║												  bit.ly/GSraildocs						  									   ║
╚══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╝
												    ╔═══════════════╗
╚═══════════════════════════════════════════════════║ EXTRA CREDITS ║══════════════════════════════════════════════════════════╝
												    ╚═══════════════╝
Shoutout to Metalwario64 for contributing fancy grind spark sprites and SA2's Final Rush ramp light sprites!

Shoutouts to Metalwario64, Blueblur, MrBoingBD, Limo(limolimonagrio), TheLastKitchenGun, Smol6606, Mayo, SuperPhanto, Furless,
and OrdoMandalore for making/ripping sprites for extra railtypes! Specifics on who did what is seen in LUA_RAILTYPES!
If nothing is mentioned, they were probably just made/ripped by Golden Shine.

Shoutouts to everyone who helped with beta testing and gave feedback, encouragement, or was just cool in general!

╔════════════════════════════╗═════════════════════════════════════════════════════════════════════════════════════════════════╗
║   OBJECT FREESLOT&CONFIG   ║ Here I set up all the freeslots, states, sounds, sprite2 animations, and associated properties.
╚════════════════════════════╝═════════════════════════════════════════════════════════════════════════════════════════════════╝*/
if GS_RAILS and GS_RAILS.version and (GS_RAILS.version < VERSION.version) 
	print("\x85"+"Extra instance of GS Grind Rails blocked.\x80 [v"+VERSION.version+"]") return --Don't load!
end

--A function that checks if the freeslot entry exist BEFORE adding it. 
local function SingleFreeSlot(...) for _,slot in ipairs({...}) if not rawget(_G, slot) freeslot(slot) end end end

//										╔═════════════════════════════════════╗
//										║ FREESLOTS! (plus their definitions) ║
//										╚═════════════════════════════════════╝
SingleFreeSlot(
"MT_GSRAIL", --The rail object itself.
"MT_GSRAILDIRECTOR", --An invisible tracker object that helps set rails up, destroys them, or toggles invisibility.
"MT_GSRAILSEG", --One of the rail's connecting sprite segments. Also acts as hitbox that refers back to the main MT_GSRAIL.
"MT_GSRAILATTACHEDTHING", --A thing attached to the rail with the Ambush flag, taking over its collission.
"MT_GSRAILGFX", --Effects object with modified CapeChase and autoframe functionality.
"MT_GSRAILLIGHT", --Sonic Riders-style light. Idea recommended by Lach for better visibility.
"MT_GSRAILDYE", --Place over your rails to dye it with the map editor.
"MT_GSRAILPOLE" --Place over your rails to dye it with the map editor.
)

SingleFreeSlot(
"SFX_GSRAL1", --Start rail
"SFX_GSRAL2", --Start rail (fast)
"SFX_GSRAL3", --Grinding 
"SFX_GSRAL4", --Grinding (fast)
"SFX_GSRAL5", --Metal Rail Landing
"SFX_GSRAL6", --Landing on vine rail
"SFX_GSRAL7", --Vine grinding sound
"SFX_GSRAL8", --Hang Rail sliding sound
"SFX_GSRAL9", --Rail Switch jump

"SFX_GSRALA", --Grinding (Modern) .Sonic 2006 is the game that started with these, but I'll call them Modern for clarity.
"SFX_GSRALB", --Grinding (Fast Modern)
"SFX_GSRALC", --Landing on Rail (Modern)
"SFX_GSRALD", --Start Rail (Modern)

"SFX_GSRALE", --Grinding (Wind)
"SFX_GSRALF", --Landing on Rail (Wind)
"SFX_GSRALG", --Landing on Rail (Heroes)

"SFX_GSRALH", --Grinding (Sonic Advance 3)
"SFX_GSRALI", --Grinding Start (Sonic Advance 3)
"SFX_GSRALJ", --Landing on Rail (Sonic Advance 3)

"SFX_GSRALK", --Grinding (Sonic Rush Adventure)
"SFX_GSRALL", --Fountain Grinding Start (Sonic Rush Adventure)

"SFX_GSRALM", --Grinding (Mario Bros Wonder)
"SFX_GSRALN", --Grinding Start (Mario Bros Wonder)
"SFX_GSRALO" --Landing on Rail (Mario Bros Wonder)
)

SingleFreeSlot(
"S_GSRAIL", --Main state for rails and rail segments once initialized.
"S_GSRAILSEG", --Initial invisible state rails spawn in.
"S_GSRAILDESTROYED", --Deathstate for the rail. Used as safeguard state, since it's not supposed to "die".

"S_PLAY_GSGRINDING", --The player's state for grinding rails. Defaults to rolling if a character has no grind frames
"S_PLAY_GSHANGRAIL", --The player's state for being hangrails. Virtually identical to S_PLAY_RIDE.
"S_PLAY_GSCUSTOMGRIND", --A pointless state to prevent SPR2_ issues.

"S_GSRAILLIGHT",
"S_GSRAILPOLE",


"SPR2_GRND", --Player's grinding sprite freeslot
"SPR_GSRL", --Grindrail's object sprite
"SPR_GSR7", --EXTRA slot for custom rail sprites.
"SPR_GSR8", --second EXTRA slot for custom rail sprites.

"SPR_GSR2", --Misc grinding GFX not related to the rail object itself.
"SPR_GSR3", --Lights and Ultimate Zonebuilder mapping sprites.

"SPR_GSR0", --Animated Rails
"SPR_GSR1", --Animated Rails 2
"SPR_GSR4", --Animated Rails 3

"SKINCOLOR_GRINDSPARK" --Because SANDY kinda sucks.
)


skincolors[SKINCOLOR_GRINDSPARK] = {
name = "GrindSpark",
ramp = {83,64,65,66,68,68,69,70,71,234,236,237,238,239,30,31},
invcolor = SKINCOLOR_BLUE,
invshade = 7,
chatcolor = V_YELLOWMAP,
accessible = false
}

//				╔═══════════════════════════════════════════════════════════════════════════════════════════════╗
//				║  THING TYPES! The dollar sign stuff are descriptions used for (Ultimate) Zone Builder, btw.   ║
//				╚═══════════════════════════════════════════════════════════════════════════════════════════════╝

mobjinfo[MT_GSRAIL] = {
--$Title Grind Rail
--$Sprite GSR3B0
--$Category GS GrindRails
--$AngleText Rail Number
--$Flags1Text Invisible (Still Grindable)
--$Flags4Text Sliding Hang-style Rail
--$Flags8Text Attach Things Above/Below Rail
--$ParameterText CustomRail	
--$NotAngled
--$Color 14
--$IgnoreRenderStyle

--$arg0 Rail Visibility
--$Arg0ToolTip Skip rendering rail sprites, making the rail invisible but grindable.\nUseful for grinding on level geometry such as midtextures, saving performance, or creating secret rails.\n\nNOTE: Your railtype's sounds & effects still properly apply, even if the rail sprites themselves are not rendered! \n\nSpawned Disabled: Your rail will not exist until enabled in-game with RAILEDIT, called via linedef action 443.\n Once enabled, it becomes identical to the `Auto-Rendered` option.
--$Arg0Type 11
--$Arg0Enum {0="Auto-Rendered"; 1="Invisible"; 2="Spawn Disabled";}

--$arg1 Rail Interactions
--$Arg1Type 26
--$Arg1Enum {0="Grind Rail (Acts like Hangrail in inverted gravity)"; 1="Hangable Rail (Acts like Grind Rail in inverted gravity)";}
--$Arg1Flags {65536="Players cannot interact with rail.";}
--$Arg1ToolTip Here you can select whether your rail is a grindrail, or a hangrail like in SA2's Crazy Gadget.\n\nNOTE: When gravity is upside down, hangrails will act like regular rails and vice versa!

--$arg2 Attach Things Above/Below
--$Arg2Type 11
--$Arg2Enum {0="Attach nothing"; 1="Attach stuff lined up in front of rail"; 2="Attach stuff all around rail object (jank)";}
--$Arg2ToolTip This lets your rail automatically attach nearby objects in front onto it, like rings, springs, monitors, or boosters.\nThe rail looks for all objects in front of it, then auto-alligns them vertically. Vertical distance doesn't matter.\n\nNOTE: Any objects that have either their "pitch" or "roll" value set to 7 will NOT be attached! Useful if you don't want certain objects attached.\n\nIMPORTANT: This flag is REQUIRED for many objects to be interactable whilst grinding.\nSo ALWAYS attach springs, boosters, monitors, etc!

--$arg3 Rail Number
--$Arg3ToolTip This becomes your Rail Number. The rail will look for any rail with the next number in sequence, and connect to it if it exists!\n\nIf nothing is entered here, the rail's "angle" field value will be used as the Rail Number instead. If you make a PERFECTLY vertical rail and also used this field, then the rail's angle field becomes the "grind angle".\nOtherwise, both the angle field and this one are indentical in function, though this field takes priority if both are used.

--$arg4 Jump Settings
--$Arg4Type 26
--$Arg4Enum {1="SA2-style jumpramp"; 2="Frontiers-style jumps (No airdrag)"; 4="SA2-style jumpramp (Inverted speed check)";}
--$Arg4Flags {65536="Disable Normal Jumps"; 131072="Disable SideHops"; 262144="Disable Emergency Dislodge (Custom3)"; 524288="Jumping flips player's gravity"; 1048576="Super JumpHeight (+Yellow Spring strength)"; 2097152="Mega Jumpheight (+Red Spring strength)"; 4194304="Disable Hangdrops (SPIN button + jumping on hangrails)"; 8388608="Force Hangdrops"; 16777216="Activate Linedef Trigger upon jump (Uses this rail's tag)";}
--$Arg4ToolTip See the button on the right? \nYeah, click that to alter various jump settings!\n\nSA2-style jumpramps make all players with enough speed jump off with a stylish trick, gaining extra height.\nFrontiers jumps will cut through all airdrag, letting you leap ridiculously far if you've got enough speed.

--$arg5 NUMBER=Rail Type
--$Arg5Type 26
--$Arg5Enum {0="Default rail"; 1="Green Forest vine"; 2="Final Rush rail"; 3="City Escape handrail"; 4="City Escape rail(no poles)";5="Sky Canyon rail";6="Crisis City rail";7="Metal Harbor railing";8="Pyramid Cave arrowed rails";9="Kingdom Valley windrail";10="Kingdom Valley rail";11="Apotos rail";12="Apotos Wooden fence";13="Savannah Citadel rail";14="Pirate Island waterspout (Rush Adventure)";15="Tropical Resort rail";16="Asteroid Coaster rail";17="Sweet Mountain rail";18="Camelot Castle ropes";19="Molten Mines minecart rail";20="Rail Canyon trainrail";21="Egg Fleet rail";22="Frog Forest vine (thicc)";23="Lost Jungle vine";24="Mirage Road hieroglyph slide";25="Tropical Resort Colorblock rail (DS)";26="Chaos Angel rail";27="Cyberspace rail (Advance 3)";28="Planet Wisp rail";29="Aquarium Park Rail";30="Ice Mountain frozen rail";31="Cyberspace Sky Sanctuary rail";32="Kronos Island Overworld rail";33="Ares Island girder rails";34="Kodaijin wooden tribal rails";35="Altitude Limit Rail(Sonic Rush)";36="Check bit.ly/GSrailtypes or the tooltip (Hover mouse over NUMBER=Rail Type:) for more!";}
--$Arg5ToolTip The number input here changes your rail's appearance, grind sounds, effects, and more!\n\nRailtypes beyond 35:\n_______________________________________________\n\n[36] DEAD LINE rails from Sonic Rush		[37] HUGE CRISIS rails from Sonic Rush		[38] LEAF STORM vines from Sonic Rush\n[39] NIGHT CARNIVAL rails from Sonic Rush		[40] WATER PALACE slides from Sonic Rush		[41] Wind Rails from Sonic Riders\n[42] Loop rails (Quiet, effectless, should be invisible)	[43] GFZ block rail from SRB2			[44] Laser Rails from Sonic Heroes\n[45] Cherry Hill rail from Q6			[46] Viridian Steppe rails from Q6			[47] Gradient Facility rail from Q6\n[48] Aqua Horizons rails from Q6			[49] Fallen City rail from Q6			[50] Jazzy Casino rail from Q6\n[51] Rooftop Run rail from Sonic Unleashed		[52] PLANT KINGDOM vines from Rush Adventure	[53] MACHINE LABYRINTH rail from Rush Adventure\n[54] CORAL CAVE crystal from Rush Adventure	[55] HAUNTED SHIP ropes from Rush Adventure	[56] BLIZZARD PEAK icerail from Rush Adventure\n[57] SKY BABYLON leaves from Rush Adventure	[58] PIRATE ISLAND rails from Rush Adventure	[59] Apotos rail (Unleashed Wii version)\n[60] HIDDEN ISLAND tanglevine from Rush Adventure	[61] Gooey sludge rail				[62]Spring rail\n[63]Conveyorbelt rail				[64]Electric Lightningbolt				[65]Lava rail\n[66]Seaside Hill rail (Generations)			[67]Speed Highway rail(Generations)		[68]Tech Light rail\n[69]Traffic railing					[70]Mario Bros Wonder rail				[71]Metropolis rail (Forces)\n[72]Chemical Plant rail (Forces)			[73]Weathered rail (Forces)				[74]Concrete Jungle arrow-rail\n[75]Jungle Joyride fencerail 				[76]Rainbow Rail					[77]Kingdom Hearts Rail\n\nFor a complete list, check: bit.ly/GSrailtypes\n\nIf the railtype number is invalid or left blank, it'll use the default rail.

--$arg6 NUMBER=Connect to Pitch
--$Arg6ToolTip This rail will attempt to connect to a rail object with the same pitch as the number entered here.\nUseful for merging multiple rails into one, or creating a rail that circles back into itself.\n\nIMPORTANT: Do NOT have this rail object connect to anything with the default method, or that will take priority!

--$arg7 Grind Settings
--$Arg7Type 26
--$Arg7Enum {0="Momentum Direction"; 1="Force forwards grinding"; 2="Force backwards grinding";}
--$Arg7Flags {65536="Cannot Focus Grind or Twist Drive"; 131072="Lock Player Controls"; 262144="Force Auto Balance"; 524288="Zero Speed (Conveyors still apply) (+SpringJump = 06 jumprope behavior)"; 1048576="Allow Homing Attacks onto Rail's center"; 2097152="Burning Rail (Hurts without Flame/Elemental Shield or Fireflower)"; 4194304="Electric Rail (Hurts without Lightning/Elemental/Attraction Shield)"; 8388608="Activate Linedef Trigger upon grinding (Uses this rail's tag)";}
--$Arg7ToolTip Force players to grind either forwards or backwards, regardless of how they landed on the rail.\nAdditionally, you may disable a player's controls while grinding on this rail. Useful for cinematic grinding.

--$arg8 Speed Modifier
--$Arg8Type 26
--$Arg8ENum {0="No Modifier"; 5="5(Slowest)"; 10="10"; 20="20"; 30="30"; 40="40"; 50="50"; 60="60"; 65="65"; 70="70"; 75="75"; 80="80"; 85="85"; 90="90"; 95="95(Fastest)";}
--$Arg8Flags {16384="OPTION = Conveyor Speed"; 32768="OPTION = Reverse Conveyor Speed"; 65536="OPTION = Grind Speed %"; 131072="OPTION = Cap Starting Speed";}
--$Arg8ToolTip [Conveyor Speed]: Rail propels the player forwards or backwards depending on the number selected, like in Sonic Lost World.\nNOTE: This does NOT animate rails to look like a conveyor, pick an animated custom railtype for that! \n\n[Grindspeed%]: Modifies how fast players grind on this rail. If this is 50, you'd grind at half speed, for example!\nVery useful for small or large stages.\n\n[Cap Start Speed]: Players can't start grinding with more speed than the entered amount, regardless of their momentum!\nThey CAN still build more momentum afterwards! (For reference, vanilla Sonic's thokspeed is 60)

--$arg9 Loops/Walljumps/Camera
--$Arg9Type 26
--$Arg9Enum {1="Default Loop (Rolling pose. Works great even in 3D without sidecam flags.)"; 2="Grind Loop (Grind pose, cannot fall off. Sidecam flag strongly recommended.)"; 3="Running Loop (ALWAYS enable sideview cam and disable grind effects+sounds! Players may optionally roll.)"; 4="WallJump Rail (Intended for vertically stacked rails)";}
--$Arg9Flags {131072="This rail is upside down (Needed for proper loop behaviour!)"; 262144="Sideview Camera (from right)"; 524288="Sideview Camera (from left)"; 1048576="Disable grind effects and sounds"; 2097152="Cannot collide unless already grinding (Recommended for upsidedown rails)"; 4194304="Front Camera (+Sideview makes it diagional)"; 8388608="Forcibly disable railcam"; 16777216="Railcam pauses on this rail";}	
--$Arg9ToolTip This assigns special behaviour & cameras intended for loops or walljumps.\n\nMake sure to always check the "upsidedown" flag for all rails that should logically be considered upsidedown,\notherwise the loop physics & camera might not make sense.

--$stringarg0 Rail Color
--$StringArg0ToolTip This rail will use the color name you enter here. Such as SKINCOLOR_ORANGE, SKINCOLOR_COBALT, etc.\n\nFor a list of all SRB2's skincolors, check: bit.ly/GSrailcolors\n\nIf you type SKINCOLOR_NONE or leave it blank, the rail will use its assigned default colors.
--$stringarg1 Wildcard
--$StringArg1ToolTip This field is a wildcard for applying advanced niche properties to your rail!\n\nOne function is to force players into specific animations. Typing in SPR2_RUN_ would force a running animation, for example.\nHere's a list of all vanilla SRB2 animations: bit.ly/GSrailsprites \n\nWhat this wildcard field can do may expand in the future.
doomednum = 3888,
spawnstate = S_GSRAIL,
deathstate = S_GSRAILDESTROYED,
spawnhealth = 1,
flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPTHING|MF_NOCLIPHEIGHT|MF_SCENERY|MF_NOBLOCKMAP,
radius = FRACUNIT*48,
height = FRACUNIT*32
}

mobjinfo[MT_GSRAILSEG] = {
spawnstate = S_GSRAILSEG,
deathstate = S_GSRAILDESTROYED,
spawnhealth = 1,
flags = MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPTHING|MF_NOCLIPHEIGHT|MF_SCENERY|MF_NOBLOCKMAP,
radius = FRACUNIT*42,
height = FRACUNIT*32
}

mobjinfo[MT_GSRAILDIRECTOR] = {
spawnstate = S_INVISIBLE,
deathstate = S_INVISIBLE,
radius = 8,
height = 8,
health = 9001,
flags = MF_NOGRAVITY|MF_NOCLIPHEIGHT|MF_NOCLIP|MF_NOCLIPTHING|MF_NOSECTOR|MF_SCENERY --Don't add MF_NOBLOCKMAP!
}

mobjinfo[MT_GSRAILGFX] = {
spawnstate = S_INVISIBLE,
deathstate = S_NULL,
radius = 8,
height = 1,	
flags = MF_NOGRAVITY|MF_NOCLIPHEIGHT|MF_NOCLIP|MF_NOCLIPTHING|MF_NOBLOCKMAP|MF_SCENERY
}

mobjinfo[MT_GSRAILATTACHEDTHING] = {
spawnstate = S_INVISIBLE,
deathstate = S_NULL,
radius = FRACUNIT,
height = FRACUNIT,
flags = MF_NOGRAVITY|MF_NOCLIPHEIGHT|MF_SOLID|MF_NOBLOCKMAP
}

mobjinfo[MT_GSRAILLIGHT] = {
//$Title Rail Light
//$Sprite GSR3A0
//$Category GS GrindRails
//$AngleText Light Color
//$Flags1Text 0.5x Size
//$Flags4Text Attach 8 lights to rail's side
//$Flags8Text 2x Size (4x if 0.5x is also checked!)
//$Color 11
//$ParameterText [>7]Ramp

//$arg0 unused
//$Arg0ToolTip Does nothing as of now.

--$arg1 Attachment Special
--$Arg1ToolTip Attach 8 Lights to Rail: If this light is placed directly on a rail object, 8 lights will be attached to it.\n4 lights on each side, evenly spaced apart dependant on the rail's length.
--$Arg1Type 26
--$Arg1Enum {0="Don't Attach."; 1="Attach 8 Lights to Rail";}
--$Arg4Flags {16384="OPTION = Enable Floorsprite Tilting";}

--$arg2 Light Style
--$Arg2ToolTip Jumpramp Light: Becomes an animated lightblock of arrows, similar to SA2's Final Rush ramp lights.\nCan be very useful to indicate a trickramp, but may be placed anywhere freely.
--$Arg2Type 11
--$Arg2Enum {0="Light Orb"; 1="Animated Ramp Light (Final Rush SA2-style)";}

--$arg3 Rendering Style
--$Arg3Type 11
--$Arg3ToolTip Auto: Picks the most fitting rendering style automatically.\nPaper Sprite: A sprite looks flat when viewed from the side.\nAdd 90 degrees to the object's angle to get it facing the way you want.\nFloorSprite: The light's sprites are laid down flat for better visibility from above and below.
--$Arg3Enum {0="Auto"; 1="Floor Sprite"; 2="Paper Sprite";}

--$arg4 Angle FloorSprite
--$Arg4Type 26
--$Arg4ENum {0="No Modifier"; 10="10 degree tilt"; 20="20 degree tilt"; 30="30 degree tilt"; 40="40 degree tilt"; 50="50 degree tilt"; 60="60 degree tilt"; 70="70 degree tilt"; 80="80 degree tilt";}
--$Arg4Flags {16384="OPTION = Enable Floorsprite Tilting";}
--$Arg4ToolTip If your light is a floorsprite, you can angle the light with this menu.\n80 degrees is basically vertical, 10 degrees is basically flat down.\n\nNOTE: If your light is not a floorsprite, this option will force it to be!

--$stringarg0 Light's Color
--$StringArg0ToolTip This light will use the color name entered here. Such as SKINCOLOR_YELLOW, SKINCOLOR_ROSY, etc.\n\nFor a list of all SRB2's skincolors, check: bit.ly/GSrailcolors\n\nIf you type SKINCOLOR_NONE or leave it blank, it'll use its default colors.	
doomednum = 3889,
spawnstate = S_GSRAILLIGHT,
flags = MF_NOGRAVITY|MF_NOCLIPHEIGHT|MF_NOCLIP|MF_NOCLIPTHING|MF_NOBLOCKMAP|MF_SCENERY,
radius = FRACUNIT*32,
height = FRACUNIT*32
}

mobjinfo[MT_GSRAILDYE] = {
//$Title Rail Color Dye
//$Sprite GSRLC0
//$Category GS GrindRails(Placeholders)
//$AngleText Rail Color
//$Flags1Text Forcibly colorize
//$Flags4Text Sparkling Rail
//$Flags8Text Rainbow Rail
//$Color 19
//$arg0 NUMBER=Rail Color
//$Arg0ToolTip When this rail dye object is placed directly on top of a rail object, it will apply the chosen color number entered here.\n\nIMPORTANT: This is intended to help mappers not using UDMF. It's completely useless for UDMF!
//$arg1 1=Forcibly Colorize
//$Arg1ToolTip When set to 1, and when this rail dye object is placed directly on top of a rail object,\nforces that rail's mo.colorized property to true, which makes every pixel in the sprite recolorable.
//$arg2 1=Sparkling Rail
//$Arg2ToolTip When set to 1, and when this rail dye object is placed directly on top of a rail object,\nadds a continuous sparkle effect to that rail.
//$arg3 1=Rainbow Rail
//$Arg3ToolTip When set to 1, and when this rail dye object is placed directly on top of a rail object,\nadds a rainbow color cycling effect to that rail.
//$Obsolete
doomednum = 3890,
spawnstate = S_INVISIBLE,
flags = MF_NOGRAVITY|MF_NOCLIPHEIGHT|MF_NOCLIP|MF_NOSECTOR|MF_NOBLOCKMAP|MF_SCENERY,
radius = FRACUNIT*60,
height = FRACUNIT*60
}

mobjinfo[MT_GSRAILPOLE] = {
//$Title Rail Pole
//$Sprite GSRLA0
//$Category GS GrindRails(Placeholders)
//$AngleText Pole color
//$Flags2Text From ceiling
//$Flags4Text Translucent
//$Flags8Text Solid (Why, tho')
//$Color 10
//$arg0 NUMBER=Force Pole Type
//$Arg0ToolTip Force the pole to use the sprite of the railtype number you enter here. \nIf left at 0, pole automatically match the rail's sprite they allign with,\nor use City Escape pole sprites if there's no rail to allign with.\n\nThe numbers you enter match the railtypes exactly. So 1 is a Green Forest vine sprite, 18 is a Camelot Castle rope, etc.\nNOTE: Poles CANNOT animate or use a rail's special effects (such as windrails)! Not all types may work equally well!
//$arg1 1=From Ceiling
//$Arg1ToolTip The pole will come down from the ceiling instead, extending down until it alligns with a rail or hits the floor.
//$arg2 [0~9]=Translucent
//$Arg2ToolTip Sets translucency onto the pole. 9 is most translucent, 1 is barely translucent.
//$arg3 1=Solid
//$Arg3ToolTip Makes the pole solid and stop players in their tracks.
//$stringarg0 Rail Color
//$StringArg0ToolTip This pole will use the color name entered here. Such as SKINCOLOR_LAVENDER, SKINCOLOR_GREY, etc.\n\nFor a list of all SRB2's skincolors, check: bit.ly/GSrailcolors\n\nIf you type SKINCOLOR_NONE or leave it blank, the pole will auto-match the rail's color it aligns with.
//$Obsolete
//$GZDB_SKIP
doomednum = 3891,
spawnstate = S_GSRAILPOLE,
flags = MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT,
radius = FRACUNIT,
height = FRACUNIT*120
}

//						╔═════════════════════════════════════════════════════════════════╗
//						║  STATES for the rails, aiding objects, and player grind states. ║
//						╚═════════════════════════════════════════════════════════════════╝

states[S_GSRAIL] = {
	sprite = SPR_GSRL,
	frame = A,
	duration = -1,
	nextstate = S_GSRAIL
}
states[S_GSRAILSEG] = {
	sprite = SPR_NULL,
	frame = A,
	duration = 35,
	nextstate = S_NULL
}
states[S_GSRAILDESTROYED] = {
	sprite = SPR_GSRL,
	frame = A,
	duration = 1,
	nextstate = S_GSRAIL
}
states[S_GSRAILLIGHT] = {
	sprite = SPR_GSR3,
	frame = A,
	duration = -1,
}
states[S_GSRAILPOLE] = {
	sprite = SPR_GSRC,
	frame = A,
	duration = -1,
}

states[S_PLAY_GSGRINDING] = {
	sprite = SPR_PLAY,
	frame = SPR2_GRND,
	var1 = 0,
	var2 = 0,
	duration = 3,
	nextstate = S_PLAY_GSGRINDING
}

states[S_PLAY_GSCUSTOMGRIND] = {
	sprite = SPR_PLAY,
	frame = SPR2_GRND,
	var1 = 0,
	var2 = 0,
	duration = 3,
	nextstate = S_PLAY_GSGRINDING
}

states[S_PLAY_GSHANGRAIL] = {
	sprite = SPR_PLAY,
	frame = SPR2_RIDE,
	var1 = 0,
	var2 = 0,
	duration = 3,
	nextstate = S_PLAY_GSHANGRAIL
}
							
//			╔════════════════════════════════════════════════════════════════════════════════════════════╗
//			║   Sound flags and captions. This is a nice way to remember what sound ID means what, tbh 	 ║
//			╚════════════════════════════════════════════════════════════════════════════════════════════╝

sfxinfo[sfx_gsral1].flags = SF_X2AWAYSOUND --Default grind start
sfxinfo[sfx_gsral2].flags = SF_X2AWAYSOUND --Default fast-grind start
sfxinfo[sfx_gsral3].flags = SF_X2AWAYSOUND --Default grinding loop sfx
sfxinfo[sfx_gsral4].flags = SF_X2AWAYSOUND --Default fast-grinding loop sfx
sfxinfo[sfx_gsral5].flags = SF_X4AWAYSOUND --Landing sound is a bit louder.
sfxinfo[sfx_gsral6].flags = SF_X2AWAYSOUND
sfxinfo[sfx_gsral7].flags = SF_X2AWAYSOUND
sfxinfo[sfx_gsral8].flags = SF_X2AWAYSOUND
sfxinfo[sfx_gsral9].flags = SF_X2AWAYSOUND
sfxinfo[sfx_gsralc].flags = SF_X4AWAYSOUND

sfxinfo[sfx_gsrale].flags = SF_X2AWAYSOUND
sfxinfo[sfx_gsralf].flags = SF_X2AWAYSOUND
sfxinfo[sfx_gsralg].flags = SF_X4AWAYSOUND
sfxinfo[sfx_gsralh].flags = SF_X2AWAYSOUND
sfxinfo[sfx_gsrali].flags = SF_X2AWAYSOUND
sfxinfo[sfx_gsralj].flags = SF_X4AWAYSOUND

sfxinfo[sfx_gsralk].flags = SF_X2AWAYSOUND
sfxinfo[sfx_gsrall].flags = SF_X2AWAYSOUND

--Now let's make some captions.
sfxinfo[sfx_gsral1].caption = "Grinding (Start)"
sfxinfo[sfx_gsral2].caption = "Grinding (Faststart!) "
sfxinfo[sfx_gsral3].caption = "Grinding"
sfxinfo[sfx_gsral4].caption = "Grinding (Fast!)"
sfxinfo[sfx_gsral5].caption = "Rail Landing"

--Vine grindrails
sfxinfo[sfx_gsral6].caption = "Rail Landing\x8b (Vine)"+"\x80"
sfxinfo[sfx_gsral7].caption = "Grinding\x8b (Vine)"+"\x80"

--Misc
sfxinfo[sfx_gsral8].caption = "Hangrail sliding" 
sfxinfo[sfx_gsral9].caption = "Rail Switchleap"

--Modern Grindrails
sfxinfo[sfx_gsrala].caption = "Grinding\x8c (M.)"+"\x80" --63 tics
sfxinfo[sfx_gsralb].caption = "Grinding\x8c (M.Start)"+"\x80" --66 tics
sfxinfo[sfx_gsralc].caption = "Rail Landing\x8c (M.)"+"\x80"
sfxinfo[sfx_gsrald].caption = "Grinding\x8c (M.Fast!)"+"\x80" --63 tics.

--Wind "Grindrails"
sfxinfo[sfx_gsrale].caption = "Grinding\x86 (Wind)"+"\x80" --54 tics
sfxinfo[sfx_gsralf].caption = "Rail Landing\x86 (Wind)"+"\x80" --22 tics

--Sonic Heroes grindrails
sfxinfo[sfx_gsralg].caption = "Rail Landing\x88 (Heroes)"+"\x80" --30 tics

--Sonic Advance 3 grindrails. 
sfxinfo[sfx_gsralh].caption = "Grinding\x8a (Advance)"+"\x80" --46 tics
sfxinfo[sfx_gsrali].caption = "Grinding Start\x8a (Advance)"+"\x80" --52 tics
sfxinfo[sfx_gsralj].caption = "Rail Landing\x8a (Advance)"+"\x80" --4 tics

--Sonic Rush Adventure grindrails. 
sfxinfo[sfx_gsralk].caption = "Grinding\x84 (Rush Adventure)"+"\x80" --34 tics
sfxinfo[sfx_gsrall].caption = "Grinding Start\x84 (Rush Adventure)"+"\x80" --35 tics

--Super Mario Bros Wonder grindrails
sfxinfo[sfx_gsralm].caption = "Grinding ("+"\x85"+"Mario\x84 Wonder"+"\x80"+")" --22 tics
sfxinfo[sfx_gsraln].caption = "Grinding Start ("+"\x85"+"Mario\x84 Wonder"+"\x80"+")" --21 tics
sfxinfo[sfx_gsralo].caption = "Rail Landing ("+"\x85"+"Mario\x84 Wonder"+"\x80"+")" --10 tics

spr2defaults[SPR2_GRND] = SPR2_ROLL --Default to rolling if you've no grinding frames.