--╔════════════════════════════╗═════════════════════════════════════════════════════════════════════════════════════════════════╗
--║   	   RAIL TYPES   	   ║  Here I set up various railtypes with different sprites/SFX/GFX, etc. You can add your own too! ║
--╚════════════════════════════╝═════════════════════════════════════════════════════════════════════════════════════════════════║
--║												[DOCUMENTATION LINK]															 ║
--║		  If you're here just looking for which railtype is what number, I have a document with pictures for that here:			 ║
--║					  							 bit.ly/GSrailtypes																 ║
--╚══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╝
local RAIL = GS_RAILS if RAIL.blockload return end

--To add your own rail types, first copy-paste this line below to the top of your script!
if GS_RAILS_TYPES==nil rawset(_G,"GS_RAILS_TYPES",{}) end local RAILTYPE = GS_RAILS_TYPES

--═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╗

--0 is the default rail type! It lists every option for referencing, but you may leave out everything you don't mind staying at default.
--The [X] number you enter will become the type number you want to use in Ultimate Zonebuilder too.
--Custom Rails can only be used beyond type 15 in UDMF, and if a faulty number is selected, it uses the default. 
--Pick a number over 1000 to decrease your odds of potential conflicts!

--PS: Ensure your freeslots are loaded before this code runs! 
--SOC files are normally loaded after Lua, so I'd recommend adding the freeslots in Lua instead of in SOC.

--═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╝

RAILTYPE[0] = {

//Color Options.
defaultcolor = SKINCOLOR_RED, --The color your rail will use if the mapper doesn't select a custom color.
alwayscolorize = 0, --If 1, colorizes rail only when mappers pick a custom color. If it's 2, colorizes rail even in its default color. 
--(NOTE: "Colorizing" being active means EVERYTHING will be recolored instead of just the green parts!)

//Sprites to use, and which frames. These are THE most important options!
TOPsprite = SPR_GSRL, --The sprite freeslot for this rail's floorsprite!
TOPstartframe = A, --If not filled in, defaults to A.
TOPendframe = A, --If the end frame is not filled in and different from the startframe, you can make animated rails!

SIDEsprite = SPR_GSRL, --The side sprite is the rail's paper sprite. It should usually be the same as the TOPsprite.
SIDEstartframe = B, --If not filled in, defaults to B.
SIDEendframe = B,

animspeed = 0, --Animation speed in tics. Defaults to 1 if the rail is animated. Higher values slow down the animation.

//Offsets to use! Important if your rail sprites aren't using standing proportions!
hangZoffset = 0, --When hanging, you can make a player hang higher or lower than usual.
Xoffset = 0, --Is Sonic too far right when grinding? Raise this. Is he too far left? Lower this!

//Rail rendering flags. For people seeking a very specific look!
noTOP = false, --If set to true, your rail can never render TOP floorsprites.
noSIDE = false, --If set to true, your rail can never render SIDE papersprites. You can't have both active.
renderflags = 0, --Extra renderflags to add, such as RF_FULLBRIGHT.
blendmode = 0, --Want a certain blendmode? AST_ADD is the shiny one. I highly recommend also adding translucency if used.
translucent = 0, --Enter FF_TRANS10 - FF_TRANS90 here as translucency values.

//Sound effects! You rarely need to bother with these and can just leave them all out.
landSFX = sfx_gsral5, --Sound when player lands on the rail.
grindstartSFX = sfx_gsral1, --Sound when player first starts grinding.  
grindSFX = sfx_gsral3, --A looping grinding sound.
faststartSFX = sfx_gsral1, --Sound when player first starts grinding with extra speed!
fastgrindSFX = sfx_gsral4, --A grinding sound that plays when going fast. If not filled in, 

SFXinit = 31, --How long initial sound plays before looping SFX kicks in. (35 = 1 second)
fastSFXinit = 19, --How long initial fast sound plays before looping SFX kicks in. (35 = 1 second)
SFXtimer = 71, --Timer in tics used for the grinding SFX before loops. (35 = 1 second)
fastSFXtimer = 26, --Timer in tics used for the fast grinding SFX before loops. (35 = 1 second)

nohangrailSFX = false, --If set to true, use grinding SFX when hanging on the rail as well.
nofastSFX = false, --If set to true, the rail will have no fast grinding SFX. 
nostackSFX = false, --if set to true, the fast grinding SFX doesn't stack with the regular SFX and instead overrides it

//Rail effects that constantly spawn from the rail. These are harsh on performance, don't add them nilly willy.
windrail = SKINCOLOR_NONE, --If set to any color, rail spawns colored wind around it.
sparklerail = SKINCOLOR_NONE, --Spawns colored sparkles from the rail. Type "rainbow" with the quotes for rainbow sparkles.
electricrail = 0, --If set to 1, spawns electricity.  (Not implemented yet)
firerail = 0, --If set to 1, spawns fire. (Not implemented yet)

//Lastly, customizable grinding GFX! Feel free to leave them nil if you're fine with just sparks!
nosparks = false, --if set to true, disables sparks appearing when grinding.
nolandingspark = false, --If set to true, no spark spawns upon landing on the rail.
sparkscale = 42000, --Determines size of the sparks. (and also dust, if enabled). Defaults to 42000 if not filled in.
sparkscolorized = false, --If set to true, enabled colorized property on some GFX below. Useful if they're not green sprites.

sparkscolor = SKINCOLOR_NONE, --If set to any color, turns sparks AND landing sparks into that color.
spawndust = SKINCOLOR_NONE, --If set to any color, spawns dust effects when grinding using that color.
spawnleaves = SKINCOLOR_NONE, --If set to any color, it'll spawns leaves when grinding using the selected color.
spawnfire = SKINCOLOR_NONE, --If to set to any color, spawns fire when grinding using that color. 
spawnbubbles = SKINCOLOR_NONE, --If to set to any color, spawns bubbles when grinding using that color. 
speedtrail = 0, --When grinding at a specified speed, spawn an adventure-style spintrail?
speedtrailcolor = SKINCOLOR_NONE, --If the rail spawns a speedtrail, force a specific speedtrail color rather than a player color?
}

--==========================================================================================================================--

--Vine rails from SA2's Green Forest. They're the first with unique sounds and effects.
RAILTYPE[1] = {
defaultcolor = SKINCOLOR_GREEN,
alwayscolorize = 1,
hangZoffset = 20,

TOPsprite = SPR_GSRL,
TOPstartframe = C,

SIDEsprite = SPR_GSRL,
SIDEstartframe = D,

landSFX = sfx_gsral6, 
grindstartSFX = sfx_none,
grindSFX = sfx_gsral7,

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

nosparks = true, 
nolandingspark = false,
sparkscale = 24768,
sparkscolorized = true,

sparkscolor = SKINCOLOR_SILVER,
spawndust = SKINCOLOR_BEIGE,
spawnleaves = SKINCOLOR_FOREST,
}


--Final Rush-style rails from SA2. The first animated rail, and always fullbright!
--Credit to Blue Blur for the textures and doing a bit of manual cleanup! 
RAILTYPE[2] = {
defaultcolor = SKINCOLOR_ORANGE,
renderflags = RF_FULLBRIGHT, 
hangZoffset = -30,

TOPsprite = SPR_GSR0,
TOPstartframe = A,
TOPendframe = H,

SIDEsprite = SPR_GSR0,
SIDEstartframe = I,
SIDEendframe = P,
}

--City handrails, similar to SA2's City Escape's. They are thin and the side-sprites connect down to what's meant to be the ground,
--so these can't be randomly placed anywhere without looking strange. Credits to Metalwario64 for handspriting these!
RAILTYPE[3] = {
defaultcolor = SKINCOLOR_RED,
Xoffset = 0,
hangZoffset = 16,

TOPsprite = SPR_GSRL,
TOPstartframe = E,
SIDEsprite = SPR_GSRL,
SIDEstartframe = F,
}

--City Escape handrails without the poles! Useful as standalone rails, or quickly removing the poles where it wouldn't make sense.
--You CAN swap railtype in the same rail sequence! Credits again to Metalwario64 for handspriting these!
RAILTYPE[4] = {
defaultcolor = SKINCOLOR_RED,
Xoffset = 0,
hangZoffset = 16,

TOPsprite = SPR_GSRL,
TOPstartframe = E, --Can simply reuse the top sprite!
SIDEsprite = SPR_GSRL,
SIDEstartframe = G,
}

--Sky Canyon rails from Shadow's SA2 stage. They're tall rails similar to most Modern rails, but with alot more grunge!
RAILTYPE[5] = {
defaultcolor = SKINCOLOR_SANDY,
hangZoffset = -37,

TOPsprite = SPR_GSRL,
TOPstartframe = H,
SIDEsprite = SPR_GSRL,
SIDEstartframe = I,
}

--Crisis City grind rails from Sonic 2006. Jagged, rusty, and with an edge of realism. They spawn fire when grinded on.
RAILTYPE[6] = {
defaultcolor = SKINCOLOR_RUST,
alwayscolorize = 1,
hangZoffset = -6,

TOPsprite = SPR_GSRL,
TOPstartframe = J,
SIDEsprite = SPR_GSRL,
SIDEstartframe = K,

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscolor = SKINCOLOR_VOLCANIC,
sparkscolorized = true,
spawnfire = SKINCOLOR_RED,
}

--Metal Harbor grindrails! Can't be a tube in SRB2, so this'll have to do!
RAILTYPE[7] = {
defaultcolor = SKINCOLOR_GOLDENROD,
Xoffset = -3,
hangZoffset = 20,

TOPsprite = SPR_GSRL,
TOPstartframe = L,
SIDEsprite = SPR_GSRL,
SIDEstartframe = M,
}

--Pyramid Cave grindrails. Thin rails with that arrow motive, aiding your sense of speed.
RAILTYPE[8] = {
defaultcolor = SKINCOLOR_CRIMSON,
hangZoffset = 16,

TOPsprite = SPR_GSRL,
TOPstartframe = N,
SIDEsprite = SPR_GSRL,
SIDEstartframe = O,
}

--Wind-Rails from Sonic 2006. Probably makes sense to give a conveyor effect? Credit to MetalWario64 for these sprites!
RAILTYPE[9] = {
defaultcolor = SKINCOLOR_SILVER,
alwayscolorize = 1,
Xoffset = -1,
hangZoffset = 9,
blendmode = AST_ADD,
translucent = FF_TRANS60, 
renderflags = RF_SEMIBRIGHT,

TOPsprite = SPR_GSR0,
TOPstartframe = Q,
TOPendframe = 27, --1

SIDEsprite = SPR_GSR0,
SIDEstartframe = 28, --2
SIDEendframe = 39, --d

landSFX = sfx_gsralf, 
grindstartSFX = sfx_none,
grindSFX = sfx_gsrale,
faststartSFX = sfx_prloop,

SFXinit = 22, --How long initial sound plays before looping SFX kicks in. (35 = 1 second)
SFXtimer = 54, --Timer in tics used for the grinding SFX before loops. (35 = 1 second)

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

windrail = SKINCOLOR_SILVER,

nosparks = true, 
nolandingspark = true,
sparkscale = 20500,
sparkscolorized = true,
sparkscolor = SKINCOLOR_WHITE,
spawndust = SKINCOLOR_WHITE,
spawnleaves = SKINCOLOR_MINT,
}

--Kingdom Valley grindrails from Sonic 2006. Ancient stone structure rails sure to crumble soon.
RAILTYPE[10] = {
defaultcolor = SKINCOLOR_GREY,
alwayscolorize = 1,
Xoffset = 1,
hangZoffset = 5,

TOPsprite = SPR_GSRL,
TOPstartframe = P,

SIDEsprite = SPR_GSRL,
SIDEstartframe = Q,

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,
}

--Apotos-style rails from Sonic Unleashed. They're huge! Where's even an edge to grind on?! Lmao, at least they're easily visible.
RAILTYPE[11] = {
defaultcolor = SKINCOLOR_SKY,
Xoffset = 6,
hangZoffset = -29,

TOPsprite = SPR_GSRL,
TOPstartframe = R,
SIDEsprite = SPR_GSRL,
SIDEstartframe = S,

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 54000, --Big and over the top for Modern rails.
}

--Apotos-style rails fences from Sonic Unleashed.
RAILTYPE[12] = {
defaultcolor = SKINCOLOR_COBALT,
hangZoffset = -58,
alwayscolorize = 2,

TOPsprite = SPR_GSRL,
TOPstartframe = T,

SIDEsprite = SPR_GSRL,
SIDEstartframe = 42, --g. I made a minor mistake, so this is a bit out of order.

landSFX = sfx_s1bb, 
grindstartSFX = sfx_none,
grindSFX = sfx_gsral7,

nofastSFX = true,
nostackSFX = true,

nosparks = true, 
sparkscale = 16500,
sparkscolorized = true,
sparkscolor = SKINCOLOR_GREY,
spawndust = SKINCOLOR_BROWN,
}


--Savannah Citadel-style rails from Sonic Unleashed. Very colorful big-ass poles that I guess are grindable somehow? Can't tell if they're made of metal or not.
RAILTYPE[13] = {
defaultcolor = SKINCOLOR_GOLD,
Xoffset = 4,
hangZoffset = 1,

TOPsprite = SPR_GSRL,
TOPstartframe = U,
SIDEsprite = SPR_GSRL,
SIDEstartframe = V,

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 54000,
}

--Apotos Grind Rails again, except how they appeared in Sonic Unleashed Wii! Requested by Limo. They do look fairly different, so I think it's worthwhile to include.
--NOTE: Moved to 59 so Binary doesn't have a duplicate rail in its very limited arsenal.
RAILTYPE[59] = {
defaultcolor = SKINCOLOR_SKY,
Xoffset = -1,
hangZoffset = -29,
renderflags = RF_SEMIBRIGHT,

TOPsprite = SPR_GSRL,
TOPstartframe = W,
SIDEsprite = SPR_GSRL,
SIDEstartframe = X,

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 54000,
}

--Tropical Resort Rails from Sonic Colors. Large and shiny neon rails with some fancy decorations on the side. Credits to MrBoingBD for getting these textures and coloring them!
RAILTYPE[15] = {
defaultcolor = SKINCOLOR_COPPER,
Xoffset = 4,
hangZoffset = -20,
renderflags = RF_SEMIBRIGHT,

TOPsprite = SPR_GSRL,
TOPstartframe = Y,
SIDEsprite = SPR_GSRL,
SIDEstartframe = Z,

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 59000,
}

--Asteroid Coaster Rails from Sonic Colors. Looks like there's some radioactive nonsense in those rails. Credits to MrBoingBD for getting these textures and coloring them!
RAILTYPE[16] = {
defaultcolor = SKINCOLOR_EMERALD,
Xoffset = 0,
hangZoffset = 2,
renderflags = RF_SEMIBRIGHT,

TOPsprite = SPR_GSRL,
TOPstartframe = 38, --c
SIDEsprite = SPR_GSRL,
SIDEstartframe = 39, --d

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 59000,
}

--Sweet Mountain Rails from Sonic Colors. Yeah, rub your shoes on them, that'll teach people to not overindulge on candy canes! Credits to MrBoingBD for these!
RAILTYPE[17] = {
defaultcolor = SKINCOLOR_RED,
hangZoffset = -20,

TOPsprite = SPR_GSRL,
TOPstartframe = 40, --e
SIDEsprite = SPR_GSRL,
SIDEstartframe = 41, --f

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 48500,
sparkscolor = SKINCOLOR_VOLCANIC,
spawndust = SKINCOLOR_PEACHY,
spawnfire = SKINCOLOR_GREY,
}

--Camelot Castle's Grindable ropes from Sonic & The Black Knight. These should be useful for a lot of situations!
RAILTYPE[18] = {
defaultcolor = SKINCOLOR_LATTE,
alwayscolorize = 1,
hangZoffset = 7,
Xoffset = 0,

TOPsprite = SPR_GSRL,
TOPstartframe = 26, --0

SIDEsprite = SPR_GSRL,
SIDEstartframe = 27, --1

landSFX = sfx_s1bb,
grindstartSFX = sfx_none,
grindSFX = sfx_gsral7,

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

nosparks = true,
sparkscale = 16500,
sparkscolorized = true,
sparkscolor = SKINCOLOR_ECRU,
spawndust = SKINCOLOR_ECRU,
}

--Molten Mines minecart rails from Sonic & The Black Knight. A clean standard steel, the perfect rail for any situation!
RAILTYPE[19] = {
defaultcolor = SKINCOLOR_CLOUDY,
hangZoffset = 14,
alwayscolorize = 1,

TOPsprite = SPR_GSRL,
TOPstartframe = 28, --2

SIDEsprite = SPR_GSRL,
SIDEstartframe = 29, --3

landSFX = sfx_gsralg,
spawnfire = SKINCOLOR_RED,
}

--Rail Canyon rails from Sonic Heroes. Maybe the first rails to come to mind due to their prominence in that stage! Credit to Metalwario64 for these.
RAILTYPE[20] = {
defaultcolor = SKINCOLOR_CRIMSON,
hangZoffset = 1,
Xoffset = -1,
renderflags = RF_SEMIBRIGHT,

TOPsprite = SPR_GSRL,
TOPstartframe = 30, --4

SIDEsprite = SPR_GSRL,
SIDEstartframe = 31, --5

landSFX = sfx_gsralg
}

--Egg Fleet rails from Sonic Heroes. ...looking at them from afar, have I seen them before? Credits to Metalwario64 for these!
RAILTYPE[21] = {
defaultcolor = SKINCOLOR_YELLOW,
hangZoffset = 1,
Xoffset = 1,

TOPsprite = SPR_GSRL,
TOPstartframe = 32, --6

SIDEsprite = SPR_GSRL,
SIDEstartframe = 33, --7

landSFX = sfx_gsralg,
}

--Frog Forest vines from Sonic Heroes. Cartoony massive vines that can grow instantly from the giant frog's magical rain!
RAILTYPE[22] = {
defaultcolor = SKINCOLOR_GREEN,
hangZoffset = -7,
Xoffset = 13,

TOPsprite = SPR_GSRL,
TOPstartframe = 36, --a

SIDEsprite = SPR_GSRL,
SIDEstartframe = 37, --b

landSFX = sfx_gsral6, 
grindstartSFX = sfx_none,
grindSFX = sfx_gsral7,
faststartSFX = sfx_none,
fastgrindSFX = sfx_none,

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

nosparks = true, 
nolandingspark = true,
spawndust = SKINCOLOR_PEAR,
spawnleaves = SKINCOLOR_MOSS,
}

--Lost Jungle vines from Sonic Heroes. Sonic Team could've reused Frog Forest's vines, but put in extra effort anyway! Great passion, guys!
RAILTYPE[23] = {
defaultcolor = SKINCOLOR_LATTE,
alwayscolorize = 1,
hangZoffset = 0,
Xoffset = 6,

TOPsprite = SPR_GSRL,
TOPstartframe = 34, --8

SIDEsprite = SPR_GSRL,
SIDEstartframe = 35, --9

landSFX = sfx_gsral6, 
grindstartSFX = sfx_none,
grindSFX = sfx_gsral7,
faststartSFX = sfx_none,
fastgrindSFX = sfx_none,

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

nosparks = true, 
nolandingspark = true,
spawndust = SKINCOLOR_SANDY,
spawnleaves = SKINCOLOR_OLIVE,
}

--Mirage Road roads (but as rails) from Sonic Rush. Desert-themed, yet surprisingly colorful. Credit to MrBoingBD!
RAILTYPE[24] = {
defaultcolor = SKINCOLOR_TOPAZ,
hangZoffset = 5,
Xoffset = 3,
renderflags = RF_SEMIBRIGHT,

TOPsprite = SPR_GSRL,
TOPstartframe = 43, --h

SIDEsprite = SPR_GSRL,
SIDEstartframe = 44, --i

spawndust = SKINCOLOR_BEIGE,
}

--Colorblock rails from Sonic Colors DS. Might double as GFZ block rails too if you specify the right brown color. 
RAILTYPE[25] = {
defaultcolor = SKINCOLOR_MAGENTA,
hangZoffset = 4,
Xoffset = 3,

TOPsprite = SPR_GSRL,
TOPstartframe = 45, --j

SIDEsprite = SPR_GSRL,
SIDEstartframe = 46, --k

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,
}

--Chaos Angel rails from Sonic Advance 3.
RAILTYPE[26] = {
defaultcolor = SKINCOLOR_GOLD,
alwayscolorize = 1,
hangZoffset = 18,
Xoffset = -1,

TOPsprite = SPR_GSRL,
TOPstartframe = 47, --l

SIDEsprite = SPR_GSRL,
SIDEstartframe = 48, --m

nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralj, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

SFXinit = 52,
SFXtimer = 46,
}

--Cyber Space rails from Sonic Advance 3.
RAILTYPE[27] = {
defaultcolor = SKINCOLOR_NEON,
hangZoffset = 6,
Xoffset = -1,
renderflags = RF_SEMIBRIGHT,

TOPsprite = SPR_GSRL,
TOPstartframe = 49, --n

SIDEsprite = SPR_GSRL,
SIDEstartframe = 50, --o

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralj, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

SFXinit = 52,
SFXtimer = 46,
}

--Planet Wisp rails from Sonic Colors. Thanks to MrBoingBD for these!
RAILTYPE[28] = {
defaultcolor = SKINCOLOR_LEMON,
Xoffset = 3,
hangZoffset = -19,
renderflags = RF_SEMIBRIGHT,

TOPsprite = SPR_GSRL,
TOPstartframe = 51, --p
SIDEsprite = SPR_GSRL,
SIDEstartframe = 52, --q

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 58000,
}

--Aquarium Park rails from Sonic Colors. Thanks to MrBoingBD for these!
RAILTYPE[29] = {
defaultcolor = SKINCOLOR_LIME,
Xoffset = 0,
hangZoffset = 1,
renderflags = RF_FULLBRIGHT,

TOPsprite = SPR_GSRL,
TOPstartframe = 53, --r
SIDEsprite = SPR_GSRL,
SIDEstartframe = 54, --s

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 58000,
}

--Ice Mountain rails from Sonic Advance 1. Thanks to Boing for recoloring them!
RAILTYPE[30] = {
defaultcolor = SKINCOLOR_ICY,
hangZoffset = 19,
Xoffset = -2,

TOPsprite = SPR_GSRL,
TOPstartframe = 55, --t

SIDEsprite = SPR_GSRL,
SIDEstartframe = 56, --u

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralj, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

SFXinit = 52,
SFXtimer = 46,

nosparks = true,
sparkscolorized = true,
sparkscolor = SKINCOLOR_GREY,

spawndust = SKINCOLOR_WHITE, 
spawnfire = SKINCOLOR_ICY,
}

--Cyberspace Sky Sanctuary rails from Sonic Frontiers. They have some slight corruption elements.
RAILTYPE[31] = {
defaultcolor = SKINCOLOR_RUBY,
hangZoffset = -5,
Xoffset = 10,

TOPsprite = SPR_GSRL,
TOPstartframe = 57, --v

SIDEsprite = SPR_GSRL,
SIDEstartframe = 58, --w

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 58000,
}

--Generic Overworld rails from Sonic Frontiers' first island amongst others, Kronos Island. Shiny!
RAILTYPE[32] = {
defaultcolor = SKINCOLOR_AQUAMARINE,
hangZoffset = -6,
Xoffset = 11,

TOPsprite = SPR_GSRL,
TOPstartframe = 59, --x

SIDEsprite = SPR_GSRL,
SIDEstartframe = 60, --y

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 58000,
}

--Girder Rails with painted arrows from Sonic Frontiers. Thanks to Metalwario64 for color-correcting them for SRB2!
RAILTYPE[33] = {
defaultcolor = SKINCOLOR_APRICOT,
hangZoffset = -17,
Xoffset = 6,

TOPsprite = SPR_GSR7,
TOPstartframe = A,

SIDEsprite = SPR_GSR7,
SIDEstartframe = B,

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 58000,
}

--Kodaijin wooden tribal rails from Sonic Frontiers. Only the sides recolor!
RAILTYPE[34] = {
defaultcolor = SKINCOLOR_GREEN,
hangZoffset = -10,
Xoffset = 9,

TOPsprite = SPR_GSR7,
TOPstartframe = C,

SIDEsprite = SPR_GSR7,
SIDEstartframe = D,

nolandingspark = true,

landSFX = sfx_s1bb, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsral7,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

spawndust = SKINCOLOR_BEIGE,
}

--Credits to TheLastKitchenGun for giving me Sonic Rush's original side-sprites for these next few rails. The top sprites didn't exist and were custom-made to fit them.
--ALTITUDE LIMIT Rails from Sonic Rush. They have blocky little lights on the side.
RAILTYPE[35] = {
defaultcolor = SKINCOLOR_SUPERSILVER2,
hangZoffset = 19,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = E,

SIDEsprite = SPR_GSR7,
SIDEstartframe = F,

nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralj, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

SFXinit = 52,
SFXtimer = 46,

nosparks = true,
sparkscolorized = true,
sparkscolor = SKINCOLOR_GREY,

spawndust = SKINCOLOR_WHITE, 
spawnfire = SKINCOLOR_AETHER,
}

--DEAD LINE Rails from Sonic Rush. Almost pure black, probably best served on stylized night stages.
RAILTYPE[36] = {
defaultcolor = SKINCOLOR_AZURE,
hangZoffset = 19,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = G,

SIDEsprite = SPR_GSR7,
SIDEstartframe = H,

nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralj, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

SFXinit = 52,
SFXtimer = 46,

sparkscolorized = true,	
sparkscolor = SKINCOLOR_MOONSTONE,
spawnfire = SKINCOLOR_MOONSTONE,
}

--HUGE CRISIS Rails from Sonic Rush. So are those inflatable balloons at the bottom? Might look good for rails on water.
RAILTYPE[37] = {
defaultcolor = SKINCOLOR_RUST,
hangZoffset = 17,
Xoffset = 0,

TOPsprite = SPR_GSR7,
TOPstartframe = I,

SIDEsprite = SPR_GSR7,
SIDEstartframe = J,

nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralg, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

SFXinit = 52,
SFXtimer = 46,
}

--LEAF STORM Rails from Sonic Rush. Very vibrant high-clarity vines, good for easy spotting in first levels!
RAILTYPE[38] = {
defaultcolor = SKINCOLOR_SHAMROCK,
hangZoffset = 20,
Xoffset = -1,

TOPsprite = SPR_GSR7,
TOPstartframe = K,

SIDEsprite = SPR_GSR7,
SIDEstartframe = L,

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralj, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsral7,

nosparks = true, 
nolandingspark = false,
sparkscale = 18768,
sparkscolorized = true,

sparkscolor = SKINCOLOR_GREY,
spawndust = SKINCOLOR_SEAFOAM,
spawnleaves = SKINCOLOR_SHAMROCK,
}

--NIGHT CARNIVAL Rails from Sonic Rush. They're constantly swapping colors, and only the green part is customizable.
RAILTYPE[39] = {
defaultcolor = SKINCOLOR_GREEN,
hangZoffset = 19,
Xoffset = -2,
animspeed = 12,

TOPsprite = SPR_GSR0,
TOPstartframe = 40, --e
TOPendframe = 43, --h

SIDEsprite = SPR_GSR0,
SIDEstartframe = 44, --i
SIDEendframe = 47, --l

nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralj, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

nolandingspark = true,
sparkscolorized = true,
sparkscolor = SKINCOLOR_ORANGE,
spawndust = SKINCOLOR_KETCHUP,
spawnfire = SKINCOLOR_TURQUOISE,
}

--WATER PALACE Rails from Sonic Rush. They're a single colorswap to bubblegum away from being trans-pride rails.
RAILTYPE[40] = {
defaultcolor = SKINCOLOR_ORANGE,
hangZoffset = 19,
Xoffset = -2,

TOPsprite = SPR_GSRL,
TOPstartframe = 61, --z
SIDEsprite = SPR_GSRL, --different sprite for side!
SIDEstartframe = 62, --!

nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralg, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

SFXinit = 52,
SFXtimer = 46,
}

--Sonic Riders style windrails. They're more intense and don't spawn leaves unlike the 06 version. Credits to Metalwario64!
RAILTYPE[41] = {
defaultcolor = SKINCOLOR_SILVER,
alwayscolorize = 1,
Xoffset = 0,
hangZoffset = 8,
blendmode = AST_ADD,
translucent = FF_TRANS30, 
renderflags = RF_FULLBRIGHT|RF_NOCOLORMAPS,

TOPsprite = SPR_GSR1,
TOPstartframe = A,
TOPendframe = L,

SIDEsprite = SPR_GSR1,
SIDEstartframe = M,
SIDEendframe = X,

landSFX = sfx_gsralf,
grindstartSFX = sfx_none,
grindSFX = sfx_gsrale,
faststartSFX = sfx_prloop,

SFXinit = 22, 
SFXtimer = 54,

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

windrail = SKINCOLOR_SILVER,

nosparks = true, 
nolandingspark = false,
sparkscale = 20500,
sparkscolorized = true,
sparkscolor = SKINCOLOR_AETHER,
spawndust = SKINCOLOR_WHITE,
}

--LOOP RAIL. Quiet rail that uses debug sprites, intended for invisible rails used for loops.
RAILTYPE[42] = {
renderflags = RF_FULLBRIGHT|RF_NOCOLORMAPS,

defaultcolor = SKINCOLOR_BROWN,
alwayscolorize = 1,
Xoffset = 8,
hangZoffset = -32,

TOPsprite = SPR_GSR7,
TOPstartframe = O,
SIDEsprite = SPR_GSR7,
SIDEstartframe = P,

landSFX = sfx_none,
grindstartSFX = sfx_none,
grindSFX = sfx_none,
faststartSFX = sfx_none,
fastgrindSFX = sfx_none,

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

nosparks = true, 
nolandingspark = true,
}

--GFZ rail! The actual GFZ rail, without the properties for loops.
RAILTYPE[43] = {
defaultcolor = SKINCOLOR_LATTE,
alwayscolorize = 1,
hangZoffset = 19,
Xoffset = -1,

TOPsprite = SPR_GSR7,
TOPstartframe = M, 
SIDEsprite = SPR_GSR7,
SIDEstartframe = N,

nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralj, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

spawndust = SKINCOLOR_BEIGE,
}

--LASER Rails from Sonic Heroes. Switches in Rail Canyon used to toggle between red and blue versions of these!
RAILTYPE[44] = {
defaultcolor = SKINCOLOR_RUBY,
blendmode = AST_ADD,
translucent = FF_TRANS30, 
renderflags = RF_FULLBRIGHT, 
hangZoffset = -23,
Xoffset = 5,

TOPsprite = SPR_GSR7,
TOPstartframe = Q, 

SIDEsprite = SPR_GSR7,
SIDEstartframe = R,

landSFX = sfx_gsralg, 

spawnfire = SKINCOLOR_ORANGE,
}

--Cherry Hill rails from OrdoMandalore's Quarrelsome Six levelpack (Q6). Special thanks to him for both the sprites and permission to use!
RAILTYPE[45] = {
defaultcolor = SKINCOLOR_FOREST,
hangZoffset = 4,

TOPsprite = SPR_GSR7,
TOPstartframe = S, 

SIDEsprite = SPR_GSR7,
SIDEstartframe = T,
}

--Viridian Steppe rails from Q6. They're basically thin GFZ blocks covered in color-able vines!
RAILTYPE[46] = {
defaultcolor = SKINCOLOR_GREEN,
hangZoffset = 3,

TOPsprite = SPR_GSR7,
TOPstartframe = U,

SIDEsprite = SPR_GSR7,
SIDEstartframe = V,

landSFX = sfx_s1bb, 
grindstartSFX = sfx_none,
grindSFX = sfx_gsral7,

nofastSFX = true,
nostackSFX = true,

nosparks = true, 
sparkscale = 17500,
sparkscolorized = true,
sparkscolor = SKINCOLOR_SILVER,
spawndust = SKINCOLOR_BOULDER,
spawnleaves = SKINCOLOR_GREEN,
}

--Gradient Facility rails from Q6. They have an interesting rock pattern attached underneath, adding some natural grunge.
RAILTYPE[47] = {
defaultcolor = SKINCOLOR_BLUEBELL,
hangZoffset = -3,

TOPsprite = SPR_GSR7,
TOPstartframe = W,

SIDEsprite = SPR_GSR7,
SIDEstartframe = X,

landSFX = sfx_gsralg,
}

--Aqua Horizons rails from Q6. They remind me a bit of Advance 3's Cyberspace rails. Good for a futuristic aquatic base!
RAILTYPE[48] = {
defaultcolor = SKINCOLOR_AQUA,
hangZoffset = 4,

TOPsprite = SPR_GSR7,
TOPstartframe = Y,

SIDEsprite = SPR_GSR7,
SIDEstartframe = Z,

spawnfire = SKINCOLOR_SUPERSKY5,
}

--Fallen City rails from Q6. There's grey concrete attached underneath, so I've made the rail itself recolorable.
RAILTYPE[49] = {
defaultcolor = SKINCOLOR_CLOUDY,
hangZoffset = 3,

TOPsprite = SPR_GSR7,
TOPstartframe = Z+1, --0

SIDEsprite = SPR_GSR7,
SIDEstartframe = Z+2, --1

landSFX = sfx_gsralg,
spawnfire = SKINCOLOR_RED,
}

--Jazzy Casino rails from Q6. Gaudy solid-gold rails that likely bankrupted their manufacturers!
RAILTYPE[50] = {
defaultcolor = SKINCOLOR_GOLDENROD,
hangZoffset = 4,
alwayscolorize = 1,

TOPsprite = SPR_GSR7,
TOPstartframe = Z+3, --2

SIDEsprite = SPR_GSR7,
SIDEstartframe = Z+4, --3

spawndust = SKINCOLOR_GOLD,
spawnfire = SKINCOLOR_GOLDENROD,
}

--Rooftop Run rails from Sonic Unleashed. These are apparently just brighter Kingdom Valley rails. Thanks to Limo for ripping!
RAILTYPE[51] = {
defaultcolor = SKINCOLOR_CLOUDY,
alwayscolorize = 1,
hangZoffset = -6,
Xoffset = 8,

TOPsprite = SPR_GSR7,
TOPstartframe = Z+5, --4

SIDEsprite = SPR_GSR7,
SIDEstartframe = Z+6, --5

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,
sparkscale = 27500,
}

--Credits to Smol6606 for giving rail sprites from Sonic Rush Adventure!
--PLANT KINGDOM vines from Sonic Rush Adventure. They're extremely simple-looking compared to other vinerails.
RAILTYPE[52] = {
defaultcolor = SKINCOLOR_MASTER,
hangZoffset = 19,
Xoffset = 6,

TOPsprite = SPR_GSR7,
TOPstartframe = 32, --6

SIDEsprite = SPR_GSR7,
SIDEstartframe = 33, --7

landSFX = sfx_gsral6, 
grindstartSFX = sfx_none,
grindSFX = sfx_gsral7,

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

nosparks = true, 
nolandingspark = false,
sparkscale = 24768,
sparkscolorized = true,

sparkscolor = SKINCOLOR_SILVER,
spawnleaves = SKINCOLOR_MASTER,
} 

--MACHINE LABYRINTH rails from Sonic Rush Adventure. Super-basic all purpose industrial rails!
RAILTYPE[53] = {
defaultcolor = SKINCOLOR_GOLD,
hangZoffset = 19,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = 34, --8

SIDEsprite = SPR_GSR7,
SIDEstartframe = 35, --9

nofastSFX = true,
nostackSFX = true,

SFXinit = 33,
SFXtimer = 34,

landSFX = sfx_gsrall, 
grindstartSFX = sfx_gsralk,
grindSFX = sfx_gsralk,

spawnfire = SKINCOLOR_GREY,
} 

--CORAL CAVE rails from Sonic Rush Adventure. Shiny bright rails made from crystal formations!
RAILTYPE[54] = {
defaultcolor = SKINCOLOR_MAGENTA,
renderflags = RF_FULLBRIGHT,

hangZoffset = 19,
Xoffset = -4,

TOPsprite = SPR_GSR7,
TOPstartframe = 36, --a

SIDEsprite = SPR_GSR7,
SIDEstartframe = 37, --b

nofastSFX = true,
nostackSFX = true,

SFXinit = 33,
SFXtimer = 34,

landSFX = sfx_gsrall, 
grindstartSFX = sfx_gsralk,
grindSFX = sfx_gsralk,

spawnfire = SKINCOLOR_ROSY,
} 

--HAUNTED SHIP ropes from Sonic Rush Adventure. They're decrepit and don't look very sturdy...
RAILTYPE[55] = {
defaultcolor = SKINCOLOR_LATTE,

hangZoffset = 19,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = 38, --c

SIDEsprite = SPR_GSR7,
SIDEstartframe = 39, --d

landSFX = sfx_s1bb,
grindstartSFX = sfx_none,
grindSFX = sfx_gsral7,

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

nosparks = true,
sparkscale = 16500,
sparkscolorized = true,
sparkscolor = SKINCOLOR_ECRU,
spawndust = SKINCOLOR_ECRU,
}

--BLIZZARD PEAK ice rails from Sonic Rush Adventure. I made these translucent and shiny to stand out from the Sonic Advance ones.
RAILTYPE[56] = {
defaultcolor = SKINCOLOR_ICY,
renderflags = RF_FULLBRIGHT|RF_NOCOLORMAPS,
blendmode = AST_ADD,
translucent = FF_TRANS30,

hangZoffset = 19,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = 40, --e

SIDEsprite = SPR_GSR7,
SIDEstartframe = 41, --f

nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralj, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

SFXinit = 52,
SFXtimer = 46,

nosparks = true,
sparkscolorized = true,
sparkscolor = SKINCOLOR_GREY,

spawndust = SKINCOLOR_BONE, 
spawnfire = SKINCOLOR_AETHER,
}

--SKY BABYLON leaf rails from Sonic Rush Adventure. These are extremely detailed, very interesting to look at!
RAILTYPE[57] = {
defaultcolor = SKINCOLOR_GREEN,

hangZoffset = 19,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = 42, --g

SIDEsprite = SPR_GSR7,
SIDEstartframe = 43, --h

nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralj, 
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

SFXinit = 52,
SFXtimer = 46,

nosparks = true,
sparkscolorized = true,
sparkscolor = SKINCOLOR_GREY,

spawnleaves = SKINCOLOR_GREEN,
spawndust = SKINCOLOR_MOSS,
}

--PIRATE ISLAND rails from Sonic Rush Adventure. Very basic steel rails with a little recolorable light at the bottom.
RAILTYPE[58] = {
defaultcolor = SKINCOLOR_COPPER,

hangZoffset = 19,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = 44, --i

SIDEsprite = SPR_GSR7,
SIDEstartframe = 45, --j

nofastSFX = true,
nostackSFX = true,

landSFX = sfx_gsralg,
grindstartSFX = sfx_gsrali,
grindSFX = sfx_gsralh,

SFXinit = 52,
SFXtimer = 46,

spawnfire = SKINCOLOR_GREY,
}

--PIRATE ISLAND waterspouts from Sonic Rush Adventure. Animated waterfall rails usually shot from a cannon in the game!
--NOTE: Moved down to 14 so binary mappers have a more unique custom rail in their very limited arsenal of 15.
RAILTYPE[14] = {
defaultcolor = SKINCOLOR_ARCTIC,
renderflags = RF_SEMIBRIGHT,
translucent = FF_TRANS20,

hangZoffset = 15,
Xoffset = 3,

TOPsprite = SPR_GSR0,
TOPstartframe = 48, --m
TOPendframe = 51, --p

SIDEsprite = SPR_GSR0,
SIDEstartframe = 52, --q
SIDEendframe = 55, --t

nohangrailSFX = true,

landSFX = sfx_gsrall, 
grindstartSFX = sfx_gsralk,
grindSFX = sfx_gsralk,

SFXinit = 33,
SFXtimer = 34,

nosparks = true,
sparkscolorized = true,
sparkscolor = SKINCOLOR_CORNFLOWER,

spawnbubbles = SKINCOLOR_BLUE,
spawndust = SKINCOLOR_AETHER, 
spawnfire = SKINCOLOR_ARCTIC,
}

--HIDDEN ISLAND tangled brown vines from Rush Adventure. They look like ropes made of plant. No leaves come off when grinding.
RAILTYPE[60] = {
defaultcolor = SKINCOLOR_SEPIA,

hangZoffset = 19,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = 46, --k

SIDEsprite = SPR_GSR7,
SIDEstartframe = 47, --l

landSFX = sfx_s1bb,
grindstartSFX = sfx_none,
grindSFX = sfx_gsral7,

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

nosparks = true,
sparkscale = 16500,
sparkscolorized = true,
sparkscolor = SKINCOLOR_ECRU,
spawndust = SKINCOLOR_ECRU,
}

--Gooey sludge rail. Animated and sludgey, perfect for indicating slow rail modifiers. Courtesy of MrBoingBD!
RAILTYPE[61] = {
defaultcolor = SKINCOLOR_JET,
translucent = FF_TRANS20,
renderflags = RF_SEMIBRIGHT,
animspeed = 1,

hangZoffset = 15,
Xoffset = 1,

TOPsprite = SPR_GSR1,
TOPstartframe = Z, --Top isn't animated!

SIDEsprite = SPR_GSR1,
SIDEstartframe = 26, --0
SIDEendframe = 32, --6

landSFX = sfx_wslap,
grindstartSFX = sfx_lavbub,
faststartSFX = sfx_splash,
grindSFX = sfx_s3k7d,
fastgrindSFX = sfx_ghit,

SFXtimer = 27,
fastSFXtimer = 17,

nohangrailSFX = true,

nosparks = true,
nolandingspark = true,
spawnbubbles = SKINCOLOR_BLUE,
}

--Spring rail, themed after the Wide Springs from Adventure 2. Perfect for indicating a SuperJump/Megajump rail property. Courtesy of MrBoingBD!
RAILTYPE[62] = {
defaultcolor = SKINCOLOR_RED,

hangZoffset = 3,
Xoffset = 7,

TOPsprite = SPR_GSR7,
TOPstartframe = 48, --m

SIDEsprite = SPR_GSR7,
SIDEstartframe = 49, --n
}

--Conveyorbelt rail! Perfect for indicating conveyor properties on your rail.
RAILTYPE[63] = {
defaultcolor = SKINCOLOR_CLOUDY,
animspeed = 1, 

hangZoffset = 1,
Xoffset = 7,

TOPsprite = SPR_GSR0,
TOPstartframe = 56, --u
TOPendframe = 59, --x

SIDEsprite = SPR_GSR0,
SIDEstartframe = 60, --y
SIDEendframe = 63, --@ Very rare that'd we use up literally every frameslot available

landSFX = sfx_gsralo,
grindSFX = sfx_grind,
nostackSFX = true,

spawnfire = SKINCOLOR_GREY,
spawndust = SKINCOLOR_BONE
}

--Electric Rail! Perfect for indicating electric damage properties on your rail. Courtesy of MrBoingBD!
RAILTYPE[64] = {
defaultcolor = SKINCOLOR_BLUEBELL,
animspeed = 1, 
renderflags = RF_FULLBRIGHT|RF_NOCOLORMAPS,
translucent = FF_TRANS10,
blendmode = AST_ADD,

hangZoffset = 16,
Xoffset = 4,

TOPsprite = SPR_GSR4,
TOPstartframe = Q,
TOPendframe = 30, --4

SIDEsprite = SPR_GSR4,
SIDEstartframe = 31, --5
SIDEendframe = 45, --j

landSFX = sfx_beelec,
faststartSFX = sfx_litng4,
grindSFX = sfx_gsralb,
grindstartSFX = sfx_s3k40,
fastgrindSFX = sfx_gsralb,

spawnfire = SKINCOLOR_AQUA,
electricrail = 1,
}

--Lava Rail! Perfect for indicating burning damage properties on your rail. Courtesy of MrBoingBD!
RAILTYPE[65] = {
defaultcolor = SKINCOLOR_FLAME,
animspeed = 1, 
alwayscolorize = 1,
renderflags = RF_FULLBRIGHT,

hangZoffset = -19,
Xoffset = 7,

TOPsprite = SPR_GSR4,
TOPstartframe = 46, --k
TOPendframe = 53, --r

SIDEsprite = SPR_GSR4,
SIDEstartframe = 54, --s
SIDEendframe = 61, --z

landSFX = sfx_fire,
grindstartSFX = sfx_lavbub,
grindSFX = sfx_splash,
faststartSFX = sfx_s3k43,
fastgrindSFX = sfx_splish,

SFXinit = 2,
fastSFXinit = 2,
SFXtimer = 18,
fastSFXtimer = 13,

nohangrailSFX = true,
nosparks = true,

spawnfire = SKINCOLOR_RED,
spawndust = SKINCOLOR_BLACK,
spawnbubbles = SKINCOLOR_FLAME,
firerail = 1, 
}

--Seaside Hill rail from Sonic Generations! They're big grass blocks with fancy arches embedded into the side. Courtesy of MrBoingBD!
RAILTYPE[66] = {
defaultcolor = SKINCOLOR_GARNET,

hangZoffset = -30,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = 50, --o

SIDEsprite = SPR_GSR7,
SIDEstartframe = 51, --p

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,
}

--Speed Highway rails from Sonic Generations! Dark High-tech rails with a glowing top! Courtesy of MrBoingBD!
RAILTYPE[67] = {
defaultcolor = SKINCOLOR_GOLDENROD,
renderflags = RF_SEMIBRIGHT,

hangZoffset = -21,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = 52, --q

SIDEsprite = SPR_GSR7,
SIDEstartframe = 53, --r

grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 59000,
}

--Tech Light rails! Original modern high-tech rails by MrBoingBD with glowing lights on the side.
RAILTYPE[68] = {
defaultcolor = SKINCOLOR_ICY,
renderflags = RF_SEMIBRIGHT,

hangZoffset = 0,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = 54, --s

SIDEsprite = SPR_GSR7,
SIDEstartframe = 55, --t

sparkscale = 50000,
spawnfire = SKINCOLOR_ORANGE,
}

--Traffic railings! Looks like it's from Advance 3's Route 99? Courtesy of Mayo!
RAILTYPE[69] = {
defaultcolor = SKINCOLOR_AZURE,

hangZoffset = -8,
Xoffset = -2,

TOPsprite = SPR_GSR7,
TOPstartframe = 56, --u

SIDEsprite = SPR_GSR7,
SIDEstartframe = 57, --v

landSFX = sfx_gsralg,
}

--Super Mario Bros Wonder rails! Conveyor properties would make sense on these! Courtesy of Furless!
RAILTYPE[70] = {
defaultcolor = SKINCOLOR_ORANGE,
animspeed = 1,

hangZoffset = 14,
Xoffset = 1,

TOPsprite = SPR_GSR1,
TOPstartframe = 33, --7. Note that the top isn't animated at all.

SIDEsprite = SPR_GSR1,
SIDEstartframe = 34, --8
SIDEendframe = 45, --j

landSFX = sfx_gsralo, 
grindstartSFX = sfx_gsraln,
grindSFX = sfx_gsralm,
faststartSFX = sfx_gsraln,
fastgrindSFX = sfx_gsralm,

nohangrailSFX = true,
nofastSFX = true,
nostackSFX = true,

SFXinit = 10, 
fastSFXinit = 10,
SFXtimer = 22, 
fastSFXtimer = 22,

nosparks = true,

spawndust = SKINCOLOR_BONE,
}

--Metropolis rails from Sonic Forces! Animated high-effort rails with cyberpunk-style lights flowing inside! Courtesy of Bypigs2!
RAILTYPE[71] = {
defaultcolor = SKINCOLOR_RED,
animspeed = 2,

hangZoffset = 16,
Xoffset = 0,

TOPsprite = SPR_GSR4,
TOPstartframe = B, 
TOPendframe = P, 

SIDEsprite = SPR_GSR4,
SIDEstartframe = A, --Note that the side isn't animated!

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 59000,
}

--Chemical Plant rails from Sonic Forces! Bright-red! Courtesy of Bypigs2!
RAILTYPE[72] = {
defaultcolor = SKINCOLOR_GREY,

hangZoffset = 8,
Xoffset = 2,

TOPsprite = SPR_GSR7,
TOPstartframe = 58, --w 

SIDEsprite = SPR_GSR7,
SIDEstartframe = 59, --x

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 59000,
}

--Weathered rails from Sonic Forces! They're Chemical Plant rails, but...weathered! This one has the sidelight recolored instead of the rail. Courtesy of Bypigs2!
RAILTYPE[73] = {
defaultcolor = SKINCOLOR_AETHER,

hangZoffset = 8,
Xoffset = 2,

TOPsprite = SPR_GSR7,
TOPstartframe = 60, --y

SIDEsprite = SPR_GSR7,
SIDEstartframe = 61, --z

landSFX = sfx_gsralc, 
grindstartSFX = sfx_gsrald,
grindSFX = sfx_gsrala,
faststartSFX = sfx_gsralb,
fastgrindSFX = sfx_gsralb,

SFXinit = 62,
fastSFXinit = 62,
SFXtimer = 62,
fastSFXtimer = 65,

sparkscale = 59000,
spawndust = SKINCOLOR_BEIGE,
}

--Concrete Jungle rails from Anton Blast. Simple colorable arrow rails, like Pyramid Cave. Courtesy of NotBlackOpsKoopa!
RAILTYPE[74] = {
defaultcolor = SKINCOLOR_ORANGE,

hangZoffset = 11,
Xoffset = 0,

TOPsprite = SPR_GSR7,
TOPstartframe = 62, --!

SIDEsprite = SPR_GSR7,
SIDEstartframe = 63, --@
}

--Jungle Joyride fencerails from Sonic Unleashed. They make for both good fencing AND wooden rails! Courtesy of MrBoingBD!
RAILTYPE[75] = {
defaultcolor = SKINCOLOR_GREEN,

Xoffset = -6,
hangZoffset = -7,

TOPsprite = SPR_GSR4, --Eh, there was no space for another animated rail in this slot anyway, may as well use it up fully.
TOPstartframe = 62, --!

SIDEsprite = SPR_GSR4,
SIDEstartframe = 63, --@

landSFX = sfx_gsralc, 
grindstartSFX = sfx_s1bb,
grindSFX = sfx_gsral7,
faststartSFX = sfx_s1bb,
fastgrindSFX = sfx_gsral7,

nofastSFX = true,
nostackSFX = true,

sparkscale = 9500,
spawndust = SKINCOLOR_BROWN,
}

--Rainbow Rails from uhh...Mario Kart, I guess? Courtesy of Furless!
RAILTYPE[76] = {
defaultcolor = SKINCOLOR_YELLOW,
renderflags = RF_SEMIBRIGHT,
alwayscolorize = 1,

hangZoffset = 11,
Xoffset = 0,

TOPsprite = SPR_GSR8,
TOPstartframe = A,

SIDEsprite = SPR_GSR8,
SIDEstartframe = B, 

landSFX = sfx_turhit,
nofastSFX = true,

spawndust = SKINCOLOR_SUPERGOLD5,
}

--Kingdom Hearts rails from Dream Drop Distance! Courtesy of Furless!
RAILTYPE[77] = {
defaultcolor = SKINCOLOR_FOREST,

hangZoffset = 11,
Xoffset = 0,

TOPsprite = SPR_GSR8,
TOPstartframe = C,

SIDEsprite = SPR_GSR8,
SIDEstartframe = D, 

landSFX = sfx_s3k81, 
grindstartSFX = sfx_none,
grindSFX = sfx_gsrale,
faststartSFX = sfx_prloop,

nofastSFX = true,
nostackSFX = true,

spawndust = SKINCOLOR_SILVER,
}

--══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
--Returns a rail's type. Assumes rail is valid!
RAIL.LearnRailType = function(rail)
	if type(rail.GStype)=="number" and type((RAILTYPE[rail.GStype]))=="table"
		return rail.GStype
	end return 0
end