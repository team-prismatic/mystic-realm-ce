--$GZDB_SKIP
--=======--
local DEBUGSPRITESCALE = 1
local DEBUGTRIPPING = 2
local DEBUGBUP = 4
local DEBUGRELEASIOHELL = 8
local debug = 0
local PEELSPEED = 60*FRACUNIT
local marathondefflags = {
	hits = 0,
	ultimate = false,
	skin = "sonic",
	bot = "none",
	ending = 0,
	episode = 0
}

local function SafeFreeslot(...)
	for _, item in ipairs({...}) do
		if rawget(_G, item) == nil then
			freeslot(item)
		end
	end
end

SafeFreeslot("SPR2_OOF_", "SPR2_SHIT", "S_PLAY_FACEPLANT")
spr2defaults[SPR2_OOF_] = SPR2_SHIT
spr2defaults[SPR2_SHIT] = SPR2_PAIN

states[S_PLAY_FACEPLANT] = {
	sprite = SPR_PLAY,
	frame = SPR2_OOF_|A,
	tics = 80,
	nextstate = S_PLAY_FACEPLANT
}
--despawn ring maces in ultimate mode

addHook("MobjSpawn", function(mobj)
    if not mobj and mobj.valid then return end

    if not ultimatemode then return end

	mobj.state = S_NULL

end, MT_RING)

addHook("MobjSpawn", function(mobj)
    if not mobj and mobj.valid then return end

    if not ultimatemode then return end

	mo.state = S_NULL

end, MT_COIN)

--credit to katsy for this tiny function pulled from reveries. it's so simple I could've written it myself, but eh.
--Makes elemental shield not render its fire when in water. Because fire can't burn in water. Obviously.

mobjinfo[MT_ELEMENTAL_ORB].spawnstate = S_ELEM1
mobjinfo[MT_ELEMENTAL_ORB].seestate = S_ELEMF1

addHook("MobjThinker", function(shield)
	if (shield.target) and shield.target.valid then
		if (shield.target.type == MT_ELEMENTAL_ORB) then
			if shield.target.flameon then
				shield.flags2 = $|MF2_DONTDRAW
			elseif ((shield.target.target.eflags & MFE_UNDERWATER or shield.target.target.eflags & MFE_TOUCHWATER) and not (shield.target.target.eflags & MFE_TOUCHLAVA)) then
				shield.flags2 = $|MF2_DONTDRAW
			elseif (shield.flags2 & MF2_DONTDRAW) then
				shield.flags2 = $ & ~MF2_DONTDRAW
			end
		elseif (shield.target.type == MT_FLAMEAURA_ORB) then
			--print(shield.target.target.player.dashticsleft)
			if shield.target.target.player.dashticsleft and shield.target.target.player.mrce.jump then
				shield.target.flags2 = $|MF2_DONTDRAW
				shield.flags2 = $|MF2_DONTDRAW
			else
				shield.target.flags2 = $ & ~MF2_DONTDRAW
				shield.flags2 = $ & ~MF2_DONTDRAW
			end
		end
	end
end, MT_OVERLAY)

--air bubble thinker
addHook("TouchSpecial", function(mo, toucher)
	if toucher.player
	and MRCE_isHyper(toucher.player) then
		return true -- hyper forms don't need bubbles
	elseif (toucher.player.yusonictable and toucher.player.yusonictable.hypersonic and toucher.player.mo.skin == "adventuresonic") then return true
	else toucher.player.powers[pw_spacetime] = 12 * TICRATE end --midnight freeze's ice water uses space countdown
end, MT_EXTRALARGEBUBBLE)

addHook("MobjSpawn", function(mobj)
	if netgame then
		mobj.fuse = 70
	else
		mobj.fuse = 140
	end
end, MT_CYBRAKDEMON_FLAMEREST)

addHook("MobjFuse", function(mobj)
	P_RemoveMobj(mobj)
end, MT_CYBRAKDEMON_FLAMEREST)

addHook("MobjDeath", function(pmo, imo, smo, dmg)
	if dmg == DMG_FIRE then
		pmo.preburncolor = pmo.color
	end
end, MT_PLAYER)

addHook("PlayerThink", function(p)
	if p.spectator then return end
	local ctime = os.date("*t")
	local x = p.mrce
	if p.mo and p.mo.valid and p.mo.preburncolor and p.playerstate == PST_DEAD then
		p.mo.colorized = true
		p.mo.color = SKINCOLOR_CARBON
		if not (leveltime % 2) and p.mo.z > p.mo.floorz then
			A_BossScream(p.mo, 0, MT_FIREBALLTRAIL)
		end
	end
	if (ctime.month == 4 and ctime.day == 1) or (debug & DEBUGSPRITESCALE) then
		--p.mo.friction = 5*FRACUNIT/4
		p.mo.spriteyscale = FRACUNIT/2
		p.mo.spritexscale = FRACUNIT*3
	end
	p.mrcetripsanity = $ and $ > 0 and $ - 1 or 0
	if ((debug & DEBUGTRIPPING) or (ctime.month == 4 and ctime.day == 1))
	and x.realspeed >= PEELSPEED and P_IsObjectOnGround(p.mo) and P_RandomRange(1, 40) == 9 then
		P_SetObjectMomZ(p.mo, 8*FRACUNIT)
		p.mrcetripping = 1
		p.mrcetripsanity = 4
		p.normalspeed = 0
		p.pflags = $ & ~(PF_SPINNING|PF_JUMPED)
	end
	if p.mrcetripping and p.mrcetripping > 0 then
		--print(p.mrcetripping)
		if not P_IsObjectOnGround(p.mo) then
			if p.mrcetripping == 2 and (p.pflags & PF_JUMPED) then
				p.mrcetripping = nil
				p.normalspeed = skins[p.mo.skin].normalspeed
			elseif p.mrcetripping == 1 then
				p.mo.rollangle = $ + ANG10
				if p.mo.state ~= S_PLAY_PAIN then
					p.mo.state = S_PLAY_PAIN
				end
				p.powers[pw_nocontrol] = 1
			end
		elseif p.mrcetripping == 1 and P_IsObjectOnGround(p.mo) and not p.mrcetripsanity then
			p.skidtime = 0
			p.normalspeed = 0
			p.mo.rollangle = 0
			p.mo.state = S_PLAY_FACEPLANT
			p.pflags = $|PF_STASIS
			p.mrcetripping = 2
		else
			p.normalspeed = 0
			p.skidtime = 0
		end
	end

	p.mrce_delaymarathonspawn = $ and $ - 1 or 0

	if (marathonmode & MA_INGAME) then
		if gamestate == GS_LEVEL and not p.exiting and not p.bot and (gamemap < 97 or gamemap > 100) and not p.mrce_delaymarathonspawn then
			mrce.marathontimer = min($ + 1, 12599999)
		end
	elseif marathonmode and p.mrce_delaymarathonspawn == 2 and gamemap == 99 then
		print("go time")
		mrce.marathontimer = 0
		mrce.marathonflags = marathondefflags
		mrce.marathonflags.skin = skins[p.skin].realname
		print(mrce.marathonflags.skin)
		for plr in players.iterate do
			if not plr.bot then continue end
			mrce.marathonflags.bot = skins[plr.skin].realname
			break
		end
		print(mrce.marathonflags.bot)
	end
	if (((mrce.globalServerTime.month == 4 and mrce.globalServerTime.day == 1) or (debug & DEBUGBUP)) and P_RandomRange(1, 50))
	or (((mrce.globalServerTime.month == 4 and mrce.globalServerTime.day == 1) and (P_RandomRange(1,30) == 9))
	or (debug & DEBUGRELEASIOHELL)) then
		p.mo.color = SKINCOLOR_BUP
        p.mo.colorized = true
	end

	if marathonmode and gamemap == 121 and p.exiting and not All7Emeralds(emeralds) then
		mrce.marathonflags.finished = true
	end
end)

local maradefflags = {
	hits = 0,
	ultimate = false,
	skin = "sonic",
	bot = "none",
	ending = 0,
	episode = 0,
	finished = false
}

addHook("GameQuit", function()
	mrce.marathontimer = 0
	mrce.marathonflags = maradefflags
end)

local endings = {
	[0] = "No End?",
	[1] = "Bad End",
	[2] = "Good End",
	[3] = "True End"
}

local function getMarathonEnding()
	if All7Emeralds(emeralds) then
		if mrce.hyperunlocked and mrce.elemshards >= 7 then
			mrce.marathonflags.ending = 3
		else
			mrce.marathonflags.ending = 2
			--print(mrce.hyperunlocked .. " hyper")
			--print(mrce.elemshards .. " shards")
		end
	else
		mrce.marathonflags.ending = 1
	end
	return mrce.marathonflags.ending
end

local function marathontimernumbers(v, p)
	if not marathonmode then return end
	local hourval = "00"
	local minutevalue = "00"
	local secondsvalue = "00"
	local centisevalue = "000"
	local me = mrce.marathontimer

	if G_TicsToHours(me) < 10 then
		minutevalue = '0'..G_TicsToHours(me)
	elseif G_TicsToHours(me) >= 10 then
		minutevalue = G_TicsToHours(me)
	end

	if G_TicsToMinutes(me) < 10 then
		minutevalue = '0'..G_TicsToMinutes(me)
	elseif G_TicsToMinutes(me) >= 10 then
		minutevalue = G_TicsToMinutes(me)
	end

	if G_TicsToSeconds(me) < 10 then
		secondsvalue = '0'..G_TicsToSeconds(me)
	elseif G_TicsToSeconds(me) >= 10 then
		secondsvalue = G_TicsToSeconds(me)
	end

	if G_TicsToMilliseconds(me) < 10 then
		centisevalue = '00'.. G_TicsToMilliseconds(me)
	elseif G_TicsToMilliseconds(me) >= 10 and G_TicsToMilliseconds(me) < 100 then
		centisevalue = '0' .. G_TicsToMilliseconds(me)
	elseif G_TicsToMilliseconds(me) >= 100 then
		centisevalue = G_TicsToMilliseconds(me)
	end
	if gamemap == 98 then
		v.drawFill(0, 175, 600, 100, 31|V_SNAPTOLEFT)
	end
	local ultflag = ultimatemode and v.getColormap(TC_RAINBOW, SKINCOLOR_NONE, "FontRedOutline") or mrce.marathonflags.finished and v.getColormap(TC_RAINBOW, SKINCOLOR_NONE, "FontGreenMap") or nil
	--TBSlib.drawTextInt(v, "MRCEHUDFNT", 160, 160, me, V_SNAPTOBOTTOM, nil, "center", -1)
	TBSlib.drawTextInt(v, "MRCEHUDFNT", 165, 177, hourval .. ":" .. minutevalue .. ":" .. secondsvalue .. "." .. centisevalue, V_SNAPTOBOTTOM, ultflag, "center", -1)
	if mrce.marathonflags.finished then
		local skin = (mrce.marathonflags.bot ~= "none") and mrce.marathonflags.skin .. " and " .. mrce.marathonflags.bot or mrce.marathonflags.skin
		local ep = mrce_lockOn and endings[getMarathonEnding()] .. "Lock-on" or endings[getMarathonEnding()]
		TBSlib.drawTextInt(v, "MRCEGFNT", 160, 160, skin, V_SNAPTOBOTTOM, ultflag, "center", -1)
		TBSlib.drawTextInt(v, "MRCEGFNT", 160, 170, ep, V_SNAPTOBOTTOM, ultflag, "center", -1)
	end
end

customhud.SetupItem("marathontimergame", "mrce", marathontimernumbers, "game", 3)
customhud.SetupItem("marathontimerint", "mrce", marathontimernumbers, "intermission", 3)

addHook("PlayerSpawn", function(p)
	if p.spectator then return end
	if p.mo.preburncolor ~= nil then
		p.mo.color = p.mo.preburncolor
		p.mo.preburncolor = nil
	end
	if marathonmode and not p.bot then
		p.mrce_delaymarathonspawn = 3
		if mapheaderinfo[gamemap].actnum == 1 or gamemap == 121 or gamemap == 164 then
			getMarathonEnding()
		end
		if gamemap == 99 then
			mrce.marathonflags = maradefflags
			mrce.marathonflags.ultimate = ultimatemode
			mrce.marathonflags.skin = skins[p.skin].realname
		elseif gamemap == 98 then
			mrce.marathonflags.finished = true
		end
	elseif marathonmode then --still marathon, so we must be a bot
		if gamemap == 99 then
			mrce.marathonflags.bot = skins[p.skin].realname
		end
	end
end)

addHook("ThinkFrame", function()
	local h = CV_FindVar("touch_inputs")
	if gamemap == 99 and (h and h.value ~= nil) then
		G_SetCustomExitVars(101, true)
		G_ExitLevel() --improvised mobile support means no support for episode select
	end
end)

local serverdate = {
	month = 1,
	day = 1,
	year = 0,
	hour = 0,
	min = 0,
	sec = 0
}
local maptimeready = false
local lastupdate = 0

COM_AddCommand("mr_serverinfo", function(s, mon, d, y, h, minute, seconds)
	if s ~= server then
		CONS_Printf(s, "Only the server host can use this")
		return
	end
	if maptimeready then
		serverdate = {
			month = mon,
			day = d,
			year = y,
			hour = h,
			min = minute,
			sec = seconds
		}
		maptimeready = false
	end
end)

addHook("MapChange", function (map)
	maptimeready = true
	local stime = os.date("*t")
	lastupdate = (os.clock()) / 1000
	COM_BufInsertText(server, "mr_serverinfo " .. stime.month .. " " .. stime.day .. " " .. stime.year .. " " .. stime.hour .. " " .. stime.min .. " " .. stime.sec .. " ")
end)

addHook("PreThinkFrame", function()
	if not (leveltime % 35) then
		serverdate.sec = min($ + 1, 60)
		if serverdate.sec == 60 then
			serverdate.sec = 0
			serverdate.min = min($ + 1, 60)
		end
		if serverdate.min == 60 then
			serverdate.min = 0
			serverdate.hour = min($ + 1, 24)
		end
		if serverdate.hour == 24 then
			serverdate.hour = 0
			serverdate.day = min($ + 1, 32)
		end
		if serverdate.day == 32 or (serverdate.day == 31 and (serverdate.month == 4 or serverdate.month == 6 or serverdate.month == 9 or serverdate.month == 11)) or ((serverdate.day == 30 and serverdate.month == 2 and not (serverdate.year % 4)) or (serverdate.day == 29 and serverdate.month == 2)) then
			serverdate.day = 1
			serverdate.month = min($ + 1, 13)
		end
		if serverdate.month == 13 then
			serverdate.month = 1
			serverdate.year = $ + 1
		end
		mrce.globalServerTime = serverdate
		--print(mrce.globalServerTime.month .. " " .. mrce.globalServerTime.day .. " " ..mrce.globalServerTime.sec)
	end
end)

freeslot("SKINCOLOR_BUP")
skincolors[SKINCOLOR_BUP] = {
	name = "BUP",
	ramp = {48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48},
	invcolor = SKINCOLOR_SUNSET,
	invshade = 4,
	chatcolor = V_PURPLEMAP,
	accessible = false
}


local function BupSplash()
    for sector in sectors.iterate do
		if P_RandomChance(FRACUNIT/100) then
			sector.floorpic = "BUP"
			sector.ceilingpic = "BUP"
		end
        for rover in sector.ffloors() do
			if P_RandomChance(FRACUNIT/100) then
				rover.toppic = "BUP"
				rover.bottompic = "BUP"
			end
        end
    end
    for side in sides.iterate do
        if side.toptexture and P_RandomChance(FRACUNIT/100) then
            side.toptexture = R_TextureNumForName("BUP")
        end
        if side.bottomtexture and P_RandomChance(FRACUNIT/100) then
            side.bottomtexture = R_TextureNumForName("BUP")
        end
        if side.midtexture and P_RandomChance(FRACUNIT/100) then
            side.midtexture = R_TextureNumForName("BUP")
        end
    end
    for mo in mobjs.iterate() do
		if P_RandomChance(FRACUNIT/100) then
			mo.color = SKINCOLOR_BUP
			mo.colorized = true
		end
    end
end

local function BupFlood()
    for sector in sectors.iterate do
        sector.floorpic = "BUP"
        sector.ceilingpic = "BUP"
        for rover in sector.ffloors() do
            rover.toppic = "BUP"
            rover.bottompic = "BUP"
        end
    end
    for side in sides.iterate do
        if side.toptexture then
            side.toptexture = R_TextureNumForName("BUP")
        end
        if side.bottomtexture then
            side.bottomtexture = R_TextureNumForName("BUP")
        end
        if side.midtexture then
            side.midtexture = R_TextureNumForName("BUP")
        end
    end
end

addHook("MapLoad", function(mapnum)
	if (mrce.globalServerTime.month == 4 and mrce.globalServerTime.day == 1) or (debug & DEBUGBUP) then
    	BupSplash()
	end
	if (((mrce.globalServerTime.month == 4 and mrce.globalServerTime.day == 1) and (P_RandomRange(1,30) == 9))
	or (debug & DEBUGRELEASIOHELL)) then
		print("hell")
    	BupFlood()
	end
end)

addHook("MobjSpawn", function (mo)
	if (((mrce.globalServerTime.month == 4 and mrce.globalServerTime.day == 1) or (debug & DEBUGBUP)) and P_RandomRange(1, 50))
	or (((mrce.globalServerTime.month == 4 and mrce.globalServerTime.day == 1) and (P_RandomRange(1,30) == 9))
	or (debug & DEBUGRELEASIOHELL)) then
		mo.color = SKINCOLOR_BUP
        mo.colorized = true
	end
end)