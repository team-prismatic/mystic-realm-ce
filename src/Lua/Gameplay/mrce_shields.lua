--some of these shields stink. lets get a little funky with them!

freeslot("MT_METEOR_ELEM_FIREBALL", "S_METEOR_ELEM_FIREBALL", "SPR_MRCE_ELEM_FIREBALL")

mobjinfo[MT_METEOR_ELEM_FIREBALL] = {
	doomednum = -1,
	spawnstate = S_METEOR_ELEM_FIREBALL,
	radius = 1,
	height = 1,
	flags = MF_NOGRAVITY|MF_SCENERY
}

states[S_METEOR_ELEM_FIREBALL] = {
	sprite = SPR_MRCE_ELEM_FIREBALL,
	frame = A|TR_TRANS40|FF_FULLBRIGHT,
	tics = 1,
	nextstate = S_NULL
}

--$GZDB_SKIP

addHook("ShieldSpecial", function(p)
    if not (p and p.mo and p.mo.valid) then return end
    if p.playerstate ~= PST_LIVE then return end
    local x = p.mrce
    --local last = x.realspeed
    if ((p.powers[pw_shield] & SH_NOSTACK) == SH_ELEMENTAL) then
        P_SetObjectMomZ(p.mo, -50*FRACUNIT, false)
        x.elemdive = 1
        p.pflags = $|PF_THOKKED--|PF_SHIELDABILITY
        --P_InstaThrust(p.mo, p.mo.angle, last)
        S_StartSound(p.mo, sfx_s1b3)
        return true
    end
end)

addHook("PlayerThink", function(p)
    if not (p and p.mo and p.mo.valid) then return end
	if p.mo.skin == "skip" then return end
	if (p.charflags & SF_NOSHIELDABILITY) then return end
    local x = p.mrce
    if not ((p.powers[pw_shield] & SH_NOSTACK) == SH_ELEMENTAL) or p.playerstate ~= PST_LIVE then
        x.elemdive = 0
    else
        if x.elemdive and not P_IsObjectOnGround(p.mo) then
            x.elemdive = $ + 1
            if not (leveltime%2) then
                local fireball = P_SpawnMobjFromMobj(p.mo, 0, 0, 0, MT_METEOR_ELEM_FIREBALL)
                fireball.blendmode = AST_ADD
                fireball.colorized = true
                fireball.color = SKINCOLOR_FLAME
            end
        end
    end
    --print(x.elemdive)
    if x.elemdive > 0 and not P_IsObjectOnGround(p.mo) and not (leveltime%2) then
        S_StartSound(p.mo, sfx_s3kcfs)
    end
	if x.elemdive > 0 and ((p.mo.eflags & MFE_GOOWATER) or (p.mo.state >= S_PLAY_SUPER_TRANS1 and p.mo.state <= S_PLAY_SUPER_TRANS6)) then
        S_StopSoundByID(p.mo, sfx_s1b3)
        S_StopSoundByID(p.mo, sfx_s3kcfs)
		x.elemdive = 0
	elseif x.elemdive > 0 and p.mo.momz*P_MobjFlip(p.mo) > 0 then --ig we're going up? cancel the bounce and try again
		x.elemdive = 0
		S_StopSoundByID(p.mo, sfx_s1b3)
		S_StopSoundByID(p.mo, sfx_s3kcfs)
		p.pflags = $ & ~PF_THOKKED
	else
		if x.elemdive > 0 and (P_IsObjectOnGround(p.mo) or (p.mo.eflags & MFE_SPRUNG)) then --brace for impact
			P_StartQuake(20*FU, 8, {p.mo.x, p.mo.y, p.mo.z}, 512*FRACUNIT)
			P_ElementalFire(p, true)
			if x.elemdive > 15 then
				if (p.mo.eflags & MFE_UNDERWATER) and not (p.mo.eflags & MFE_TOUCHLAVA) then
					A_OldRingExplode(p.mo, MT_EXTRALARGEBUBBLE)
					P_RadiusAttack(p.mo, p.mo, 192<<FRACBITS, DMG_WATER, true)
					S_StartSound(p.mo, sfx_s3k57)
				else
					A_OldRingExplode(p.mo, MT_FIREBALLTRAIL)
					P_RadiusAttack(p.mo, p.mo, 192<<FRACBITS, DMG_FIRE, true)
					S_StartSound(p.mo, sfx_s3k6c)
				end
			end
			x.elemdive = 0
			S_StartSound(p.mo, sfx_s3k49)
			S_StopSoundByID(p.mo, sfx_s1b3)
			S_StopSoundByID(p.mo, sfx_s3kcfs)
		end
	end
end)

addHook("MobjThinker", function(orb)
	if (orb.target) and orb.target.valid then
        if orb.target.player.mrce.elemdive > 0 then
            orb.flags2 = $|MF2_DONTDRAW
            orb.tracer.flags2 = $|MF2_DONTDRAW
            orb.meteorflameon = true
            if not (leveltime%2) then
                --print("FUCK")
                A_BossScream(orb.target, 0, MT_FIREBALLTRAIL)
                local take5 = P_SpawnMobjFromMobj(orb.target, 0, 0, 0, MT_THOK)
                --take5.state = S_METEOR_ELEM_FIREBALL
                take5.fuse = 1
                take5.scale = 5*FRACUNIT
            else
                --print("SHIT")
            end
        end
        if orb.meteorflameon and not orb.target.player.mrce.elemdive then
            orb.flags2 = $ & ~MF2_DONTDRAW
            orb.tracer.flags2 = $ & ~MF2_DONTDRAW
			orb.meteorflameon = false
        end
    end
end, MT_ELEMENTAL_ORB)

-- Armageddon Blast, based on Junio Sonic's ability Bullet Dash
-- I wasn't particularly looking forward to figuring this stuff out until I remembered that junio is reusable. Lucky me I get to save some time and effort
-- Sonic Freedom Air Dash
-- Prototype by Tatsuru
-- Finished by MotdSpork
-- modified for Armageddon Shield by Xian

local function GetFlippedZ(mo)
	if P_MobjFlip(mo) == 1 then
		return mo.height/4
	elseif P_MobjFlip(mo) == -1 then
		return -mo.height/4
	end
end

local function SpawnDashAim(mo)
	if mo.player.freeaim == 0 then
		for i = 1, 13 do
			local freedomaim = P_SpawnMobjFromMobj(mo, mo.x+mo.momx, mo.y+mo.momy, mo.z, MT_THOK)
			freedomaim.color = SKINCOLOR_SALMON
			freedomaim.scale = mo.scale/2
			freedomaim.tics = 2
			freedomaim.angle = mo.angle
			freedomaim.momx = mo.momx
			freedomaim.momy = mo.momy
			freedomaim.frame = freedomaim.frame|FF_TRANS70
            freedomaim.blendmode = AST_SUBTRACT

			local dist = 18*i
			local vangle = cos(FixedAngle(mo.player.armarevaiming)*P_MobjFlip(mo))

			local x = mo.x + FixedMul(dist * cos(mo.angle), vangle)
			local y = mo.y + FixedMul(dist * sin(mo.angle), vangle)
			local z = mo.z + GetFlippedZ(mo) + dist * sin(FixedAngle(mo.player.armarevaiming))
			P_SetOrigin(freedomaim, x, y, z)
		end
	else
		for i = 1, 13 do
			local freedomaim = P_SpawnMobjFromMobj(mo, mo.x+mo.momx, mo.y+mo.momy, mo.z, MT_THOK)
			freedomaim.color = SKINCOLOR_SALMON
			freedomaim.scale = mo.scale/2
			freedomaim.tics = 2
			freedomaim.angle = mo.angle
			freedomaim.momx = mo.momx
			freedomaim.momy = mo.momy
			freedomaim.frame = freedomaim.frame|FF_TRANS70
            freedomaim.blendmode = AST_SUBTRACT

			local dist = 7*i
			local vangle = cos(mo.player.aiming)*P_MobjFlip(mo)

			local x = mo.x + FixedMul(dist * cos(mo.angle), vangle)
			local y = mo.y + FixedMul(dist * sin(mo.angle), vangle)
			local z = mo.z + GetFlippedZ(mo) + dist * sin(mo.player.aiming)
			P_SetOrigin(freedomaim, x, y, z)
		end


	end
end

local function ShouldResetFreedomDash(mo)
	if P_IsObjectOnGround(mo)
	or P_PlayerInPain(mo.player)
	or mo.player.powers[pw_carry]
	or mo.player.pflags & PF_STASIS
	or mo.player.pflags & PF_FULLSTASIS
	or mo.player.exiting
	or mo.state == S_PLAY_FALL
	or mo.state == S_PLAY_SPRING then
		return true
	end
end

local function ResetFreedomDash(p)
	p.armausable = true
	p.armacharging = false
	--p.mo.flags = $ & ~MF_NOGRAVITY
	p.armarev = 0
    p.armaboost = 0
	if p.armarevaiming ~= 0 then
		p.armarevaiming = 0
		p.aiming = 0
	end
	if p.resetanalog and p.resetanalog == true then
		p.pflags = $|PF_ANALOGMODE
		p.resetanalog = false
	end
end

addHook("PlayerThink", function(p)
	if not p.mo then return end
	if not ((p.powers[pw_shield] & SH_NOSTACK) == SH_ARMAGEDDON) then return end
	if p.playerstate == PST_DEAD then return end
	if p.mo.skin == "skip" then return end
	if (p.charflags & SF_NOSHIELDABILITY) then return end

	p.armarev = $ or 0
	p.armarevaiming = $ or 0
	p.armaboost = $ or 0
	if p.armarev > 0
	and not ShouldResetFreedomDash(p.mo) then
		if not p.mo.state == S_PLAY_ROLL
		and not (p.mo.state == S_PLAY_FALL)
		and not (p.mo.state == S_PLAY_SPRING) then
			p.mo.state = S_PLAY_ROLL
		end

	end
	if ShouldResetFreedomDash(p.mo) then
		ResetFreedomDash(p)
	end
	if p.armarev and (not (p.cmd.buttons & BT_SPIN) or p.armarev >= (5*TICRATE) / 3) then
		local rev = max(((7 * p.armarev) / 5) + p.armaboost, 50)
        if p.armarev == (5*TICRATE) / 3 then
            rev = (11 * $) / 10
        end
		if p.freeaim == 0 then
			local vangle = cos(FixedAngle(p.armarevaiming))
			if p.mo.eflags & MFE_UNDERWATER then
				p.mo.momx = FixedMul(rev/2 * cos(p.mo.angle), vangle)
				p.mo.momy = FixedMul(rev/2 * sin(p.mo.angle), vangle)
				p.mo.momz = rev*2/5/2 * sin(FixedAngle(p.armarevaiming))
			else
				p.mo.momx = FixedMul(rev * cos(p.mo.angle), vangle)
				p.mo.momy = FixedMul(rev * sin(p.mo.angle), vangle)
				p.mo.momz = rev*2/4 * sin(FixedAngle(p.armarevaiming))
			end
			p.aiming = 0
		else
			local vangle = cos(p.aiming)
			if p.mo.eflags & MFE_UNDERWATER then
				p.mo.momx = FixedMul(rev/2 * cos(p.mo.angle), vangle)
				p.mo.momy = FixedMul(rev/2 * sin(p.mo.angle), vangle)
				p.mo.momz = rev*2/5/2 * sin(p.aiming)
			else
				p.mo.momx = FixedMul(rev * cos(p.mo.angle), vangle)
				p.mo.momy = FixedMul(rev * sin(p.mo.angle), vangle)
				p.mo.momz = rev*2/4 * sin(p.aiming)
			end

		end
		if p.resetanalog and p.resetanalog == true then
			p.pflags = $|PF_ANALOGMODE
			p.resetanalog = false
		end
		S_StartSound(p.mo, sfx_s3k83)
		--p.mo.flags = $ & ~MF_NOGRAVITY
		p.armacharging = false
        if p.armarev >= (5*TICRATE) / 3 then
            P_BlackOw(p)
        end

        if S_SoundPlaying(p.mo, sfx_s3k84) then
            S_StopSoundByID(p.mo, sfx_s3k84)
        end

		p.armarev = 0
		P_SpawnGhostMobj(p.mo)
	end
	if p.armarev and p.mrce.shield == 1 then
		P_BlackOw(p)
		--P_InstaThrust(p.mo, R_PointToAngle2(0, 0, p.rmomx, p.rmomy), 0)
		p.mo.state = S_PLAY_FALL
		p.pflags = $|PF_SHIELDABILITY|PF_THOKKED
		if p.resetanalog and p.resetanalog == true then
			p.pflags = $|PF_ANALOGMODE
			p.resetanalog = false
		end
        if S_SoundPlaying(p.mo, sfx_s3k84) then
            S_StopSoundByID(p.mo, sfx_s3k84)
        end
		p.armarev = 0
	end
end)

addHook("ShieldSpecial", function(p)
	if not p.mo then return end
	if not ((p.powers[pw_shield] & SH_NOSTACK) == SH_ARMAGEDDON) then return end
	if p.mo.skin == "skip" then return end
	if p.pflags & PF_THOKKED
	or p.pflags & PF_SHIELDABILITY then
		return
	end
	if (p.charflags & SF_NOJUMPSPIN) then
		p.mo.state = S_PLAY_ROLL
	end
	p.pflags = $|PF_THOKKED
	if p.armausable then
		p.armacharging = true
		p.armarev = 0
		p.armarevaiming = 0
		p.armausable = false
        p.armaboost = p.mrce.realspeed / (5*FRACUNIT)/3
	end
	return true
end)

local function armagetonmaballs(p)
	if not p.mo then return end
	if not ((p.powers[pw_shield] & SH_NOSTACK) == SH_ARMAGEDDON) then return end
	if p.mo.skin == "skip" then return end
	if (p.charflags & SF_NOSHIELDABILITY) then return end
	if not p.armacharging then return end
	if P_PlayerInPain(p) then return end
	if p.pflags & PF_STASIS
	or p.pflags & PF_FULLSTASIS then
	--or p.mo.state == S_PLAY_SPRING
	--or p.mo.state == S_PLAY_FALL then
		return
	end

	--p.mo.flags = $ | MF_NOGRAVITY
	p.pflags = $ & ~PF_SPINNING
	p.pflags = $ | PF_SHIELDABILITY


	p.mo.momz = 0
	if p.pflags & PF_ANALOGMODE then
		p.resetanalog = true
		p.pflags = $ & ~PF_ANALOGMODE
	end
	p.drawangle = p.mo.angle
	if p.resetanalog then
		if p.invertedaim == 1 then
			p.armarevaiming = $ - p.cmd.forwardmove*P_MobjFlip(p.mo) * FRACUNIT/8
		else
			p.armarevaiming = $ + p.cmd.forwardmove*P_MobjFlip(p.mo) * FRACUNIT/8
		end
		p.armarevaiming = min($, 90*FRACUNIT)
		p.armarevaiming = max($, -90*FRACUNIT)
		if (p.mo.flags2 & MF2_TWOD)
		or twodlevel then
			p.aiming = FixedAngle(p.armarevaiming/3)
		else
			p.aiming = FixedAngle(p.armarevaiming/2)
			p.mo.angle = $ - p.cmd.sidemove * FRACUNIT * 32
		end
	end
	if p.mrce.realspeed > FixedMul(3*FRACUNIT, p.mo.scale) then
		local speed = FixedHypot(p.mo.momx,p.mo.momy)
		if speed > p.mo.scale then
			local dir = R_PointToAngle2(0,0,p.mo.momx,p.mo.momy)
			P_InstaThrust(p.mo,dir,speed-p.mo.scale*(p.speed > FixedMul(75*FRACUNIT, p.mo.scale) and 4 or p.speed > FixedMul(50*FRACUNIT, p.mo.scale) and 3 or 2))
		end
	elseif p.mrce.realspeed <= FixedMul(3*FRACUNIT, p.mo.scale) then
		p.mo.momx = 0
		p.mo.momy = 0
	end
	SpawnDashAim(p.mo)

    if not S_SoundPlaying(p.mo, sfx_s3k84) then
        S_StartSound(p.mo, sfx_s3k84)
    end

	if p.speed < FixedMul(p.normalspeed, p.mo.scale) then
		p.armarev = $ + 1
	end
end

addHook("SpinSpecial", armagetonmaballs)

addHook("MobjDeath", function(enemy, inf, source)
	if (inf and inf.valid and inf.player and ((inf.player.powers[pw_shield] & SH_NOSTACK) == SH_ARMAGEDDON)) then
		if (enemy and enemy.valid and not (enemy.flags & MF_BOSS))
		and (enemy.flags & MF_ENEMY or enemy.flags & MF_MONITOR) then
			if (inf.player.armausable == false)
			and inf.player.armacharging == false then
				inf.player.armausable = true
				inf.player.pflags = $1 & ~PF_SHIELDABILITY
			end
		end
		return false
	end
end)

--Force Stop EX

addHook("PlayerThink", function(p)
    if not (p and p.mo and p.mo.valid) then return end
	if p.mo.skin == "skip" then return end
	if (p.charflags & SF_NOSHIELDABILITY) then return end
    p.forcehalt = $ or 0
    p.forcehalted = $ or false
    p.forcestore = $ or 0
    if p.playerstate ~= PST_LIVE
    or (not(p.powers[pw_shield] & SH_FORCE))
    or P_PlayerInPain(p) then
        p.forcehalt = 0
        p.forcehalted = false
        p.forcestore = 0
    end
    if (p.powers[pw_shield] & SH_FORCE) and p.forcehalted then
        if p.forcehalt <= 40 then
            p.forcehalt = $ + 1
            if p.mrce.jump then
                P_InstaThrust(p.mo, p.mo.angle, p.forcestore)
                p.forcehalted = false
                p.forcestore = 0
                p.forcehalt = 0
                S_StartSound(p.mo, sfx_s3k81)
            elseif p.mrce.shield == 1 and p.forcehalt > 5 then
                P_InstaThrust(p.mo, p.mo.angle, 0)
                p.forcehalted = false
                p.forcestore = 0
                p.forcehalt = 0
                p.mo.state = S_PLAY_JUMP
                S_StartSound(p.mo, sfx_s3ka2)
            else
                P_SetObjectMomZ(p.mo, 0, false)
                P_InstaThrust(p.mo, p.mo.angle, 0)
            end
        else
            --p.pflags = $ & ~PF_JUMPED
            p.forcehalted = false
            p.forcestore = 0
            p.forcehalt = 0
            p.mo.state = S_PLAY_FALL
            S_StartSound(p.mo, sfx_s3k51)
        end
    end
end)

addHook("ShieldSpecial", function(p)
    if not (p and p.mo and p.mo.valid) then return end
    if not (p.powers[pw_shield] & SH_FORCE) then return end
    if (p.pflags & PF_THOKKED)
    or (p.pflags & PF_SHIELDABILITY) then return end
    p.forcestore = FixedHypot(p.mrce.realspeed, p.mo.momz)
    p.forcehalted = true
    p.forcehalt = 1
end)