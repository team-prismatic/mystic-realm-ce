-- Handles the HUD seen when the player is editing paths and waypoints.

if MapPathsVersionInfo.loaded -- Duplicate prevention
or MapPathsVersionInfo.editing == false
	return true
end
local path, waypoints, lastway
local pathhud = false

local nativeelements = {
	"textspectator",
	"score",
	"time",
	"rings",
	"lives",
	"teamscores",
	"weaponrings",
	"powerstones",
	"nightslink",
	"nightsdrill",
	"nightsscore",
	"nightsrecords",
-- 	"rankings",
-- 	"coopemeralds",
-- 	"tokens",
-- 	"tabemblems",
}

local xoffset1 = 4*FRACUNIT
local yoffset1 = 4*FRACUNIT
local xoffset2 = 48*FRACUNIT
local yoffset2 = 24*FRACUNIT
local xoffset3 = 176*FRACUNIT
local yoffset3 = 192*FRACUNIT
local xoffset4 = 248*FRACUNIT
local topleft = V_SNAPTOTOP|V_SNAPTOLEFT
local topright = V_SNAPTOTOP|V_SNAPTORIGHT
local topleft = V_SNAPTOBOTTOM|V_SNAPTOLEFT
local topright = V_SNAPTOBOTTOM|V_SNAPTORIGHT
local align = "small-fixed"

local killSwitch = function(v, player, cam, obj)
	if not path
		return true -- Destroy this HUD element
	end
end

local listtags = function(tags, limit)
	local str = ""
	for n, m in ipairs(tags) do
		if n == limit and #tags > limit
			str = $.."..."
			return str
		end
		str = $..m.."\n"
	end
	return str
end

local listexits = function(exits, limit)
	local str = ""
	for n, m in ipairs(exits) do
		if n == limit and #exits > limit
			str = $.."..."
			return str
		end
		str = $..tostring(m and m.path and m.path.index)..", "..tostring(m and m.health).."\n"
		if #exits[m]
			str = $.."Tags\n"..listtags(exits[m])
		end
	end
	return str
end

local bt = function(n,str)
	return n & 2 and "\x82"..str
		or n & 1 and "\x80"..str
		or "\x86"..str
end

local head = function(str)
	return "\x86"..str..": "
end

local str = function(str1,str2,n)
	return "\x88".."["..str1.."]"..bt(n,str2).." "
end

local keyStr = function(v, player, cam, obj)
	local t = player.patheditor_highlight
	if not(t)
		return ""
	end
	local gotokey = --head("Warp")
		--..
		str("1","First",t.goto_first)
		..str("2","Previous",t.goto_previous)
		..str("3","Current",t.goto_current)
		..str("4","Next",t.goto_next)
		..str("5","Last",t.goto_last)
		..str("6","Bot Start",t.bot_start)
		..str("7","Bot End",t.bot_end)
		..str("+-","Height",t.height_up|t.height_down)
		.."\n"
	local selectkey = --head("Waypoint")
		--..
		str("C1","Select",t.select)
		..str("C2","Multiselect",t.multiselect)
		..str("C3","Deselect",t.deselect)
		..str("C2+C1","Sel.All",t.selectall)
		..str("C2+C3","Ds.All",t.deselectall)
		..str("+-","Width",t.radius_up|t.radius_down)
		.."\n"
	local waykey = str("Fire","New Way/Exit",t.create)
		..str("Alt","Drag",t.drag)
		..str("TossF.","Del. Way",t.delete)
		..str("F+A","New 2-Way",t.create2way)
		..str("A+TF","Delete 2-Way",t.delete2way)
		.."\n"
	return gotokey..selectkey..waykey
end


-- 	.."\x86".."Waypoint:\x88 [Custom 1]\x80 New\x88 [Custom 2]\x80 Move\x88 [Custom 3]\x80 Delete\x88 [Wep -/+]\x80 Height/Radius\n"
-- 	.."\x88".."[Fire]\x80 Select\x88 [Fire Normal]\x80 MultiSel.\x88 [Toss Flag]\x80 Deselect\x88 [6]\x80 Sel. All\x88 [7]\x80 Dsel. All"

-- local highlightkey = "\x86".."Go to:\x88 [1]\x80 First\x88 [2]\x80 Previous\x88 [3]\x80 Current\x88 [4]\x80 Next\x88 [5]\x80 Last\n"
-- 	.."\x86".."Waypoint:\x88 [Custom 1]\x80 New\x88 [Custom 2]\x80 Move\x88 [Custom 3]\x80 Delete\x88 [Wep -/+]\x80 Height/Radius\n"
-- 	.."\x88".."[Fire]\x80 Select\x88 [Fire Normal]\x80 MultiSel.\x88 [Toss Flag]\x80 Deselect\x88 [6]\x80 Sel. All\x88 [7]\x80 Dsel. All"


local botStr = function(v, player, cam, obj)
	local ai = player.MapPath_AI
	local count = ai.route and #ai.route or 0
	local routestr = "\x88".."Waypoints left\x80 "..count
	local waypoint = ai.wayhist
	local pathstr = "\x88".."Path\x80 "..tostring(waypoint and waypoint.path.index or 0)
	local waystr = tostring(waypoint and waypoint.health or 0)
	local waynext = ai.nextway
	local pathnextstr = "\x88".."Path\x80 "..tostring(waynext and waynext.path.index or 0)
	local waynextstr = tostring(waynext and waynext.health or 0)
	local jump = (player.cmd.buttons & BT_JUMP and "\x82" or '\x80').."Jump\x80"
	local spin = (player.cmd.buttons & BT_SPIN and "\x82" or '\x80').."Spin\x80"
	local fwdmove = "\x88".."Forward\x80 "..tostring(player.cmd.forwardmove)
	return "\n".."		"..routestr.."\n"
		..pathstr.."."..waystr.." to "..pathnextstr.."."..waynextstr
		..",\x88 ".."Inputs:\x80 "..jump.." "..spin.." "..fwdmove
end

local pathString1 = function()
	return "\x82".."Path "..path.index.."\x80\n"
		.."Type: "..path.type.."\n"
		.."\x88".."Tags\x80\n"..listtags(path.tags, 10)
end
local pathString2 = function()
	return "Series: "..path.series.."\n"
		.."Backside: "..tostring(path.backside).."\n"
		.."\x88".."Backtags\x80\n"..listtags(path.backtags, 10)
end

local color1 = function()
	return lastway.editing and "\x82" or "\x84"
end
local color2 = function()
	return lastway.editing and "\x80" or "\x86"
end

local waypointString1 = function()
	if lastway == nil or not lastway.valid
		return
	end

	return color1().."Waypoint "..lastway.path.index.."."..lastway.health..color2().."\n"
		.."Radius: "..lastway.radius/FRACUNIT.."\n"
		.."Height: "..lastway.height/FRACUNIT.."\n"
		.."ExtraValue1: "..lastway.extravalue1.."\n"
		.."\x88".."Tags\x80\n"..listtags(lastway.tags, 10)
		.."\x88".."Entrances\x80\n"..listexits(lastway.entrances, 6)
end

local waypointString2 = function()
	if lastway == nil or not lastway.valid
		return
	end
	return color2().."x: "..lastway.x/FRACUNIT.."\n"
		.."y: "..lastway.y/FRACUNIT.."\n"
		.."z: "..lastway.z/FRACUNIT.."\n"
		.."ExtraValue2: "..lastway.extravalue2.."\n"
		.."\x88".."Backtags\x80\n"..listtags(lastway.backtags, 10)
		.."\x88".."Exits\x80\n"..listexits(lastway.exits, 6)
end


local selectedString = function(v, player, cam, obj)
	local str = "Waypoints: "
	local color
	local height = 0
	for n,m in ipairs(path) do
		if #m.exits or #m.entrances
			if m.editing
				color = "\x83"
			else
				color = "\x8D"
			end
		elseif #m.tags or #m.backtags
			if m.editing
				color = "\x80"
			else
				color = "\x88"
			end
		else
			if m.editing
				color = "\x82"
			else
				color = "\x84"
			end
		end
		str = $..color..n.." "
		if n % 30 == 0
			str = $.."\n"
			height = $+1
		end
	end
	obj.y = yoffset3 - height * 6 * FRACUNIT
	return str
end

local pathObj1 = {
	drawtype = "string",
	flags = topleft,
	x = xoffset1,
	y = yoffset2,
	align = align,
	func = function(v, player, cam, obj)
		if killSwitch()
			return true
		end
		if player.realmo
			obj.string = pathString1()
		end
	end
}

local pathObj2 = {
	drawtype = "string",
	flags = topleft,
	x = xoffset2,
	y = yoffset2,
	align = align,
	func = function(v, player, cam, obj)
		if killSwitch()
			return true
		end
		if player.realmo
			obj.string = pathString2()
		end
	end
}

local waypointObj1 = {
	drawtype = "string",
	flags = topright,
	x = xoffset3,
	y = yoffset2,
	align = align,
	func = function(v, player, cam, obj)
		if killSwitch()
			return true
		end
		if player.realmo
			obj.string = waypointString1()
		end
	end
}

local waypointObj2 = {
	drawtype = "string",
	flags = topright,
	x = xoffset4,
	y = yoffset2,
	align = align,
	func = function(v, player, cam, obj)
		if killSwitch()
			return true
		end
		if player.realmo
			obj.string = waypointString2()
		end
	end
}

local selectedObj = {
	drawtype = "string",
	flags = topleft,
	x = xoffset1,
	y = yoffset3,
	align = align,
	func = function(v, player, cam, obj)
		if killSwitch()
			return true
		end
		if player.realmo
			obj.string = selectedString(v, player, cam, obj)
		end
	end
}

local keyObj = {
	drawtype = "string",
	flags = bottomleft,
	x = xoffset1,
	y = yoffset1,
	align = align,
-- 	string = controlkey,
	func = function(v, player, cam, obj)
		if killSwitch()
			return true
		end
		if player.bot and player.MapPath_AI
			obj.string = botStr(v, player, cam, obj)
		else
			obj.string = keyStr(v, player, cam, obj)
		end
	end
}

local masterController = function(v, player, cam, obj)
	if not player.realmo
		return
	end
	path = L_GetSelectedPath()
	waypoints = L_GetSelectedWaypoints()
	lastway = player.realmo.waypoint
	-- Initialize path HUD
	if path and not pathhud
		pathhud = true
		for _,e in pairs(nativeelements) do -- Disable native hud elements
			hud.disable(e)
		end
		-- Spawn HUD elements
		table.insert(hudobjs, pathObj1)
		table.insert(hudobjs, pathObj2)
		table.insert(hudobjs, waypointObj1)
		table.insert(hudobjs, waypointObj2)
		table.insert(hudobjs, selectedObj)
		table.insert(hudobjs, keyObj)
	end
	-- Restore native hud elements
	if not path and pathhud
		pathhud = false
		for _,e in pairs(nativeelements) do
			hud.enable(e)
		end
	end
end

table.insert(hudobjs, {func = masterController})

COM_AddCommand("hudobjs", do print(#hudobjs) end, COM_LOCAL)