-- This lump handles all luafile functionality, as well as some of the code that triggers writing to lua scripts (see Path_WriteLua.lua)

if MapPathsVersionInfo.loaded -- Duplicate prevention
	return
end

local autoLoad
local loadFile
local filePath

local getInternalFunc = function(map)
	local path = string.lower(tostring(L_GetInternalMapPaths(map or gamemap)))
	return rawget(_G, path)
end

-- Trigger auto-load routine
addHook("MapLoad", function(map)
	local internal = getInternalFunc(map)
	if MapPathsVersionInfo.editing and autoLoad.value and io.type(io.openlocal(filePath(map),"r"))
		-- Luafile load
		io.close()
		loadFile()
	elseif internal and type(internal) == "function"
		-- Lump load
		internal()
	end
end)

if MapPathsVersionInfo.editing == false
	return
end
-- CVars
autoLoad = CV_RegisterVar({
	name = "autoloadpathfiles",
	defaultvalue = 1,
	PossibleValue = CV_OnOff
})

local autoSave = CV_RegisterVar({
	name = "autosavepathfiles",
	defaultvalue = 1,
	PossibleValue = CV_Unsigned
})

-- Filepath builder
filePath = function(map, path)
	local str = path or "server/waypoints"
	str = $.."/"..tostring(G_BuildMapName(map)).."_"..tostring(G_BuildMapTitle(map))..".cfg"
	return string.lower(str)
end

-- Load file for building paths
loadFile = function(path)
	io.open(filePath(gamemap, path), "r", L_CreatePathsFromFile)
end

-- Read from file (does not build paths)
local readFile = function(path, backup)
	io.open(filePath(gamemap, path), "r", function(file, filename)
		if file == nil
			print("No file found on path "..filePath(gamemap))
			return
		end
		print("\x82".."Reading from file:\x80")
		for line in file:lines() do
			print(line)
		end
		io.close()
		print("\x86".."End file")
	end)
end

-- Write to file
local strTags = function(str, tags)
	for _,tag in ipairs(tags) do
		str = $.." "..tag
	end
	return str
end
local writeFile = function(path, backup)
	if consoleplayer != server
		return
	end
	local filename = filePath(gamemap, path)
	local file = io.openlocal(filename, "w")
	L_WritePathsToFile(file, filename, backup)
	io.close(file)
end

-- Auto-save routine
local editing = 0
addHook("ThinkFrame",do
	if autoSave.value == 0
		return
	end
	if not editing
		if L_GetSelectedPath() 
			editing = 1
		end
	else
		editing = $+1
		if editing >= TICRATE*60*autoSave.value
			writeFile("server/waypoints/autobackup", true)
			editing = 0
		end
	end
end)

-- Console commands
COM_AddCommand("readpathfile", function(player, arg)
	if arg == "backup"
		readFile("server/waypoints/autobackup")
	elseif arg == nil
		readFile()
	else
		CONS_Printf(player, "invalid argument "..arg)
	end
end, COM_ADMIN)
COM_AddCommand("loadpathfile", function(player, arg)
	if arg == "backup"
		loadFile("server/waypoints/autobackup")
	elseif arg == "internal"
		local func = getInternalFunc(gamemap)
		if not(func and type(func) == "function")
			CONS_Printf(player, "No internal settings found for this map.")
		else
			func()
		end
	elseif arg == nil
		loadFile()
	else
		CONS_Printf(player, "invalid argument "..arg)
	end
end, COM_ADMIN)
COM_AddCommand("savepathfile", function(player, arg)
	if arg == "backup"
		writeFile("server/waypoints/autobackup", true)
	elseif arg == nil
		writeFile()
	else
		CONS_Printf(player, "invalid argument "..arg)
	end
end, COM_ADMIN)

-- Global read and write functions.
local lineSleuth = function(line)
	return string.sub(line, string.find(line, ":")+2, #line)
end
local lineTagSleuth = function(line, nosleuth)
	if not nosleuth
		line = lineSleuth(line)
	end
	local entries = {}
	local i
	repeat
		local found = string.find(line," ") or #line+1
		i = string.sub(line, 0, found-1)
		line = string.sub(line, found+1, #line)
		if #i
			table.insert(entries, i)
		end
	until #line == 0
	return entries
end
local exitSleuth = function(line)
	local path1, path2, way1, way2, tags
	local _,path1Start = string.find(line, "StartPath ")
	local path1End,way1Start = string.find(line, " StartWaypoint ")
	local way1End,path2Start = string.find(line, " EndPath ")
	local path2End,way2Start = string.find(line," EndWaypoint ")
	local way2End, tagsStart = string.find(line," Tags:")
	path1Start = $+1
	path1End = $-1
	way1Start = $+1
	way1End = $-1
	path2Start = $+1
	path2End = $-1
	way2Start = $+1
	way2End = $-1
	local path1str = string.sub(line, path1Start, path1End)
	local way1str = string.sub(line, way1Start, way1End)
	local path2str = string.sub(line, path2Start, path2End)
	local way2str = string.sub(line, way2Start, way2End)
	local tagsstr = string.sub(line, tagsStart, #line)
	path1 = MapPaths[tonumber(path1str)]
	way1 = path1[tonumber(way1str)]
	path2 = MapPaths[tonumber(path2str)]
	way2 = path2[tonumber(way2str)]
	tags = lineTagSleuth(string.sub(tagsstr, 3, #tagsstr), true)
	assert(way1, "Got invalid waypoint #"..tostring(way1str).." for path #"..tostring(path1str))
	assert(way2, "Got invalid waypoint #"..tostring(way2str).." for path #"..tostring(path2str))
	L_AddWaypointExit(way1, way2, tags)
end
rawset(_G,"L_CreatePathsFromFile", function(file, filename)
	if file == nil
		return
	end
	L_SilencePathMessages(true) -- Prevent messages from being printed during loading sequence
	-- Remove any existing paths.
	while #MapPaths > 0 do
		L_DeletePath(MapPaths[1])
	end
	-- Fill table
	local currentpath
	local waypoint
	local exitmode = false
	for line in file:lines() do
		local ch = string.sub(line, 1, 1)
		if ch == "#" -- This is a comment and shouldn't be acted on.
			continue
		end
		local subject = string.sub(line, 1, string.find(line, " "))
		-- Constructing header
		if currentpath == nil
			if string.match(subject,"Tags")
				MapPaths.tags = lineTagSleuth(line)
			elseif string.match(subject,"Backtags")
				MapPaths.backtags = lineTagSleuth(line)
			end
		end
		if waypoint and ch == "$"
			-- Close out this waypoint and make way for a new instance to be created
			L_CreateWaypoint(currentpath, waypoint)
			waypoint = nil
		end
		if string.match(subject,"$Exits")
			exitmode = true
		elseif exitmode
			-- Constructing exits
				exitSleuth(line)
		elseif waypoint 
			-- Constructing waypoints
			if string.match(subject,"Radius")
				waypoint.radius = lineSleuth(line) * FRACUNIT
			elseif string.match(subject,"Height")
				waypoint.height = lineSleuth(line) * FRACUNIT
			elseif string.match(subject,"ExtraValue1")
				waypoint.extravalue1 = lineSleuth(line)
			elseif string.match(subject,"ExtraValue2")
				waypoint.extravalue2 = lineSleuth(line)
			elseif string.match(subject,"Tags")
				waypoint.tags = lineTagSleuth(line)
			elseif string.match(subject,"Backtags")
				waypoint.backtags = lineTagSleuth(line)
			elseif string.match(subject,"Position")
				line = lineSleuth(line)
				local comma = string.find(line, ",")
				waypoint.x = tonumber(string.sub(line,0,comma-1)) * FRACUNIT
				line = string.sub($, comma+1, #line)
				local comma = string.find(line, ",")
				waypoint.y = tonumber(string.sub(line,0,comma-1)) * FRACUNIT
				line = string.sub($, comma+1, #line)
				waypoint.z = tonumber(line) * FRACUNIT
			end
		end
		-- Constructing paths
		if string.match(subject,"$Path") 
			currentpath = L_CreatePath()
			waypoint = nil
		elseif currentpath
			if string.match(subject,"$Waypoint")
				waypoint = {}
			elseif waypoint == nil
				if string.match(subject,"Type")
					currentpath.type = lineSleuth(line)
				elseif string.match(subject,"Series")
					currentpath.series = tonumber(lineSleuth(line)) or -1
				elseif string.match(subject,"Backside")
					currentpath.backside = lineSleuth(line) == "true" and true or false
				elseif string.match(subject,"Color")
					currentpath.color = R_GetColorByName(lineSleuth(line))
				elseif string.match(subject,"Tags")
					L_AddTags(currentpath.tags, lineTagSleuth(line))
				elseif string.match(subject,"Backtags")
					L_AddTags(currentpath.backtags, lineTagSleuth(line))
				end
			end
		end
	end
	-- Reached the end of file? Make sure we've created the last waypoint
	if waypoint
		L_CreateWaypoint(currentpath, waypoint)
	end
	-- Adding paths automatically puts us in editing mode. Get us out of it
	L_SelectPath()
	-- Complete!
	print("Loaded paths and waypoints from file: "..filename)
	L_SilencePathMessages()
end)

rawset(_G,"L_WritePathsToFile", function(file, filename, backup)
	file:write("# MapPaths Configuration File\n"
		.."# "..G_BuildMapName(gamemap)..": "..G_BuildMapTitle(gamemap).."\n"
		.."# File generated with MapPaths "..MapPathsVersionInfo.string..". Manually edit at your own risk!\n"
		)
	file:write("\n".."$Header\n")
	file:write(strTags("Tags:",MapPaths.tags).."\n")
	file:write(strTags("Backtags:",MapPaths.tags).."\n")
	for n,m in ipairs(MapPaths) do
		-- Write paths
		file:write("\n".."$Path "..n.."\n")
		file:write("Type: "..tostring(m.type).."\n")
		file:write("Series: "..tostring(m.series).."\n")
		file:write("Backside: "..tostring(m.backside).."\n")
		file:write("Color: ",R_GetNameByColor(m.color).."\n")
		file:write(strTags("Tags:",m.tags).."\n")
		file:write(strTags("Backtags:",m.backtags).."\n")
		for nn,mm in ipairs(m) do
			-- Write waypoints
			file:write("\n".."$Waypoint "..tostring(nn).."\n")
			file:write("Position: "..tostring(mm.x/FRACUNIT)..", "
				..tostring(mm.y/FRACUNIT)..", "
				..tostring(mm.z/FRACUNIT).."\n")
			file:write("Radius: "..tostring(mm.radius/FRACUNIT).."\n")
			file:write("Height: "..tostring(mm.height/FRACUNIT).."\n")		
			file:write(strTags("Tags:",mm.tags).."\n")
			file:write(strTags("Backtags:",mm.backtags).."\n")
			file:write("ExtraValue1: "..tostring(mm.extravalue1).."\n")
			file:write("ExtraValue2: "..tostring(mm.extravalue2).."\n")		
		end
	end
	-- Write exits (Done after, because all waypoints need to spawn in first.)
	file:write("\n".."$Exits\n")
	for n,m in ipairs(MapPaths) do
		for nn,mm in ipairs(m) do
			for nnn,mmm in ipairs(mm.exits) do
				file:write("StartPath "..m.index.." StartWaypoint "..mm.health.." EndPath "..mmm.path.index.." EndWaypoint "..mmm.health..strTags(" Tags:",mm.exits[mmm]).."\n")
			end
		end
	end
	-- End
	if backup
		print("\x83".."Saved backup:\x80 "..filePath(gamemap, path))
	else
		print("\x83".."Saved file:\x80 "..filePath(gamemap, path))
	end
end)