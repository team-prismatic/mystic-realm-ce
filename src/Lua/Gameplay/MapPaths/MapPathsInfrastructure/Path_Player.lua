if MapPathsVersionInfo.loaded -- Duplicate prevention
or MapPathsVersionInfo.editing == false
	return
end
-- Input control
local bt = {
	goto_first = 1,
	goto_previous = 2,
	goto_current = 3,
	goto_next = 4,
	goto_last = 5,
	bot_start = 6,
	bot_end = 7,
	select = BT_CUSTOM1,
	multiselect = BT_CUSTOM2,
	deselect = BT_CUSTOM3,
	create = BT_ATTACK,
	drag = BT_FIRENORMAL,
	delete = BT_TOSSFLAG,
	height_up = BT_WEAPONPREV,
	height_down = BT_WEAPONNEXT,
	radius_up = BT_WEAPONPREV,
	radius_down = BT_WEAPONNEXT
}

local buttonPressed = function(player, button)
	if button & BT_WEAPONMASK
	return player.waybuttons & BT_WEAPONMASK == button
		and player.waybuttons_last & BT_WEAPONMASK != button
	end
	return (player.waybuttons & button) &~ (player.waybuttons_last & button)
end
local getInput = function(player, str, button)
	if str and player.patheditor_highlight[str] != 2
		return false
	elseif not(button) or buttonPressed(player, button)
		return true
	end
	return false
end

addHook("PreThinkFrame", do
	if L_GetSelectedPath()
		for player in players.iterate do
			player.waybuttons = player.cmd.buttons
			if player.waybuttons_last == nil
				player.waybuttons_last = player.waybuttons
			end
			player.cmd.buttons = $ & (BT_JUMP|BT_SPIN|BT_TOSSFLAG)
		end
	end
end)
addHook("PostThinkFrame", do
	for player in players.iterate do
		player.waybuttons_last = player.waybuttons
	end
end)

-- TouchSpecial handler
local setPlayerWaypoint = function(player, waypoint)
	if not L_GetSelectedPath() --!= waypoint.path
	and not(player.MapPath_AI)
		return
	end
	player.realmo.waypoint = waypoint
	if #waypoint.path > waypoint.health
		player.realmo.nextwaypoint = waypoint.path[waypoint.health+1]
	else
		player.realmo.nextwaypoint = nil
	end
	if waypoint.health > 1
		player.realmo.prevwaypoint = waypoint.path[waypoint.health-1]
	else
		player.realmo.prevwaypoint = nil
	end
end
MapWaypointTouchSpecial = function(mo, pmo) setPlayerWaypoint(pmo.player, mo) end

local fr = FixedRound
addHook("PlayerThink", function(player)
	local mo = player.realmo
	if mo == nil or player.bot
	return end
	local path = L_GetSelectedPath()
	local waypoints = L_GetSelectedWaypoints()
	if path
		player.pflags = $|PF_GODMODE
	else
		player.pflags = $&~PF_GODMODE
		mo.waypoint = nil
	end
	if mo.waypoint and not mo.waypoint.valid
		mo.waypoint = nil
	end
	if not path
		return
	end
	-- Editing mode
	if player.exiting
		player.exiting = $+1 -- Stall exiting
	end
	-- Last waypoint touched (for spectators)
	if player.spectator
		searchBlockmap("objects", function(refmo, mo)
			if mo.type == MT_MAPWAYPOINT and mo.z <= refmo.z + refmo.height and refmo.z <= mo.z + mo.height
				setPlayerWaypoint(refmo.player, mo)
			end
		end, mo, mo.x - mo.radius, mo.x + mo.radius, mo.y - mo.radius, mo.y + mo.radius)
	end
	
	
	local last = mo.waypoint
	-- Touched waypoint visuals
	if leveltime % 10 == 0 and last and consoleplayer
		local thok = P_SpawnMobjFromMobj(last, x, y, z, MT_THOK)
		thok.color = last.color
	end
	-- Inputs
	local waycmd = player.waybuttons
	local waycmdlast = player.waybuttons_last
	local wephold = (waycmd & BT_WEAPONMASK)
	local wep = (waycmd & BT_WEAPONMASK) &~ (waycmdlast & BT_WEAPONMASK)
	local cmd = player.cmd

	player.patheditor_highlight = {
		goto_first =
			wephold == bt.goto_first 	and 2
										or 1,
		goto_previous =
			wephold == bt.goto_previous	and 2
										or 1,
		goto_current =
			wephold == bt.goto_current 	and 2
										or 1,
		goto_next =
			wephold == bt.goto_next 	and 2
										or 1,
		goto_last = 
			wephold == bt.goto_last 	and 2
										or 1,
		bot_start = 
			wephold == bt.bot_start 	and 2 
										or 1,
		bot_end = 
			wephold == bt.bot_end 		and 2 
										or 1,
		select = 
			waycmd & (bt.select|bt.multiselect) == bt.select	and 2 
			or not(waycmd & bt.multiselect) 					and 1 
																or 0,
		deselect = 
			waycmd & (bt.deselect|bt.select) == bt.deselect 	and 2 
			or not(waycmd & bt.multiselect) 					and 1 
																or 0,
		multiselect = 
			waycmd & bt.multiselect 		and 2 
											or 1,
		selectall = 
			waycmd & (bt.multiselect|bt.select) == (bt.multiselect|bt.select) 	and 2 
			or waycmd & bt.multiselect 											and 1 
																				or 0,
		deselectall = 
			waycmd & (bt.multiselect|bt.deselect) == (bt.multiselect|bt.deselect) 	and 2 
			or waycmd & bt.multiselect 												and 1 
																					or 0,
		create = 
			waycmd & (bt.create|bt.drag) == bt.create 								and 2 
			or waycmd & bt.drag == 0 												and 1 
																					or 0,
		create2way = 
			waycmd & (bt.create|bt.drag) == bt.create|bt.drag 						and 2 
			or waycmd & bt.drag == bt.drag											and 1 
																					or 0,
		drag = 
			waycmd & bt.drag 	and 2 
								or 1,
		delete = 
			waycmd & (bt.delete|bt.drag) == bt.delete 	and 2  
			or waycmd & bt.drag == 0 					and 1 
														or 0,
		delete2way = 
			waycmd & (bt.delete|bt.drag) == bt.drag|bt.delete 	and 2 
			or waycmd & bt.drag == bt.drag						and 1 
																or 0,
		height_up = 
			waycmd & bt.height_up and waycmd & bt.drag == 0 	and 2
			or waycmd & bt.drag == 0 							and 1 
																or 0,
		height_down = 
			waycmd & bt.height_down and waycmd & bt.drag == 0 	and 2
			or waycmd & bt.drag == 0 							and 1 
																or 0,
		radius_up = 
			waycmd & bt.radius_up and waycmd & bt.drag == 1 	and 2
			or waycmd & bt.drag == bt.drag 						and 1 
																or 0,
		radius_down = 
			waycmd & bt.radius_down and waycmd & bt.drag == 0 	and 2
			or waycmd & bt.drag == bt.drag 						and 1 
																or 0,
	}
	local h = player.patheditor_highlight
	-- Controls
	if cmd.buttons
	or cmd.forwardmove
	or cmd.sidemove
	or waycmd or waycmdlast
		player.playpath = nil -- Abort path playback
		-- Waypoint Selection
		if getInput(player,"selectall",bt.select)
			-- Select all waypoints
			L_SelectWaypoints(path)
		end
		if getInput(player,"deselectall",bt.deselect)
			-- Deselect all waypoints
			L_SelectWaypoints()
		end	
		if getInput(player,"deselect",bt.deselect)
			-- Deselect waypoint
			L_DeselectWaypoints(last)
		end
		if getInput(player,"multiselect")
		and last and last.path == path
			-- Multiselect waypoint
			L_MultiselectWaypoints(last)
		end
		if getInput(player,"select",bt.select)
			-- Select waypoint
			if last and last.path != path
				L_SelectPath(last.path)
			end
			L_SelectWaypoints(last)
		end

		-- Create connections
		local mode = getInput(player,"create",bt.create) 	and 1
			or getInput(player,"create2way",bt.create) 		and 2
															or 0
		if mode
			if last
				for n,way in ipairs(waypoints) do
					if not way.exits[last]
						-- Add waypoint exit
						print("Added exit to touching waypoint")
						L_AddWaypointExit(way, last)
					end
					if mode == 2
					and not last.exits[way]
						-- Add waypoint exit on other side
						print("Added entrance from touching waypoint")
						L_AddWaypointExit(last, way)					
					end
				end
			elseif mode == 1
				-- Add waypoint
				COM_BufInsertText(player, "addwaypoint")
			end
		end
		-- Delete connections
		local mode = getInput(player,"delete",bt.delete) 	and 1
			or getInput(player,"delete2way",bt.delete) 		and 2
															or 0
		if mode
			if last and mode == 2 
				-- Remove connections
				for _,way in ipairs(waypoints)
					if L_RemoveWaypointExit(last, way)
						print("Removed exit to touching waypoint")
					end
					if L_RemoveWaypointExit(way, last)
						print("Removed entrance from touching waypoint")
					end
				end
			end
			if mode == 1 and not connections
				-- Delete waypoints
				COM_BufInsertText(player, "deletewaypoints")
			end
		end
		if getInput(player,"drag") and #waypoints
			-- Drag waypoints
			if not(waycmdlast & bt.drag)
-- 				print("Moving waypoints. Hold CUSTOM2 to drag, see control list for other actions.")
			else
				local x,y,z = fr(mo.x) - fr(mo.xprev), fr(mo.y) - fr(mo.yprev), fr(mo.z) - fr(mo.zprev)
				for _,way in ipairs(waypoints) do
					P_TeleportMove(way, FixedRound(way.x+x), FixedRound(way.y+y), FixedRound(way.z+z))
				end
			end
			mo.xprev = fr(mo.x)
			mo.yprev = fr(mo.y)
			mo.zprev = fr(mo.z)
		end
		-- Change size
		if #waypoints
			local scroll = (buttonPressed(player, bt.height_up) and 1 or 0) - (buttonPressed(player, bt.height_down) and 1 or 0)
			if scroll and not(waycmd & bt.drag)
				local i = 16 * FRACUNIT
				for _,way in ipairs(waypoints) do
					way.height = max(i, $ + i*scroll)
					way.hitbox_updated = true
				end
			end
			local scroll = (buttonPressed(player, bt.radius_up) and 1 or 0) - (buttonPressed(player, bt.radius_down) and 1 or 0)
			if scroll and waycmd & bt.drag
				local i = 16 * FRACUNIT
				for _,way in ipairs(waypoints) do
					way.radius = max(i, $ + i*scroll)
					way.hitbox_updated = true
				end
			end
		end
		-- Warp
		local tp
		if wep == bt.goto_first
			tp = path[1]
		elseif wep == bt.goto_previous and last
			tp = path[max(1, last.health-1)]
		elseif wep == bt.goto_current and last
			tp = last
		elseif wep == bt.goto_next and last
			tp = path[min(#path, last.health+1)]
		elseif wep == bt.goto_last
			tp = path[#path]
		elseif wep == bt.bot_start and last
			L_WarpPathBot(last)
		elseif wep == bt.bot_end and last
			local bot = L_GetPathBot()
			if not(bot)
				CONS_Printf(player,"Path Bot isn't present. (Choose BOT START first)")
				return
			end
			L_StartPathBot(nil, last, bot.MapPath_AI.blacklist)
		end
		if tp
			P_TeleportMove(mo, tp.x, tp.y, tp.z)
			COM_BufInsertText(player, "Teleported to waypoint #"..tp.health)
		end
		return
	end
	-- Path playback
	local path = player.playpath
	if path and #path
		if last == nil and mo.nextwaypoint == nil
			local w = path[1]
			P_TeleportMove(mo, w.x, w.y, w.z)
			mo.waypoint = w
		else
			local w = mo.nextwaypoint or path[mo.waypoint.health+1]
			if w == nil
				mo.momx = 0
				mo.momy = 0
				mo.momz = 0
				player.playpath = nil
				return
			end
			mo.nextwaypoint = w
			local angle = R_PointToAngle2(mo.x, mo.y, w.x, w.y)
			local dist = R_PointToDist2(mo.x, mo.y, w.x, w.y)
			local aiming = R_PointToAngle2(0, mo.z, dist, w.z)
			mo.angle = angle
			player.aiming = aiming
			local speed = FRACUNIT*(player.pathspeed or 6)
			P_InstaThrust(mo, angle, P_ReturnThrustX(nil, aiming, speed))
			mo.momz = P_ReturnThrustY(nil, aiming, speed)
			if dist < speed
				setPlayerWaypoint(player,w)
			end
		end
		return
	end
	-- Reset touched waypoint for next frame
	mo.waypoint = nil
end)

COM_AddCommand("playpath", function(player, index, speed)
	if not(player) return end
	index = tonumber($)
	if index
		player.playpath = MapPaths[index]
		L_SelectPath(player.playpath)
		player.pathspeed = speed
		player.realmo.waypoint = nil
	end
end, COM_ADMIN)

local getNearestWaypoint = function(mo, type)
-- 	local tags = {...}
	local nearway
	local neardist
	for _,path in ipairs(MapPaths) do
		if type and path.type != type
			continue
		end
		for _,way in ipairs(path) do
			if not P_CheckSight(mo, way)
				continue
			end
			local dist = R_PointToDist2(0, mo.z, R_PointToDist2(mo.x, mo.y, way.x, way.y), way.z)
			if not nearway
			or dist < neardist
				neardist = dist
				nearway = way
			end
		end
	end
	return nearway, neardist
end
COM_AddCommand("nearestWaypoint", function(player, type)
	if type == nil
		print("getnearestwaypoint <path type>")
		return
	end
	local mo = player.realmo or player.mo
	local way,dist = getNearestWaypoint(mo, type)
	if way
		print("Found Path "..way.path.index.." Waypoint "..way.health..", Distance "..dist/FRACUNIT)
	else
		print("No nearby waypoints found.")
	end
end, COM_LOCAL)
rawset(_G, "L_GetNearestWaypoint", getNearestWaypoint) -- args(refmo, pathtype)