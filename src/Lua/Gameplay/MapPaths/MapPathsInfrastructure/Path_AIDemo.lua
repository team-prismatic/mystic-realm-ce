-- This creates and exposes functionality for a bot which will follow paths according to a player's instructions

if MapPathsVersionInfo.loaded -- Duplicate prevention
or MapPathsVersionInfo.editing == false -- This AI is intended strictly to assist with path editing. However, feel free to pull or modify the code for your own projects.
	return
end

local pathplayer
-- Net synchronize
addHook("NetVars", function(net)
	pathplayer = net($)
end)
-- Spawn and register path bot
local createPathBot = function()
	if not(pathplayer and pathplayer.valid)
		local bottype = gametyperules & GTR_RINGSLINGER and BOT_MPAI or BOT_2PAI
		pathplayer = G_AddPlayer('tails', nil, "Path Bot", bottype)
		if pathplayer == nil
			print("Could not create path-following AI player. (Has the player limit been reached?)")
			return false
		end
	end
	pathplayer.MapPath_AI = {}
	return true
end

local warpPathBot = function(way)
	if not createPathBot()
		return
	end
	if way == nil
		return true
	end
	pathplayer.MapPath_AI = {
		teleport = way,
		wayhist = nil,
		nextway = nil,
		route = nil,
		blacklist = nil,
		whitelist = nil
	}
	return true
end
local startPathBot = function(way1, way2, ...)
	if warpPathBot(way1)
		if way1 == nil
			way1 = L_GetNearestWaypoint(pathplayer.realmo or pathplayer.mo, "ai")
			if not way1
				print("Could not find any waypoints near the AI.")
				return
			end
		end
		local map = L_GetWaypointRoutes(way1, ...)
		local route
		if map[way2]
			route = map[way2].sequence
			print("An AI has been sent to find the waypoint.")
			pathplayer.MapPath_AI.route = route
			pathplayer.MapPath_AI.blacklist = {...}
		else
			print("Could not find a route to Path #"..way2.path.index.." Route #"..way2.health.." from this position.")
		end
	end
end
COM_AddCommand('pathbot', function(player, path1, way1, path2, way2, ...)
	if path1 and not tonumber(path1)
		if path1 == "remove" 
			if pathplayer and pathplayer.valid
				G_RemovePlayer(#pathplayer)
				return
			end
		elseif path1 == "skin"
			createPathBot()
			if tonumber(way1) != nil
				R_SetPlayerSkin(pathplayer, tonumber(way1))
			elseif way1
				R_SetPlayerSkin(pathplayer, way1)
			else
				CONS_Printf(player, "No skin specified.")
			end
			return
		else
			CONS_Printf(player, "Invalid argument "..path1)
		end
		return
	end
	path1 = tonumber($)
	way1 = tonumber($) or 1
	path2 = tonumber($)
	way2 = tonumber($) or 1
	if not (path1 and way1 and path2 and way2)
		CONS_Printf(player, "pathbot <path1> <waypoint1> <path2> <waypoint2> <path tags>")
		CONS_Printf(player, "OR pathbot <remove>")
		CONS_Printf(player, "OR pathbot skin <skin>")
		return
	end
	if path1 < 1 or not MapPaths[path1]
		CONS_Printf(player, "Path "..path1.." out of bounds. (Max "..#MapPaths..")")
		return
	end
	if path2 < 1 or not MapPaths[path2]
		CONS_Printf(player, "Path "..path2.." out of bounds. (Max "..#MapPaths..")")
		return
	end
	path1 = MapPaths[$]
	path2 = MapPaths[$]
	if way1 > #path1
		CONS_Printf(player, "Waypoint "..way1.." out of bounds. (Max "..#path1..")")
		return
	end
	if way2 > #path2
		CONS_Printf(player, "Waypoint "..way2.." out of bounds. (Max "..#path2..")")
		return
	end
	if path1.type != "ai"
		CONS_Printf(player, "Path #"..path1.index.." is not an ai type path. Please change the type to ai using the editpath command.")
		return
	end
	if path2.type != "ai"
		CONS_Printf(player, "Path #"..path2.index.." is not an ai type path. Please change the type to ai using the editpath command.")
		return
	end
	way1 = path1[$]
	way2 = path2[$]
	startPathBot(way1, way2, ...)
end, COM_ADMIN)

local tagFunc = {
	jump = function(player, way1, way2)
		if P_IsObjectOnGround(player.mo) and not(player.pflags & PF_JUMPDOWN)
		or not P_IsObjectOnGround(player.mo) and player.pflags & PF_JUMPDOWN
			player.cmd.buttons = $|BT_JUMP
		end
	end,
	spin = function(player, way1, way2)
		if player.pflags & (PF_SPINDOWN|PF_SPINNING)
			return
		end
		if P_IsObjectOnGround(player.mo) and not(player.pflags & PF_JUMPDOWN)
			player.cmd.buttons = $|BT_SPIN
		end
	end,
	spindash = function(player, way1, way2)
		if player.pflags & PF_SPINNING
			if player.pflags & PF_STARTDASH and player.dashspeed < player.maxdash
				-- Charge spindash to completion
				player.cmd.buttons = $|BT_SPIN
			end
			return
		end
		if not(player.pflags & PF_STARTDASH)
			if R_PointToDist2(player.mo.x, player.mo.y, way1.x, way1.y) > player.mo.scale * 8
				-- Get into position
				player.cmd.angleturn = R_PointToAngle2(player.mo.x, player.mo.y, way1.x, way1.y) >> 16
			else
				-- Prepare to spindash
				player.cmd.forwardmove = 0
				if player.speed < player.mo.scale * 2 and not(player.pflags & PF_SPINDOWN)
				and P_IsObjectOnGround(player.mo)
					player.cmd.buttons = $|BT_SPIN
				end
			end
		end
	end,
}

local pathBotAI = function(player, cmd)
	if leveltime < 5 and player.MapPath_AI
		player.MapPath_AI = {}
	end
	if gametyperules & GTR_SPECTATORS and not pathplayer.ctfteam
		COM_BufInsertText(server, "serverchangeteam "..#pathplayer.." 1")
	end
	local mo = player.mo
	if not(mo)
		return
	end

	local ai = player.MapPath_AI
	-- Initial warp sequence
	if ai.teleport
		local t = ai.teleport
		P_TeleportMove(mo, t.x, t.y, t.z+FRACUNIT)
		ai.teleport = false
		mo.waypoint = t
		ai.wayhist = nil
		ai.nextway = nil
		return
	end
	-- All further instructions apply to route travelling
	if not (ai.route and #ai.route)
		return true
	end
	-- Waypoint seeking
	local a,b = mo.waypoint
	if ai.wayhist != a and a != nil
		-- Log waypoint history
		ai.wayhist = a
		if ai.route[1] == a
			table.remove(ai.route, 1)
		end
-- 		print(player.name.." has reached waypoint "..a.path.index.."."..a.health..".")
		-- Find next waypoint
		ai.nextway = ai.route[1]
-- 		if ai.nextway
-- 			print("Now searching for "..ai.nextway.path.index..'.'..ai.nextway.health)
-- 		end
	end
	b = ai.nextway
	local backtrack
	if not b
		if not #ai.route
			print(player.name.." has completed the path!")
			ai.route = nil
		else
			ai.wayhist = nil
			ai.nextway = ai.route[1]
		end
		return true
-- 	elseif a.path == b.path
-- 		backtrack = a.health > b.health
	end
		
	-- Direction
	cmd.angleturn = R_PointToAngle2(mo.x, mo.y, b.x, b.y)>>16
	-- Control
	if b.x+b.radius > mo.x and b.x-b.radius < mo.x
	and b.y+b.radius > mo.y and b.y-b.radius < mo.y
		cmd.forwardmove = 25
	else
		cmd.forwardmove = 50
	end
	
	if a and b
		local tags = L_GetWaypointTags(a,b)
		for n,m in ipairs(tags) do
			if type(tagFunc[m]) == "function"
				tagFunc[m](player, a, b)
			end
		end
	end
end

addHook("BotTiccmd", function(player, cmd)
	if player != pathplayer
		return
-- 	elseif player.playerstate == PST_DEAD
-- 		if leveltime & 1
-- 			cmd.buttons = $|BT_JUMP
-- 		end
-- 		return true
	end
	for p in players.iterate() do
		if not(p.bot)
			player.botleader = p
			break
		end
	end
	pathBotAI(player, cmd)
	return true
end)

addHook("BotRespawn", function(player, bot)
	if bot.player.MapPath_AI and bot.player.playerstate == PST_LIVE
		return false
	end
end)

rawset(_G,"L_WarpPathBot",warpPathBot)
rawset(_G,"L_StartPathBot",startPathBot)
rawset(_G,"L_GetPathBot",do
	return pathplayer
end)