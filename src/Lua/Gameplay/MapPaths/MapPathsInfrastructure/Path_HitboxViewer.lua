if MapPathsVersionInfo.loaded -- Duplicate prevention
or MapPathsVersionInfo.editing == false
	return true
end

freeslot("mt_hitbox","spr_htbx","s_hitbox","mt_pathedit")
mobjinfo[MT_HITBOX] = {
	spawnstate = S_HITBOX,
	flags = MF_SCENERY|MF_NOTHINK|MF_NOBLOCKMAP
}

states[S_HITBOX] = {
	sprite = SPR_HTBX,
	frame = A,
	tics = -1,
}

mobjinfo[MT_PATHEDIT] = {
	spawnstate = S_HITBOX,
	flags = MF_SCENERY|MF_NOBLOCKMAP|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_NOGRAVITY
}

local CV_Distance = CV_RegisterVar{
	name = "waypointhitbox_distance",
	defaultvalue = 320,
	flags = CV_NETVAR,
	PossibleValue = {MIN = 0, MAX = 5120}
}

local Hitboxes = {}

local Vertices = {}

local Faces = {}


local CanSeeHitbox = function(mo)
	if L_GetSelectedPath() != mo.path return false end
	local dist = R_PointToDist(mo.x,mo.y)
	if mo.editing == false and dist > CV_Distance.value*FRACUNIT 
		return false
	end
	if mo.type == MT_PLAYER and dist < FRACUNIT*72 then return false end
	return true
end

local DoVertex = function(mo)
	//Object does not exist anymore
	if not(mo and mo.valid) then return nil end
	//Target does not exist anymore
	local target = mo.target
	if not(target and target.valid) then
		P_RemoveMobj(mo)
	return nil end
	//Hitbox is too far away
	if not(CanSeeHitbox(target)) then
		P_RemoveMobj(mo)
	return nil end
	//Hitbox requires updating
	if target.hitbox_updated then
		P_RemoveMobj(mo)
	return nil end
	//All checks passed. Updating position
	P_TeleportMove(mo,target.x+FixedMul(mo.xo,target.scale),target.y+FixedMul(mo.yo,target.scale),target.z+FixedMul(mo.zo,target.scale))
	target.vertices_exist = true //Confirming to our target object that vertices still exist

	//Do color coding
	mo.color = target.color
	
	return mo
end

local MakeBox = function(mo)
	if not(mo and mo.valid) return end
	local face
	for n = 1, 4 do
		local angle = FixedAngle((90*n)<<FRACBITS)
		local x = FixedMul(cos(angle),mo.radius)
		local y = FixedMul(sin(angle),mo.radius)
		face = P_SpawnMobjFromMobj(mo, x, y, 0, MT_HITBOX)
		face.xo = x
		face.yo = y
		face.zo = 0
		face.angle = angle+ANGLE_90
		face.renderflags = $|RF_PAPERSPRITE
		face.frame = 1|FF_TRANS80
		face.spritexscale = mo.radius
		face.spriteyscale = mo.height>>1
-- 		face.flags2 = $|MF2_SHADOW
		face.target = mo
		table.insert(Faces,face)
		DoVertex(face)
	end
	for n = 1, 2 do
		local z = mo.height * (n&1)
		face = P_SpawnMobjFromMobj(mo, 0, 0, z, MT_HITBOX)
		face.renderflags = $|(RF_FLOORSPRITE|RF_NOSPLATBILLBOARD)
		face.xo = 0
		face.yo = 0
		face.zo = z
		face.frame = 2|FF_TRANS90
		face.spritexscale = mo.radius
		face.spriteyscale = mo.radius
-- 		face.flags2 = $|MF2_SHADOW
		face.target = mo
		table.insert(Faces,face)
		DoVertex(face)
	end
end

local UpdateHitbox = function(mo)
	if not(mo and mo.valid) then return nil end
	if mo.vertices_exist then
		mo.vertices_exist = false //Reset the check for next frame
	return true end
	mo.hitbox_updated = false
	if not(CanSeeHitbox(mo)) then return true end

	MakeBox(mo)
	
	return true
end

addHook("MobjSpawn",function(mo)
	Hitboxes[#Hitboxes+1] = mo
	mo.vertices_exist = false
end,MT_MAPWAYPOINT)

local duration = TICRATE
local pathMovement = function(start, finish, color, offset)
	local dist = R_PointToDist2(start.x, start.y, finish.x, finish.y)
	local zdist = abs(start.z - finish.z)
	if dist+zdist == 0
		return
	end
	local lerp = position
	local angle = R_PointToAngle2(start.x, start.y, finish.x, finish.y)
	local zangle = R_PointToAngle2(0, start.z, dist, finish.z)
	local dot = P_SpawnMobjFromMobj(start, 0, 0, offset, MT_PATHEDIT)
	dot.fuse = duration
	dot.color = color
	local path = L_GetSelectedPath()
	local speed = R_PointToDist2(0, 0, dist, zdist)/duration
	P_InstaThrust(dot, angle, P_ReturnThrustX(nil, zangle, speed))
	dot.momz = P_ReturnThrustY(nil, zangle, speed)
end

local position = 0
addHook("PostThinkFrame",function()
	if displayplayer == nil or displayplayer.realmo == nil
		return 
	end
	local path = L_GetSelectedPath()
	if path == nil -- Run a sanity check (no path means no hitboxes to display)
		while #Faces do
			if Faces[1].valid
				P_RemoveMobj(Faces[1])
			end
			table.remove(Faces,1)
		end
		return
	end

	//Update faces
	local queue = Faces
	Faces = {}
	local size = #queue
	for n = 1, size do
		local face = queue[n]
		if DoVertex(face) then
			Faces[#Faces+1] = face
		end
	end
	
	//Update hitboxes
	queue = {}
	for n = 1,#Hitboxes do
		queue[#queue+1] = Hitboxes[n]
	end
	Hitboxes = {}
	local size = #queue
	for n = 1, size do 
		local hitbox = queue[n]
		if UpdateHitbox(hitbox) then
			Hitboxes[#Hitboxes+1] = hitbox
		end
	end
	// Add path movement indicators
	if path and leveltime % 12 == 0
		for _,path in ipairs(MapPaths)
			for n,m in ipairs(path) do
				-- Exit motion
				if #m.exits
					for nn,mm in ipairs(m.exits) do
						pathMovement(m, mm, mm.color, FRACUNIT*40)
					end
				end
				if n == #path
					continue
				end
				-- Next waypoint motion
				pathMovement(m, path[n+1], SKINCOLOR_JET, FRACUNIT*40)
			end
			if path.backside
				for n,m in ipairs(path) do
					if n == #path
						continue
					end
					-- Previous waypoint motion
					pathMovement(path[n+1], m, SKINCOLOR_BLACK, FRACUNIT*32)
				end
			end
		end
	end
end)

addHook("MapChange",function()
	Hitboxes = {}
	Faces = {}
end)

