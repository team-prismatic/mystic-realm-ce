-- This lump handles the writing of paths and waypoints into a lua script, to be later stored inside a wad or pk3 for loading.

local ver = MapPathsVersionInfo
if ver.loaded -- Duplicate prevention
	return
end

rawset(_G, "L_GetInternalMapPaths", function(map)
	return "mappaths_"..tostring(G_BuildMapName(map)).."_"..tostring(G_BuildMapTitle(map))
end)

-- Filepath builder
local filePath = function(map, path)
	local str = path or "server/waypoints/lua"
	str = $.."/"..L_GetInternalMapPaths(map)..".txt"
	return string.lower(str)
end

-- Write to file
local fracunit = function(number)
	return tostring(number>>FRACBITS).." * FRACUNIT"
end
local list = function(...)
	local t = {...}
	local str = ""
	for n,m in ipairs(t) do
		if n != 1
			str = $..", "
		end
		str = $..tostring(m)
	end
	return str
end
local quote = function(arg)
	return "'"..tostring(arg).."'"
end
local strTags = function(str, tags, suffix)
	str = $ or ""
	for n,m in ipairs(tags) do
		if n != 1
			str = $..", "
		end
		str = $..quote(m)
	end
	if #tags == 0
		str = $.."nil"
	end
	return str..tostring(suffix or "")
end
local asgn = function(var, val, suffix)
	return tostring(var).." = "..tostring(val)..tostring(suffix or "")
end
local buildLines = function(file)
	-- Header
	file:write("\-\- This file was generated using the in-game editor for MapPaths "..MapPathsVersionInfo.string.."\n")
	file:write("\-\- This is a lua file, NOT a text file! Place the file in the Lua directory of your .pk3 file, or name it as LUA_xxxx if you are using a .wad file.\n")
	file:write("\-\- If you change your map's name or number, the game won't find this file when loading the map. Edit the mapname and mapnumber (located below) to keep this script up to date.\n\n")
	file:write("local mapname = '"..G_BuildMapTitle(gamemap).."'\n")
	file:write("local mapnumber = '"..G_BuildMapName(gamemap).."'\n\n")
	file:write("rawset(_G,'mappaths_'..string.lower(mapnumber)..'_'..string.lower(mapname), do\n")
	file:write("	if MapPathsVersionInfo.compat > "..ver.compat.."\n")
	file:write("	or MapPathsVersionInfo.backcompat < "..ver.backcompat.."\n")
	file:write("		print('\\x85'..'Error:\\x80 Could not load mappaths from lump; an incompatible version of MapPaths is currently loaded (lump: "..ver.string..", mod: '..MapPathsVersionInfo.string..')')\n")
	file:write("		return false\n")
	file:write("	end\n\n")
	file:write("	L_SilencePathMessages(true)\n")
	file:write("	while #MapPaths > 0 do\n")
	file:write("		L_DeletePath(MapPaths[1])\n")
	file:write("	end\n\n")
	-- Header Tags
	file:write("	\-\- Header Info\n")
	file:write(strTags("	L_AddTags(MapPaths.tags, ", MapPaths.tags, ")\n"))
	file:write(strTags("	L_AddTags(MapPaths.backtags, ", MapPaths.backtags, ")\n\n"))
	-- Paths
	for n,path in ipairs(MapPaths) do
		file:write("	\-\- Path #"..n.."\n")
		file:write(asgn("	local path", "L_CreatePath()", "\n"))
		file:write(asgn("	path.type",quote(path.type), "\n"))
		file:write(asgn("	path.series",path.series), "\n")
		file:write(asgn("	path.color",path.color), "\n")
		file:write(asgn("	path.backside",path.backside, "\n"))
		file:write(strTags("	L_AddTags(path.tags, ", path.tags, ")\n"))
		file:write(strTags("	L_AddTags(path.backtags, ", path.backtags, ")\n"))
		-- Waypoints
		for nn,mm in ipairs(path) do
			file:write("	L_CreateWaypoint(path, {"..list(
				asgn("x", fracunit(mm.x)), 
				asgn("y", fracunit(mm.y)), 
				asgn("z", fracunit(mm.z)), 
				asgn("radius", fracunit(mm.radius)), 
				asgn("height", fracunit(mm.height)), 
				asgn("extravalue1", mm.extravalue1), 
				asgn("extravalue2", mm.extravalue2), 
				strTags("tags = {", mm.tags,"}"),
				strTags("backtags = {", mm.backtags,"}")
			).."})\n")
		end
		file:write("\n")
	end
	-- Waypoint Exits
	file:write("\n".."	-- Waypoint Exits\n")
	for _,path in ipairs(MapPaths) do
		for _,way1 in ipairs(path) do
			for _,way2 in ipairs(way1.exits) do
				file:write("	L_AddWaypointExit(MapPaths["..path.index.."]["..way1.health.."], MapPaths["..way2.path.index.."]["..way2.health.."], "..strTags(nil, way1.exits[way2], nil)..")\n")
			end
		end
	end
	-- Closing
	file:write("\n	L_SelectPath() -\-\ Close the editor \n")
	file:write("	L_SilencePathMessages(false)\n")
	file:write("	print('Loaded internal paths lump for '..G_BuildMapName(gamemap))\n")
	file:write("	return true\n")
	file:write("end)")
end

local writeFile = do
	local filename = filePath(gamemap)
	local file = io.openlocal(filename, "w")
	buildLines(file, filename)
	print("\x83".."Wrote lua code for current map paths. Find the file at:\x80 "..filename)
	io.close(file)
end
COM_AddCommand('writepathlua',writeFile)