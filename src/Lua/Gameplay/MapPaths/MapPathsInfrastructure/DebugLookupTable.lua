

COM_AddCommand("gettable", function(player, ...)
	local base = MapPaths -- Define this local variable if you want to repurpose this script for a specific table or userdata.
	if ... == nil and base == nil
		print("Define a global table to look up. Once done, you can look into subtables by specifying them as additional arguments.")
		return
	end
	local variable = tostring(base)
	local value = base
	if variable == "nil"
		local v = ...
		variable = v
		value = rawget(_G, v)
		if not value
			print("Could not look up global variable "..v)
			return
		end
	end
	local str = ""
	local t = {...}
	for n,m in ipairs(t) do
		if not base and n == 1
			continue
		end
		m = tonumber($) or $
		if type(value) != "table" and type(value) != "userdata"
			print("Got as far as variable "..variable.." with value "..tostring(value))
			return
		end
		local old = value
		variable, value = m, $[m]
		if value and type(value) == "userdata" and old[value]
			value = old[$]
		end
	end
	if type(value) == "table"
		str = $..variable.."\n"
		for n,m in pairs(value) do
			str = $..tostring(n)..": "..tostring(m).."\n"
		end
	else
		str = $..variable..": "..tostring(value)
	end
	print(str)
end, COM_LOCAL)
