-- This lump builds the core infrastructure for paths and waypoints.
-- It also contains the bulk of console commands for editing, as well rawset functions for external/cross-file use.
-- If your mod needs access to the path infrastructure, you can find many of the rawset functions toward the bottom.

if MapPathsVersionInfo.loaded -- Duplicate prevention
	return
end

-- Map waypoint object.
freeslot("MT_MAPWAYPOINT")
mobjinfo[MT_MAPWAYPOINT].spawnstate = S_TEAMRING
mobjinfo[MT_MAPWAYPOINT].flags = $|MF_SPECIAL|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIP|MF_NOCLIPHEIGHT
mobjinfo[MT_MAPWAYPOINT].radius = 24*FRACUNIT
mobjinfo[MT_MAPWAYPOINT].height = 48*FRACUNIT

-- If your mod need access to MT_MAPWAYPOINT's TouchSpecial hook, you can overwrite this function to do so.
rawset(_G, "MapWaypointTouchSpecial", do end)
addHook("TouchSpecial", function(mo, pmo)
	MapWaypointTouchSpecial(mo, pmo)
	return true
end, MT_MAPWAYPOINT)


-- Global table. All paths and waypoints can be found in here.
rawset(_G,"MapPaths", {
	tags = {},
	backtags = {}
})

-- rawset(_G,"MAPWAY_FRONT", 1)
-- rawset(_G,"MAPWAY_BACK", 2)
local currentpath
local currentwaypoints = {}
local silent = false

-- Helper function for packing multiple instances into tables (If not already packed)
local pack = function(...)
	if ... and type(...) == "table" -- Already packed
		return ...
	else -- Needs packing
		return {...}
	end
end

-- Cleanup helper for removing bad entries
local cleanPaths = function()
	for _,path in ipairs(MapPaths) do
		for _,way in ipairs(path) do
			for _,t in ipairs({way.exits, way.entrances}) do
				local clean = 0
				while clean < #t do
					local mo = t[clean+1]
					if not mo.valid
						table.remove(t, clean+1)
						t[mo] = nil
					else
						clean = $+1
					end
				end
			end
		end
	end
end

-- Tagging functions.
local getStr = function(path)
	return path == MapPaths and "header"
		or "#"..tostring(path.index)
end
local listTags = function(tags, str)
	str = $ or "Current tags:"
	for _,tag in ipairs(tags) do
		str = $.." "..tag
	end
	return str
end
local addTags = function(tags, ...)
	-- Core function for adding tags to tables
	local count = 0
	local duplicate = 0
	for _,n in ipairs(pack(...)) do
		n = string.lower($)
		if not tags[n]
			table.insert(tags, n)
			tags[n] = true
			count = $+1
		else
			duplicate = $+1
		end
	end
	return count, duplicate
end
local addPathTags = function(path, ...)
	local count, duplicate = addTags(path.tags, ...)
	if not silent
		print("Added "..count.." tags to path "..getStr(path).." ("..duplicate.." duplicates ignored)")
	end
end
local addPathBacktags = function(path, ...)
	local count, duplicate = addTags(path.backtags, ...)
	if not silent
		print("Added "..count.." backtags to path "..getStr(path).." ("..duplicate.." duplicates ignored)")
	end
end
local addWaypointTags = function(waypoint, ...)
	local count, duplicate 
	for n, m in ipairs(pack(waypoint)) do
		count, duplicate = addTags(m.tags, ...)
	end
	if not silent
		print("Added "..count.." waypoint tags ("..duplicate.." duplicates ignored)")
	end
end
local addWaypointBacktags = function(waypoint, ...)
	local count, duplicate 
	for n, m in ipairs(pack(waypoint)) do
		count, duplicate = addTags(m.backtags, ...)
	end
	if not silent
		print("Added "..count.." waypoint backtags ("..duplicate.." duplicates ignored)")
	end
end
local removeTags = function(tags, ...)
	-- Core function for removing tags from tables
	local count = 0
	local unfound = 0
	for _,m in ipairs(pack(...)) do
		if tags[m]
			tags[m] = nil
			for nn, mm in ipairs(tags) do
				if mm == m
					table.remove(tags, nn)
					break
				end
			end
			count = $+1
		else
			unfound = $+1
		end
	end
	return count, unfound
end
local removePathTags = function(path, ...)
	local count, unfound = removeTags(path.tags, ...)
	if not silent
		print("Removed "..count.." tags from path "..getStr(path).." ("..unfound.." tags not found)")
	end
end
local removePathBacktags = function(path, ...)
	local count, unfound = removeTags(path.backtags, ...)
	if not silent
		print("Removed "..count.." backtags from path "..getStr(path).." ("..unfound.." tags not found)")
	end
end
local removeWaypointTags = function(waypoint, ...)
	local count, unfound
	for n, m in ipairs(pack(waypoint)) do
		count, unfound = removeTags(m.tags, ...)
	end
	if not silent
		print("Removed "..count.." tags ("..unfound.." tags not found)")
	end
end
local removeWaypointBacktags = function(waypoint, ...)
	local count, unfound
	for n, m in ipairs(pack(waypoint)) do
		count, unfound = removeTags(m.backtags, ...)
	end
	if not silent
		print("Removed "..count.." backtags ("..unfound.." tags not found)")
	end
end

-- Waypoint functions.
local getSequentialWaypoints = function(waypoints)
	-- This returns a table in which waypoints are ordered numerically.
	local old = {unpack(waypoints)}
	local new = {}
	while #old > 0 do
		local i = old[1]
		local sorted = false
		for n,m in ipairs(new) do
			if m.health > i.health
				table.insert(new, n, i)
				sorted = true
				break
			end
		end
		if not sorted
			table.insert(new, i)
		end
		table.remove(old, 1)
	end
	return new
end

local updateWaypointColor = function(way)
	if #way.exits or #way.entrances
		if not way.editing
			way.color = SKINCOLOR_ORANGE
		else
			way.color = SKINCOLOR_MINT
		end
	elseif #way.tags
		if not way.editing
			way.color = SKINCOLOR_SKY
		else
			way.color = SKINCOLOR_WHITE
		end
	else
		if not way.editing
			way.color = way.path.color
		else
			way.color = SKINCOLOR_YELLOW
		end
	end
end
local deselectWaypoint = function(way)
	if not way.valid return end
	way.editing = false
	updateWaypointColor(way)
end
local selectWaypoint = function(way)
	way.editing = true
	updateWaypointColor(way)
end
local updateWaypoints = function(path, start)
	for n = start or 0, #path do
		path[n].health = n
	end
end
local createWaypoint = function(path, t, pos)
	if pos == nil or pos > #path+1
		pos = #path+1
	end
	assert(path != nil, "Attempted waypoint creation without a valid path")
	local w = P_SpawnMobj(t.x, t.y, t.z, MT_MAPWAYPOINT)
	w.path = path
	w.tags = {}
	w.backtags = {}
	if t.tags
		addTags(w.tags, t.tags)
	end
	if t.backtags
		addTags(w.backtags, t.backtags)
	end
	for _,m in pairs(w.tags) do
		w.tags[m] = true
	end
	for _,m in pairs(w.backtags) do
		w.backtags[m] = true
	end
	w.radius = t.radius or mobjinfo[MT_MAPWAYPOINT].radius
	w.height = t.height or mobjinfo[MT_MAPWAYPOINT].height
	w.editing = false
	w.entrances = {}
	w.exits = {}
	w.extravalue1 = t.extravalue1 or 0
	w.extravalue2 = t.extravalue2 or 0
	if t.entrances
		for n,m in ipairs(t.entrances) do
			L_AddWaypointExit(m,w)
		end
	end
	if t.exits
		for n,m in ipairs(t.exits) do
			L_AddWaypointExit(w,m)
		end
	end
	if not currentpath
		w.flags2 = $|MF2_DONTDRAW
	end
	table.insert(path, pos, w)
	updateWaypoints(path, pos)
	updateWaypointColor(w)
	if not silent
		print("Inserted waypoint at position "..pos.." for path "..path.index)
	end
	return w
end
local adjustWaypointOrder = function(path, old, new)
	local way = path[old]
	new = min($,#path+1)
	local minimum = min(old,new)
	table.remove(path, old)
	table.insert(path, new, way)
	updateWaypoints(path, minimum)
	print("Reordered waypoint #"..old.." to #"..new)
end
local deleteWaypoints = function(path, ways)
	ways = getSequentialWaypoints($)
	local count = #ways
	while #ways > 0 do
		local way = ways[#ways]
		-- Unlink all associated exits/entrances
		for n,m in ipairs(way.entrances) do
			L_RemoveWaypointExit(m, way)
		end
		for n,m in ipairs(way.exits) do
			L_RemoveWaypointExit(way, m)
		end
		-- Remove the waypoint from its path
		table.remove(ways, #ways)
		local pos = way.health
		if way and way.valid
			P_RemoveMobj(way)
		end
		table.remove(path,pos)
		updateWaypoints(path, pos) -- Yes, we will be updating waypoint numbers every time we delete a single waypoint. It's not very efficient, but making sure the tables are properly calibrated at all times is far more important.
	end
	L_SelectWaypoints()
	if not silent
		print("Removed "..count.." waypoints from path #"..path.index)
	end
	cleanPaths()
end
local reverseWaypoints = function(path, pos1, pos2)
	local t = {}
	for n = pos1, pos2 do
		table.insert(t, path[n])
	end
	for n = pos1, pos2 do
		path[n] = t[#t]
		table.remove(t, #t)
	end
	updateWaypoints(path, pos1)
	print("Reversed path #"..path.index.." waypoints "..pos1.." through "..pos2)
end

-- Pathing functions.
local startPath = function(pathtype, series, backside, ...)
	local t = {
		type = pathtype or "",
		series = series or 1,
		backside = backside or false,
		tags = {},
		backtags = {},
		color = SKINCOLOR_BLUE
	}
	table.insert(MapPaths, t)
	L_SelectPath(t)
	currentpath.index = #MapPaths
	if not silent
		print("Created path "..currentpath.index)
	end
	addPathTags(currentpath, ...)
	if backside
		addPathBacktags(currentpath, ...)
	end
	return t
end
local deletePath = function(path)
	local pathindex = path.index
	if currentpath == path
		L_SelectPath() -- Deselect this path
	end
	deleteWaypoints(path,path) -- This deletes ALL waypoints inside the path
	table.remove(MapPaths, path.index) -- Path is removed from global listing
	-- Update the internal indices for each consecutive path
	for n = index or 1, #MapPaths do
		MapPaths[n].index = n
	end
	if not silent
		print("Removed path #"..pathindex)
	end
end
local copyPath = function(path1)
	-- Create new path
	local path2 = startPath(path1.type, path1.series+1 or -1, path1.backside, unpack(path1.tags))
	path2.backtags = {unpack(path1.backtags)}
	silent = true
	-- Create waypoints for new path
	for n,m in ipairs(path1) do
		createWaypoint(path2, {
			x = m.x,
			y = m.y,
			z = m.z,
			tags = m.tags,
			backtags = m.backtags,
			radius = m.radius,
			height = m.height,
			exits = m.exits,
			entrances = m.entrances
		}, tonumber(n))
	end
	silent = false
	print("Created new path #"..path2.index.." from path #"..path1.index..".")

	return path2
end
local splitPath = function(path1, split, connect)
	silent = true
	-- Copy Path
	local path2 = copyPath(path1)
	-- Remove waypoints from each path
	local kill1 = {}
	for n = split, #path1 do
		table.insert(kill1, path1[n])
	end
	local kill2 = {}
	for n = 1, split-1 do
		table.insert(kill2, path2[n])
	end
	deleteWaypoints(path1, kill1)
	deleteWaypoints(path2, kill2)
	if connect != false
		L_AddWaypointExit(path1[split-1], path2[1])
	end
	silent = false
	print("Split path #"..path1.index.." and #"..path2.index.." at waypoint #"..split..".")
end
local mergePaths = function(...)
	local t = pack(...)
	local inheritor = t[1] -- Path to inherit all waypoints
	for n = 2, #t do
		local path = t[n]
		-- Copy waypoints over
		for nn,mm in ipairs(path) do
			createWaypoint(inheritor, {
				x = mm.x,
				y = mm.y,
				z = mm.z,
				tags = mm.tags,
				backtags = mm.backtags,
				radius = mm.radius,
				height = mm.height
			})
		end
		deletePath(path)
	end
end

-- Routing waypoints to other paths
local addWaypointExit = function(way1, way2, ...)

	if way1 == way2 -- Don't even try it.
		print("Can't link "..way1.health.." into itself")
		return false
	end
	-- Check if exists
	for n,m in ipairs(way1.exits) do
		if m == way2
			print("Waypoint "..way1.health.." is already linked to Path "..way2.path.index.." Waypoint "..way2.health)
			removeTags(way1.exits[way2], unpack(way1.exits[way2]))
			addTags(way1.exits[way2], ...)
			print("Updated exit with new tags")
			return false
		end
	end
	-- Insert
	table.insert(way1.exits, way2)
	table.insert(way2.entrances, way1)
-- 	way1.exits[way2] = {...}
	way1.exits[way2] = {}
	addTags(way1.exits[way2], ...)
	way2.entrances[way1] = way1.exits[way2]
	updateWaypointColor(way1)
	updateWaypointColor(way2)
	return true
end
local removeWaypointExit = function(way1, way2)
	if way1 == way2 -- Pointless
		print("Can't unlink "..way1.health.." from itself (not linked)")
		return false
	end
	local path1
	if not (way1 and way1.valid)
		return
	end
	for n,m in ipairs(way1.exits) do
		if m == way2
			-- Remove
			table.remove(way1.exits, n)
			way1.exits[m] = nil
-- 			print('Removed table entries from exits')
			-- Now that we've confirmed there was an exit, it also makes sense to check way2's entrances
			for nn, mm in ipairs(m.entrances) do
				if mm == way1
-- 					print('Removed table entries from entrances')
					table.remove(m.entrances, nn)
					way2.entrances[mm] = nil
					updateWaypointColor(way2)
				end
			end
			updateWaypointColor(way1)
			return true
		end
	end
	print("Waypoint "..way1.health.." is not linked to Path "..way2.path.index.." Waypoint "..way2.health)
	return false
end

local addQueue = function(t,n,m, blacklist, tags)
	if not m
		return false
	end
	if blacklist and tags -- Don't let this waypoint into the queue if its tags appear in the blacklist.
		for n,m in ipairs(blacklist) do
			if tags[m]
				return false
			end
		end
	end
	
	if not t[m] -- Add this waypoint to the queue, if not already inserted
		table.insert(t,m)
	end
	-- Calculate distance
	local zdist = abs(m.z-n.z)
	local dist = zdist + R_PointToDist2(n.x,n.y,m.x,m.y) 
	dist = zdist + $ + (t[n] and t[n].dist or 0) -- Add the connecting waypoints' total travel distance

	if t[m] == nil -- New waypoint
	or dist < t[m].dist -- Closer waypoint
		t[m] = {dist = dist, sequence = {}} -- Construct data table for this waypoint
		-- Create sequence information for this waypoint
		for nn,mm in ipairs(t[n].sequence) do
			table.insert(t[m].sequence, mm)
		end
		table.insert(t[m].sequence, m)
	end
	return true
end
local searchRoute = function(way, ...)
	local black = pack(...)
	local queue = {}
	addQueue(queue, way, way)
	while #queue do
		way = queue[1]
		local path = way.path
		addQueue(queue, way, path[way.health+1], black, way.tags)
		if path.backside
			addQueue(queue, way, path[way.health-1], black, way.backtags)
		end
		for n,m in ipairs(way.exits) do
			addQueue(queue, way, m, black, way.exits[m])
		end
		table.remove(queue, 1)
	end
	return queue
end

-- Clear paths on mapload.
addHook("MapChange", do
	while #MapPaths > 0 do
		table.remove(MapPaths, 1)
	end
	removeTags(MapPaths.tags)
	removeTags(MapPaths.backtags)
	currentpath = nil
	currentwaypoints = {}
end)

-- Net synchronize
addHook("NetVars", function(net)
	MapPaths = net($)
	currentpath = net($)
	currentwaypoints = net($)
	silent = net($)
end)

-- Global functions for external / cross-file use
rawset(_G,"L_GetSelectedPath", do -- Get the editor's current path selection
	return currentpath
end)
rawset(_G,"L_GetSelectedWaypoints", do -- Get the editor's current waypoint selection (returns a table)
	return currentwaypoints
end)
rawset(_G,"L_SelectPath",function(path) -- Selects a path in the editor. A nil argument closes the editor.
	L_SelectWaypoints()
	if currentpath
		for _,m in ipairs(currentpath) do
			m.flags2 = $|MF2_SHADOW
		end
	end
	currentpath = path
	if currentpath
		for _,m in ipairs(currentpath) do
			m.flags2 = $&~MF2_SHADOW
		end
		for _,m in ipairs(MapPaths) do
			for _,mm in ipairs(m) do
				mm.flags2 = $&~MF2_DONTDRAW
			end
		end
	else
		for _,m in ipairs(MapPaths) do
			for _,mm in ipairs(m) do
				mm.flags2 = $|MF2_DONTDRAW
			end
		end		
	end
end)
rawset(_G,"L_SelectWaypoints",function(...) -- Selects waypoints in the editor.
	for _,way in ipairs(currentwaypoints) do
		deselectWaypoint(way)
	end
	local ways = pack(...)
	currentwaypoints = ways
	for _,way in ipairs(currentwaypoints) do
		selectWaypoint(way)
	end
	return #currentwaypoints
end)
rawset(_G,"L_MultiselectWaypoints", function(...) -- Selects additional waypoints in the editor.
	local count = 0
	for _,new in ipairs(pack(...)) do
		local present = false
		for _,old in ipairs(currentwaypoints) do
			if new == old
				present = true
				break
			end
		end
		if not present
			table.insert(currentwaypoints, new)
			selectWaypoint(new)
			count = $+1
		end
	end
	return count
end)
rawset(_G,"L_DeselectWaypoints",function(...) -- Deselects specified waypoints in the editor.
	local count = 0
	for _,d in ipairs(pack(...)) do
		for n,m in ipairs(currentwaypoints) do
			if d == m
				if m.valid
					deselectWaypoint(m)
				end
				table.remove(currentwaypoints, n)
				count = $+1
			end
		end
	end
	return count
end)
rawset(_G,"L_CreatePath",startPath) -- args(pathtype, series, enablebackside, tags...). Automatically selects the path for editing; Use L_SelectPath() afterward to keep the editor closed.
rawset(_G,"L_DeletePath",deletePath) -- args(path). The global table and all paths' corresponding indices are adjusted in response.
rawset(_G,"L_CreateWaypoint",createWaypoint) -- args(path, {x,y,z,tags,backtags,radius,height}, position). Creates a waypoint on the selected path.
rawset(_G,"L_DeleteWaypoints",deleteWaypoints) -- args(path, waypoints)
rawset(_G,"L_AddTags",addTags) -- args(tags_table, ...) Tags for both paths and waypoints are stored in tables "tags" and "backtags" under each instance.
rawset(_G,"L_RemoveTags",removeTags) -- args(tags_table, ...) Removes all tags specified, if found.
rawset(_G,"L_SilencePathMessages",function(arg) -- args(true/false) Silences or unsilences console messages associated with the editor.
	silent = arg
end)
rawset(_G,"L_AddWaypointExit", addWaypointExit) -- args(waypoint1, waypoint2, tags...)
rawset(_G,"L_RemoveWaypointExit", removeWaypointExit) --args (waypoint1, waypoint2)
rawset(_G,"L_GetWaypointRoutes", searchRoute) -- args(waypoint, {blacklist tags})
rawset(_G,"L_GetWaypointTags", function(way1, way2) -- Gets the tags table to be used between these two waypoints. 
	if way1.path == way2.path
		local n = way2.health - way1.health
		if n == 1
			return way1.tags
		elseif n == -1
			return way1.backtags
		end
	end
	return way1.exits[way2] or {}
end)

-- Console commands.
if MapPathsVersionInfo.editing == false
	return
end
local needCurrentPath = function(player)
	if currentpath == nil
		CONS_Printf(player, "You must create or select a path first. (use 'selectpath')")
		return true
	end
	return false
end
local needCurrentWaypoints = function(player)
	if needCurrentPath(player)
		return true
	elseif #currentwaypoints == 0
		CONS_Printf(player, "You must create or select a waypoint first.")
		return true
	end
	return false
end
COM_AddCommand("addwaypoint", function(player, pos)
	pos = tonumber($)
	if needCurrentPath(player)
		return
	end
	local mo = player.realmo
	local t = {
		x = FixedRound(mo.x),
		y = FixedRound(mo.y),
		z = FixedRound(mo.z)
	}
	createWaypoint(currentpath, t, pos)
end, COM_ADMIN)
COM_AddCommand("selectwaypoints", function(player, mode, pos1, pos2)
	if needCurrentPath(player)
		return
	elseif mode == nil
		CONS_Printf(player, "selectwaypoints <s/m/d/a/n> <starting waypoint> <ending waypoint>\n"
			.."s: Select waypoints and deselect any previous waypoints.\n"
			.."m: Select waypoints while keeping previous waypoints selected.\n"
			.."d: Deselect the specified waypoints.\n"
			.."a: Select all waypoints on the current path.\n"
			.."n: Deselect all waypoints.")
	elseif (mode == "s" or mode == "m" or mode == "d") and pos1 == nil
		CONS_Printf(player, "Specify a waypoint or range of waypoints. (Path length: "..#currentpath..")")
	elseif mode == "a"
		local t = {}
		for _,way in ipairs(currentpath) do
			table.insert(t, way)
		end
		L_SelectWaypoints(t)
		print("Selected all waypoints.")
	elseif mode == "n"
		L_SelectWaypoints()
		print("Deselected all waypoints.")
	else
		-- Interpret position values
		pos1 = tonumber($)
		pos2 = tonumber($)
		pos2 = $ or pos1 -- Secure nil/zero values
		if pos2 and pos2 < pos1 -- Fix backwards range
			local p1,p2 = pos2,pos1
			pos1,pos2 = p1,p2
		end
		-- Assemble waypoints
		local t = {}
		for n = pos1,pos2 do
			table.insert(t, currentpath[n])
		end
		-- Do select mode operations
		if mode == "s"
			print("Selected "..L_SelectWaypoints(t).." waypoints.")
		elseif mode == "m"
			print("Selected "..L_MultiselectWaypoints(t).." additional waypoints.")
		elseif mode == "d"
			print("Deselected "..L_DeselectWaypoints(t).." waypoints.")
		else
			CONS_Printf(player, "Invalid select mode: "..mode)
		end
	end
end, COM_ADMIN)
local variables = {
	x = "fracunit",
	y = "fracunit",
	z = "fracunit",
	radius = "positivefracunit",
	height = "positivefracunit",
	health = "health",
	tags = "tags",
	backtags = "tags",
	extravalue1 = "integer",
	extravalue2 = "integer",
}
COM_AddCommand("editwaypoints", function(player, variable, value)
	if needCurrentWaypoints(player)
		return
	end
	if variable == nil
		CONS_Printf(player, "editwaypoint <variable> <value>")
		return
	end
	local ways = L_GetSelectedWaypoints()
	for _,way in pairs(ways) do
		if way[variable] == nil
			CONS_Printf(player, "Invalid variable "..variable)
			return
		end
		local form = variables[variable]
		if form == nil
			CONS_Printf(player, "Can't edit this variable directly.")
			return
		end
		if form == "health"
			CONS_Printf(player, "Can't edit this variable here (use reorderwaypoints instead)")
			return
		end
		if form == "tags"
			CONS_Printf(player, "Can't edit this variable here (use edittags instead)")
			return
		end

		if value == nil
			local str
			if form == "fracunit" or form == "positivefracunit"
				str = tostring(way[variable]/FRACUNIT).." fracunits"
			else
				str = tostring(way[variable])
			end
			CONS_Printf(player, variable.." = "..str)
			return
		end
		if form == "fracunit" or form == "positivefracunit" or form == "integer" or form == "positiveinteger"
			value = tonumber($)
			if value == nil
				CONS_Printf(player, "Value must be an integer")
				return
			elseif (form == "positivefracunit" or form == "positiveinteger") and value < 1
				CONS_Printf(player, "Value must be a positive integer")
				return
			elseif form == "fracunit" or form == "positivefracunit"
				if variable == "x"
					P_TeleportMove(way, value*FRACUNIT, way.y, way.z)
				elseif variable == "y"
					P_TeleportMove(way, way.x, value*FRACUNIT, way.z)
				else
					way[variable] = value*FRACUNIT
				end
				if variable == "radius" or variable == "height"
					way.hitbox_updated = true
				end
			elseif form == "integer" or form == "positiveinteger"
				way[variable] = value
			end
		elseif form == "string"
			way[variable] = value
		end
	end
end, COM_ADMIN)
COM_AddCommand("deletewaypoints", function(player)
	if needCurrentWaypoints(player)
		return
	end
	
	deleteWaypoints(currentpath, L_GetSelectedWaypoints())
end, COM_ADMIN)
COM_AddCommand("reorderwaypoints", function(player, newpos)
	newpos = tonumber($)
	if newpos == nil
		CONS_Printf(player, "movewaypoint <new position>\n"
			.."If multiple waypoints are selected, they will be reordered to directly follow each other in sequence.")
		return
	end
	if needCurrentWaypoints(player)
		return
	end
	local t = getSequentialWaypoints(currentwaypoints)
	local startingpos = t[1].health
	while #t > 0 do
		local oldpos = t[1].health
		adjustWaypointOrder(currentpath, oldpos, newpos)
		newpos = $+1
		table.remove(t, 1)
	end
end, COM_ADMIN)
COM_AddCommand("reversewaypoints", function(player, pos1, pos2)
	if needCurrentPath(player)
		return
	elseif tonumber(pos1) == nil and pos1 != "all"
	or tonumber(pos1) and tonumber(pos2) == nil
		CONS_Printf(player, "reversewaypoints <start> <end>\n".."Type\x84 reversewaypoints all\x80 to reverse all waypoints.")
	elseif pos1 == "all"
		reverseWaypoints(currentpath, 1, #currentpath)
	else
		reverseWaypoints(currentpath, tonumber(pos1), tonumber(pos2))
	end
end, COM_ADMIN)
COM_AddCommand("rotatewaypoints", function(player, rotangle)
	if tonumber(rotangle) == nil
		CONS_Printf(player, "rotatewaypoints <angle>\n".."player position is used as the centerpoint (use the go-to keys to warp to a specific point)")
		return
	end
	if needCurrentWaypoints(player)
		return
	end
	local p = player.realmo or player.mo
	rotangle = FixedAngle(-tonumber($) * FRACUNIT)
	for n,m in ipairs(currentwaypoints) do
		local wayangle = R_PointToAngle2(p.x, p.y, m.x, m.y)
		local waydist = R_PointToDist2(p.x, p.y, m.x, m.y)
		local angle = rotangle+wayangle
		local x = p.x + FixedRound(P_ReturnThrustX(nil, angle, waydist))
		local y = p.y + FixedRound(P_ReturnThrustY(nil, angle, waydist))
		P_TeleportMove(m, x, y, m.z)
	end
	print("Rotated waypoints by "..tostring(AngleFixed(-rotangle)>>FRACBITS).." degrees")
end, COM_ADMIN)
COM_AddCommand("flipwaypoints", function(player, axis, orientation)
	orientation = $ or "global"
	if axis == nil
		CONS_Printf(player, "flipwaypoints <x/y> <global/player/ctf> \n".."with all but ctf, player position is used as the centerpoint (use the go-to keys to warp to a specific point)")
		return
	end
	if orientation != "player" and orientation != "ctf" and orientation != "global"
		CONS_Printf(player,"invalid argument "..orientation)
		return
	end
	if axis != "x" and axis != "y"
		CONS_Printf(player,"invalid argument "..axis)
		return
	end
	if needCurrentWaypoints(player)
		return
	end
	-- Get centerpoint and orientation
	local p = player.realmo or player.mo
	if orientation == "ctf"
		local red,blue
		for mo in mobjs.iterate() do
			if mo.type == MT_BLUEFLAG
				blue = mo
				print('Found blue flag')
				if red break end
			elseif mo.type == MT_REDFLAG
				red = mo
				print('Found red flag')
				if blue break end
			end
		end
		if not (red and blue)
			CONS_Printf(player, "Could not find red and blue flags to measure against.")
			return
		end
		p = {}
		p.x = (blue.x + red.x) / 2
		p.y = (blue.y + red.y) / 2
		p.angle = R_PointToAngle2(blue.x, blue.y, red.x, red.y)
	end
	-- Get angle to flip from. x or y flips would be a simple if/else check, but for more nuanced orientations we'll need to perform angle logic.
	local flipangle = 0
	if orientation == "player" or orientation == "ctf"
		flipangle = p.angle
	end
	for n,m in ipairs(currentwaypoints) do
		local x,y = m.x - p.x, m.y - p.y
		local dist = R_PointToDist2(0,0,x,y)
		if flipangle
			local ang = R_PointToAngle2(0,0,x,y)
			x = P_ReturnThrustX(nil, ang-flipangle, dist)
			y = P_ReturnThrustY(nil, ang-flipangle, dist)
		end
		if axis == "x"
			x = -$
		else
			y = -$
		end
		if flipangle
			local ang = R_PointToAngle2(0,0,x,y)
			x = P_ReturnThrustX(nil, ang+flipangle, dist)
			y = P_ReturnThrustY(nil, ang+flipangle, dist)			
		end
		-- Get the angle of orientation so that we can adjust to the axis to flip.
		P_TeleportMove(m, p.x+x, p.y+y, m.z)
	end
	print("Flipped waypoints along the "..tostring(orientation).." "..axis.." axis")
end, COM_ADMIN)

COM_AddCommand("addwaypointexits", function(player, path2, way2, ...)
	path2 = tonumber($)
	way2 = tonumber($)
	if needCurrentWaypoints(player)
		return
	elseif not(path2 and way2)
		CONS_Printf(player, "editwaypointexits <to path> <to waypoint> <tags>")
		return
	elseif not(MapPaths[path2])
		CONS_Printf(player, "To path #"..path2.." out of bounds (Max "..#MapPaths..")")
		return
	elseif not(MapPaths[path2][way2])
		CONS_Printf(player, "To waypoint #"..way2.." out of bounds (Max "..#MapPaths[path2]..")")
		return
	end
	path2 = MapPaths[$]
	way2 = path2[$]
	for n,way1 in ipairs(currentwaypoints) do
		if addWaypointExit(way1, way2, ...)
			print("Created exit to path "..way2.path.index.." waypoint "..way2.health.." for exiting waypoint "..way1.health)
		end
	end
end)

COM_AddCommand("removewaypointexits", function(player, path2, way2)
	path2, way2 = tonumber($1), tonumber($2)
	if needCurrentWaypoints(player)
		return
	elseif not (path2 and way2)
		CONS_Printf(player, "editwaypointexits <to path> <to waypoint>")
		return
	elseif not(MapPaths[path2])
		CONS_Printf(player, "To path #"..path2.." out of bounds (Max "..#MapPaths..")")
		return
	elseif not(MapPaths[path2][way2])
		CONS_Printf(player, "To waypoint #"..way2.." out of bounds (Max "..#MapPaths[path2]..")")
		return
	end
	path2 = MapPaths[$]
	way2 = path2[$]
	for n,way1 in ipairs(currentwaypoints) do
		if removeWaypointExit(way1, way2)
			print("Removed exit to path "..way2.path.index.." waypoint "..way2.health.." for exiting waypoint "..way1.health)
		end
	end
end)

COM_AddCommand("gotowaypoint", function(player, pos)
	pos = tonumber($)
	if pos == nil
		CONS_Printf(player, "gotowaypoint <index>")
		return
	end
	if currentpath == nil
		CONS_Printf(player, "You must create or select a path first.")
		return
	end
	local w = currentpath[pos]
	if w == nil
		CONS_Printf(player, "Invalid waypoint.")
		return
	end
	P_TeleportMove(player.realmo, w.x, w.y, w.z)
end, COM_ADMIN)
COM_AddCommand("startpath", function(player, type, series, enablebackside, ...)
	if type == nil
		CONS_Printf(player, "\x83".."startpath <type> <series> <enablebackside> <tags> <..> \n"
		.."\x84".."type\x80 determines the purpose of the path and should be a name that would be recognized by specific mods, i.e. 'ai' or 'camera'."
		.."\x84".."Series\x80 is generally used to recommend path order, i.e. an instance should follow series 1 into series 2.\n"
		.."Answer yes/1/on to\x84 enablebackside\x80 to allow instances to backtrack through the path.\n"
		.."Any number of tags can be entered. If backside is enabled, tags will also be applied to the path's 'backtag' list.")
		return
	end
	series = tonumber(series)
	enablebackside = ($ == nil or $ == "yes" or $ == "1" or $ == "on") and true or false
	startPath(type, series, enablebackside, ...)
end, COM_ADMIN)
COM_AddCommand("listpath", function(player, arg)
	arg = tonumber($)
	if arg == nil
		local str = "\x82".."MapPath header properties\x80\n"
		str = $..listTags(MapPaths.tags,"Tags: ").."\n"
		str = $..listTags(MapPaths.backtags,"Backtags: ").."\n"
		str = $.."\x82".."Available paths:\n"
		for n,m in ipairs(MapPaths) do
			str = $.."\x81".."#"..n..":\x80 "
			str = $..m.type
			str = $.." path ("..#m.." waypoints)\n"
		end
		print(str)
	elseif MapPaths[arg] == nil
		print("Path #"..arg.." not found.")
	else
		local path = MapPaths[arg]
		print("\x82".."Path index "..tostring(path.index))
		print("Type: "..tostring(path.type))
		print("Series: "..tostring(path.series))
		print("Backside: "..tostring(path.backside and "Enabled" or "Disabled"))
		print(listTags(path.tags, "Tags: "))
		print(listTags(path.backtags, "Backtags: "))
		print("Waypoints: "..tostring(#path))
		print("Color: "..R_GetNameByColor(path.color))
	end
end, COM_LOCAL)
COM_AddCommand("selectpath", function(player, index)
	index = tonumber($)
	if index == nil
		CONS_Printf(player, "selectpath <index>\n".."Enter listpath for a list of valid entries")
	elseif index == 0
		L_SelectPath()
	elseif MapPaths[index] == nil
		CONS_Printf(player, "Invalid path index "..index)	
	else
		L_SelectPath(MapPaths[index])
		print("Now editing path #"..currentpath.index.." with type: "..currentpath.type)	
	end
end, COM_ADMIN)
COM_AddCommand("editpath", function(player, variable, value)
	if needCurrentPath(player)
		return
	elseif variable == nil
		CONS_Printf(player, "editpath <type/series/backside/color> <value>")
	elseif variable == "type"
		if value == nil
			CONS_Printf(player, "Current type is "..tostring(currentpath.type))
		else
			currentpath.type = value
		end
	elseif variable == "series"
		value = tonumber($)
		if value == nil
			CONS_Printf(player,"Enter a number value. Current series is "..tostring(currentpath.series))
		else
			currentpath.series = value
		end
	elseif variable == "color"
		if value == nil
			CONS_Printf(player,"Enter a color. Current path color is "..R_GetNameByColor(currentpath.color))
		else
			local color = R_GetColorByName(value)
			if color == 0
				CONS_Printf(player,"Could not find color with name "..value)
			else
				currentpath.color = color
				for _,m in ipairs(currentpath) do
					updateWaypointColor(m)
				end
			end
		end
	elseif variable == "backside"
		currentpath.backside = not($)
		print("Path #"..tostring(currentpath.index).." backside "..(currentpath.backside and "enabled." or "disabled."))
	else
		CONS_Printf(player, "Invalid argument "..variable)
	end
end, COM_ADMIN)
COM_AddCommand("deselectpath", function(player)
	if currentpath
		L_SelectPath()
		print("Closed the path editor.")
	end
end, COM_ADMIN)
COM_AddCommand("deletepath", function(player, index)
	index = tonumber($)
	if index == nil
		CONS_Printf(player, "deletepath <index>\n".."Be careful!! All residing waypoints will be deleted as well.")
		return
	end
	local path = MapPaths[index]
	if path == nil
		CONS_Printf(player, "Invalid path index "..index)	
		return
	end
	deletePath(path)
end, COM_ADMIN)
COM_AddCommand("copypath", function(player)
	if needCurrentPath(player)
		return
	end
	copyPath(currentpath)
end, COM_ADMIN)
COM_AddCommand("splitpath", function(player, splitindex, connect)
	if needCurrentPath(player)
		return
	end
	if #currentpath == 0
		CONS_Printf(player, "This path has no waypoints to split!")
		return
	end
	splitindex = tonumber($)
	if not splitindex
		CONS_Printf(player, "splitpath <waypoint> <connect on/off> \n".."The waypoint chosen will appear on the second path and connect to the first path.")
	elseif splitindex >= #currentpath or splitindex < 1
		CONS_Printf(player, "Waypoint index out of bounds (Max "..#currenpath..")")
	else
		if connect == "off"
			connect = false
		else
			connect = true
		end
		splitPath(currentpath, splitindex, connect)
	end
end, COM_ADMIN)
COM_AddCommand("mergepaths", function(player, ...)
	if ... == nil
		CONS_Printf(player, "mergepaths <...>\n".."The first path specified will define the final settings of the merged path.")
	end
	local t = {...}
	for n,m in ipairs(t) do
		local num = tonumber(m)
		if num == nil
			CONS_Printf(player, "Invalid path: "..m)
			return
		elseif num < 1 or num > #MapPaths
			CONS_Printf(player, "Path #"..num.." out of bounds. (Max "..#MapPaths..")")
			return
		end
		t[n] = MapPaths[num]
	end
	mergePaths(t)
end, COM_ADMIN)
COM_AddCommand("edittags", function(player, target, side, action, ...)
	if target == nil
		CONS_Printf(player, "\x83".."edittags <path/waypoints/header> <front/back/both> <add/remove/copy> <...> <tags>")
		CONS_Printf(player, "Note: 'copy' will use the specified front/back side to replace ALL tags on the other side.\n"
			.."If both sides are selected to 'copy', then each side will copy their respective tags to the other side.")
		return
	end
	
	-- Find our target instance
	local t
	if target == "path"
		if needCurrentPath(player)
			return
		end
		t = L_GetSelectedPath()
	elseif target == "waypoints"
		if needCurrentWaypoints(player)
			return
		end
		t = L_GetSelectedWaypoints()
	elseif target == "header"
		t = MapPaths
	else
		CONS_Printf(player, "Invalid argument #2 "..target.." (Valid arguments: path, waypoints, header)")
		return
	end
	-- Get side
	local s = 0 -- For bitwise operations
	if side == "front"
		s = 1
	elseif side == "back"
		s = 2
	elseif side == "both"
		s = 3
	else
		CONS_Printf(player, "Invalid argument #3 "..tostring(side).." (Valid arguments: front, back, both)")
		return
	end
	-- Get action
	if action == nil
		if target == "waypoints"
			t = t[1]
		end
		if s&1
			CONS_Printf(player, listTags(t.tags, "Front tags:"))
		end
		if s&2
			CONS_Printf(player, listTags(t.backtags, "Back tags:"))
		end
	else
		if action == "add"
			if ... == nil
				CONS_Printf(player, "No tags specified.")
			else
				if target == "path" or target == "header"
					if s&1
						addPathTags(t, ...)
					end
					if s&2
						addPathBacktags(t, ...)
					end
				else
					if s&1
						addWaypointTags(t, ...)
					end
					if s&2
						addWaypointBacktags(t, ...)
					end
				end
			end
		elseif action == "remove"
			if ... == nil
				CONS_Printf(player, "No tags specified.")
			else
				if target == "path" or target == "header"
					if s&1
						removePathTags(t, ...)
					end
					if s&2
						removePathBacktags(t, ...)
					end
				else
					if s&1
						removeWaypointTags(t, ...)
					end
					if s&2
						removeWaypointBacktags(t, ...)
					end
				end
			end
		elseif action == "copy"
			if s == 1
				-- Copy front to back
				removeTags(t.backtags, unpack(t.backtags))
				addTags(t.backtags, unpack(t.tags))
				print("Replaced "..target.." back tags with front tags")
			elseif s == 2
				-- Copy back to front
				removeTags(t.tags, unpack(t.tags))
				addTags(t.tags, unpack(t.backtags))
				print("Replaced "..target.." front tags with back tags")
			else
				-- Swap tags
				local front = {unpack(t.tags)}
				local back = {unpack(t.backtags)}
				removeTags(t.tags, unpack(front))
				removeTags(t.backtags, unpack(back))
				addTags(t.tags, unpack(back))
				addTags(t.backtags, unpack(front))
				print("Swapped "..target.." front and back tags")
			end
		else
			CONS_Printf(player, "Invalid argument #4 "..action.." (Valid arguments: add, remove, copy)")
			return
		end
	end
end, COM_ADMIN)
COM_AddCommand("searchtags", function(player, target, ...)
	if target == nil
		CONS_Printf(player, "\x83".."searchtags <paths/pathwaypoints/allwaypoints> <tags>")
		return
	end
	
	-- Find our target instance
	local t
	if target != "paths" and target != "pathwaypoints" and target != "allwaypoints"
		print("Invalid argument #2 "..target.." (Valid arguments: paths, pathwaypoints, allwaypoints)")
		return
	end
	local str
	local found = {}
	if ... == nil
		if target == "paths"
			str = "\x82".."Path Tags\n\x80"
			for n,m in ipairs(MapPaths) do
				str = $.."\x81".."#"..n..":\x80 "..#m.tags.." tags, "..#m.backtags.." backtags\n"
				str = $..listTags(m.tags, "Tags: ").."\n"
				str = $..listTags(m.backtags, "Backtags: ").."\n"
			end
		elseif target == "pathwaypoints"
			if not needCurrentPath(player)
				str = "\x82".."Path #"..currentpath.index.." Waypoint Tags\n\x80"
				for n,m in ipairs(currentpath) do
					if #m.tags or #m.backtags
						str = $.."\x81".."#"..n..":\x80 "..#m.tags.." tags, "..#m.backtags.." backtags\n"
						str = $..listTags(m.tags, "Tags: ").."\n"
						str = $..listTags(m.backtags, "Backtags: ").."\n"
					end
				end
			end
		elseif target == "allwaypoints"
			str = "\x82".."All Waypoint Tags\n\x80"
			for n,m in ipairs(MapPaths) do
				for nn,mm in ipairs(m) do
					if #mm.tags or #mm.backtags
						str = $.."\x81".."#"..n.."##"..nn..":\x80 "..#mm.tags.." tags, "..#mm.backtags.." backtags\n"
						str = $..listTags(mm.tags, "Tags: ").."\n"
						str = $..listTags(mm.backtags, "Backtags: ").."\n"
					end
				end
			end
		end
	else
		for n, m in ipairs({...}) do
			if target == "paths"
				for nn, mm in ipairs(MapPaths) do
					if found[mm]
						continue
					end
					if mm.tags[m] or mm.backtags[m]
						table.insert(found, mm)
						found[mm] = true
					end
				end
			elseif target == "pathwaypoints"
				if not needCurrentPath(player)
					for nn, mm in ipairs(currentpath) do
						if found[mm]
							continue
						end
						if mm.tags[m] or mm.backtags[m]
							table.insert(found, mm)
							found[mm] = true
						end
					end
				end
			elseif target == "allwaypoints"
				for nn, mm in ipairs(MapPaths) do
					for nnn, mmm in ipairs(mm) do
						if found[mmm]
							continue
						end
						if mmm.tags[m] or mm.backtags[m]
							table.insert(found, mmm)
							found[mmm] = true
						end
					end
				end
			end
		end
		local comma = "\x86"..",\x80 "
		if target == "paths"
			str = "\x81".."Got "..#found.." paths:\x80 "
			for n,m in ipairs(found) do
				str = $..tostring(m.index)..tostring(n != #found and comma or "")
			end
		elseif target == "pathwaypoints" and currentpath
			str = "\x81".."Got "..#found.." #"..tostring(currentpath.index).." waypoints:\x80 "
			for n,m in ipairs(found) do
				str = $..tostring(m.health)..tostring(n != #found and comma or "")
			end
		elseif target == "allwaypoints"
			str = "\x81".."Got "..#found.." waypoints: "
			for n,m in ipairs(found) do
				str = $.."\x84"..tostring(m.path.index)..".\x80"..tostring(m.health)..tostring(n != #found and comma or "")
			end
		end
	end
	if str
		print(str)
	end
	-- Select found waypoints
	if target == "pathwaypoints" and #found
		L_SelectWaypoints(found)
	end
end, COM_LOCAL)

COM_AddCommand("findroute", function(player, path1, way1, path2, way2, debug)
	path1 = tonumber($)
	way1 = tonumber($)
	path2 = tonumber($)
	way2 = tonumber($)
	if not (path1 and way1 and path2 and way2)
		CONS_Printf(player, "findroute <path1> <waypoint1> <path2> <waypoint2>")
		return
	elseif not MapPaths[path1]
		CONS_Printf(player, "Invalid path #"..path1)
		return		
	elseif not MapPaths[path2]
		CONS_Printf(player, "Invalid path #"..path2)
		return		
	elseif not MapPaths[path1][way1]
		CONS_Printf(player, "Invalid waypoint #"..way1)
		return		
	elseif not MapPaths[path2][way2]
		CONS_Printf(player, "Invalid waypoint #"..way2)
		return		
	end
	path1 = MapPaths[$]
	path2 = MapPaths[$]
	way1 = path1[$]
	way2 = path2[$]
	-- Initialize queue
	local queue = searchRoute(way1)
	if debug
		for n,m in pairs(queue) do
			print("#"..n.path.index.."."..n.health.." Dist "..m.dist/FRACUNIT.." #Sequence "..#m.sequence)
		end
	end
	if queue[way2]
		print("\x82".."Found route: ")
		for n,m in ipairs(queue[way2].sequence) do
			print("#"..m.path.index.."."..m.health)
		end
	else
		CONS_Printf(player, "Could not find a route to Path #"..path2.index.." Route #"..way2.health.." from this position.")
	end
end, COM_LOCAL)

COM_AddCommand("listpathcommands", function(player)
	print("\x83".."Path editor commands")
	print("addwaypoint, selectwaypoints, editwaypoints, deletewaypoints, reorderwaypoints, reversewaypoints, gotowaypoint")
	print("flipwaypoints", "rotatewaypoints", "addwaypointexits", "removewaypointexits")
	print("startpath, listpath, selectpath, editpath, deselectpath, deletepath, copypath, splitpath, mergepaths")
	print("edittags, searchtags, playpath, findroute, pathbot", "pathstats")
	print("loadpathfile, savepathfile, readpathfile, writepathlua")
	print("\x83".."Path editor cvars")
	print("autoloadpathfiles, autosavepathfiles")
	print("waypointhitbox_distance")
	print("Type\x83 startpath\x80 or\x83 selectpath\x80 to begin editing. Type\x83 deselectpath\x80 to close the editor.")
end, COM_LOCAL)

-- Debugging
COM_AddCommand("checkwaypoints", function(player, debug)
	local count = 0
	for mo in mobjs.iterate() do
		if mo.type == MT_MAPWAYPOINT
			if mo.path == nil -- Check valid path
				print("Found waypoint #"..mo.health.." without a valid path.")
				count = $+1
			elseif mo.path[mo.health] != mo -- Check valid path waypoint
				print("Found Waypoint #"..mo.health.." referencing path #"..tostring(path.index).." that does not reference the waypoint back.")
				count = $+1
			end
			for n,m in ipairs({mo.entrances,mo.exits}) do -- Check valid exit and entrance listings
				for nn,mm in pairs(m) do
					if type(nn) == "userdata" and mm == true and not(nn.valid)
						print("Waypoint "..mo.health.." "..(n == 1 and "entrance" or "exit").." links to invalid id "..tostring(nn).." (numerical entry)")
						count = $+1
					elseif type(nn) == "number" and not(mm.valid)
						print("Waypoint "..mo.health.." "..(n == 1 and "entrance" or "exit").." links to invalid id "..tostring(mm).." (userdata entry)")
						count = $+1
					end
				end
			end
		end
	end
	for n,m in ipairs(MapPaths) do
		if n != m.index
			print('Path #'..n..' has incorrect self-index ('..tostring(m.index)..')')
			count = $+1
		end
		for nn,mm in ipairs(m) do -- Check valid path waypoints
			if not mm.valid
				print('Path #'..n..' Waypoint #'..nn..' is invalid')
				count = $+1
			end
		end
	end
	if count
		print("\x85".."Counted "..count.." bad references.")
	else
		print("\x83".."Counted "..count.." bad references.")
	end
end, COM_ADMIN)

local addnum = function(count, t)
	for _,m in ipairs(t) do
		count = $+1
	end
	return count
end
COM_AddCommand('pathstats', function(player)
	local numpaths = #MapPaths
	local numwaypoints = 0
	local numtags = 0
	local numexits = 0
	numtags = addnum($, MapPaths.tags)
	numtags = addnum($, MapPaths.backtags)
	for _,path in ipairs(MapPaths) do
		numtags = addnum($, path.tags)
		numtags = addnum($, path.backtags)
		for _,way in ipairs(path) do
			numwaypoints = $+1
			for _,exit in ipairs(way.exits) do
				numexits = $+1
				numtags = addnum($, way.exits[exit])
			end
			numtags = addnum($, way.tags)
			numtags = addnum($, way.backtags)
		end
	end
	print('Paths: '..numpaths..', Waypoints: '..numwaypoints..', Exits: '..numexits..', Tags: '..numtags)
end)