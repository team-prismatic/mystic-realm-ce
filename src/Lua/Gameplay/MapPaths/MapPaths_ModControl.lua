-- Version Control
local t = {
	-- Mod Settings
	addon = "MRCE", -- Modify this to the name of your mod. Must be in quotes. If there is an addon conflict, this helps tell the player which addons are involved.
	editing = true, -- Setting to false will disable all editing features, including external use of luafiles for adding and loading mappaths.
	-- Info
	version = 1,
	subversion = 0,
	develop = false,
	compat = 0,
	backcompat = 0,
}


-- Don't tamper with anything below this line.

if MapPathsVersionInfo and not(MapPathsVersionInfo.warn_duplicate) then
	-- Cross compatibility warning
	local M = MapPathsVersionInfo
	print("Prevented duplicate loading sequence for MapPaths system (Initial load: "..M.addon..". Duplicate: "..t.addon)
	if M.editing == false and t.editing == true then
		print("\x82".."This addon ('"..t.addon.."') allows editing, but a previously loaded '"..M.addon
			.."' has disabled it. To enable editing, load this addon first.")
	end
	MapPathsVersionInfo.warn_duplicate = true
	return
end
-- Create nametable
rawset(_G,"MapPathsVersionInfo", t)
-- Create version string
MapPathsVersionInfo.string = "v"..tostring(MapPathsVersionInfo.version).."."..tostring(MapPathsVersionInfo.subversion)..tostring(MapPathsVersionInfo.develop and "dev" or "")