--[[
    titlecard credits for music and level

    (C) 2022 by K. "ashifolfi" J.
]]

local text_slideout = false
local text_animdur = TICRATE/2
local text_animtime = 0

local animtime_capped = nil
local animate_percentage = nil

local debug = CV_RegisterVar{
name = "mr_creditsdebug",
defaultvalue = 0,
flags = 0,
PossibleValue = CV_OnOff}


addHook("MapLoad", function()
	text_slideout = false
	text_animtime = 0
end)

local function hud_tcard_credits_str_manager(credits)
	local lines = {}
	local cnt_w = 0
	local cnt_s = 0
	local cnt_i = 1

	for credit in credits:gmatch("([^%s]+)") do
		if not lines[cnt_i] then
			lines[cnt_i] = ""
		end

		cnt_s = $+string.len(credit)
		if cnt_s > 42 then
			cnt_w = 0
			cnt_s = 0
			cnt_i = $+1
			lines[cnt_i] = ""
		end

		lines[cnt_i] = $..' '..credit

		cnt_w = $+1
		if cnt_w > 6 then
			cnt_w = 0
			cnt_i = $+1
		end
	end

	return lines
end

local function hud_tcard_credits(v, p, ticker, endticker)
	if (animate_percentage == 65520 or ticker > endticker) then return end

	if (ticker > endticker - TICRATE and not text_slideout) then
    	text_animtime = 0
    	text_slideout = true
    end

	animtime_capped = max(min(text_animtime, text_animdur), 0)
    animate_percentage = FU / text_animdur * animtime_capped

    local X = text_slideout and ease.inquint(animate_percentage, 10, -310) or ease.outquint(animate_percentage, -310, 10)
    local Y = 0
    local snap_flags = V_SNAPTOTOP|V_SNAPTOLEFT

	-- Level design credits
	if mapheaderinfo[gamemap].lcredits ~= nil then
		v.draw(X - 8, Y+8, v.cachePatch("TCCFN022"), snap_flags, v.getStringColormap(131))

		local levelcredits = hud_tcard_credits_str_manager(mapheaderinfo[gamemap].lcredits:match('(%w.*[^"])'))
		for i = #levelcredits,1,-1 do
			Y = $+8
			v.drawString(X, Y, levelcredits[i], snap_flags|V_ALLOWLOWERCASE, "thin")
		end
	elseif debug.value then
		Y = $+8
		v.draw(X - 8, Y, v.cachePatch("TCCFN022"), snap_flags, v.getStringColormap(131))
		v.drawString(X, Y, "Unavailable", snap_flags|V_ALLOWLOWERCASE, "thin")
	end

	Y = $ + 15

	-- Music credits
	if mapheaderinfo[gamemap].mcredits ~= nil then
		v.drawString(X - 8, Y, "\25", snap_flags|V_GREENMAP, "left")
		v.drawString(X, Y, mapheaderinfo[gamemap].mcredits:match('(%w.*[^"])'), snap_flags|V_ALLOWLOWERCASE, "thin")
	elseif debug.value then
		v.drawString(X - 8, Y, "\25", snap_flags|V_GREENMAP, "left")
		v.drawString(X, Y, "Unavailable", snap_flags|V_ALLOWLOWERCASE, "thin")
	end

	if not modeattacking then
		text_animtime = $ + 1
	end
end
hud.add(hud_tcard_credits, "titlecard")


addHook("ThinkFrame", function()
	if leveltime > 300 then return end
	if not modeattacking then return end
	if leveltime == 1 then
		text_animtime = 0
		text_slideout = false
	elseif leveltime > 5 and leveltime < 50 then
		text_animtime = $ + 1
	elseif leveltime > 90 then
		text_slideout = true
		text_animtime = $ + 1
	end
end)