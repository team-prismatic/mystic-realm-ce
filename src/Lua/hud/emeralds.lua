--	DISPLAY OF EMERALDS
--	Scripted by AceLite
--	Contributed from Sonic Adventure Style Pack
--

--$GZDB_SKIP

local emerald = {EMERALD1, EMERALD2, EMERALD3, EMERALD4, EMERALD5, EMERALD6, EMERALD7}
local emerald_max = EMERALD1|EMERALD2|EMERALD3|EMERALD4|EMERALD5|EMERALD6|EMERALD7

local V_ALPHASHIFT = V_ALPHASHIFT
local TC_DEFAULT = TC_DEFAULT
local ANGLE_225 = ANGLE_225
local FRACUNIT = FRACUNIT
local FRACBITS = FRACBITS
local ANG1 = ANG1
local ANG5 = ANG1*5

local circle_resting = ANGLE_225-ANG5
local circle_bool = false
local circle_scale = 0
local circle_split = (360/#emerald)*ANG1
local circle_size = 34
local circle_bgsize = 45
local circle_timing = FRACUNIT/10
local circle_start = -(FRACUNIT << 1)
local circle_end = FRACUNIT << 1
local circle_rotation = 0
local circle_speed = 0
local circle_aimspeed = ANG1

local black_em_alpha = ANG1*(360/30)
local black_em_delta = -8*TICRATE
local black_em_animt = 15
local black_em_goal = -black_em_delta+black_em_animt
local black_em_timer = 0
local black_em_color = 1

local em_x = (151+8) << FRACBITS -- base+new_offset
local em_y = (78+9) << FRACBITS -- base+new_offset
local em_s = FRACUNIT*(3/2)

local bem_x = 200 << FRACBITS
local bem_y = 9 << FRACBITS
local bem_s = FRACUNIT >> 1

local draw_scaled
local patches_untilized = true
local patches = {}

local hud_enable = hud.enable
local hud_disable = hud.disable
local getColormap
local RandomRange

local ease_linear = ease.linear

local FixedMul = FixedMul
local FixedDiv = FixedDiv

local sin = sin
local cos = cos
local abs = abs

local hyper_pal = {
	SKINCOLOR_SUPERAETHER2,
	SKINCOLOR_SUPERSAPPHIRE2,
	SKINCOLOR_SUPERBUBBLEGUM2,
	SKINCOLOR_SUPERMINT2,
	SKINCOLOR_SUPERRUBY2,
	SKINCOLOR_SUPERWAVE2,
	SKINCOLOR_SUPERCOPPER2,
	SKINCOLOR_SUPERAETHER2,
}

local flash_translation = {
	"Emeralds_Flash1",
	"Emeralds_Flash2",
	"Emeralds_Flash2",
	"Emeralds_Flash3",
	"Emeralds_Flash3",
	"Emeralds_Flash4",
	"Emeralds_Flash4",
	"Emeralds_Flash4",
	"Emeralds_Flash4",
	"Emeralds_Flash4",
	"Emeralds_Flash3",
	"Emeralds_Flash3",
	"Emeralds_Flash2",
	"Emeralds_Flash2",
	"Emeralds_Flash1",
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	-- Zeroes are empty
}

hud.add(function(v)
	local p = displayplayer
	if patches_untilized then
		for l = 1, 3 do
			patches[l] = {}
			for i = 1, 8 do
				patches[l][i] = v.cachePatch("CEN"..l.."CHAOS"..i)
			end
			patches[l][0] = v.cachePatch("CEN"..l.."CHAOSX")
			patches[l][-1] = v.cachePatch("CEN"..l.."CHAOS8")
		end

		patches_untilized = nil
		draw_scaled = v.drawScaled
		getColormap = v.getColormap
		RandomRange = v.RandomRange
	end

	if multiplayer then
		hud.enable("coopemeralds")
		if mrce.hyperunlocked or p.mrce and p.mrce.hypercheat then
			draw_scaled(bem_x, bem_y, bem_s, patches[3][-1])
		end
	else
		hud.disable("coopemeralds")
		circle_scale = min(ease_linear(circle_timing, circle_scale, circle_end), FRACUNIT)
		circle_bool = true
	end
end, "scores")

--https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
--https://stackoverflow.com/questions/27755514/circle-with-thickness-drawing-algorithm (M Oehm)
local function V_DrawLine_x(v, x_1, x_2, y, color)
	v.drawFill(x_1, y, x_2-x_1, 1, color)
end

local function V_DrawLine_y(v, x, y_1, y_2, color)
	v.drawFill(x, y_1, 1, y_2-y_1, color)
end


local function V_DrawThickCircle(v, x_center, y_center, inner, outer, color)
	local x_o = outer
	local x_i = inner
	local y = 0
	local erro = 1 - x_o
	local erri = 1 - x_i

	local bandaid = 3*(outer-inner)/4
	v.drawFill(x_center - 5*x_o/7, y_center - 5*x_o/7, bandaid, bandaid, color)

	while (x_o >= y) do
		V_DrawLine_x(v, x_center + x_i, x_center + x_o, y_center + y, color)
		V_DrawLine_y(v, x_center + y, y_center + x_i, y_center + x_o, color)
		V_DrawLine_x(v, x_center - x_o, x_center - x_i, y_center + y, color)
		V_DrawLine_y(v, x_center - y, y_center + x_i, y_center + x_o, color)
		V_DrawLine_x(v, x_center - x_o, x_center - x_i, y_center - y, color)
		V_DrawLine_y(v, x_center - y, y_center - x_o, y_center - x_i, color)
		V_DrawLine_x(v, x_center + x_i, x_center + x_o, y_center - y, color)
		V_DrawLine_y(v, x_center + y, y_center - x_o, y_center - x_i, color)

		y = $+1

		if erro < 0 then
			erro = $+2*y+1
		else
			x_o = $-1
			erro = $+2*(y-x_o+1)
		end

		if (y > inner) then
			x_i = y
		else
			if erri < 0 then
				erri = $+2*y+1
			else
				x_i = $-1
				erri = $+2*(y-x_i+1)
			end
		end
	end
end

local function Math_FakeSquare(num)
    if num <= 0 then return 1 end
	if num < 2 then
		return num
	end

	local shift = 2
	while ((num >> shift) ~= 0) do
		shift = $+2
	end

	local result = 0
	while (shift >= 0) do
		result = result << 1
		local large_cand = result + 1
		if ((large_cand * large_cand) <= (num >> shift)) then
			result = large_cand
		end
		shift = $-2
	end

	return result
end

local frac_half = FRACUNIT/2

local function V_DrawCircle(v, x_center, y_center, radius, color, skips)
	local x
	local rad_2 = radius*radius

	for y = -radius, radius, 1+(skips or 0) do
		x = Math_FakeSquare(((rad_2 - y*y) << FRACBITS + frac_half) >> FRACBITS)
		v.drawFill(x_center - x, y_center+y, x * 2, 1, color)
	end
end

local function V_DrawEmeraldCircle(v)
	local radius = FixedInt(circle_scale*circle_bgsize)
	--print(radius)

	local x = em_x >> FRACBITS
	local y = em_y >> FRACBITS - 1

	local color = 115
	if consoleplayer.mo and consoleplayer.mo then
		color = skincolors[consoleplayer.mo.color].ramp[5]
	end

	--V_DrawCircle(v, x-1, y-1, radius, color|V_70TRANS)
	--V_DrawDitherCircle(v, x, y, radius+4, 31|V_20TRANS)
	V_DrawCircle(v, x, y, radius, color|V_80TRANS, 1)

	V_DrawThickCircle(v, x, y, radius-6, radius+2, 0|V_20TRANS)
	V_DrawThickCircle(v, x, y, radius-4, radius, 31|V_20TRANS)
end

customhud.SetupItem("mrce_emeralds", "mrce", function(v, p, c, e)
	if mapheaderinfo[gamemap].mrce_emeraldstage then
		em_y = (78+20) << FRACBITS -- base+new_offset
	else
		em_y = (78+9) << FRACBITS -- base+new_offset
	end

	if circle_scale and emeralds > 0 then

		--Off loading local variables for optimalization purposes
		local leveltime_ang = circle_resting
		local Is_hyper = MRCE_isHyper(p) or false
		local time_half = leveltime>>1
		local time_quad = leveltime>>2
		local translation = nil

		--Rotation shall start
		if emerald_max == emeralds then
			translation = flash_translation[(leveltime % #flash_translation) + 1] or nil
			circle_speed = ease_linear(FRACUNIT/32, circle_speed, circle_aimspeed)
			circle_rotation = $-circle_speed
			leveltime_ang = $+circle_rotation
		end

		V_DrawEmeraldCircle(v)

		-- Vanilla Emeralds
		for id,k in ipairs(emerald) do
			-- IF check compares from table whenever or not emerald is in player's possession
			if emeralds & k then
				local pos_ang = id*circle_split + leveltime_ang
				local x = (circle_size * cos(pos_ang)) + em_x
				local y = (circle_size * sin(pos_ang)) + em_y

				local array = patches[(GlobalBanks_Array[0] & 1 << (id - 1)) and 3 or (mapheaderinfo[gamemap].mysticrealms and 2 or 1)]
				local patch = array[id] or array[1]
				draw_scaled(x, y,
				circle_scale, patch, 0,
				getColormap(TC_DEFAULT, SKINCOLOR_NONE, translation))

				if Is_hyper and p.mo and p.mo.valid then
					local sine = abs(6*sin((id+time_quad)*black_em_alpha)) >> FRACBITS

					if sine and sine < 10 then
						sine = 8-sine
					end

					local flag = sine << V_ALPHASHIFT

					draw_scaled(x, y,
					circle_scale, array[0], flag,
					not hyper and getColormap(TC_DEFAULT, hyper_pal[((time_quad+id) % #hyper_pal)+1]))
				end
			end
		end


		-- "Master Emerald"
		local master_lookup = patches[3]

		if mrce.hyperunlocked or (p.mrce and p.mrce.hypercheat and not p.bot) then
			draw_scaled(em_x, em_y, FixedMul(em_s, circle_scale), master_lookup[8])

			if emeralds == emerald_max then
				if (black_em_timer >= black_em_goal) or (Is_hyper and black_em_timer >= black_em_animt) then
					black_em_timer = 0
					black_em_color = RandomRange(1, 7)
				end

				local flag = 0
				local sine = abs(10*sin(max((Is_hyper == true and leveltime or black_em_timer+black_em_delta), 0)*black_em_alpha)) >> FRACBITS

				if sine and sine < 10 then
					sine = 10-sine
					flag = sine << V_ALPHASHIFT
				end

				if sine then
					draw_scaled(em_x, em_y, FixedMul(em_s, circle_scale), master_lookup[black_em_color], flag)
				end
				black_em_timer = $+1
			end
		end

		if not circle_bool then
			circle_scale = max(ease_linear(circle_timing, circle_scale, circle_start), 0)
		end

		circle_bool = false
	else
		circle_rotation = 0
		--circle_speed = 0 -- no point having the rotation speed reset
	end
end, "game")
