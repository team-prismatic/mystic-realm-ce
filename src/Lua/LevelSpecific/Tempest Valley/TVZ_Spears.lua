--
-- Tempest Valley Zone spears
-- by Radicalicious
--


freeslot(
"MT_TVZ_SPEAR_VERTICAL",
"MT_TVZ_SPEAR_HORIZONTAL",
"MT_TVZ_SPEAR_HORIZONTAL_BASE",

"S_TVZ_SPEAR_VERTICAL1",
"S_TVZ_SPEAR_VERTICAL2",
"S_TVZ_SPEAR_VERTICAL3",
"S_TVZ_SPEAR_VERTICAL4",
"S_TVZ_SPEAR_VERTICAL5",
"S_TVZ_SPEAR_VERTICAL6",

"S_TVZ_SPEAR_HORIZONTAL1",
"S_TVZ_SPEAR_HORIZONTAL2",
"S_TVZ_SPEAR_HORIZONTAL3",
"S_TVZ_SPEAR_HORIZONTAL4",
"S_TVZ_SPEAR_HORIZONTAL5",
"S_TVZ_SPEAR_HORIZONTAL6",

"S_TVZ_SPEAR_HORIZONTAL_BASE",

"SPR_TVSK"
)

-- objects: vertical spear
---@diagnostic disable-next-line
mobjinfo[MT_TVZ_SPEAR_VERTICAL] = {
	--$Name Spear
	--$Sprite TVSKA0
	--$Category Tempest Valley
	--$NotAngled
	--$Arg0 Retraction interval
	--$Arg1 Start interval
	--$Arg2 Flags
	--$Arg2Type 12
	--$Arg2Enum { 1 = "Start retracted"; 2 = "Intangible"; }
	doomednum = 2225,
	spawnstate = S_TVZ_SPEAR_VERTICAL1,
	deathstate = S_NULL,
	meleestate = S_TVZ_SPEAR_VERTICAL4,
	painsound = sfx_s3k64,
	radius = 8*FRACUNIT,
	height = 128*FRACUNIT,
	speed = 2*TICRATE,
	flags = MF_NOGRAVITY|MF_SCENERY|MF_SOLID
}

-- objects: horizontal spear
---@diagnostic disable-next-line
mobjinfo[MT_TVZ_SPEAR_HORIZONTAL] = {
	--$Name Spear (Horizontal)
	--$Sprite TVSKELER
	--$Category Tempest Valley
	--$Arg0 Retraction interval
	--$Arg1 Start interval
	--$Arg2 Flags
	--$Arg2Type 12
	--$Arg2Enum { 1 = "Start retracted"; 2 = "Intangible"; }
	doomednum = 2226,
	spawnstate = S_TVZ_SPEAR_HORIZONTAL1,
	deathstate = S_NULL,
	meleestate = S_TVZ_SPEAR_HORIZONTAL4,
	painsound = sfx_s3k64,
	radius = 64*FRACUNIT,
	height = 16*FRACUNIT,
	speed = 2*TICRATE,
	flags = MF_NOGRAVITY|MF_SCENERY|MF_SOLID|MF_PAPERCOLLISION|MF_NOCLIPHEIGHT
}

---@diagnostic disable-next-line
mobjinfo[MT_TVZ_SPEAR_HORIZONTAL_BASE] = {
	doomednum = -1,
	spawnstate = S_TVZ_SPEAR_HORIZONTAL_BASE,
	deathstate = S_NULL,
	radius = 8*FRACUNIT,
	height = 16*FRACUNIT,
	flags = MF_NOGRAVITY|MF_SCENERY|MF_NOCLIP|MF_NOCLIPTHING
}

-- states: vertical spear
states[S_TVZ_SPEAR_VERTICAL1] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|A,
	tics = -1,
	action = A_SpikeRetract,
	var1 = 1,
	var2 = 0,
	nextstate = S_TVZ_SPEAR_VERTICAL2
}

---@diagnostic disable-next-line
states[S_TVZ_SPEAR_VERTICAL2] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|B,
	tics = 2,
	action = A_Pain,
	nextstate = S_TVZ_SPEAR_VERTICAL3
}

---@diagnostic disable-next-line
states[S_TVZ_SPEAR_VERTICAL3] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|C,
	tics = 2,
	nextstate = S_TVZ_SPEAR_VERTICAL4
}

states[S_TVZ_SPEAR_VERTICAL4] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|D,
	tics = -1,
	action = A_SpikeRetract,
	var1 = 0,
	var2 = 0,
	nextstate = S_TVZ_SPEAR_VERTICAL5
}

---@diagnostic disable-next-line
states[S_TVZ_SPEAR_VERTICAL5] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|C,
	tics = 2,
	action = A_Pain,
	nextstate = S_TVZ_SPEAR_VERTICAL6
}

---@diagnostic disable-next-line
states[S_TVZ_SPEAR_VERTICAL6] = {
	sprite = SPR_TVSK,
	frame = FF_SEMIBRIGHT|B,
	tics = 2,
	nextstate = S_TVZ_SPEAR_VERTICAL1
}

-- states: horizontal spear
states[S_TVZ_SPEAR_HORIZONTAL1] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|E,
	tics = -1,
	action = A_SpikeRetract,
	var1 = 1,
	var2 = 0,
	nextstate = S_TVZ_SPEAR_HORIZONTAL2
}

---@diagnostic disable-next-line
states[S_TVZ_SPEAR_HORIZONTAL2] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|F,
	tics = 2,
	action = A_Pain,
	nextstate = S_TVZ_SPEAR_HORIZONTAL3
}

---@diagnostic disable-next-line
states[S_TVZ_SPEAR_HORIZONTAL3] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|G,
	tics = 2,
	nextstate = S_TVZ_SPEAR_HORIZONTAL4
}

states[S_TVZ_SPEAR_HORIZONTAL4] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|H,
	tics = -1,
	action = A_SpikeRetract,
	var1 = 0,
	var2 = 0,
	nextstate = S_TVZ_SPEAR_HORIZONTAL5
}

---@diagnostic disable-next-line
states[S_TVZ_SPEAR_HORIZONTAL5] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|G,
	tics = 2,
	action = A_Pain,
	nextstate = S_TVZ_SPEAR_HORIZONTAL6
}

---@diagnostic disable-next-line
states[S_TVZ_SPEAR_HORIZONTAL6] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|F,
	tics = 2,
	nextstate = S_TVZ_SPEAR_HORIZONTAL1
}

---@diagnostic disable-next-line
states[S_TVZ_SPEAR_HORIZONTAL_BASE] = {
	sprite = SPR_TVSK,
	frame = FF_PAPERSPRITE|FF_SEMIBRIGHT|I,
	tics = -1,
	nextstate = S_NULL
}

-- behavior: vertical spear
addHook("MobjCollide", function(spike, pmo)
	if not pmo then return end
	if not pmo.player then return end
	if not spike then return end

	if pmo.player.playerstate ~= PST_LIVE then return end

	if (pmo.z > spike.z+spike.height) or (pmo.z+pmo.height < spike.z) then return end
	P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)

	--[[
		if (spike.state == S_TVZ_SPEAR_VERTICAL1 and spike.tics == AngleFixed(spike.angle)/FRACUNIT-1 and not (spike.flags & MF_SCENERY))
			P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
			if (spike.eflags & MFE_VERTICALFLIP)
				P_SetOrigin(pmo, spike.x, spike.y, spike.z-pmo.height)
			else
				P_SetOrigin(pmo, spike.x, spike.y, spike.z+spike.height)
			end
		end

		if (spike.eflags & MFE_VERTICALFLIP)
			if ((pmo.z + pmo.height <= spike.z+(spike.height/6)) and (pmo.z+pmo.height >= spike.z-(spike.height/6)))
				P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
			end
		else
			if ((pmo.z >= spike.z+(spike.height/6)*5) and (pmo.z <= spike.z+(spike.height/6)*7))
				P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
			end
		end
	--]]
end, MT_TVZ_SPEAR_VERTICAL)

addHook("MobjFuse", function(mo)
	if not mo then return end
	if not mo.valid then return end

	mo.state = states[mo.state].nextstate
	if (mo.spawnpoint) then
		mo.fuse = mo.spawnpoint.args[0]
	else
		mo.fuse = mo.info.speed
	end
	return true
end, MT_TVZ_SPEAR_VERTICAL)

addHook("MapThingSpawn", function(mo, mt)
	local TMSF_RETRACTED = 1
	local TMSF_INTANGIBLE = 1<<1

	if mt.args[0] then
		mo.flags = $ & ~MF_SCENERY
		mo.fuse = mt.args[1]
	end

	if mt.args[2] & TMSF_RETRACTED then
		mo.state = mo.info.meleestate
	end

	if not (mt.args[2] & TMSF_INTANGIBLE) and not (metalrecording) then
		mo.flags = $ & ~(MF_NOBLOCKMAP|MF_NOGRAVITY|MF_NOCLIPHEIGHT)
		mo.flags = $ | MF_SOLID
	end
end, MT_TVZ_SPEAR_VERTICAL)

-- behavior: horizontal spear
addHook("MobjCollide", function(spike, pmo)
	if not pmo then return end
	if not pmo.player then return end
	if not spike then return end
	
	if pmo.player.playerstate ~= PST_LIVE then return end
	
	if not spike.tracer then return end

	if (pmo.z > spike.z+spike.height) or (pmo.z+pmo.height < spike.z) then return end
	
	P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
	
	--[[
		-- did we run into the spikes?
		local touchangle = R_PointToAngle2(spike.tracer.x, spike.tracer.y, pmo.x, pmo.y)
		local bottomz
		local topz
		if (spike.eflags & MFE_VERTICALFLIP)
			bottomz = spike.z+spike.height
			topz = spike.z
		else
			bottomz = spike.z
			topz = spike.z+spike.height
		end

		if ((pmo.z + pmo.height > bottomz) and (pmo.z < topz) and (R_PointToDist2(spike.tracer.x, spike.tracer.y, pmo.x, pmo.y) > (spike.radius/16)*15))
			touchangle = spike.angle - $
			if (touchangle > ANGLE_180)
				touchangle = InvAngle(touchangle)
			end
			if (touchangle <= ANGLE_22h)
				P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
			end
		end

		-- getting stabbed by extending spikes
		if not spike.spawnpoint then return end
		if (spike.state == S_TVZ_SPEAR_HORIZONTAL1 and spike.tics == spike.spawnpoint.args[1]-1 and not (spike.flags & MF_SCENERY))
			P_DamageMobj(pmo, spike, spike, 1, DMG_SPIKE)
			P_SetOrigin(pmo, spike.x+P_ReturnThrustX(spike, spike.angle, spike.radius), spike.y+P_ReturnThrustY(spike, spike.angle, spike.radius), spike.z+spike.height/2)
		end
	--]]
end, MT_TVZ_SPEAR_HORIZONTAL)

addHook("MapThingSpawn", function(mo, mt)
	local TMSF_RETRACTED = 1
	local TMSF_INTANGIBLE = 1<<1

	if mt.args[0] then
		mo.flags = $ & ~MF_SCENERY
		mo.fuse = mt.args[1]
	end

	if mt.args[2] & TMSF_RETRACTED then
		mo.state = mo.info.meleestate
	end

	if not (mt.args[2] & TMSF_INTANGIBLE) and not (metalrecording) then
		mo.flags = $ & ~(MF_NOBLOCKMAP|MF_NOCLIPHEIGHT)
		mo.flags = $ | MF_SOLID
	end

	local base = P_SpawnMobj(mo.x - P_ReturnThrustX(mo, mo.angle - ANGLE_90, mo.radius), mo.y - P_ReturnThrustY(mo, mo.angle - ANGLE_90, mo.radius), mo.z, MT_TVZ_SPEAR_HORIZONTAL_BASE)
	base.angle = mo.angle
	base.scale = mo.scale
	base.target = mo
	mo.tracer = base
end, MT_TVZ_SPEAR_HORIZONTAL)

addHook("MobjFuse", function(mo)
	if not mo then return end
	if not mo.valid then return end

	mo.state = states[mo.state].nextstate
	if (mo.spawnpoint) then
		mo.fuse = mo.spawnpoint.args[0]
	else
		mo.fuse = mo.info.speed
	end
	return true
end, MT_TVZ_SPEAR_HORIZONTAL)

addHook("MobjThinker", function(mo)
	if not mo then return end
	if not mo.valid then return end

	if not mo.target then
		P_RemoveMobj(mo)
		return
	end

	P_SetOrigin(
		mo,
		mo.target.x - P_ReturnThrustX(mo.target, mo.target.angle, mo.target.radius),
		mo.target.y - P_ReturnThrustY(mo.target, mo.target.angle, mo.target.radius),
		mo.target.z
	)
	mo.angle = mo.target.angle + ANGLE_90
	mo.scale = mo.target.scale
end, MT_TVZ_SPEAR_HORIZONTAL_BASE)