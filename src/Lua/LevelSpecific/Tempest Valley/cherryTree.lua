--Sprite by Renato Madman
--Lua by Speed2411

freeslot(
"MT_BIGCHERRYTREE",
"S_BIGCHERRYTREE",
"SPR_MRCE_TVZ_BIGCHERRYTREE")

mobjinfo[MT_BIGCHERRYTREE] = {
--$Category Tempest Valley
--$Name Big Cherry Tree
--$Sprite 2TVZA0
	doomednum = 70,
	spawnstate = S_BIGCHERRYTREE,
	spawnhealth = 1000,
	reactiontime = 8,
	radius = 40*FRACUNIT,
	height = 600*FRACUNIT,
	mass = 16,
	dispoffset = 1,
	flags = MF_SOLID|MF_SCENERY|MF_NOTHINK
}
states[S_BIGCHERRYTREE] = {
	sprite = SPR_MRCE_TVZ_BIGCHERRYTREE,
	frame = A
}