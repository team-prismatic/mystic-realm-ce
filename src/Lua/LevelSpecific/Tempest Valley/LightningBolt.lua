--Tempest Valley Lightning Strikes
freeslot("MT_LIGHTNING_BOLT_PART", "MT_LIGHTNING_SPAWNER", "S_LIGHTNING_BOLT_PART")

mobjinfo[MT_LIGHTNING_SPAWNER] = {
--$Name Lightning Bolt Spawner
--$Sprite THOKA0
--$Category Mystic Realm Special
--$NotAngled
--$Arg0 Radius
--$Arg0RenderStyle Rectangle
--$Arg1 Min Spawntime
--$Arg2 Max Spawntime
--$Arg3 Instakill?
--$Arg4 Bolts
	doomednum = 2223,
	spawnstate = S_INVISIBLE,
	radius = 12*FRACUNIT,
	height = 24*FRACUNIT,
	flags = MF_NOGRAVITY
}

mobjinfo[MT_LIGHTNING_BOLT_PART] = {
	doomednum = -1,
	spawnstate = S_LIGHTNING_BOLT_PART,
	spawnhealth = 1000,
	reactiontime = 8,
	radius = 72*FRACUNIT,
	height = 80*FRACUNIT,
	mass = 16,
	dispoffset = 1,
	flags = MF_SPECIAL|MF_NOGRAVITY
}

states[S_LIGHTNING_BOLT_PART] = {SPR_THOK,A|TR_TRANS50,-1,nil,7,2,S_LIGHTNING_BOLT_PART}

local zapsafe = function(p)
    if (p.powers[pw_shield] & SH_PROTECTELECTRIC) then return true end
    if p.mo.skin == "surge" then return true end
end

addHook("TouchSpecial", function(mo, toucher)
    local KIIEELLL = 0
    if mo.superzap and not zapsafe(toucher.player) then
        KIIEELLL = DMG_INSTAKILL
    end
    P_DamageMobj(toucher, mo, mo, 1, DMG_ELECTRIC|KIIEELLL)
    return true
end, MT_LIGHTNING_BOLT_PART)

addHook("MobjThinker", function(mo)
    if mo.z > mo.floorz + 96*FRACUNIT then
        local spawned = P_SpawnMobjFromMobj(mo, 0, 0, -96*FRACUNIT, MT_LIGHTNING_BOLT_PART)
        spawned.fuse = 10
        spawned.blendmode = AST_ADD
        spawned.color = SKINCOLOR_WHITE
        if mo.superzap then
            spawned.superzap = true
        end
    end

end, MT_LIGHTNING_BOLT_PART)

addHook("MobjThinker", function(mo)
    if not (mo and mo.valid) then return end
    local ag = mo.spawnpoint.args
    mo.boltspawntime = $ or 0
    if mo.boltspawntime > 0 then
        mo.boltspawntime = $ - 1
    else
        for i = 1, max(1, ag[4]), 1 do
            local bx, by = P_RandomRange(-ag[0], ag[0]), P_RandomRange(-ag[0], ag[0])
            local bz = P_CeilingzAtPos(bx, by, mo.z, 24*FRACUNIT)
            local spawned = P_SpawnMobjFromMobj(mo, bx*16*FRACUNIT, by*16*FRACUNIT, bz, MT_LIGHTNING_BOLT_PART)
            S_StartSound(spawned, P_RandomRange(sfx_litng2, sfx_litng4))
            spawned.fuse = 10
            spawned.blendmode = AST_ADD
            spawned.color = SKINCOLOR_WHITE
            if ag[3] and ag[3] > 0 then
                spawned.superzap = true
            end
        end
        mo.boltspawntime = P_RandomRange(ag[1], ag[2])*10
    end
end, MT_LIGHTNING_SPAWNER)