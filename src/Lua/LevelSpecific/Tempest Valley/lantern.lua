

freeslot(
	"MT_LANTERN",
	"S_LCORONA",
	"S_LANTERN",
	"S_LANTERN1",
	"S_LANTERN2",
	"S_LANTERNPOP1",
	"S_LANTERNPOP2",
	"S_LANTERNPOP3",
	"S_LANTERNPOP4",
	"S_LANTERNPOP5",
	"S_LANTERNPOP6",
	"SPR_MRCE_TORO_NAGASHI_LANTERNS"
)

local COLOR_LUT = {
SKINCOLOR_WHITE,
SKINCOLOR_BONE,
SKINCOLOR_CLOUDY,
SKINCOLOR_GREY,
SKINCOLOR_SILVER,
SKINCOLOR_CARBON,
SKINCOLOR_JET,
SKINCOLOR_BLACK,
SKINCOLOR_AETHER,
SKINCOLOR_SLATE,
SKINCOLOR_BLUEBELL,
SKINCOLOR_PINK,
SKINCOLOR_YOGURT,
SKINCOLOR_BROWN,
SKINCOLOR_BRONZE,
SKINCOLOR_TAN,
SKINCOLOR_BEIGE,
SKINCOLOR_MOSS,
SKINCOLOR_AZURE,
SKINCOLOR_LAVENDER,
SKINCOLOR_RUBY,
SKINCOLOR_SALMON,
SKINCOLOR_RED,
SKINCOLOR_CRIMSON,
SKINCOLOR_FLAME,
SKINCOLOR_KETCHUP,
SKINCOLOR_PEACHY,
SKINCOLOR_QUAIL,
SKINCOLOR_SUNSET,
SKINCOLOR_COPPER,
SKINCOLOR_APRICOT,
SKINCOLOR_ORANGE,
SKINCOLOR_RUST,
SKINCOLOR_GOLD,
SKINCOLOR_SANDY,
SKINCOLOR_YELLOW,
SKINCOLOR_OLIVE,
SKINCOLOR_LIME,
SKINCOLOR_PERIDOT,
SKINCOLOR_APPLE,
SKINCOLOR_GREEN,
SKINCOLOR_FOREST,
SKINCOLOR_EMERALD,
SKINCOLOR_MINT,
SKINCOLOR_SEAFOAM,
SKINCOLOR_AQUA,
SKINCOLOR_TEAL,
SKINCOLOR_WAVE,
SKINCOLOR_CYAN,
SKINCOLOR_SKY,
SKINCOLOR_CERULEAN,
SKINCOLOR_ICY,
SKINCOLOR_SAPPHIRE,
SKINCOLOR_CORNFLOWER,
SKINCOLOR_BLUE,
SKINCOLOR_COBALT,
SKINCOLOR_VAPOR,
SKINCOLOR_DUSK,
SKINCOLOR_PASTEL,
SKINCOLOR_PURPLE,
SKINCOLOR_BUBBLEGUM,
SKINCOLOR_MAGENTA,
SKINCOLOR_NEON,
SKINCOLOR_VIOLET,
SKINCOLOR_LILAC,
SKINCOLOR_PLUM,
SKINCOLOR_RASPBERRY,
SKINCOLOR_ROSY
}

local TYPE_LANTERN_LUT = {
	[0] = S_LANTERN,
	S_LANTERN1,
	S_LANTERN2,
}

addHook("MapThingSpawn", function(mo, mt)
	local color_arg = mt.stringargs[0] and
	string.upper(mt.stringargs[0] or '') or nil					-- Color
	local sine_arg = mt.args[0] 								-- Z Range
	local spee_arg = mt.args[1]									-- Z Speed
	local sprr_arg = mt.args[2]									-- Disable Tangibility
	local resp_arg = mt.args[3]									-- Disable Respawn
	local spri_arg = mt.args[4]									-- Sprite Type [0, 1, 2]

	if color_arg and _G[color_arg] and skincolors[_G[color_arg]]
	and skincolors[_G[color_arg]] ~= SKINCOLOR_NONE then
		mo.extravalue1 = _G[color_arg]
	else
		mo.extravalue1 = COLOR_LUT[P_RandomRange(1, #COLOR_LUT)]
		if color_arg then
			print("\x85".."WARNING:".."\x80".."Object #"..#mt.." has invalid skincolor indentificator!")
		end
	end

	if spri_arg == -1 then
		mo.state = TYPE_LANTERN_LUT[P_RandomRange(0, #TYPE_LANTERN_LUT)]
	elseif spri_arg < -1 then
		mo.state = TYPE_LANTERN_LUT[P_RandomRange(1, #TYPE_LANTERN_LUT)]
	else
		mo.state = TYPE_LANTERN_LUT[min(max(spri_arg, 0), #TYPE_LANTERN_LUT)]
	end

	mo.disrespawn = (resp_arg == 1)
	mo.extravalue2 = sine_arg >> 2
	mo.speed = spee_arg*(ANG1 >> 2)

	if sprr_arg then
		mo.flags = $ &~ MF_SPRING
	end
end, MT_LANTERN)

addHook("MobjThinker", function(mo)
	if mo.overlay and mo.overlay.valid and (mo.flags2 & MF2_DONTDRAW) then
		mo.overlay.flags2 = $ | MF2_DONTDRAW
	end

	if mo.extravalue2 then
		mo.z = mo.z+(mo.extravalue2*sin(mo.threshold))*P_MobjFlip(mo)
		if mo.hasbeencolored then
			mo.threshold = $+mo.speed
		end
	end

	if mo.disrespawn and mo.state == S_LANTERNPOP4 then
		P_RemoveMobj(mo)
		return
	end

	if mo.hasbeencolored then return false end

	if udmf and not mo.extravalue2 and mo.spawnpoint and mo.spawnpoint.valid then
		local sine_arg = mo.spawnpoint.args[0] 				-- Z Range
		local spee_arg = mo.spawnpoint.args[1] 				-- Z Speed
		local sprr_arg = mo.spawnpoint.args[2]				-- Disable Tangibility
		local spri_arg = mo.spawnpoint.args[4]				-- Sprite Type [0, 1, 2]

		if spri_arg == -1 then
			mo.state = TYPE_LANTERN_LUT[P_RandomRange(0, #TYPE_LANTERN_LUT)]
		elseif spri_arg < -1 then
			mo.state = TYPE_LANTERN_LUT[P_RandomRange(1, #TYPE_LANTERN_LUT)]
		else
			mo.state = TYPE_LANTERN_LUT[min(max(spri_arg, 0), #TYPE_LANTERN_LUT)]
		end
		mo.extravalue2 = sine_arg >> 2
		mo.speed = spee_arg*(ANG1 >> 2)

		if sprr_arg then
			mo.flags = $ &~ MF_SPRING
		end
	end

	mo.shadowscale = (3 << FRACBITS) >> 2
	local myass_corona = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_OVERLAY)
	myass_corona.state = S_LCORONA
	myass_corona.target = mo
	mo.overlay = myass_corona

	if mo.spawnpoint and mo.spawnpoint.options and (mo.spawnpoint.options & MTF_EXTRA) then
		mo.color = COLOR_LUT[P_RandomRange(1, #COLOR_LUT)]
		myass_corona.color = mo.color
		mo.hasbeencolored = true
	else
		if mo.extravalue1 then
			mo.color = mo.extravalue1
		else
			mo.color = max(min(FixedInt(AngleFixed(mo.angle)), #skincolors - 1), 1)
		end
		myass_corona.color = mo.color
		mo.hasbeencolored = true
	end

	mo.angle = FixedAngle(mo.color << FRACBITS)
end, MT_LANTERN)

--addHook("MobjThinker", function(mo)
--	if mo.target and mo.target.valid
--		mo.color = mo.target.color
--		if mo.target.eflags & MFE_VERTICALFLIP then
--			mo.eflags = $ | MFE_VERTICALFLIP
--		else
--			if mo.eflags & MFE_VERTICALFLIP then
--				mo.eflags = $ & ~MFE_VERTICALFLIP
--			end
--		end
--	else
--		P_RemoveMobj(mo)
--	end
--end, MT_LCORONA)

mobjinfo[MT_LANTERN] = {
--$NotAngled
--$Category Tempest Valley
--$Name Lantern
--$Sprite SPR_MRCE_TORO_NAGASHI_LANTERNS
--$Arg0 Z Movement Range
--$Arg1 Z Movement Speed
--$Arg2 Disable Tangibility?
--$Arg2Type 11
--$Arg2Enum noyes
--$Arg3 Disable Respawn?
--$Arg3Type 11
--$Arg3Enum noyes
--$Arg4 Sprite
        doomednum = 69,
        spawnstate = S_LANTERN,
        spawnhealth = 1,
        seestate = S_NULL,
        seesound = sfx_None,
        reactiontime = 0,
        attacksound = sfx_none,
        painstate = S_NULL,
        painchance = MT_THOK,
        painsound = sfx_s3k77,
        meleestate = S_NULL,
        missilestate = S_NULL,
        deathstate = S_LANTERNPOP2,
        xdeathstate = S_NULL,
        deathsound = sfx_None,
        speed = 0,
        radius = 22*FRACUNIT,
        height = 47*FRACUNIT,
        dispoffset = 3,
        mass =17*FRACUNIT,
        damage = 0,
        activesound = sfx_None,
        flags = MF_SPRING|MF_NOGRAVITY|MF_RUNSPAWNFUNC,
        raisestate = S_LANTERNPOP1
}

states[S_LCORONA] = {SPR_MRCE_TORO_NAGASHI_LANTERNS, B|FF_SEMIBRIGHT|FF_ADD|FF_TRANS70, -1, nil, 1, 0, S_NULL}
states[S_LANTERN] = {SPR_MRCE_TORO_NAGASHI_LANTERNS, A|FF_FULLBRIGHT, -1, A_SetObjectFlags2, MF2_DONTDRAW, 1, S_NULL}
states[S_LANTERN1] = {SPR_MRCE_TORO_NAGASHI_LANTERNS, D|FF_FULLBRIGHT, -1, A_SetObjectFlags2, MF2_DONTDRAW, 1, S_NULL}
states[S_LANTERN2] = {SPR_MRCE_TORO_NAGASHI_LANTERNS, E|FF_FULLBRIGHT, -1, A_SetObjectFlags2, MF2_DONTDRAW, 1, S_NULL}
states[S_LANTERNPOP1] = {SPR_MRCE_TORO_NAGASHI_LANTERNS, A, 0, A_RemoteDamage, 0, 1, S_LANTERNPOP2}
						--
states[S_LANTERNPOP2] = {SPR_NULL, A, 1, A_SetObjectFlags2, MF2_DONTDRAW, 2, S_LANTERNPOP3}
						--
states[S_LANTERNPOP3] = {SPR_NULL, A, 1, A_Pain, 0, 0, S_LANTERNPOP4}
						--
states[S_LANTERNPOP4] = {SPR_NULL, A, 35, A_CheckFlags2, MF2_AMBUSH, 0, S_LANTERNPOP5}
						--
states[S_LANTERNPOP5] = {SPR_NULL, A, 490, nil, 0, 0, S_LANTERNPOP6}
						--
states[S_LANTERNPOP6] = {SPR_NULL, A, 0, A_SpawnFreshCopy, 0, 0, S_NULL}



