--===============Declare variables=====================--

local PhilipStrength = 5
COM_AddCommand("WindStrength", function(player, str)
	PhilipStrength = tonumber(str)
	print("Set Philip strength to " .. str)
end)

local stormSectors = {}


--==============Functions==================--


local function applyWind(mo)
	if mo.player then

		local moveDirX = (mo.x + (PhilipStrength*FRACUNIT))
		local moveDirY = mo.y + 0
		local moveDirZ = mo.z + 0


		if not P_IsObjectOnGround(mo) then
			moveDirX = mo.x + (2*PhilipStrength*FRACUNIT)
		end


		if P_CheckPosition(mo, moveDirX, moveDirY, moveDirZ) then
			P_TryMove(mo, moveDirX, moveDirY, moveDirZ)
		end
	else
		P_Move(mo,PhilipStrength)
	end
end

local function stormNoises(mo)
	if not mo then return end
	if not mo.valid then return end
	if not mo.player then return end
	if S_IdPlaying(sfx_philip) then return end

	S_StartSound(mo, sfx_philip, mo.player)


	--S_StopSoundByID(mo,sfx_philip)
end



local function Philip(mob) --MAIN
	if gamemap ~= 124 then return end
	if not mob then return end

	local mo

	if mob.mo then --Is this a player? If so, then set mo to player mo --OOOORRR you could just check the type with userdataType(mob)
		mo = mob.mo
	else
		mo = mob
	end

	if not mo.valid then return end

	if mo.player or mo.info.flags & MF_ENEMY then

		for fof in mo.subsector.sector.ffloors() do
			if mo.z <= fof.topheight and mo.z + mo.height >= fof.bottomheight and fof.sector.specialflags & SSF_RETURNFLAG then
				applyWind(mo)
				stormNoises(mo)
				--mo.blendmode = mo.blendmode | AST_ADD
				--print("Inside Philip")
			end
		end
	end
end
addHook("MobjThinker", Philip)
addHook("PlayerThink", Philip)

--==========HUD STUFF====================--

local function drawText(v, p)

	if gamemap == 124 then
		local text = "Wind Direction: East"

		v.drawString(325*FRACUNIT, 40*FRACUNIT, text, V_20TRANS, "fixed-right")
	end
end
hud.add(drawText, "game")
