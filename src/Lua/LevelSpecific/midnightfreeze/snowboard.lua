--snowboards
freeslot(
"MT_SNOWBOARD",
"S_SNOWBOARD",
"MT_SNOWBOARDSPLAT",
"S_SNWBRD1",
"S_SNWBRD2",
"S_SNWBRDF",
"S_PLAY_SNOWBOARDRIDE",
"SPR_SBRD"
)

local skinstuff = {
	["sonic"] = "SPR2_FALL",
	["trip"] = "SPR2_TAL8"
}

states[S_PLAY_SNOWBOARDRIDE] = {
	sprite = SPR_PLAY,
	frame = SPR2_GRND|FF_ANIMATE,
	tics = -1,
	nextstate = S_PLAY_SNOWBOARDRIDE
	}


--Todo: Add more unmounting conditions, like if the player touches a water FOF or maybe if they get frozen, they lose the board.


addHook("MobjThinker", function(mo)
	-- Break the snowboard
	if not mo.playerOnBoard then
		P_InstaThrust(mo, mo.angle, 24*FRACUNIT)
		mo.flags = $|MF_NOCLIPHEIGHT
		return
	end

	local p = mo.playerOnBoard

	-- Make the snowboard face the correct direction
	mo.angle = p.drawangle

	P_MoveOrigin(mo, p.mo.x, p.mo.y, p.mo.z)

end, MT_SNOWBOARDSPLAT)


-- Improved Snowboard functionality with enhanced downhill acceleration
addHook("PlayerThink", function(p)
	if not p.realmo then return end
	if p.spectator then return end

	local x = p.mrce
	local sb = x.snowboard
	local momangle = R_PointToAngle2(0, 0, p.rmomx, p.rmomy)

	if sb == true and p.playerstate == PST_LIVE then

		-- Ensure player spins on board
		if not (p.pflags & PF_SPINNING) then
			p.pflags = $|PF_SPINNING
		end

		-- Enable thok state while in the air
		if not P_IsObjectOnGround(p.mo) and not (p.pflags & PF_THOKKED) then
			p.pflags = $|PF_THOKKED
		end

		-- Ensure no spinitem while on board
		if p.spinitem then
			p.spinitem = 0
		end

		-- Speed management with cap
		local maxSpeed = 100 * FRACUNIT  -- Set a reasonable speed cap
		local currentSpeed = x.realspeed

		-- Check if the player is going downhill
		local downSlopeAcceleration = 0
		if p.mo.momz < 0 then
			downSlopeAcceleration = 3 * FRACUNIT  -- Increase acceleration when going downhill
		end

		if currentSpeed < (30 * FRACUNIT) - p.mo.momz then
			P_Thrust(p.mo, momangle, FRACUNIT + downSlopeAcceleration)  -- Faster acceleration down slopes
		elseif currentSpeed > maxSpeed then
			-- Cap the speed to avoid going too fast
			P_Thrust(p.mo, momangle + ANGLE_180, (currentSpeed - maxSpeed) / 2)  -- Smooth deceleration when over max speed
		end

		-- Apply drawangle to improve responsiveness in visuals
		p.drawangle = momangle

		-- Keep player in falling state while on board

		for k, v in pairs(skinstuff) do
			if p.mo.skin == k and p.mo.sprite2 ~= _G[v] then
				p.mo.sprite2 = _G[v]
				--return
			elseif p.mo.state ~= S_PLAY_SNOWBOARDRIDE then
				p.mo.state = S_PLAY_SNOWBOARDRIDE
			end
		end

		-- Fix inverted controls at high speeds by normalizing sidemove
		if (p.cmd.sidemove ~= 0) then
			if P_IsObjectOnGround(p.mo) then
				-- Ensure turning behavior is correct at all speeds
				if currentSpeed < maxSpeed then
					p.mo.movefactor = $ * 6  -- Normal turning speed on the ground
				else
					p.mo.movefactor = $ * 3  -- Reduce turning sensitivity at higher speeds
				end
			else
				p.mo.movefactor = $ / 2  -- Looser control in the air
			end
		end

		-- Adjust spin kick behavior for smoother speed boost
		if x.spinkick > 0 then
			x.spinkick = $ - 1
			if currentSpeed < (28 * FRACUNIT) and P_IsObjectOnGround(p.mo) then
				if currentSpeed > (7 * FRACUNIT) then
					P_InstaThrust(p.mo, momangle, 30 * FRACUNIT)  -- Boost, but within a reasonable range
				else
					P_InstaThrust(p.mo, p.mo.angle, 30 * FRACUNIT)
				end
			end
		end

		-- Activate spin kick
		if x.spin == 2 then
			x.spinkick = 7
		end

		-- Unmount if the player touches water
		if p.mo.watertop >= p.mo.z and p.mo.waterbottom <= p.mo.z then
			toggleSnowboardSprite(p, 0)
			unmountBoard(p)
		end

		-- Unmount if the player is frozen
		if p.frozentimer then
			toggleSnowboardSprite(p, 0)
			unmountBoard(p)
		end
	end
end)






addHook("SpinSpecial", function(p)
	if not p.realmo then return end
	if p.spectator then return end
	if p.mrce.snowboard then
		return true
	end
end)


addHook("PlayerCmd", function(p,c)
	if p.mrce and p.mrce.snowboard and p.playerstate == PST_LIVE then
		c.forwardmove = 25
	end
end)

--Object and state declerations
mobjinfo[MT_SNOWBOARD] = {
	doomednum = 3114,
	spawnstate = S_SNOWBOARD,
	radius = 28*FRACUNIT,
	height = 38*FRACUNIT,
	flags = MF_SPECIAL
}

states[S_SNOWBOARD] = {
	sprite = SPR_KYST,
	frame = A,
}

mobjinfo[MT_SNOWBOARDSPLAT] = {
	doomednum = 3115,
	spawnstate = S_SNWBRD1,
	radius = 28*FRACUNIT,
	height = 38*FRACUNIT,
	flags = MF_NOCLIP
}

states[S_SNWBRD1] = {
	sprite = SPR_SBRD,
	frame = FF_FLOORSPRITE|A,
	tics = 1,
	nextstate = S_SNWBRD2,
}
states[S_SNWBRD2] = {
	sprite = SPR_SBRD,
	frame = FF_FLOORSPRITE|A,
}

states[S_SNWBRDF] = {
	sprite = SPR_SBRD,
	frame = FF_FLOORSPRITE|FF_TRANS50|A,
	tics = 5*TICRATE
}


local function toggleSnowboardSprite(player, toggle)

	if toggle == 1 and player.mrce.snowboard ~= true then
		player.personalSnowboard = P_SpawnMobj(player.mo.x, player.mo.y, player.mo.z, MT_SNOWBOARDSPLAT)
		--player.mo.skate = player.personalSnowboard
		player.personalSnowboard.playerOnBoard = player
		P_CreateFloorSpriteSlope(player.personalSnowboard)
		player.personalSnowboard.renderflags = $|RF_NOSPLATBILLBOARD|RF_OBJECTSLOPESPLAT
	elseif player.mrce.snowboard == true and toggle == 0 then
		if not player.personalSnowboard then return end
		if not player.personalSnowboard.valid then return end

		S_StartSound(player.personalSnowboard, 	sfx_wbreak, player)
		player.personalSnowboard.playerOnBoard = nil
		player.personalSnowboard.state = S_SNWBRDF
		player.personalSnowboard = 0
		player.mo.state = S_PLAY_WALK
		--player.mo.skate = nil
		P_Thrust(player.mo, player.drawangle, skins[player.mo.skin].runspeed)
		player.pflags = $ & ~PF_SPINNING
	else
		return
	end
end

local physicsCharIsUsing --Net game compatible?

local function mountBoard(player)
	player.mrce.snowboard = true
	physicsCharIsUsing = player.mrce.physicsMode
	player.mrce.physicsMode = 4
end

local function unmountBoard(player)
	if not player.mrce then return end
	player.mrce.snowboard = false
	if physicsCharIsUsing then player.physicsMode = physicsCharIsUsing end
end


--Activating snowboard
addHook("TouchSpecial", function(mo, toucher)
	if mo and mo.valid and toucher and toucher.valid and toucher.player and toucher.player.mrce and not toucher.player.mrce.snowboard then
		toucher.player.mrce.snowboard = true
		return true
	end
end, MT_SNOWBOARD)


addHook("LinedefExecute",function(line, mo, d)
	if mo and mo.player then
		toggleSnowboardSprite(mo.player, 1)
		mountBoard(mo.player)
	end
end, "START_SNOWBOARD")

addHook("LinedefExecute",function(line, mo, d)
	if mo and mo.valid and mo.player then
		toggleSnowboardSprite(mo.player, 0)
		unmountBoard(mo.player)
	end
end, "STOP_SNOWBOARD")

addHook("MobjMoveBlocked", function(mo, t, l)
	if not mo.player then return end
	local p = mo.player
	if not p.realmo then return end
	if p.spectator then return end
	if not l then return end

	-- Unmount if the player touches a non-climbable wall
	if p.mrce.snowboard and l.flags&ML_NOCLIMB then
		toggleSnowboardSprite(mo.player, 0)
		P_DoPlayerPain(p) -- Also throw the player back
		unmountBoard(p)
	end
end, MT_PLAYER)

addHook("MobjDeath", function(mo, moIn, moSrc, dmgType)
	if mo and mo.valid and mo.player then
		unmountBoard(mo.player)
	end
end, MT_PLAYER)

addHook("PlayerSpawn", unmountBoard)
