-- Table to track progress for each sector
local sectorProgress = {}
local coroutineTable = {}
local respawnTime = 15 * TICRATE

local function findSectorPerimeter(sector)
	local curLine
	local ver1
	local ver2
	local numberOfLinesInSector = #sector.lines
	local runningDist = 0
		
	for i = 1, numberOfLinesInSector, 1 do
		if sector.lines[i] then 
			curLine = sector.lines[i]
			ver1 = curLine.v1
			ver2 = curLine.v2						
			runningDist = runningDist + R_PointToDist2(ver1.x, ver1.y, ver2.x, ver2.y)
		end
	end
	return runningDist
end

local function crackIce(rover)
	local isIceGone = false
	local storeAlpha = rover.alpha
	
	if not isIceGone then
		rover.flags = rover.flags & ~FOF_SOLID
		rover.alpha = 0
		isIceGone = true
		--print("Breaking ice!")
		coroutine.yield()
	end
	
	rover.flags = (rover.flags | FOF_EXISTS) | FOF_SOLID
	rover.alpha = storeAlpha
	isIceGone = false
	
	rover.sector.floorpic = "ICEFLRA"
	rover.sector.ceilingpic = "ICEFLRA"
	--print("Respawning ice: Alpha = " .. storeAlpha)
	return
end

addHook("LinedefExecute", function (line, mo, sector)
	if not mo.floorrover then return end
	
	local iceFOF = mo.floorrover
	local timeTillIceBreak = findSectorPerimeter(iceFOF.target)
	
	timeTillIceBreak = FixedSqrt(timeTillIceBreak) / FRACUNIT
	timeTillIceBreak = timeTillIceBreak - ((timeTillIceBreak / 3) * 2)

	if timeTillIceBreak == 0 then 
		print("Invalid ice block size")
		return
	end

	-- Initialize progress if not already set
	if not sectorProgress[sector] then
		sectorProgress[sector] = 0
	end

	local iceProgress = sectorProgress[sector]
	local quarterOfIceProgress = timeTillIceBreak / 4

	-- Only create coroutine if progress is complete and no coroutine exists for this sector
	if iceProgress >= timeTillIceBreak and not coroutineTable[sector] then
		local iceCoroutine = coroutine.create(function() crackIce(iceFOF) end)
		coroutineTable[sector] = {co = iceCoroutine, benchmarkedTime = leveltime}
		coroutine.resume(iceCoroutine)
		S_StartSound(mo, 161)
		return
	end

	-- Update progress and textures if cracking is not yet complete
	sectorProgress[sector] = iceProgress + 1
	if iceProgress < quarterOfIceProgress then
		sector.ceilingpic = "ICECRCK1"
		sector.floorpic = "ICECRCK1"
	elseif iceProgress < quarterOfIceProgress * 2 then
		sector.ceilingpic = "ICECRCK2"
		sector.floorpic = "ICECRCK2"
	elseif iceProgress < quarterOfIceProgress * 3 then
		sector.ceilingpic = "ICECRCK3"
		sector.floorpic = "ICECRCK3"
	elseif iceProgress < quarterOfIceProgress * 4 then	
		sector.ceilingpic = "ICECRCK4"
		sector.floorpic = "ICECRCK4"
	end

	-- Play cracking sound
	if not S_IdPlaying(160) then
		S_StartSound(mo, 160)
	end
end, "MFZ2_ICECRACK")

local function iceUpdate()
	if gamemap == 114 or gamemap == 127 or gamemap == 785 then
		for sector, entry in pairs(coroutineTable) do
			local co = entry.co
			local benchmarkedTime = entry.benchmarkedTime
			
			if coroutine.status(co) == "suspended" and leveltime >= respawnTime + benchmarkedTime then
				coroutine.resume(co)
				coroutineTable[sector] = nil  -- Remove coroutine entry after respawn
				sectorProgress[sector] = 0  -- Reset sector progress for next cycle
			end
		end
	end
end
addHook("ThinkFrame", iceUpdate)
