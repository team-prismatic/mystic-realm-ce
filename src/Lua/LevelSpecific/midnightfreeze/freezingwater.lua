--
-- Frozen screen effect for Midnight Freeze and Silver Cavern
-- by Radicalicious
--

-- Bit-wise operations are still operations, so please pre-compute flags.
local TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT
local TOP_LEFT_FLAGS = V_PERPLAYER|V_SNAPTOTOP|V_SNAPTOLEFT
local TOP_RIGHT_FLAGS  = V_PERPLAYER|V_SNAPTOTOP|V_SNAPTORIGHT
local BUTTOM_LEFT_FLAGS = V_PERPLAYER|V_SNAPTOBOTTOM|V_SNAPTOLEFT
local BUTTOM_RIGHT_FLAGS = V_PERPLAYER|V_SNAPTOBOTTOM|V_SNAPTORIGHT

-- Optimalization measures, perhaps?
-- Sounds dumb but according to https:--www.lua.org/gems/sample.pdf paper
-- It may be even more efficient, we will see

-- Reducing Global/C api calls is the way.

local SH_PROTECTWATER = SH_PROTECTWATER
local MFE_UNDERWATER = MFE_UNDERWATER
local FF_TRANSSHIFT = FF_TRANSSHIFT
local TICRATE = TICRATE

local min = min
local max = max

local P_InSpaceSector = P_InSpaceSector

-- WARNING, d/v is a drawlist for that specific instance!
-- So each function allocation for each hud scene requires this
-- At least according my C knowledge,
local game_draw
local game_fade

local function onfire(player)
	if player.spectator then return false end
	if not player.realmo then return false end
	if (player.powers[pw_shield] & SH_PROTECTFIRE)
	or ((player.mo.skin == "supersonic" or player.mo.skin == "adventuresonic") and (player.powers[pw_shield] & SH_FIREFLOWER))
	or (player.mo.skin == "blaze" and (player.solchar and player.solchar.istransformed) or (player.blazeboosting and not (player.mo.eflags & MFE_UNDERWATER))
		or (player.mo.blazejumping and not (player.mo.eflags & MFE_UNDERWATER))
		or (player.blazehover and not (player.mo.eflags & MFE_UNDERWATER)))
	or MRCE_isHyper(player) then
		return true
	end
	return false
end

hud.add(function(d, p)
	-- Initiation
	if not TOP_LEFT then
		TOP_LEFT = d.cachePatch("FROZOVR1")
		TOP_RIGHT = d.cachePatch("FROZOVR2")
		BOTTOM_LEFT = d.cachePatch("FROZOVR3")
		BOTTOM_RIGHT = d.cachePatch("FROZOVR4")

		game_draw = d.draw
		game_fade = d.fadeScreen
	end

	if (p.mrce and p.mrce.freezeeffect ~= nil) then
		local time_def = min(max(p.mrce.freezeeffect/TICRATE, 0), 8)
		if 1 > time_def then return end
		local TRANSPARENCY = (9-time_def) << FF_TRANSSHIFT
		game_fade(141, time_def >> 1)

		game_draw(0, 0, TOP_LEFT, TOP_LEFT_FLAGS|TRANSPARENCY)
		game_draw(320, 0, TOP_RIGHT, TOP_RIGHT_FLAGS|TRANSPARENCY)
		game_draw(0, 200, BOTTOM_LEFT, BUTTOM_LEFT_FLAGS|TRANSPARENCY)
		game_draw(320, 200, BOTTOM_RIGHT, BUTTOM_RIGHT_FLAGS|TRANSPARENCY)
	end
end, "game")

addHook("PlayerThink", function(p)
	if not p then return end
	if not p.valid then return end
	if not p.mo then return end
	if not p.mo.valid then return end

	if p.mrce.freezeeffect ~= nil and (p.mrce.freezeeffect >= 0 or p.mrce.infreezingwind) then
		local mo = p.mo
		local powers = p.powers

		if onfire(p) then
			p.mrce.freezeeffect = max(0, $ - 20)
		else
			if not (powers[pw_shield] & SH_PROTECTWATER)
			and (powers[pw_spacetime] > 0
			and	mo.eflags & MFE_UNDERWATER
			and	P_InSpaceSector(mo)) then
				p.mrce.freezeeffect = $ + 1
			elseif not p.mrce.infreezingwind then
				p.mrce.freezeeffect = $ - 5
				if p.mrce.freezeeffect <= 0 then
					mo.spritexoffset = 0
					p.mrce.freezeeffect = 0
				end
			elseif p.mrce.frozentimer then
				p.mrce.freezeeffect = $ - 15
			elseif p.mrce.infreezingwind and not (leveltime % 6) then
				if p.mrce.freezeeffect > 3*TICRATE/2 then
					p.mrce.freezeeffect = $ - 5
				else
					p.mrce.freezeeffect = $ + 1
				end
			end
		end

		p.mrce.freezeeffect = max(min(p.mrce.freezeeffect, TICRATE*35), 0) -- CAP
		mo.spritexoffset = sin(ANG2*leveltime*((2*p.mrce.infreezingwind+1)*p.mrce.freezeeffect/2))
	else
		p.mrce.freezeeffect = 0
	end

	--print(tostring(p.mrce.freezeeffect))
end)

addHook("PlayerSpawn", function(p)
	if not p then return end
	if not p.valid then return end
	if not p.mo then return end
	if not p.mo.valid then return end

	p.mrce.infreezingwind = 0
	p.mrce.freezeeffect = 0
end)
