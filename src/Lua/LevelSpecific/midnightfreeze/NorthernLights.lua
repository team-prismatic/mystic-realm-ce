--
--	Mystic Realms Community Editions' Northen Lights
--	Contributed by Ace Lite, Changes made by Nami and Xian
--

-- note: it no longer is similar in anyway to C-code of CEZ Flags,
-- 		 it is possible there are more comments that are outdated to code.

-- Set custom number of segments...
-- In case of changing dimensions of sprites, refer to changing mobj radius as well.
local SEGMENTS = 28

-- Freeslots
local stop = 0

if stop then return end

freeslot("MT_AURORABOREALIS", "MT_AURORASEG", "S_AURORASEG", "S_AURORASEG1", "SPR_MRCE_AURORABOREALIS")

-- Cvars (Please organize things, don't make me do it)

local CV_auroraDeletus = CV_RegisterVar{
	name = "mr_mfz1boost",
	defaultvalue = 0,
	flags = CV_NETVAR, --Why is this a netvar? --probably bc it would cause desyncs if it wasn't
	PossibleValue = CV_OnOff
}


--[[
	Skydusk:

	Bleh, not necessary horrible.

	TODO: Stil needs some good movement algorithm.
]]

-- Optimalization measures, perhaps?
-- Sounds dumb but according to https:--www.lua.org/gems/sample.pdf paper
-- It may be even more efficient, we will see

-- Reducing Global/C api calls is the way.

local FRACUNIT = FRACUNIT

local ANG1 = ANG1
local ANG2 = ANG2
local ANG15 = ANG15

local ANGLE_90 = ANGLE_90
local ANGLE_180 = ANGLE_180

local P_SpawnMobjFromMobj = P_SpawnMobjFromMobj
local P_SetOrigin = P_SetOrigin

local FixedMul = FixedMul
local FixedDiv = FixedDiv
local FixedAngle = FixedAngle

local ease_inquint = ease.inquint
local ease_outquint = ease.outquint
local ease_inoutsine = ease.inoutsine

local cos = cos
local sin = sin
local abs = abs
local max = max
local min = min

local ipairs = ipairs

-- Mobjinfo

-- controller of Aurora mobjinfo
-- mapthing num - doomednum (921)

---@diagnostic disable-next-line
mobjinfo[MT_AURORABOREALIS] = {
	--$Name "Aurora Borealis Skybox Deco"
	--$Sprite 11ABA0
	--$Category "Midnight Freeze"
	doomednum = 921,
	spawnstate = S_INVISIBLE,
	spawnhealth = 1000,
	reactiontime = 8,
	deathsound = sfx_pop,
	radius = 48*FRACUNIT,
	height = 128*FRACUNIT,
	mass = 100,
	flags = MF_NOCLIP|MF_NOCLIPTHING|MF_NOBLOCKMAP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT
}

---@diagnostic disable-next-line
-- invidiual segment mobjinfo
mobjinfo[MT_AURORASEG] = {
	spawnstate = S_AURORASEG,
	spawnhealth = 1000,
	reactiontime = 8,
	deathsound = sfx_pop,
	radius = 28*FRACUNIT,
	height = 128*FRACUNIT,
	mass = 100,
	flags = MF_NOCLIP|MF_NOCLIPTHING|MF_NOBLOCKMAP|MF_SCENERY|MF_NOGRAVITY|MF_NOCLIPHEIGHT
}

-- States

-- 40% less sugar in your borealis.
states[S_AURORASEG] = {
	sprite = SPR_MRCE_AURORABOREALIS,
	frame = A|FF_PAPERSPRITE|FF_ADD|FF_TRANS10|FF_SEMIBRIGHT|FF_ANIMATE,
	tics = 42,
	var1 = 13,
	var2 = 3,
	nextstate = S_AURORASEG,
}

states[S_AURORASEG1] = {
	sprite = SPR_MRCE_AURORABOREALIS,
	frame = A|FF_PAPERSPRITE|FF_ADD|FF_TRANS60|FF_SEMIBRIGHT|FF_ANIMATE,
	tics = 42,
	var1 = 13,
	var2 = 3,
	nextstate = S_AURORASEG1,
}

-- Functions

-- offload
local angaur = ANG1/12
local aurdif = 4*FRACUNIT/5
local aurinc = 9*FRACUNIT/4
local aurmin = 2*FRACUNIT/5

-- Spawn function; makes 39 segments of Aurora;
local function auroraSpawn(a)
	if CV_auroraDeletus.value then return end

	-- container of all segments.
	a.segs = {}

	-- set up
	local scaley = 0
	local angx = 0
	local angy = 0

	-- Why was check there? I moved at very frist line of block.

	-- pre-calculation -- you don't want to make loading screens *longer* by calculating same thing.
	local SegCalStart = SEGMENTS/3
	local SegCalEnd = (SEGMENTS*2)/3

	local thrFRAC = FRACUNIT/SegCalStart
	local sixFRAC = FRACUNIT/16
	--local rand = P_RandomRange(1, 8)

	-- you can change amount of segments but don't forget to change scaling as well at the end.
	for i = 0,SEGMENTS do
		local seg = P_SpawnMobjFromMobj(a, 0,0,0, MT_AURORASEG)
		seg.tracer = (i > 0 and a.segs[i-1] or a)

		-- edit the scale for specific skybox purposes.
		seg.scale = a.scale -->> 2
		seg.spritexscale = seg.spritexscale >> 3

		seg.z = $+5*sin(i*ANGLE_90)

		-- segmenter
		seg.cusval = angy
		seg.threshold = i*angaur
		seg.extravalue1 = sin(angy)
		seg.extravalue2 = a.angle+FixedAngle(22*seg.extravalue1)+angx
		seg.angle = seg.extravalue2
		angx = $+ANG2
		angy = (angy+ANG15) % ANGLE_180

		-- Segments on each end are scaled to be smaller. Yes, I could do it more simplier but
		-- ease functions are simply giving me ease
		if i % 2 then
			seg.spriteyscale = FRACUNIT+FRACUNIT/4
		end

		if i < SegCalStart then
			seg.spriteyscale = FixedMul(ease_outquint(i*thrFRAC, sixFRAC, seg.spriteyscale), aurinc)
			seg.state = S_AURORASEG1
			seg.radius = $-(FRACUNIT << 1)
		elseif i > SegCalEnd then
			seg.spriteyscale = FixedMul(ease_inquint((i-SegCalEnd)*thrFRAC, seg.spriteyscale, sixFRAC), aurinc)
			seg.state = S_AURORASEG1
			seg.radius = $-(FRACUNIT << 1)
		else
			seg.spriteyscale = FixedMul(seg.spriteyscale, aurinc)
		end

		--seg.reactiontime = max(seg.spriteyscale - ease.outsine((((i+rand) % 10)/10) << FRACBITS, aurdif, 0), aurmin)
		seg.reactiontime = seg.spriteyscale

		-- insert into custom table in mobj userdata.
		table.insert(a.segs, seg)
	end
end

local function P_CalcEdgeCoordinates(a)
	local x = a.x + FixedMul(a.radius, cos(a.angle))
	local y = a.y + FixedMul(a.radius, sin(a.angle))
	return x, y
end

local function P_ConnectEdges(a, t)
	local radius = t.radius
	local x, y = P_CalcEdgeCoordinates(a)
	P_SetOrigin(t, x+FixedMul(radius, cos(t.angle)), y+FixedMul(radius, sin(t.angle)), a.z)
end

local AuroraColors = {
	SKINCOLOR_AQUA,
	SKINCOLOR_TEAL,
	SKINCOLOR_SKY,
	SKINCOLOR_SAPPHIRE,
	SKINCOLOR_RED,
	SKINCOLOR_FLAME,
	SKINCOLOR_PURPLE,
	SKINCOLOR_EMERALD,
	SKINCOLOR_APPLE,
}

-- Offing constant calculation -- for crying out loud!
local movemv = FRACUNIT/700
local angmax = ANG1*16
local angmin = -angmax
local angcal = ANG2/6

-- Thinker function; moves 97 segments of Aurora;
local function auroraThinker(a)
	local segs = a.segs
	local lvltime_ang = leveltime*angaur
	local leveltimedif = abs((leveltime % 1500)-699)*movemv
	local easedif = ease_inoutsine(leveltimedif, angmin, angmax)
	local leveltimex = leveltime*angcal

	-- Connect Edges
	for i = 1, #segs do
		local seg = segs[i]
		local size_sine = sin(i*lvltime_ang+seg.threshold)
		seg.extravalue1 = sin(seg.cusval+leveltimex)
		seg.angle = seg.extravalue2+FixedMul(easedif, seg.extravalue1)
		seg.spriteyscale = max(seg.reactiontime+(size_sine or 2)/2, movemv) -- math expectation otherwise
		P_ConnectEdges(seg.tracer, seg)
	end

	-- Switch the frame
	-- If I were making it for my own project I would use MapThingSpawn hook to spawn segments in
	-- Too bad I have to do this crime against humanity.
	if a.extravalue1 ~= 0 then return end
	a.extravalue1 = 1

	-- just decrease in checking if Aurora was spawned by different means.
	if not a.spawnpoint then return end

	local framenum = AuroraColors[min(max(a.spawnpoint.extrainfo or a.spawnpoint.args[0], 0), #AuroraColors-1)+1]

	for id,seg in ipairs(segs) do
		seg.color = framenum
	end
end

-- Hooks

addHook("MobjSpawn", auroraSpawn, MT_AURORABOREALIS)
addHook("MobjThinker", auroraThinker, MT_AURORABOREALIS)

--
-- Do I have to comment everything?
--

