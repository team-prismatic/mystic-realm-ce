--[[
Mystic Realm CE Shrine Script

(C) 2020-2021 by Ashi

Level Header Variable format:
Lua.shrine = <emeraldcolor>, <emeraldstagemapnumber>
]]

local soffset = 220
local soffsetresettimer = 0
local currentmap = nil
local mysticticker = 0
local shrinedata

-- Text ease variables
local text_pos = {320, 340, 360, 380}

local text_animdur = 1*TICRATE
local text_maxslide = 160
local text_animtime = 0

local animtime_capped = nil
local animate_percentage = nil

addHook("MapLoad", function(p, v)
	if mrce.shrine_active --Has the shrine been activated, and are we still on the same map?
	and currentmap == gamemap then
		P_LinedefExecute(5000, nil, 5001)
		G_SetCustomExitVars(shrinedata[2])
	else
		mrce.shrine_active = false
		text_animtime = 0
		text_maxslide = 160
		text_pos = {320, 340, 360, 380}
	end
	shrinedata = GetNumberList(mapheaderinfo[gamemap].shrine)
end)

local emeraldindex = {EMERALD1, EMERALD2, EMERALD3, EMERALD4, EMERALD5, EMERALD6, EMERALD7}

local function shrine_pressed(line, mo, d) --This is where we actually activate the shrine.
	if mrce.shrine_active == false then --The player hasn't activated the shrine yet. let's do that now.
		currentmap = gamemap --Sets currentmap to the map number
		mrce.shrine_active = true
		mysticticker = 0
		S_StartSound(nil, sfx_kc5c)
		G_SetCustomExitVars(shrinedata[2])
		if shrinedata[3] then
			local emerald = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_FAKEEMERALD1)
			emerald.flags = $|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_NOCLIPTHING|MF_NOBLOCKMAP &~ MF_SPECIAL
			emerald.scale = 3*FRACUNIT/5
			emerald.state = S_FAKEEMERALD1 + (shrinedata[3] - 1)
			emerald.target = mo
			emerald.emeraldchase = true
			emeralds = $ | emeraldindex[min(max(shrinedata[3], 1), 7)]
			mo.player.shrineemerald = emeraldindex[min(max(shrinedata[3], 1), 7)]
		end
	end
	soffsetresettimer = 95 --This makes the shrine marker stay up on screen for a bit longer
end

addHook("LinedefExecute", shrine_pressed, "SHRINE_PRESSED") --When the player steps on the button activate the shrine.

addHook("PlayerSpawn", function(p)
	p.shrineemerald = $ or 0
	if not (p and p.valid and p.mo and p.mo.valid) then return end
	if mrce.shrine_active and p.shrineemerald and shrinedata[3] then
		local mo = p.mo
		local emerald = P_SpawnMobjFromMobj(mo, 0, 0, 0, MT_FAKEEMERALD1)
		emerald.flags = $|MF_NOCLIP|MF_NOCLIPHEIGHT|MF_NOCLIPTHING|MF_NOBLOCKMAP &~ MF_SPECIAL
		emerald.scale = 3*FRACUNIT/5
		emerald.state = S_FAKEEMERALD1 + (shrinedata[3] - 1)
		emerald.target = mo
		emerald.emeraldchase = true
	end
end)

addHook("PlayerThink", function(p) --This is the script that manages the positioning of the marker.
	if not p.bot and p.valid then
		if mrce.shrine_active and mysticticker < 120 then
			p.shrinemarker = 1 --Using an entirely separate variable to prevent the NewEmblemHUD from appearing
		else
			p.shrinemarker = 0
		end
		if p.shrinemarker or p.changeoffset then
			if soffset > 155 then --our y is greater than 155.
				soffset = $ - 5 --We gotta lower that. bring the marker up.
			end
			if soffset == 155 and soffsetresettimer then --our y is equal to 155
				soffsetresettimer = $ - 1 --let's count down the timer until it auto dismisses itself
			end
		else
			if soffset < 220 then --Our y is less than 220. Usually means the marker is on screen.
				soffset = $ + 10 --Let's increase it. Lowering the marker!
			end
		end
	end
end)

hud.add(function(d, p) --The shrine marker draw code
	if mapheaderinfo[gamemap].shrine == nil then return end
	if mrce.shrine_active then
		if gamemap == 129 then
			d.drawScaled(165*(FRACUNIT/2+FRACUNIT/3), (soffset*FRACUNIT), (FRACUNIT/2)+FRACUNIT/3, d.cachePatch("MSMRK1"), V_SNAPTOBOTTOM, d.getColormap(TC_DEFAULT, R_GetColorByName("Hyper1")))
		elseif gamemap == 133 then
			d.drawScaled(165*(FRACUNIT/2+FRACUNIT/3), (soffset*FRACUNIT), (FRACUNIT/2)+FRACUNIT/3, d.cachePatch("MSMRK1"), V_SNAPTOBOTTOM, d.getColormap(TC_DEFAULT, R_GetColorByName("Hyper2")))
		else
			d.drawScaled(165*(FRACUNIT/2+FRACUNIT/3), (soffset*FRACUNIT), (FRACUNIT/2)+FRACUNIT/3, d.cachePatch("MSMRK1"), V_SNAPTOBOTTOM, d.getColormap(TC_DEFAULT, shrinedata[1]))
		end
		if mysticticker == 240 then
			return
		else
			if mysticticker == 120 then
				text_maxslide = -380
				text_animtime = 0
				text_pos = {160, 160, 160, 160}
			end
			mysticticker = $ + 1
			text_animtime = text_animtime + 1

			animtime_capped = max(min(text_animtime, text_animdur), 0)
    		animate_percentage = FRACUNIT / text_animdur * animtime_capped

			d.drawString(ease.outquart(animate_percentage, text_pos[1], text_maxslide), 105, mapheaderinfo[gamemap].lvlttl.."'s Mystic Shrine", V_ALLOWLOWERCASE, "center")
			d.drawString(ease.outquart(animate_percentage, text_pos[2], text_maxslide), 115, "has been activated.", V_ALLOWLOWERCASE, "center")
			d.drawString(ease.outquart(animate_percentage, text_pos[3], text_maxslide), 135, "The next level has been changed to", V_ALLOWLOWERCASE, "center")
			d.drawString(ease.outquart(animate_percentage, text_pos[4], text_maxslide), 145, mapheaderinfo[shrinedata[2]].lvlttl, V_ALLOWLOWERCASE, "center")
		end
	else
		d.drawScaled(165*(FRACUNIT/2+FRACUNIT/3), (soffset*FRACUNIT), (FRACUNIT/2)+FRACUNIT/3, d.cachePatch("MSMRK2"), V_SNAPTOBOTTOM, d.getColormap(TC_DEFAULT, SKINCOLOR_GREY))
	end
end, "game")

hud.add(function(d, p)--Shrine marker multiplayer code
	if mapheaderinfo[gamemap].shrine == nil then return end
	if (multiplayer and netgame) then
		if mrce.shrine_active then
			if gamemap == 129 then
				d.drawScaled(175*(FRACUNIT/2+FRACUNIT/3), (1*FRACUNIT), (FRACUNIT/2), d.cachePatch("MSMRK1"), 0, d.getColormap(TC_DEFAULT, R_GetColorByName("Hyper1")))
			elseif gamemap == 133 then
				d.drawScaled(175*(FRACUNIT/2+FRACUNIT/3), (1*FRACUNIT), (FRACUNIT/2), d.cachePatch("MSMRK1"), 0, d.getColormap(TC_DEFAULT, R_GetColorByName("Hyper2")))
			else
				d.drawScaled(175*(FRACUNIT/2+FRACUNIT/3), (1*FRACUNIT), (FRACUNIT/2), d.cachePatch("MSMRK1"), 0, d.getColormap(TC_DEFAULT, shrinedata[1]))
			end
		else
			d.drawScaled(175*(FRACUNIT/2+FRACUNIT/3), (1*FRACUNIT), (FRACUNIT/2), d.cachePatch("MSMRK2"), 0, d.getColormap(TC_DEFAULT, SKINCOLOR_GREY))
		end
	end
end, "scores")

--wheee
local camerafreezmaA = 0
local camerafreezmaZ = 0

addHook("PlayerThink", function(p)
	if p.spectator then return end
	if not (p.mo and p.mo.valid) then return end
	if p.playerstate ~= PST_LIVE then return end
	if p.exiting and mrce and mrce.shrine_active then
		--print("fly you fool!")
		if p.exiting >80 then
			camerafreezmaA = camera.aiming
			camerafreezmaZ = camera.z
		else
			P_SetObjectMomZ(p.mo, min(p.mo.momz + (3*(FRACUNIT/2)), 32*FRACUNIT), false)
			--p.mo.spriteyoffset = $ + 6*FRACUNIT --doesn't interpolate so it looks choppy
			p.mo.flags = $|MF_NOCLIPHEIGHT
			if p.exiting == 75 then
				S_StartSound(nil, sfx_s1a8, p)
			end
			if p.mo.state ~= S_PLAY_EDGE then
				p.mo.state = S_PLAY_EDGE
			end
			p.mo.emexitspark = $ or 0
			p.mo.emexitspark = $1 + 28*FRACUNIT
			P_SpawnMobjFromMobj(p.mo, cos(FixedAngle(p.mo.emexitspark))*FixedMul(75, p.mo.scale), sin(FixedAngle(p.mo.emexitspark))*FixedMul(75, p.mo.scale), 0, MT_SUPERSPARK)
			P_SpawnMobjFromMobj(p.mo, cos(FixedAngle(p.mo.emexitspark))*FixedMul(-75, p.mo.scale), sin(FixedAngle(p.mo.emexitspark))*FixedMul(-75, p.mo.scale), 0, MT_SUPERSPARK)
			for i = 1, 4, 1 do
				local sprk = P_SpawnMobjFromMobj(p.mo, P_RandomRange(-50, 50)*FRACUNIT, P_RandomRange(-50, 50)*FRACUNIT, (P_RandomRange(-50, 50)*FRACUNIT) + (p.mo.height / 2), MT_BOXSPARKLE)
				--sprk.fuse
			end
			p.mrce.speen = $ + FixedAngle(15*FRACUNIT)
			p.shrineemerald = 0
			if camera.chase then
				camera.aiming = camerafreezmaA
				camera.z = camerafreezmaZ --prevent the camera from following the player
			end
		end
	end
end)

addHook("NetVars", function(net)
	currentmap=net($)
	mysticticker=net($)
	shrinedata = net($)
end)