--vote screen to exitlevel for agz4
local debug = 1
local agz4_activevote = false
local agz4_votetimer = -1
local agz4_activeplayers = 1
local agz4_votedyes = 0
local agz4_activeexit = 0
local agz4_exiting = -1
local agz4_votescast = 0
local cv = CV_RegisterVar({name = "mr_agz4votemode", defaultvalue = "majority", flags = CV_NETVAR, PossibleValue = {majority=0, twothirds=1, all=2}, func = 0})

local function resetMystExitParams()
    agz4_activevote = false
    agz4_votetimer = -1
    agz4_activeplayers = 1
    agz4_votedyes = 0
    agz4_votescast = 0
end

local function MRCE_prepMystExit(zone)
    resetMystExitParams()
    S_StartSound(nil, sfx_kc48)
    G_SetCustomExitVars(zone, 1)
    agz4_exiting = 2*TICRATE
end

addHook("MapLoad", function(map)
    resetMystExitParams()
end)

addHook("PlayerThink", function(p)
    if p.spectator then return end
    p.agz4_votedata = $ or {p, 0}
    p.agz4_votecast = $ or false
    if agz4_activevote or agz4_exiting >= 0 then
        p.pflags = $|PF_FULLSTASIS
    end
    if agz4_activevote and p.agz4_votecast == false and ((agz4_votetimer <= (29*TICRATE) + 10) or not multiplayer) then
        if p.mrce.jump == 20 then
            p.agz4_votecast = true
            agz4_votedyes = $ + 1
            agz4_votescast = $ + 1
            if not multiplayer then
                agz4_votetimer = 0
            end
            S_StartSound(nil, sfx_s1a1, p)
        elseif p.mrce.spin == 20 then
            p.agz4_votecast = true
            agz4_votescast = $ + 1
            S_StartSound(nil, sfx_kc43, p)
            if not multiplayer then
                resetMystExitParams()
                p.agz4_votecast = false
                p.pflags = $ & ~PF_FULLSTASIS
            end
        end
    end

    if debug then
        --print("votecast: " .. tostring(p.agz4_votecast))
        --print("votedata1: " .. tostring(p.agz4_votedata[1]))
        --print("votedata2: " .. tostring(p.agz4_votedata[2]))
    end
end)

addHook("ThinkFrame", function()
    if agz4_activevote then
        if multiplayer then
            agz4_votetimer = $ - 1 --vote ticker only runs in multiplayer
        end
        if agz4_votetimer == 0 or (agz4_votescast == agz4_activeplayers) then
            if multiplayer then
                if cv.value == 0 then
                    local div = max(agz4_activeplayers / 2, 1)
                    print(div)
                    if agz4_votedyes >= div then
                        MRCE_prepMystExit(agz4_activeexit)
                    else
                        resetMystExitParams()
                    end
                elseif cv.value == 1 then
                    local div = max((agz4_activeplayers / 3) * 2)
                    print(div)
                    if agz4_votedyes >= div then
                        MRCE_prepMystExit(agz4_activeexit)
                    else
                        resetMystExitParams()
                    end
                else
                    if agz4_votedyes == agz4_activeplayers then
                        MRCE_prepMystExit(agz4_activeexit)
                    else
                        resetMystExitParams()
                    end
                end
                for plr in players.iterate() do
                    plr.agz4_votecast = false
                end
            else
                MRCE_prepMystExit(agz4_activeexit)
            end
        end
    end
    if agz4_exiting > 0 then
        agz4_exiting = $ - 1
    elseif agz4_exiting == 0 then
        agz4_exiting = -1
        G_ExitLevel()
    end
    if debug then
        --print("active players: " .. agz4_activeplayers)
        --print("active exit: " .. agz4_activeexit)
        --print("active vote: " .. tostring(agz4_activevote))
    end
end)

local maps = {101, 104, 107, 110, 113, 116, 119}

addHook("LinedefExecute", function(l, mo)
	local p = mo.player
    if agz4_activevote then return end
    if p.bot then return end
    agz4_activeplayers = 0
    for plr in players.iterate() do
        if plr.spectator then continue end
        if plr.bot then continue end
        --MRCE_prepMystExit(plr, p, l.args[0])
        plr.agz4_votedata = {p, l.args[0]}
        agz4_activeplayers = $ + 1
    end
    agz4_activeexit = maps[l.args[0] + 1]
    agz4_activevote = true
    agz4_votetimer = 30*TICRATE
    if debug then
        --print("active players: " .. agz4_activeplayers)
        --print("active exit: " .. agz4_activeexit)
        --print("active vote: " .. tostring(agz4_activevote))
    end
end, "MYSTEXIT")

local strlen = string.len
local zones = {"Jade Coast Zone", "Tempest Valley Zone", "Verdant Forest Zone", "Flame Rift Zone", "Midnight Freeze Zone", "Sunken Plant Zone", "Aerial Garden Zone"}

hud.add(function(v, p)
    if not agz4_activevote then return end
    if p.playerstate ~= PST_LIVE then return end
    if p.spectator then return end

    local zonename = zones[p.agz4_votedata[2] + 1]
    local zlen = strlen(zonename) * FRACUNIT

    if multiplayer and p ~= p.agz4_votedata[1] then
        DrawMotdString(v, 80*FRACUNIT, 100*FRACUNIT, FRACUNIT, tostring(p.agz4_votedata[1]) .. " has called a vote to return to", "MRCEGFNT", 0, -1)
        --v.drawString(40, 100, tostring(p.agz4_votedata[1]) .. "\n has called a vote to return to")
        DrawMotdString(v, 130*FRACUNIT + zlen, 120*FRACUNIT, 3*FRACUNIT/2, zonename, "MRCEGFNT", 0, -1)
    else
        DrawMotdString(v, 140*FRACUNIT, 100*FRACUNIT, FRACUNIT, "Would you like to return to", "MRCEGFNT", 0, -1)
        --TBSlib.drawTextInt(v, "MRCEGFNT", 40, 100, "Would you like to return to", TOP_LEFT_FLAGS, nil, "left", -1)
        --v.drawString(40, 100, "Would you like to return to")
        DrawMotdString(v, 130*FRACUNIT + zlen, 120*FRACUNIT, 3*FRACUNIT/2, zonename .. "?", "MRCEGFNT", 0, -1)
    end

    if (emeralds & _G["EMERALD" .. tostring(p.agz4_votedata[2] + 1)]) then
        if (GlobalBanks_Array[0] & (1 << (p.agz4_votedata[2]))) then
            --draw super emerald
            v.drawScaled(250*FRACUNIT, 60*FRACUNIT, FRACUNIT/2, v.cachePatch("CEN3CHAOS" .. tostring(p.agz4_votedata[2] + 1)))
            DrawMotdString(v, 220*FRACUNIT, 70*FRACUNIT, 2*FRACUNIT/3, "You already have the ", "MRCEGFNT", 0, -1)
            DrawMotdString(v, 210*FRACUNIT, 75*FRACUNIT, 2*FRACUNIT/3, "Super Emerald of this zone", "MRCEGFNT", 0, -1)
            --print("You already have the Super Emerald of this zone")
        else
            v.drawScaled(250*FRACUNIT, 50*FRACUNIT, FRACUNIT/2, v.cachePatch("CEN2CHAOS" .. tostring(p.agz4_votedata[2] + 1)))
            DrawMotdString(v, 300*FRACUNIT, 70*FRACUNIT, 2*FRACUNIT/3, "You already have the Chaos Emerald of this zone,", "MRCEGFNT", 0, -1)
            DrawMotdString(v, 285*FRACUNIT, 80*FRACUNIT, 2*FRACUNIT/3, "but have not found the super emerald yet,", "MRCEGFNT", 0, -1)
            --print("You already have the Chaos Emerald of this zone")
            --v.drawString()
        end
    else
        v.drawScaled(250*FRACUNIT, 50*FRACUNIT, FRACUNIT/2, v.cachePatch("CEN3CHAOS9"))
        --v.drawString(150, 70, "You have not yet found\n an emerald in this zone")
        DrawMotdString(v, 310*FRACUNIT, 70*FRACUNIT, FRACUNIT/2, "You have not yet found an emerald in this zone", "MRCEGFNT", 0, -1)
    end
    if p.agz4_votedata[2] == 6 then
        if All7Emeralds(emeralds) then
            DrawMotdString(v, 120*FRACUNIT, 160*FRACUNIT, FRACUNIT/2, "You have found all 7 Chaos Emeralds", "MRCEGFNT", 0, -1)
        else
            DrawMotdString(v, 110*FRACUNIT, 160*FRACUNIT, FRACUNIT/2, "You have not yet found all 7 Chaos Emeralds", "MRCEGFNT", 0, -1)
        end
    end
    DrawMotdString(v, 20*FRACUNIT, 180*FRACUNIT, FRACUNIT/2, tostring(agz4_votetimer/TICRATE), "MRCEFNT", 0, -1)
end, "game")

addHook("NetVars", function(net)
	agz4_activevote = net($)  --net sync that shit
	agz4_votetimer = net($)
	agz4_activeplayers = net($)
	agz4_votedyes = net($)
	agz4_activeexit = net($)
	agz4_exiting = net($)
    agz4_votescast = net($)
end)