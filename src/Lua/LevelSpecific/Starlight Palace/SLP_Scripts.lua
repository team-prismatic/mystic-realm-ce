local function checkIfMapIsValid()
	if gamemap == 129 then
		return true
	else return false end
end

freeslot("sfx_sptic")


--===================POLYOBJECT CLOCK======================--


local function clockManager()
	if not checkIfMapIsValid() then return end
    local clockTime = mrce.globalServerTime
    local littleHand_PO
    local bigHand_PO
	local HourHand_PO

    for po in polyobjects.iterate() do --Find dem polyobjects!
        if po.id == 117 then littleHand_PO = po end
        if po.id == 115 then bigHand_PO = po end
		if po.id == 116 then HourHand_PO = po end
    end

    if not littleHand_PO or not bigHand_PO then
        print("Error: Could not find PolyObjects for clock hands.")
        return
    end



    -- Calculate target angles:
    local targetLittleHandAngle = (clockTime.sec * ANG1 * 6)
    local targetBigHandAngle = (clockTime.min * ANG1 * 6)
	local targetHourHandAngle = (clockTime.hour * ANG1 * 6)

    -- Calculate the difference in angles and rotate hands.
    local deltaLittleHand = (targetLittleHandAngle - littleHand_PO.angle)
    if deltaLittleHand ~= 0 then
        littleHand_PO:rotate(deltaLittleHand, 2)
		S_StartSound(sectors[2158], sfx_sptic)
    end

    local deltaBigHand = (targetBigHandAngle - bigHand_PO.angle)
    if deltaBigHand ~= 0 then
        bigHand_PO:rotate(deltaBigHand, 2)
    end


    local deltaHourHand = (targetHourHandAngle - HourHand_PO.angle)
    if deltaHourHand ~= 0 then
        HourHand_PO:rotate(deltaHourHand, 2)
    end




    --print("Sec: " .. clockTime.sec .. ", Sec Angle: " .. littleHand_PO.angle)
    --print("Min: " .. clockTime.min .. ", Min Angle: " .. bigHand_PO.angle)
	--print("Hour: " .. clockTime.hour .. ", Hour Angle: " .. HourHand_PO.angle)
end

addHook("ThinkFrame", clockManager)






--===================Wanted Poster area======================--
local function shouldPlayerBeTeleported(line, mo, sector)
--Determine which way the camera is facing
--If the player is looking away from the gargoyle, do not teleport the player
--The player cannot see the teleportation happen. Prevent at all costs
	if not mo and not mo.player then return end

	local ang = AngleFixed(mo.angle)

	--print(ang)

	if ang >= AngleFixed(ANGLE_90) and ang <= AngleFixed(ANGLE_270) then
		--print("Not teleporting player")
		return
	else
		--print("Teleporting player")
		P_LinedefExecute(171, mo, mo.sector)
	end




end
addHook("LinedefExecute", shouldPlayerBeTeleported, "GARG_TELE")






--===================POLYOBJECT SIN WAVE EXPERIMENT======================--


local time = 0
local theMainCharacter

-- Table of polyobject tags
local polyobject_tags = {
    133, -- Tag for the first polyobject
    134, -- Tag for the second polyobject
    135, -- Tag for the third polyobject
    136, -- Tag for the fourth polyobject
    137  -- Tag for the fifth polyobject
}



addHook("PlayerSpawn", function (p)
	theMainCharacter = p
end)


local polyMoTable = {}
local actualPolyMoTable = {}

local function findPolyobjects()

	if gamemap ~= 129 then return end

	polyMoTable = {} --Clear table at mapload
	actualPolyMoTable = {}
	time = 0


	for elm in ipairs(polyobject_tags) do
		for poTH in polyobjects.iterate() do
			local searchTag = poTH.sector.tag
			local cur = polyobject_tags[elm]

			--print("searchTag: " .. searchTag .. " Cur element: " .. cur)

			if cur == searchTag then
				--print("Found the polyobject!!!")

				for mt in mapthings.tagged(searchTag) do
					if mt.mobj then
						local mo = mt.mobj
						if mo.type == MT_POLYSPAWN then
							table.insert(polyMoTable,mt.mobj)
							table.insert(actualPolyMoTable,poTH)
							--print("Found a polyobject: " .. mt.mobj.info.doomednum)
						end
					end
				end

			end
		end
	end
end
addHook("MapLoad", findPolyobjects)

-- Define the properties for the sine wave movement


-- Function to move polyobjects in a sine wave pattern in 3D
local function MovePattern_sinWave()

	local amplitude = 2 -- Height of the sine wave in map units
	local frequency = 1 -- Speed of the sine wave

	--amplitude = $ * FRACUNIT
	--frequency = $ * FRACUNIT

    for poTH in ipairs(polyMoTable) do
		local po = polyMoTable[poTH]

		if po then
            -- Get the current position
            local currentPosX = po.x / FRACUNIT
            local currentPosY = po.y / FRACUNIT
            local currentPosZ = po.z

            -- Store base positions (if not already stored) to oscillate around
            local baseX = po.startX or currentPosX
            local baseY = po.startY or currentPosY
            local baseZ = po.startZ or currentPosZ

            -- Update the stored base positions
            po.startX = baseX
            po.startY = baseY
            po.startZ = baseZ

            --local nextPosZ = baseZ + sin(time * frequency) * amplitude
			--nextPosY = baseY + sin(time * frequency) * amplitude -- adjust frequency for varied movement

			if theMainCharacter.mo then
				po.destPointX = theMainCharacter.mo.x / FRACUNIT
				po.destPointY = theMainCharacter.mo.y / FRACUNIT
				print("Player position: X:" .. po.destPointX .. " Y:" .. po.destPointY)
			else
				po.destPointX = 9000
				po.destPointY = 2000
			end

			local deltaX = po.destPointX - currentPosX
			local deltaY = po.destPointY - currentPosY

			-- Calculate the squared distance first to check for negative values
			local distanceSquared = (deltaX * deltaX) + (deltaY * deltaY)

			-- Prevent domain error and ensure distance is positive
			local distance = distanceSquared > 0 and FixedSqrt(distanceSquared) / FRACUNIT or 0

			local movementSpeed = 1000

			-- Ensure we don't divide by zero
			if distance > 0 then
				--local moveDirX = FixedMul(FixedDiv(deltaX, distance), movementSpeed)
				--local moveDirY = FixedMul(FixedDiv(deltaY, distance), movementSpeed)

				local moveDirX = (deltaX / distance) * movementSpeed
				local moveDirY = (deltaY / distance) * movementSpeed


				--print("CurPosition = X: " .. currentPosX .. " Y: " .. currentPosY)
				--print("MoveDirection = X: " .. moveDirX .. " Y: " .. moveDirY)
				--print("Delta = X: " .. deltaX .. " Y: " .. deltaY)
				--print("Distance = " .. distance)

				actualPolyMoTable[poTH]:moveXY(moveDirX, moveDirY) -- Move polyobject
			else
				-- Distance is zero, so no movement is needed
				--print("Distance is zero, no movement required.")
			end

        end
    end
end

-- Hook to call sinewavePlatformMovement each frame
addHook("ThinkFrame", function()
	if not checkIfMapIsValid() then return end

	time = $ + 1
	--MovePattern_sinWave()

end)











--===================OBJECTS (MOVE LATER)======================--

freeslot(
"S_FOGCLOUD1",
"S_FOGCLOUD2",
"MT_FOGCLOUD"
)

mobjinfo[MT_FOGCLOUD] = {
	--$Name Fog Cloud
	--$Category MRCE Generic
	--$Sprite BARDA0
doomednum = 4521,
spawnstate = S_FOGCLOUD1,
spawnhealth = 1000,
radius = 20*FRACUNIT,
height = 30*FRACUNIT,
flags = MF_NOGRAVITY|MF_SPECIAL|MF_SCENERY
}

states[S_FOGCLOUD1] = {
    sprite = SPR_BARD,
    frame = FF_FULLBRIGHT | FF_TRANS90 | A,
    tics = 30,
	nextstate = S_FOGCLOUD2
}

states[S_FOGCLOUD2] = {
    sprite = SPR_BARD,
    frame = FF_FULLBRIGHT | FF_TRANS90 | A,
    tics = 30,
	nextstate = S_FOGCLOUD1
}



