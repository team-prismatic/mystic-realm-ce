freeslot("SPR_MRCE_SPZ_ORBPLANT", "MT_ORBPLANT", "S_ORBPLANT1", "S_ORBPLANT2")


mobjinfo[MT_ORBPLANT] = {
	--$Name Orb Plant
	--$Sprite SPR_MRCE_SPZ_ORBPLANT
	--$Category Sunken Plant
	doomednum = 5066,
	spawnstate = S_ORBPLANT1,
	reactiontime = 8,
	speed = 1,
	radius = 40*FRACUNIT,
	height = 40*FRACUNIT,
	flags = MF_SCENERY
}

states[S_ORBPLANT1] = {
        sprite = SPR_MRCE_SPZ_ORBPLANT,
        frame = A|FF_FULLBRIGHT,
        tics = 1,
        nextstate = S_ORBPLANT2
}
states[S_ORBPLANT2] = {
        sprite = SPR_MRCE_SPZ_ORBPLANT,
        frame = A|FF_FULLBRIGHT,
        tics = 1,
        nextstate = S_ORBPLANT2
}