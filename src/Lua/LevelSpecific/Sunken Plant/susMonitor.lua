freeslot("MT_SUS_BOX","S_SUS_BOX","SPR_TSUS")


mobjinfo[MT_SUS_BOX] = {
	--$Name Suspicious Monitor
	--$Sprite TSUSA0
	--$Category Monitors
	doomednum = 4008,
	spawnstate = S_SUS_BOX,
	reactiontime = 8,
	painstate = S_SUS_BOX,
	deathstate = S_BOX_POP1,
	deathsound = sfx_pop,
	speed = 1,
	radius = 18*FRACUNIT,
	height = 40*FRACUNIT,
	mass = 100,
	damage = S_RING_BOX,
	flags = MF_SOLID|MF_SHOOTABLE|MF_MONITOR
}

states[S_SUS_BOX] = {
        sprite = SPR_TSUS,
        frame = A,
        tics = 1,
        action = none,
        nextstate = S_BOX_FLICKER
}

local function triggerOnPop(mo, moInflict, moIParent, dmgType)
	if(mo.spawnpoint.tag ~= 0) then
		P_LinedefExecute(mo.spawnpoint.tag, moInflict, 0)
	end
end
addHook("MobjDeath",triggerOnPop,MT_SUS_BOX)