freeslot("MT_SPZ_FSPLAT", "SPR_MRCE_SPZ_FLOORDEBRIS1", "S_FLRSPLATSTATE1", "S_FLRSPLATSTATE2")

mobjinfo[MT_SPZ_FSPLAT] = {
	--$Name Floor Debris Splat
	--$Sprite FLSPA0
	--$Category Sunken Plant
	--$FlatSprite
	doomednum = 4009,
	spawnstate = S_FLRSPLATSTATE1,
	painstate = S_FLRSPLATSTATE1,
	deathstate = S_FLRSPLATSTATE1,
	speed = 1,
	radius = 1*FRACUNIT,
	height = 1*FRACUNIT,
	mass = 1,
}

function A_SPLATIFY(mo)
	if mo.valid then
		mo.renderflags = $|RF_NOSPLATBILLBOARD
	end
end

states[S_FLRSPLATSTATE1] = {
        sprite = SPR_MRCE_SPZ_FLOORDEBRIS1,
		frame = FF_FLOORSPRITE|FF_TRANS50|A,
		tics = 1,
		nextstate = S_FLRSPLATSTATE2,
}


states[S_FLRSPLATSTATE2] = {
        sprite = SPR_MRCE_SPZ_FLOORDEBRIS1,
		frame = FF_FLOORSPRITE|FF_TRANS50|A,
        action = A_SPLATIFY
}