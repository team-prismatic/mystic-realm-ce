freeslot("sfx_bstrt")




local function checkIfMapIsValid()
	if gamemap == 128 or gamemap == 117 or gamemap == 785 then
		return true
	else return false end
end



local function bioLumPlants(mo)
	if not gamemap == 127 then return end
	
	mo.renderflags = $ | RF_FULLBRIGHT

end
addHook("MobjSpawn", bioLumPlants, MT_HHZSHROOM)
addHook("MobjSpawn", bioLumPlants, MT_CORAL2)










--====================--


--======Electric Barriers======--

--Notes: Currently does not make zappy noise. Does not account for if its pegged yet, does not account for midtex scaling.

local electricBarrierTextures = {
	3740, --APPLMS1 (These are wrong and idk why theyre wrong but Im including them here anyway cause like... whos gonna use cave textures as a midtexture :^) )
	3741, --APPLMS2
	3742, --APPLMS3
	3743, --APPLMS4
	3718
}

local electricBarriers = {}
local function findElectricGates()
	for line in lines.iterate do
		if line and line.frontside and line.backside then
			for i, tx in ipairs(electricBarrierTextures) do
				if line.frontside.midtexture == tx or line.backside.midtexture == tx then
					--print("Adding a gate to the table...")
					table.insert(electricBarriers, line)
				end
			end
		end
	end
end

local function electricBarriersWalls(mo, line)
	if checkIfMapIsValid() then
		local isThisLineAGate = false
		for i, curLine in ipairs(electricBarriers) do
			if curLine == line then
				isThisLineAGate = true
			end
		end

		if isThisLineAGate == true then
			local gateFront = line.frontside
			local gateBack = line.backside

			if gateFront.offsety_mid ~= gateBack.offsety_mid then
				print("Gate Z is not equal")
				return
			end

			local gateWidth = 20
			local gateZ = mo.subsector.sector.floorheight + gateFront.offsety_mid


			local heightDifFromGate = abs(mo.z - gateZ)
			heightDifFromGate = $ / FRACUNIT

			--print("HeightFromGate: " .. heightDifFromGate)

			if heightDifFromGate <= gateWidth then
				P_DamageMobj(mo, nil, nil, 1, 3)
			end
		end
	end
end
addHook("MobjLineCollide", electricBarriersWalls, MT_PLAYER)


--===========MAP INIT==========--

local function mapInit(mapNum)
	findElectricGates()
end
addHook("MapLoad", mapInit)
