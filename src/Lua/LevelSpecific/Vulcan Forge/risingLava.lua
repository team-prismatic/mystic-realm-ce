freeslot("SPR_MRCE_FRZ_FIRENFLAME")

local isIndicatorActive = 1
local lavaHeight = 0
local lavaBurst = 0
local lavaSaferoomBurst = 0
local lavaSpeed = 5 --this one's the base speed the lava starts out at, and is used for how fast the lava should rise when unaffected by burst
local lavabst = 0  --this one's the total speed the lava should rise at, combining base speed and burst
local lastsfroom = 0


local function init_lava(mapNum)
	lastsfroom = 0
	lavaBurst = 0
	lavaSaferoomBurst = 0
	if mapheaderinfo[mapNum].lvlttl ~= "Vulkan Forge" then return
	else lavaSpeed = 5 end

end
addHook("MapChange", init_lava)




local function getLavaHeight()
    if isIndicatorActive == 1 and mapheaderinfo[gamemap].lvlttl == "Vulkan Forge" then
        lavaHeight = P_CeilingzAtPos(-25888 * FRACUNIT, -11152 * FRACUNIT, 0, 0)
		for p in players.iterate do
        	p.distanceFromLava = p.realmo.z - lavaHeight
		end
    end
end
addHook("ThinkFrame", getLavaHeight)


local function adjustLavaSpeed(line, mo, d)
    lavaSpeed = line.args[0]
	lavaBurst = line.args[1]
end
addHook("LinedefExecute", adjustLavaSpeed, "ADJUST_LAVA_SPD")


local function handleScreenFX(v, p)
	local distThresh = 400
	if p.distanceFromLava >= distThresh*FRACUNIT then return end


	local colorStrength = ((p.distanceFromLava/FRACUNIT) * 8) / distThresh
	colorStrength = abs(colorStrength - 8) / 2




	local color = 56

	--print(colorStrength)
	if colorStrength <= 0 or colorStrength > 9 then return end

	v.fadeScreen(color, colorStrength)

end

--here we lay out each saferoom in order, define a linedef tag to trigger when the lava reaches a certain height.
--doortag is the tag of a "Once" linedef executor that should shut the doors of a given saferoom,
--while floorheight is to be the absolute z value of the saferoom floor, measured as int not fixed_t (I'll apply the FRACUNIT in the function)
--burstspeedcatchup indicates how much of a boost in speed the lava will receive when the saferoom is triggered, in order to catch up. this value is added to lavaSpeed when calculating how much to raise the lava, while bursting
--burstspeedpost is what the lava speed should be set to after the lava has caught up to this saferoom
--burstleeway is how far below floorheight should the lava stop accelerating from burst + a portion of the lava's speed. increase this to make the burst stop sooner
local saferooms = { --safe room level, as indicated ingame. lvl one is last, while lvl 5 is first in the level
	[1] = {
		doortag = 255,
		floorheight = 9829,
		burstspeedcatchup = 50,
		burstspeedpost = 5,
		burstleeway = 0
	},
	[2] = {
		doortag = 252,
		floorheight = 7129,
		burstspeedcatchup = 30,
		burstspeedpost = 14,
		burstleeway = 2000
	},
	[3] = {
		doortag = 250,
		floorheight = 4367,
		burstspeedcatchup = 20,
		burstspeedpost = 10,
		burstleeway = 2000
	},
	[4] = {
		doortag = 245,
		floorheight = 3225,
		burstspeedcatchup = 20,
		burstspeedpost = 7,
		burstleeway = 1000
	},
	[5] = {
		doortag = 280,
		floorheight = 583,
		burstspeedcatchup = 0,
		burstspeedpost = 5,
		burstleeway = 900
	}
}

addHook("LinedefExecute", function (line, tmo, sec)
	if line.args[0] and (line.args[0] <= lastsfroom or lastsfroom == 0) and not line.args[1] then
		lastsfroom = line.args[0]
		if not multiplayer then
			--print("burstma")
			if lavaHeight <= (saferooms[lastsfroom].floorheight - ((lavabst*4) / 5))*FRACUNIT then
				--print("diffstart " .. saferooms[lastsfroom].floorheight - lavaHeight)
				if saferooms[lastsfroom].burstspeedcatchup ~= 0 then
					lavaSaferoomBurst = saferooms[lastsfroom].burstspeedcatchup
					--print("BOOST")
					--print(lavaSaferoomBurst)
				end
			end
		end
	end
	if line.args[1] and line.args[0] and line.args[0] == lastsfroom and lavaHeight >= saferooms[line.args[0]].floorheight*FRACUNIT + 32*FRACUNIT then
		if multiplayer then
			G_SetCustomExitVars(gamemap, 1)
			for p in players.iterate do
				p.emsfailstupidcheck = 150
				P_DoPlayerExit(p)
			end
		else
			for p in players.iterate do
				P_DamageMobj(p.mo, nil, nil, 1, DMG_SPACEDROWN)
				S_StartSoundAtVolume(mo, 399, 180, p)
				if p.screenflash == true then
					P_FlashPal(p, PAL_WHITE, 5)
				end
			end
		end
		lastsfroom = 0
	end
	--print(lastsfroom)
end, "VFGSAFEROOM")


local function controlLava()
    if mapheaderinfo[gamemap].lvlttl ~= "Vulkan Forge" then return end

	--[[if lavaBurst > 0 then
		bst = lavaBurst*FRACUNIT
	end

	if bst > 0 then
		bst = $ - FRACUNIT
	end]]

	if lastsfroom ~= 0 then
		--print("lastsfroom " .. lastsfroom)
		if lavaHeight > (saferooms[lastsfroom].floorheight*FRACUNIT) - (((5*(saferooms[lastsfroom].burstspeedcatchup*FRACUNIT)) / 4) + saferooms[lastsfroom].burstleeway*FRACUNIT) then
			--print("diff " .. ((saferooms[lastsfroom].floorheight*FRACUNIT) - lavaHeight) / FRACUNIT)
			if lavaSaferoomBurst ~= 0 then
				--print("lavaSaferoomBurst " .. lavaSaferoomBurst)
				lavaSaferoomBurst = 0
				lavaSpeed = saferooms[lastsfroom].burstspeedpost
			end
		end
	end

	lavabst = lavaSpeed + lavaSaferoomBurst

	local diff = 0
	local speedmult = 1
	if lavaSaferoomBurst and lastsfroom ~= 0 then
		diff = saferooms[lastsfroom].floorheight*FRACUNIT - lavaHeight
		if diff > 1024*FRACUNIT then
			speedmult = 12
		elseif diff > 512*FRACUNIT then
			speedmult = 8
		elseif diff > 256*FRACUNIT then
			speedmult = 4
		end
	end

	lavabst = $*speedmult

	--print("bst " .. lavabst)


	for sector in sectors.tagged(40) do
		--print(lavaBurst)
		sector.ceilingheight = $ + lavabst * (FRACUNIT / 10) --Raise that lava bitch
	end

	for i = 1, #saferooms do
		if lavaHeight >= (saferooms[i].floorheight*FRACUNIT) - 8*FRACUNIT then
			P_LinedefExecute(saferooms[i].doortag)
		end
	end


	--	sector.floorheight = $ + lavaSpeed * (FRACUNIT / 10)

end
addHook("ThinkFrame", controlLava)

local function drawIndicators(v, p)
    if mapheaderinfo[gamemap].lvlttl == "Vulkan Forge" then

		handleScreenFX(v,p)

		local contsprite = v.getSprite2Patch(p.realmo.skin, SPR2_LIFE, false, A)
		local flameSprite = v.getSpritePatch(SPR_MRCE_FRZ_FIRENFLAME, A)
        local hudMaxHeight = 150
        local worldMaxHeight = 11000 * FRACUNIT


        -- Scale player height to the HUD range
        local n_pHeight = FixedDiv(p.mo.z, worldMaxHeight) -- Normalize to 0-1
        n_pHeight = FixedMul(n_pHeight, hudMaxHeight) -- Scale to HUD range

        -- Scale lava height to the HUD range
        local n_lavaY = FixedDiv(lavaHeight, worldMaxHeight) -- Normalize to 0-1
        n_lavaY = FixedMul(n_lavaY, hudMaxHeight) -- Scale to HUD range

        -- Draw the lava and player height bars
		v.drawFill(291, 178 - n_lavaY, 17, n_lavaY, 40|V_SNAPTORIGHT|V_HUDTRANS)
        v.drawScaled(300*FRACUNIT, (179 - n_lavaY)*FRACUNIT, FRACUNIT/4, flameSprite, V_SNAPTORIGHT) -- Lava Height Bar
        v.drawScaled(300*FRACUNIT, (175 - n_pHeight)*FRACUNIT, FRACUNIT, contsprite, V_SNAPTORIGHT, v.getColormap(p.realmo.skin, p.skincolor, p.realmo.translation)) -- Player Height Bar

        -- Optionally draw the distance text
        local text = "Distance: " .. string.format("%.0f", FixedTrunc(p.distanceFromLava))
        --v.drawString(325 * FRACUNIT, 40 * FRACUNIT, text, V_20TRANS, "fixed-right")
    end
end
hud.add(drawIndicators, "game")

--@speed2411, please remember to netsync any local variables that involve actual gameplay
addHook("NetVars", function(net)
	lavaHeight = net($)  --net sync that shit
	lavaBurst = net($)
	lavaSpeed = net($)
	lastsfroom = net($)
	lavaSaferoomBurst = net($)
	lavabst = net($)
end)

