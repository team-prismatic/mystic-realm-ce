local defaultFriction = 29*FRACUNIT/32

local sinkingSpeedScalar = 5
local MudfrictionScalar = 57




local function mud(mo)
	if not mo.player then return end

	if gamemap == 123 then
		local p = mo.player

		if P_PlayerTouchingSectorSpecialFlag(p, SSF_RETURNFLAG) then
			
			local mudDist = mo.watertop - mo.z
			mudDist = $ / FRACUNIT
			
			if mudDist >= 1200 then mudDist = 1200 end
			
			mo.friction = FRACUNIT - (mudDist*50)
			--print("Friction set to: " .. mudDist)
		else mo.friction = defaultFriction end
	end
end
addHook("MobjThinker", mud, MT_PLAYER)