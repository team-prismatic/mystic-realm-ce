--Credit Snu for the custom weather script

--== Pseudorandom Numbers Library v1.0 by LJ Sonic ==--
local t, x, y, z, w = 0, 6278, 975, 39207, 45678

rawset(_G, "N_Random", function()
    t = x ^^ (x << 11)
    x, y, z, w = y, z, w, w ^^ (w >> 19) ^^ t ^^ (t >> 8)
    return w
end)

rawset(_G, "N_RandomKey", function(n) return abs(N_Random() % n) end)
rawset(_G, "N_RandomRange", function(a, b) return a + abs(N_Random() % (b - a + 1)) end)
rawset(_G, "N_RandomDatas", function() return d end)

--== Custom Weather (Fire) ==--
-- Command for performance
local cv_cwdensity = CV_RegisterVar({
  name = "cw_density",
  defaultvalue = 1,
  PossibleValue = {MIN = 0, MAX = 5},
})

-- Freeslotting & States
freeslot("S_MRCE_CWEMBER1", "MT_MRCE_CWEMBER1", "SPR_MRCE_CWEMBER", "S_MRCE_CWICECRYSTAL1", "SPR_MRCE_CWICECRYSTAL")

states[S_MRCE_CWEMBER1] = {SPR_MRCE_CWEMBER, A|FF_FULLBRIGHT, -1, nil, 0, 0, S_MRCE_CWEMBER1}

states[S_MRCE_CWICECRYSTAL1] = {
	sprite = SPR_MRCE_CWICECRYSTAL,
	frame = A,
	tics = -1,
	nextstate = S_MRCE_CWICECRYSTAL1
}
mobjinfo[MT_MRCE_CWEMBER1] = {
	doomednum = -1,
    spawnstate = S_MRCE_CWEMBER1,
    spawnhealth = 1,
    speed = 1*FRACUNIT,
    radius = 1*FRACUNIT,
    height = 1*FRACUNIT,
    flags = MF_NOBLOCKMAP|MF_NOGRAVITY|MF_NOCLIP|MF_SCENERY|MF_NOCLIPHEIGHT
}

local CWTable = {
	[1] = {
		state = S_MRCE_CWEMBER1,
		scale = FRACUNIT,
		blend = AST_ADD,
		frames = 0,
		color = 0,
		density = 5
	},
	[2] = {
		state = S_MRCE_CWICECRYSTAL1,
		scale = FRACUNIT/22,
		blend = 0,
		frames = 4,
		color = 0,
		density = 3
	}
}

-- Actual ThinkFrame for this
addHook("ThinkFrame", function()
	if not mapheaderinfo[gamemap].mrce_customweather then return end
	local wthrtype = tonumber(mapheaderinfo[gamemap].mrce_customweather)
	local wthr = CWTable[wthrtype]

	for p in players.iterate do
		if not (p and p.realmo) then continue end

		if p == displayplayer then
			local zdiff = displayplayer.realmo.z - displayplayer.realmo.floorz
			for i = 0, (wthr.density*cv_cwdensity.value)-1 do
				local fire = P_SpawnMobj(displayplayer.realmo.x+N_RandomRange(-1024, 1024)*FRACUNIT + displayplayer.rmomx, displayplayer.realmo.y+N_RandomRange(-1024, 1024)*FRACUNIT + displayplayer.rmomy, displayplayer.realmo.floorz+(2*FRACUNIT) + (N_RandomRange(3, 8)*FRACUNIT) + ((5*zdiff)/6) + displayplayer.realmo.momz, MT_MRCE_CWEMBER1)
				fire.state = wthr.state
				fire.momx = (N_RandomRange(-2, 2)*FRACUNIT)
				fire.momy = (N_RandomRange(-2, 2)*FRACUNIT)
				fire.scale = N_RandomRange(1, 3)*wthr.scale
				fire.frame = $|N_RandomRange(0, wthr.frames)
				--fire.scale = 8*FRACUNIT
				fire.blendmode = wthr.blend
				fire.fuse = N_RandomRange(3, 6)*TICRATE
				P_SetObjectMomZ(fire, 2*FRACUNIT)
			end
		end
	end
end)

-- MobjThinker to make sure they despawn to reduce lag
addHook("MobjThinker", function(fire)
	if not mapheaderinfo[gamemap].mrce_customweather then return end

	for p in players.iterate do
		if not (p and p.realmo) then continue end

		if p == displayplayer then
			if R_PointToDist2(displayplayer.realmo.x, displayplayer.mo.y, fire.x, fire.y) > 1024*FRACUNIT then
				P_RemoveMobj(fire) -- dont eat up too much performance, y'hear?!
			end
		end
	end
end, MT_MRCE_CWEMBER1)