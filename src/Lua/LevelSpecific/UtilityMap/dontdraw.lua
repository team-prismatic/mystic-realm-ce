addHook("PlayerThink", function(player)
	if gamemap > 96 and gamemap < 101 then
		player.momx = 0
		player.momy = 0
		player.momz = 0
		player.mo.flags2 = $|MF2_DONTDRAW
		player.powers[pw_nocontrol] = 2
	end
end)

local special_map = true

local maps = {
	[97] = true,
	[98] = true,
	[99] = true,
	[100] = true,
}

addHook("ThinkFrame", function()
	if maps[gamemap] then
		hud.disable("lives")
		hud.disable("score")
		hud.disable("rings")
		hud.disable("time")

		special_map = true
	elseif special_map then
		hud.enable("lives")
		hud.enable("score")
		hud.enable("rings")
		hud.enable("time")

		special_map = nil
	end
end)

addHook("MapLoad", function(map)
	if gamemap == 99 and CV_FindVar("touch_inputs") then
		G_SetCustomExitVars(101,1)
		G_ExitLevel()
	end


end)
	