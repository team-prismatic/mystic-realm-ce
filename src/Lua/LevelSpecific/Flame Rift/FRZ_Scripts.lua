






--Emblem secret - Blows up tunnel with TNT, emblem sinks into floorheight

local debugPrint = 0 --when testing scripts in a development state, set this value

local function lowerEmblem(line, mo, sector)

	local emblemObject




	for mob in mobjs.iterate() do
		if mob.type == MT_EMBLEM then
			if mob.spawnpoint.tag == 359 then
				emblemObject = mob.spawnpoint
				--print("Found Emblem!")
				return
			end
		end
	end

	if not emblemObject then return end
	emblemObject.scale = 10
	emblemObject.z = $ - 50*FRACUNIT

end
addHook("LinedefExecute", lowerEmblem, "HIDE_FRZ2_EMBLEM")





--TROLL PLATFORM

local troll1Primed
local troll1Running
local troll1Done

local function raisePlatform(s,maxHeight, mo)
	if s.floorheight < maxHeight then
		s.floorheight = $ + 10*FRACUNIT
	else
		S_StartSound(mo, 237)
		troll1Done = true
	end
end





local function eggTroll_1(line, mo, sector)
	--Check if the conditions are aligned to run
	if troll1Done == true then return end
	if not mo.player then return end

	if troll1Primed == false then
		if mo.z == mo.floorz then
			if debugPrint then
				print("Troll primed")
			end
			troll1Primed = true
			return
		else
			return
		end
	end

	--Get the two vertexes of the line, figure out what the possible highest Y value is, and store that
	--Once stored, if player is jumping and increases their Y pos higher than the stored Y value, then thats when the troll will be activated. This does not account for if the level is ever rotated but watcha gonna do
	--Perhaps we should clamp the lowest and highest X values of the vertexes as well



--[[
	local topmostLine = nil

	local i = 0
	do
		if topmostLine == nil then topmostLine = sector.lines[i] end

	while(i < sector.lines[].tonumber) --Counts the amount of lines]]








	if mo.z <= (sector.floorheight + 20) and troll1Running == false then return
	else
		troll1Running = true
	end
	--print ("Activating Troll 1")

	local newHeight = line.frontsector.floorheight

	for s in sectors.tagged(line.tag) do
		if s.floorheight == newHeight then return end --No need to adjust height if its already the same
		--print("Found primed sector!")
		raisePlatform(s, newHeight, mo)

	end
end
addHook("LinedefExecute",eggTroll_1,"EGG_TROLL_1")



local trainRoomEpicenter = {
    x = -18920*FRACUNIT,
    y = -7640*FRACUNIT,
    z = 500*FRACUNIT
}



local function trainRoom(line, mo, sector)
	if gamemap ~= 111 then return end
	if not mo or not mo.valid then return end
	if not mo.player then return end
	
	local player = mo.player
	local interiaEffect

	P_StartQuake(3*FRACUNIT, TICRATE, trainRoomEpicenter, 10000*FRACUNIT)
	
	if not P_IsObjectOnGround(player.mo) then
		local xF = player.mo.x
		local yF = player.mo.y
		local zF = player.mo.z

		if not interiaEffect then interiaEffect = FRACUNIT/20 end
		

		if P_CheckPosition(player.mo, xF, yF, zF) then
			--P_MoveOrigin(mo, xF, yF, zF)
			P_Thrust(mo, ANGLE_270, interiaEffect)
		end


		if not player.landedLastFrame then
			local xF = player.mo.x
			local yF = player.mo.y
			local zF = player.mo.z

			yF = yF + 1 * FRACUNIT

			if P_CheckPosition(player.mo, xF, yF, zF) then
				P_Thrust(mo, ANGLE_270, interiaEffect)
			end
		end
	else
		--interiaEffect = 1*FRACUNIT
	end
	player.landedLastFrame = P_IsObjectOnGround(player.mo)
end
addHook("LinedefExecute",trainRoom,"TRAIN_ROOM")






-----Set effects back to normal when maploads-----
addHook("MapLoad", function(mapNum)
	if not mapNum == 111 then return
	else
		--print("FRZ2 loaded!")
		troll1Done = false
		troll1Primed = false
		troll1Running = false
	end
end)