--Egg Reverie Zone Super Sonic by Frostiikin. Feel free to use & edit this anywhere as long as you give the proper credits.
--Bug Fixes, comments, and code cleanup courtesy of Dabir, Thanks for cleaning up my shoddy code lmfao
sfxinfo[sfx_spdash].caption = "Super Dash"
local INT16TO50 = (INT16_MAX/2)/1000
addHook("PlayerThink",  function(p)
    -- Only Sonic can be Super Sonic
    if not (p.mo and p.mo.valid and (p.mo.skin == "sonic" or p.mo.skin == "supersonic" or p.mo.skin == "metalsonic" or p.mo.skin == "tails" or p.mo.skin == "knuckles")) then
        return
    end

    -- Only Super people can be Super Sonic
    if not p.powers[pw_super] then
        -- Non-super people can't be super-flying
        p.eggsuperflying = false
		p.thrustfactor = skins[p.mo.skin].thrustfactor
		p.mo.flags = $ & ~MF_NOGRAVITY
        return
    end

    -- Not not Sonic, not not Super, must be Super Sonic!

    -- Can't go flying if you're in a minecart or a zoom tube
    if p.mo.state == S_PLAY_RIDE or p.powers[pw_carry] == CR_ZOOMTUBE then
        return
    end

    -- Definitely flying, so initialise/confirm flying-related variables
    p.eggsuperflying = $ or false  -- Boolean for whether Super Sonic is flying
    p.eggflystart = $ or false -- Boolean for whether Super Sonic just started flying
    p.eggflyboost = $ or false  -- Boolean for whether Super Sonic can boost this tic
    p.eggboostcost = 5  -- How many rings it costs to boost
    p.eggmaxvert = 22*FRACUNIT  -- Maximum vertical flying speed in either direction
	p.eggboostcooldown = $ or 0
	p.mrceflyswitch = $ or 0
	p.mrceflyswitch = $ > 0 and $ - 1 or 0
	p.mrceflightzboost = 0
--  p.eggoldability = $ or CA_NONE -- Storage for non-super jump ability

    -- If you still have a jump ability, take it away and remember what it was
--    if p.charability ~= CA_NONE
--        p.charability, p.eggoldability = CA_NONE, $1
--    end

    -- You stop flying when you touch the ground
    if P_IsObjectOnGround(p.mo) then
        p.eggsuperflying = false
		p.thrustfactor = skins[p.mo.skin].thrustfactor
--    else
    -- Prevent rolling when holding spin and touching ground while flying.
--        p.pflags = $ | PF_THOKKED
    end

	p.viewrollangle = ease.linear(FRACUNIT/12, p.viewrollangle, p.mo.roll)

    -- If you're not flying right now, we're done here.
    if not p.eggsuperflying then
		p.mo.rollangle = 0
		p.mo.roll = 0
		p.mo.flags = $ & ~MF_NOGRAVITY
        return
    end

	-- Various rotations
	local dist = R_PointToDist2(0, 0, p.mo.momx, p.mo.momy)
	p.mo.roll = ease.linear(FRACUNIT/12, p.mo.roll, max(min((p.cmd.sidemove or 1)/3, 10), -10)*ANG1 + (camera.angle - p.mo.angle)/6)
	local pitch = R_PointToAngle2(0, 0, dist, p.mo.momz)
	MRCElibs.cameraSpriteRot(p.mo, p.mo.angle, InvAngle(p.mo.roll), pitch)

    -- Gravity doesn't apply when you're flying
	p.mo.flags = $ | MF_NOGRAVITY

	if p.mo.flags & MF_NOGRAVITY then
		p.thrustfactor = 2*skins[p.mo.skin].thrustfactor
	else
		p.thrustfactor = skins[p.mo.skin].thrustfactor
	end

    -- You can't fall when you're flying
    if p.mo.state == S_PLAY_FALL
	and p.mo.skin == "sonic" or p.mo.skin == "supersonic" or p.mo.skin == "metalsonic" then
        p.mo.state = S_PLAY_FLOAT
    end

	if p.speed >= FixedMul(p.runspeed, p.mo.scale) then
		if (p.mo.skin == "sonic" or p.mo.skin == "supersonic" or p.mo.skin == "metalsonic") then
			if (p.dashmode > 3*TICRATE) or (p.mo.gpe_hasrun2 and p.eggboostcooldown and p.mrce.realspeed >= 60*FU) then
				p.mo.state = S_PLAY_DASH
            else
				p.mo.state = S_PLAY_FLOAT_RUN
			end
		elseif p.charability == CA_GLIDEANDCLIMB then
			p.mo.state = S_PLAY_GLIDE
		else
			p.mo.state = S_PLAY_RUN
		end
	else
		if p.mo.skin == "sonic" or p.mo.skin == "supersonic" or p.mo.skin == "metalsonic"then
			p.mo.state = S_PLAY_FLOAT
		else
			p.mo.state = S_PLAY_FALL
		end
	end

    -- You're not jumping or spinning when you're flying
	p.pflags = $ & ~PF_JUMPED & ~PF_SPINNING

    -- You have to let go of jump after you start floating before you can ascend
    if not (p.cmd.buttons & BT_SPIN) then
        p.eggflystart = false
    end

	--Prevent stored momentum when initiating hover
	if p.eggflystart == true and p.eggflymode == 1 then
		P_SetObjectMomZ(p.mo, 0*FRACUNIT)
	end

    -- You have to let go of either jump or spin after boosting before you can boost again
    if not (p.cmd.buttons & BT_SPIN) or not (p.cmd.buttons & BT_JUMP) then
        p.eggflyboost = true
    end

    -- If you're holding jump and not spin, accelerate up
    if (p.cmd.buttons & BT_JUMP) and not (p.cmd.buttons & BT_SPIN) and not p.eggflystart or (p.cmd.buttons & BT_JUMP) and not (p.cmd.buttons & BT_SPIN) and p.eggflymode == 2 then
        P_SetObjectMomZ(p.mo, (5/3)*FRACUNIT, true) -- Accelerate up
    end

    -- If you're holding spin and not jump, accelerate down
    if (p.cmd.buttons & BT_SPIN) and not (p.cmd.buttons & BT_JUMP) then
        P_SetObjectMomZ(p.mo, -(5/3)*FRACUNIT, true)
    end

    -- Cap the max flying speed at both ends
    p.mo.momz = max(min(p.eggmaxvert, p.mo.momz), 0-p.eggmaxvert)

	if MRCE_isHyper(p) then
		p.eggboostcost = 2
	else
		p.eggboostcost = 5
	end

	p.eggboostcooldown = $ > 0 and $ - 1 or 0

    -- If you're holding both, and you're allowed to boost, and you can pay for it, boost
    if (p.cmd.buttons & BT_JUMP) and (p.cmd.buttons & BT_SPIN)
    and p.eggflyboost and p.rings >= p.eggboostcost and p.eggboostcooldown == 0 then
		P_GivePlayerRings(p, -p.eggboostcost)
        P_InstaThrust(p.mo, p.mo.angle, 150*FRACUNIT)
		if p.screenflash == true then
			P_FlashPal(p, PAL_WHITE, 10)
		end
		P_NukeEnemies(p.mo, p.mo, 384*FRACUNIT)
        S_StartSound(p.mo, sfx_spdash)
		p.eggboostcooldown = 2 * TICRATE
        p.eggflyboost = false -- Can't boost if you've just boosted
    end

    -- If you're holding neither, come to a halt
    if not (p.cmd.buttons & BT_JUMP) and not (p.cmd.buttons & BT_SPIN) then
        p.mo.momz = abs($)>FRACUNIT and $*9/10 or 0
    end
end)

addHook("JumpSpinSpecial", function(p)
	if p.mo and p.mo.valid then
		if not p.powers[pw_super]
		or (mapheaderinfo[gamemap].lvlttl ~= "Dimension Warp" and (p.mrce.flycheat == false or p.mrce.flycheat == nil)) then
			return
		end
		if p.mo.skin == "supersonic" or p.mo.skin == "metalsonic" or p.mo.skin == "tails" or p.mo.skin == "knuckles" and not p.eggsuperflying then
			p.eggsuperflying = true
			p.eggflystart = true
			if p.mo.skin == "supersonic" or p.mo.skin == "metalsonic" then
				p.mo.state = S_PLAY_FLOAT
			else
				p.mo.state = S_PLAY_RUN
			end
			p.pflags = $1 | PF_THOKKED
			return true
		end
		return false
	end
end)
--flyswitching. def not busted as all hell as hyper, no sir. Also disables super cancel
addHook("PlayerThink", function(p)
	if p.powers[pw_super] then
		--print("shield")
		if MRCE_isHyper(p) or p.mrce.cosmichysteria or p.mrce.flycheat then
			--print("hyper")
			if p.mrce.shield == 1 and p.powers[pw_super] > 38 then
				--print("shield")
				if p.mo.skin == "sonic" and not p.eggsuperflying and not p.mrceflyswitch and not P_IsObjectOnGround(p.mo) then
					p.eggsuperflying = true
					p.eggflystart = true
					p.mo.state = S_PLAY_FLOAT
					p.pflags = $|PF_THOKKED
					p.mrceflyswitch = 5
					if p.doubledash then p.doubledash = false end
					p.hyperbonus = 0
				elseif p.mo.skin == "sonic" and not p.mrceflyswitch and not P_IsObjectOnGround(p.mo) then
					p.eggsuperflying = false
					p.eggflystart = false
					p.mo.state = S_PLAY_FALL
					p.pflags = $|PF_JUMPED & ~(PF_THOKKED|PF_SHIELDABILITY)
					p.thrustfactor = skins[p.mo.skin].thrustfactor
					p.mrceflyswitch = 5
					p.hyperbonus = 2
					if not p.mrce.c1 then
						if p.mrce.jump > 10 and p.mrce.jump > p.mrce.spin + 10 then
							if p.mrce.c2 then
								P_SetObjectMomZ(p.mo, 80*FRACUNIT)
							else
								P_SetObjectMomZ(p.mo, 40*FRACUNIT)
							end
							p.mo.state = S_PLAY_ROLL
							P_NukeEnemies(p.mo, p.mo, RING_DIST)
							S_StartSound(pmo, sfx_s3kb6)
						elseif p.mrce.spin > 10 and p.mrce.spin > p.mrce.jump + 10 then
							if p.mrce.c2 then
								P_SetObjectMomZ(p.mo, -150*FRACUNIT)
							else
								P_SetObjectMomZ(p.mo, -50*FRACUNIT)
							end
							p.mrce.floatpause = TICRATE
							p.mo.state = S_PLAY_ROLL
							P_NukeEnemies(p.mo, p.mo, RING_DIST)
							S_StartSound(pmo, sfx_s3kb6)
						end
					end
				end
			end
		end
	end
end)