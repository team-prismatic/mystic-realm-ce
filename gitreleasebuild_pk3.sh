#!/bin/zsh

#	  Compile PK3 for Linux/MacOS
#	  By Ashi
#	  make sure you have 7z and zsh installed on your system

#	  Skydusk: This is merely version for gitlab.
#	  Only actual difference is output folder and version being specified by cmd arguments

NAME="SRML_MysticRealmCE"
VERSION="$2"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

IFS=. VER=(${VERSION##*-})

if ([ -n "$VER[0]" ] -a [[ $VER[0] =~ ^-?[0-9]+$ ]]); then
    MAJOR_VERSION="${VER[0]}"
    SUB_VERSION=0

    if ([-n "$VER[1]"] -a [[ $VER[1] =~ ^-?[0-9]+$ ]]); then
        SUB_VERSION="${VER[1]}"
    fi;

    sed -i 's/versionstring =.*/versionstring = \"'$VERSION'\",/' $SCRIPT_DIR'/src/Lua/Core/globals.lua'

    sed -i 's/version =.*/versionstring = \"'$MAJOR_VERSION'\",/' $SCRIPT_DIR'/src/Lua/Core/globals.lua'

    sed -i 's/subversion =.*/versionstring = \"'$SUB_VERSION'\",/' $SCRIPT_DIR'/src/Lua/Core/globals.lua'


    sed -i 's/CustomVersion =.*/CustomVersion = MRCE '$VERSION'/' $SCRIPT_DIR'/src/SOC/A - Core/MAINCFG'

    printf '=====================================================================\n' >> $SCRIPT_DIR/pk3_output.log
    printf 'PK3 Compile Script for Linux/MacOS\nCreated by Ashi\n\nCurrent Directory: '$SCRIPT_DIR'\n\nCompiling' $NAME'-'$VERSION'.pk3' >> $SCRIPT_DIR/pk3_output.log

    7z a $NAME-$VERSION.pk3 ./src/* -x!.bak -x!.dbs -x!.DS_Store -x!.backup1 -x!.backup2 -x!.backup3 -tzip >> $SCRIPT_DIR/pk3_output.log -o/$1
    printf '=====================================================================\n' >> $SCRIPT_DIR/pk3_output.log;
else
    if ([ -n "$VERSION" ]); then
        sed -i 's/versionstring =.*/versionstring = \"'$VERSION'\",/' $SCRIPT_DIR'/src/Lua/Core/globals.lua'

        sed -i 's/CustomVersion =.*/CustomVersion = MRCE '$VERSION'/' $SCRIPT_DIR'/src/SOC/A - Core/MAINCFG'

        printf '=====================================================================\n' >> $SCRIPT_DIR/pk3_output.log
        printf 'PK3 Compile Script for Linux/MacOS\nCreated by Ashi\n\nCurrent Directory: '$SCRIPT_DIR'\n\nCompiling' $NAME'-'$VERSION'.pk3' >> $SCRIPT_DIR/pk3_output.log

        7z a $NAME-$VERSION.pk3 ./src/* -x!.bak -x!.dbs -x!.DS_Store -x!.backup1 -x!.backup2 -x!.backup3 -tzip >> $SCRIPT_DIR/pk3_output.log -o/$1
        printf '=====================================================================\n' >> $SCRIPT_DIR/pk3_output.log;
    fi;
fi;